﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using gSpy3_Common;
using gSpy3_Common.API.Cyanide;
using gSpy3_Common.Database;
using gSpy3_Common.Database.AccessInterface;
using gSpy3_Common.Database.Format;
using gSpy3_Common.Helpers;
using gSpy3_DiscordBot.Modules;
using gSpy3_DiscordBot.Modules.channel;
using gSpy3_DiscordBot.Modules.competition;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace gSpy3_DiscordBot
{
    public class GoblinSpyDiscordBot : gSpy3_Common.Module.ModuleBase
    {
        DatabaseManager dbManager;
        System.Timers.Timer heartbeatTimer;
        protected enum Work {  }
        Log log = new Log();
        CyanideBBAPI cyanide;
        private DiscordSocketClient discord;
        private string discordToken;
        private CommandHandler discordHandler;
        private CommandService discordCommandService;

        QueryResult _channelsAtStart = null;

        public GoblinSpyDiscordBot(DatabaseManager dbm, string bb2APIKey, string aDiscordToken) : base(ModuleEnum.gSpy3_DiscordBot, dbm)
        {
            discordToken = aDiscordToken;
            cyanide = new CyanideBBAPI(bb2APIKey);
            dbManager = dbm;
            heartbeatTimer = new System.Timers.Timer();
            heartbeatTimer.Interval = 1;
            heartbeatTimer.Elapsed += HeartbeatTimer_Elapsed;
            heartbeatTimer.Start();
        }

        private void HeartbeatTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            heartbeatTimer.Enabled = false;
            heartbeatTimer.Interval = 1000 * 60;
            base.Tick();
            heartbeatTimer.Enabled = true;
        }

        protected override int GetMaxFuturePlanning()
        {
            return 1;
        }


        public void Run()
        {
            _channelsAtStart = dbManager.BackendDB.Query("select * from botsettings as bs inner join botchannels as bc on  bc.channel_id=bs.channel_id where setting_key=\"status alert\" and setting_value=@status", new Dictionary<string, string>() { { "@status", "\"status\"" } });


            while (true)
            {
                try
                {
                    while (discord == null || discord.ConnectionState != Discord.ConnectionState.Connected)
                    {
                        var waiter = new AutoResetEvent(false);
                        var task = new Task(async () =>
                        {


                            log.Info("Connecting to discord");
                            discord = new DiscordSocketClient();
                            discord.Log += Discord_Log;
                            discord.MessageReceived += Discord_MessageReceived;
                            await discord.LoginAsync(Discord.TokenType.Bot, discordToken, true);
                            await discord.StartAsync();
                            // discordCommandService = new CommandService();
                            //discordHandler = new CommandHandler(discord, discordCommandService);
                            //await discordHandler.InstallCommandsAsync();
                            waiter.Set();
                        });
                        task.Start();
                        waiter.WaitOne(7000);
                        if (discord.ConnectionState == ConnectionState.Connected)
                        {
                            discord.SetGameAsync("Type !gs for help");
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(7000);
                        }
                    }

#if DEBUG
                    TriggerMatchPlayed(new BotWork()
                    {
                        from = "2020-01-01 10:00:00",
                        idcomp = 4
                    });
#endif

                    var numRemoved = base.RemoveOldWork();
                    if (numRemoved > 0)
                    {
                        log.Info("Removed " + numRemoved + " old work items");
                    }

                    // Plan new work, if we don't have enough
                    try
                    {
                        var res = dbManager.BackendDB.Query("select count(*) from work where idmodule=@id and finished is NULL",
                                   new Dictionary<string, string>()
                                   {
                                                {"@id", ((int)this.ID).ToString() },
                                   });
                        if (res.Rows.Count == 0 || int.Parse(res.Rows[0][0]) == 0)
                        {
                            base.AddWork("CheckErrorStatus", "", 10);
                            System.Threading.Thread.Sleep(30000);
                        }


                    }
                    catch (Exception ex)
                    {
                        log.Info("Error in work planning", ex.ToString());
                    }

                    // Execute work
                    int idwork = 0;
                    try
                    {

                        while (discord == null || discord.ConnectionState != ConnectionState.Connected)
                        {
                            System.Threading.Thread.Sleep(1000);
                        }

                        string command, data;
                        if (base.GetNextWork(out idwork, out command, out data))
                        {
                            log.Info("Work: " + command + " : " + data);
                            string result = "";
                            switch (command)
                            {
                                case "TriggerMatchPlayed":
                                    result = TriggerMatchPlayed(Newtonsoft.Json.JsonConvert.DeserializeObject<BotWork>(data));
                                    break;
                                case "CheckErrorStatus":
                                    result = CheckErrorStatus();
                                    break;

                            }
                            log.Info(result);
                            base.FinishWork(idwork, result);
                        }

                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "WAIT")
                        {
                            log.Info("Skip while waiting for match parsing");
                            base.ResetUnfinishedWork();
                        }
                        else
                        {
                            log.Error("Error in work execution", ex.ToString());
                            base.FailWork(idwork, ex.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error in main loop: " + ex.ToString());
                    try {
                        if (_channelsAtStart != null)
                        {
                            // Send
                            foreach (var c in _channelsAtStart.Rows)
                            {
                                try
                                {
                                    int idchannel = int.Parse(c[_channelsAtStart.Cols["idbotchannel"]]);
                                    ulong channel_id = ulong.Parse(c[_channelsAtStart.Cols["channel_id"]]);
                                    ulong server_id = ulong.Parse(c[_channelsAtStart.Cols["server_id"]]);

                                    // Build message
                                    int col = 7;
                                    var reply = new EmbedBuilder();
                                    reply.Description = "Status changed ```" +
                                        "Module".PadRight(col) + " " + ID.ToString() + Environment.NewLine +
                                        "Status".PadRight(col) + " " + ex.ToString() +
                                        "```";

                                    // Send the message
                                    if (discord != null && discord.ConnectionState == ConnectionState.Connected)
                                    {
                                        var ch = discord.GetChannel(channel_id) as IMessageChannel;
                                        ch.SendMessageAsync("", false, reply.Build());
                                    }
                                }
                                catch (Exception ex2)
                                {
                                    log.Error("Error sending status event: " + ex2.ToString());
                                }
                            }
                        }
                    }
                    catch { }
                 }
                System.Threading.Thread.Sleep(1000);
            }
        }

        DateTime lastErrorCheck = DateTime.Now;
        Dictionary<ModuleEnum, string> lastErroMap = new Dictionary<ModuleEnum, string>();
     
        string CheckErrorStatus()
        {
            int changedStatusFound = 0;

            // Any errors from modules ?
            foreach (var modName in Enum.GetNames(typeof(ModuleEnum)))
            {
                var module = (ModuleEnum)Enum.Parse(typeof(ModuleEnum), modName);

                var res = dbManager.BackendDB.Query("select * from work where idmodule="+(int)(module)+" and finished > \"" + lastErrorCheck.ToDatabaseUniversalString() + "\" order by finished asc limit 25");
                string lastError = "";
                lastErroMap.TryGetValue(module, out lastError);
                if (lastError == null) lastError = "";
                foreach (var row in res.Rows)
                {
                    string result = row[res.Cols["result"]];
                    bool isError = result.Contains("Exception");
                    if (!isError) result = "";
                    

                    if (result != lastError)
                    {
                        // changed
                        lastErroMap[module] = result;
                        lastError = result;
                        changedStatusFound++;

                        // Grab channels
                        var channels = dbManager.BackendDB.Query("select * from botsettings as bs inner join botchannels as bc on  bc.channel_id=bs.channel_id where setting_key=\"status alert\" and setting_value=@status", new Dictionary<string, string>() {{ "@status", "\"status\"" }});

                        // Send
                        foreach (var c in channels.Rows)
                        {
                            try
                            {
                                int idchannel = int.Parse(c[channels.Cols["idbotchannel"]]);
                                ulong channel_id = ulong.Parse(c[channels.Cols["channel_id"]]);
                                ulong server_id = ulong.Parse(c[channels.Cols["server_id"]]);

                                // Build message
                                int col = 7;
                                var reply = new EmbedBuilder();
                                reply.Description = "Status changed ```" +
                                    "Module".PadRight(col) + " " + module +Environment.NewLine+
                                    "Status".PadRight(col) + " " + (result==""?"OK":result) +
                                    "```";

                                // Send the message
                                if (discord != null && discord.ConnectionState == ConnectionState.Connected)
                                {
                                    var ch = discord.GetChannel(channel_id) as IMessageChannel;
                                    ch.SendMessageAsync("", false, reply.Build());
                                }
                            }
                            catch (Exception ex)
                            {
                                log.Error("Error sending status event: " + ex.ToString());
                            }
                        }
                    }
                }
            
            }
            lastErrorCheck = DateTime.Now;


        


            return "Status checked: " + changedStatusFound+" changes";

        }

        string TriggerMatchPlayed(BotWork work)
        {
            if (discord.ConnectionState == ConnectionState.Connected)
            {
                discord.SetGameAsync("Type !gs for help");
            }

            string retString = "";
            var comp = LeagueManager.FindCompetitionByID(work.idcomp);

            var matches = dbManager.StatsDB.Query("select * from resultsview where idcompetition=" + work.idcomp + " and finished >= @earliest", new Dictionary<string, string>()
            {
                {"@earliest", work.from }
            });
            retString += "" + matches.Rows.Count + " matches";
            int sendCount = 0;


            foreach(var m in matches.Rows)
            {
                // A small wait to allow for parsing of match data etc
                var res = dbManager.StatsDB.Query("select workstatus from matches where idmatch=" + m[matches.Cols["idmatch"]]);
                if (res.Rows.Count > 0 && res.Rows[0][0] == "0") // Not done yet
                {
                    // So check datetime
                    var finishedDatetime = DateTime.Parse(m[matches.Cols["finished"]] + "Z");
                    if ((DateTime.Now - finishedDatetime) < new TimeSpan(0, 10, 0)) // Give it a little while
                    {
                        throw new Exception("WAIT");
                    }
                }
            }

            List<string> idList = new List<string>();

            // Check if any collections contains this competition
            var colls = dbManager.StatsDB.Query("select filter,collection_name from collections");
            List<CompetitionWrapper> results;
            Dictionary<string, List<CompetitionWrapper>> ambigous;
            FilteredQueryRequest q = new FilteredQueryRequest() { id = "comp", idmap = new Dictionary<string, string>() { { "idcompetition", "0" } } };
            Dictionary<string, List<int>> collectionMap = new Dictionary<string, List<int>>();
            foreach (var collrow in colls.Rows)
            {
                var filter = collrow[0];
                var collection_name = collrow[1];
                var compfilter = Newtonsoft.Json.JsonConvert.DeserializeObject<FilteredQueryRequest>(filter);
                compfilter.id = "comp";
                var tmpReqSql = FilteredQueryManager.Get().CreateSQL(compfilter);
                var compRes = dbManager.StatsDB.Query(tmpReqSql.Item1, tmpReqSql.Item2);
                bool gotTheCompetition = false;
                foreach(var comItem in compRes.Rows)
                {

                    if (int.Parse(comItem[compRes.Cols["idcompetition"]]) == work.idcomp) {
                        gotTheCompetition = true;
                        break;
                    }
                    
                    
                }
                if (gotTheCompetition && idList.Contains(collection_name)==false)
                {
                    idList.Add(collection_name);
                }
            }

            // And competitions based on name
            try
            {
                var comps = dbManager.StatsDB.Query("select idcompetition, competition_name from competitions where idcompetition = @idcomp", new Dictionary<string, string>()
            {
                {"@idcomp",""+work.idcomp  }
            });
                foreach (var r in comps.Rows)
                {
                    idList.Add(r[1]); // Add name to list
                }
            }catch(Exception ex)
            {
                log.Error("Error adding comp based on name", ex);
            }



            foreach (var m in matches.Rows)
            {
                try
                {
                    string round = m[matches.Cols["round"]];

                    // Grab channels
                    string chq = "select * from botsettings as bs inner join botchannels as bc on  bc.channel_id=bs.channel_id and bc.last_updated < @earliest where setting_key=\"comp list\" and (setting_value like @value)";
                    var map = new Dictionary<string, string>()
                    {
                            {"@earliest", m[matches.Cols["finished"]] },
                           {"@value", "%\""+work.idcomp+"\"%"}
                    };

                    // Add settings for collections that include the competition
                    int pindex = 0;
                    string repl = "";
                    foreach (var item in idList)
                    {
                        map["@p" + pindex] = "%\"" + item + "\"%";
                        repl += " or setting_value like @p" + pindex+" ";
                        pindex++;
                    }
                    if (repl != "")
                    {
                        chq = chq.Replace("@value", "@value " + repl);
                    }
                    // And perform the query
                    var channels = dbManager.BackendDB.Query(chq, map);




                    // Send
                    foreach (var c in channels.Rows)
                    {
                        try
                        {
                            int idchannel = int.Parse(c[channels.Cols["idbotchannel"]]);
                            ulong channel_id = ulong.Parse(c[channels.Cols["channel_id"]]);
                            ulong server_id = ulong.Parse(c[channels.Cols["server_id"]]);


                            // First make sure we have enabled trigger
                            var trigger = BotChannels.GetBotSetting<string>(dbManager, ulong.Parse(c[channels.Cols["bot_id"]]), channel_id, "comp event","");
                            if (trigger.Contains("match") == false)
                            {
                                continue;
                            }

                            // Build message
                            var reply = new EmbedBuilder();
                            reply.Description = "";
                            reply.Description += OutputCreator.GetCompetitionLink(""+work.idcomp, comp.competition_name, (OriginID)comp.idorigin);
                            if (string.IsNullOrEmpty(round) == false)
                            {
                                reply.Description += ", round";
                            }

                            string teamNameShort1 = GetShortTeamName(m[matches.Cols["team_name_home"]]);
                            string teamNameShort2 = GetShortTeamName(m[matches.Cols["team_name_away"]]);

                            reply.Description += ", " + GetDateTime(server_id, channel_id, m[matches.Cols["finished"]]) + Environment.NewLine;
                            reply.Description += OutputCreator.GetMatchLink(""+work.idcomp, int.Parse(m[matches.Cols["idmatch"]]), m[matches.Cols["team_name_home"]]+" ", m[matches.Cols["team_name_away"]])+" ";
                            reply.Description += "||  " +m[matches.Cols["score_home"]] + " - " + m[matches.Cols["score_away"]] + "  ||" + Environment.NewLine;
                            reply.Description += "```";
                            reply.Description += GetTableRow(teamNameShort1, "VS", teamNameShort2) + " " + GetTableRow(teamNameShort1, "VS", teamNameShort2) + Environment.NewLine;
                            reply.Description += GetTableRow("-----", "-----", "-----") + " " + GetTableRow("-----", "-----", "-----") + Environment.NewLine;

                            var ts1 = dbManager.StatsDB.Query("select * from teammatchstats where idmatch=" + m[matches.Cols["idmatch"]] + " and home=1");
                            var ts2 = dbManager.StatsDB.Query("select * from teammatchstats where idmatch=" + m[matches.Cols["idmatch"]] + " and home=0");

                            List<string> leftCol = new List<string>();
                            List<string> rightCol = new List<string>();

                            leftCol.Add(GetTableRow(ts1, ts2, "team_value","TV"));
                            leftCol.Add(GetTableRow(ts1, ts2, "rerolls", "RR"));
                            leftCol.Add(GetTableRow(ts1, ts2, "turnovers", "TurnO"));//
                            leftCol.Add(GetTableRow(ts1, ts2, "sacks", "Sacks"));//
                            leftCol.Add(GetTableRow(ts1, ts2, "passes","Pass"));
                            leftCol.Add(GetTableRow(ts1, ts2, "completions", "Compl"));//
                            leftCol.Add(GetTableRow(ts1, ts2, "interceptions", "Inter"));

                            rightCol.Add(GetTableRow(ts1, ts2, "dodges","Dodge"));//
                            rightCol.Add(GetTableRow(ts1, ts2, "rushes", "Rush"));//
                            rightCol.Add(GetTableRow(ts1, ts2, "blocks", "Blocks"));
                            rightCol.Add(GetTableRow(ts1, ts2, "casualties", "Cas"));
                            rightCol.Add(GetTableRow(ts1, ts2, "kills", "Kill"));

                            int maxRows = leftCol.Count > rightCol.Count ? leftCol.Count : rightCol.Count;
                            int iLeft = 0; int iRight = 0;
                            for(int i=0; i < maxRows; i++)
                            {
                                bool leftAdded = false;
                                while (iLeft < leftCol.Count) {
                                    var row = leftCol[iLeft];
                                    if(row != null) { leftAdded = true; reply.Description += row; iLeft++; break; }
                                    iLeft++;
                                }
                                if (!leftAdded)
                                {
                                    reply.Description += "                 ";
                                }
                                reply.Description += " ";
                                bool rightAdded = false;
                                while (iRight < rightCol.Count)
                                {
                                    var row = rightCol[iRight];
                                    if (row != null) { reply.Description += row; rightAdded = true; iRight++; break; }
                                    iRight++;
                                }
                                reply.Description += Environment.NewLine;
                                if (!leftAdded && !rightAdded)
                                    break;
                            }

                            reply.Description += "```";

                            // Send the message
                            if (discord != null && discord.ConnectionState == ConnectionState.Connected)
                            {
                                var ch = discord.GetChannel(channel_id) as IMessageChannel;
                                
                                ch.SendMessageAsync("", false, reply.Build());


                                // Update the last updated date
                                dbManager.BackendDB.Query("update botchannels set last_updated=@finished where channel_id=@botchannel",
                                     new Dictionary<string, string>()
                                            {
                                                {"@finished", m[matches.Cols["finished"]] },
                                               {"@botchannel",""+ channel_id}
                                            });
                            }
                            sendCount++;
                        }
                        catch (Exception ex)
                        {
                            log.Error("Error getting botsettings data: " + ex.ToString());
                        }

                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error sending match event: " + ex.ToString());
                }

            }
            retString += "," + sendCount+ " messages sent";
            return retString;
        }

      
        string GetTableRow(string left, string middle, string right)
        {
            try
            {
                if (right.Length > 0 && char.IsNumber(right[0]))
                    return left.PadLeft(5) + " " + middle.PadRight(5) + " " + (int.Parse(right) < 10 ? " " : "") + right.PadRight(int.Parse(right) < 10 ? 4 : 5);
            }
            catch { }
            return left.PadLeft(5) + " " + middle.PadRight(5) + " " + right.PadRight(5);
        }
        string GetTableRow(QueryResult t1, QueryResult t2, string key, string name)
        {
            string keyFor = t1.Cols.ContainsKey(key + "_for") ? key + "_for" : key;
            //string keyAgainst = t1.Cols.ContainsKey(key + "_against") ? key + "_against" : null;
            string val1 = t1.Rows[0][t1.Cols[keyFor]];
            if (string.IsNullOrEmpty(val1)) return null;
            string val2 = t2.Rows[0][t2.Cols[keyFor]];

            string left = val1.PadLeft(5);// + (keyAgainst == null ? "" : (" (" + t1.Rows[0][t1.Cols[keyAgainst]] + ")"));
            string right = val2.Length>0&&char.IsNumber(val2[0]) && int.Parse(val2) < 10 ? " "+val2.PadRight(4) : val2.PadRight(5);// + (keyAgainst == null ? "" : (" (" + t2.Rows[0][t2.Cols[keyAgainst]] + ")"));
            return GetTableRow(left, name, right);
        }
        string GetShortTeamName(string name)
        {
            if (name == "") return "<>";
            string s = ""+name[0];
            char last = 'a';
            for(int i=1; i < name.Length;i++)
            {
                char cur = name[i];
                if (last==' '||char.IsUpper(cur))
                {
                    s += name[i];
                }
                last = cur;
            }
            if (s.Length > 5) return s.Substring(0, 5);
            return s;
        }

        string PadCenter(string txt, int num)
        {
            if (txt.Length > num) return txt.Substring(0, num);
            int l = txt.Length;
            while(l < num)
            {
                if(l%2 == 0)
                    txt = " " + txt;
                else
                    txt = txt+ " ";
                l++;
            }
            return txt;

        }

        string GetDateTime(ulong server_id, ulong channel_id, string datetime)
        {
            DateTime dt = DateTime.Parse(datetime + "Z");
            // What timezone?

            return dt.ToDatabaseUniversalString();
        }


        private async Task Discord_MessageReceived(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;
            if (message == null) { log.Info("null message received"); return; }

            // Determine if the message is a command based on the prefix and make sure no bots trigger commands
            int argPos = 0;
            if (!(message.HasCharPrefix('!', ref argPos) ||
                message.HasMentionPrefix(discord.CurrentUser, ref argPos)) ||
                message.Author.IsBot)
                return;

            // Create a WebSocket-based command context based on the message
            //var context = new SocketCommandContext(discord, message);

#if DEBUG
            if (arg.Author.Username != "mordrek") return;
#endif

            var reply = new EmbedBuilder();
            reply.Description = "";
            string msg = arg.Content;
            if (msg.IndexOf("!gs") < 0) return;
            msg = msg.Substring(msg.IndexOf("!gs")+ "!gs".Length).Trim();
            if (msg == "" || msg == "?" || msg == "help")
            {
                int colsize = 25;
                reply.WithTitle("GoblinSpy bot v" + typeof(Program).Assembly.GetName().Version);
                reply.Description +=
                         "```"+
                         "!gs channel?".PadRight(colsize) + " - Channel commands" + Environment.NewLine +
                         "!gs comp?".PadRight(colsize) + " - Competition commands" + Environment.NewLine +
                         "!gs rank <w> <d> <l>".PadRight(colsize) + " - Calculate ranking score" + Environment.NewLine +
                         "!gs coach <name>".PadRight(colsize) + " - Global coach info" + Environment.NewLine +
                         // "!gs coach?".PadRight(colsize) + " - Coach commands" + Environment.NewLine +
                         "" + Environment.NewLine +
                         "Names like <this> means user input." + Environment.NewLine +
                         "Names like (<this>) means optional input." + Environment.NewLine +
                         "<id|name> means id or name." + Environment.NewLine +
                         "More info about how to write id or name: https://www.mordrek.com/gspy/help/idinput" + Environment.NewLine+
                         //"!gspy alias?".PadRight(colsize) + " - Info on aliases" + Environment.NewLine+
                         "```";
            }
            else if (msg.StartsWith("ch"))
            {
                ChannelCommand.Parse(dbManager, arg, msg, ref reply);
            }
            else if (msg.StartsWith("com"))
            {
                CompetitionCommand.Parse(dbManager, arg, msg, ref reply);
            }
            else if (msg.StartsWith("coa"))
            {
                CoachCommand.Parse(dbManager, arg, msg, ref reply);
            }
            else if (msg.StartsWith("r"))
            {
                RankCommand.Parse(dbManager, arg, msg, ref reply);
            }
            else
            {
                reply.Description = "Didn't recognize that command. Try '!gs ?' for help";
            }
            await arg.Channel.SendMessageAsync("", false, reply.Build());


            return;
        }

      
     


        private System.Threading.Tasks.Task Discord_Log(Discord.LogMessage arg)
        {
            log.Info(arg.Message);
            return Task.CompletedTask;
        }
    }
}
