﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common;
using gSpy3_Common.Database;
using gSpy3_Common.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace gSpy3_DiscordBot.Modules.competition
{
    public class CompetitionTeamCommand
    {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply, string compName, string compStringList)
        {
            try
            {
                msg = Command.NextWord(msg);
                string team = msg;
                string teamName = team;

                if (string.IsNullOrEmpty(team))
                {
                    {
                        reply.Description += ("You need to enter a team name or id");
                        return;
                    }

                }
                
                var replacer = new Dictionary<string, string>();
                replacer["in:idcompetition"] = compStringList;


                var req = new FilteredQueryRequest()
                {
                    id = "compStandings",
                    filters = new Dictionary<string, string>(),
                    idmap = new Dictionary<string, string>()
                    {
                        {"idcompetition","0" }
                    }
                };

                var ambigousteams = new Dictionary<string, List<TeamWrapper>>();
                var resultsteam = new List<TeamWrapper>();
                var teamId = InputParser.GetTeamIDFromIDOrName(dbManager, team, resultsteam, out ambigousteams);
                if (ambigousteams.Count > 0)
                {
                    reply.Description += OutputCreator.ShowAmbigousTeams(ambigousteams);
                    return;
                }
                if (teamId == null)
                {
                    reply.Description += ("Unable to find any team named '" + team + "'");
                    return;

                }
                // First get some competition info ,for format etc
                req.limit = 1;
                req.id = "comp";
                var sqlC = FilteredQueryManager.Get().CreateSQL(req, replacer);
                var qresC = dbManager.StatsDB.Query(sqlC.Item1, sqlC.Item2, null, 10);
                var sformat = qresC.Rows[0][qresC.Cols["format"]];
                var ssorting = qresC.Rows[0][qresC.Cols["sorting"]];
                bool isRanked = string.IsNullOrEmpty(ssorting) || ssorting.Contains("rank");

                // And then the standings
                req.id = "compStandings";
                req.filters["idteam"] = "" + (int)teamId;
                req.order = null;
                req.ordercol = null;
                req.limit = 3;
                var sql = FilteredQueryManager.Get().CreateSQL(req, replacer);
                var qres = dbManager.StatsDB.Query(sql.Item1, sql.Item2, null, 10);
                if (qres.Rows.Count > 0)
                {
                    reply.Description += OutputCreator.GetTeamStandingLink(compName, "" + teamId, teamName, null, false);//, race != null ? req : null);
                    int wins = 0;
                    int draws = 0;
                    int losses = 0;
                    int concedes = 0;
                    foreach (var row in qres.Rows)
                    {
                        wins += int.Parse(row[qres.Cols["wins"]]);
                        draws += int.Parse(row[qres.Cols["draws"]]);
                        losses += int.Parse(row[qres.Cols["losses"]]);
                        concedes += int.Parse(row[qres.Cols["concedes"]]);
                    }
                    if ((wins + draws + losses) > 0)
                    {
                        double rank = Ranking.CalculateRank(wins, draws, losses, concedes);
                        reply.Description += Environment.NewLine + rank.ToString("0.0").Replace(",", ".") + " % winrate";

                    }

                    reply.Description += Environment.NewLine + " ```" +
                        "#".PadLeft(5) + " " +
                        OutputCreator.PadRightFit("Team", 16) + " " +
                       OutputCreator.PadLeftFit("W", 2) + " " +
                       OutputCreator.PadLeftFit("D", 2) + " " +
                       OutputCreator.PadLeftFit("L", 2) + " " +
                        (!isRanked ? OutputCreator.PadRightFit("Pts", 2) : OutputCreator.PadRightFit("Rnk", 6)) +
                       OutputCreator.PadLeftFit("TDD", 3) + " " +
                       OutputCreator.PadLeftFit("CaD", 3) + " " + Environment.NewLine +

                        "-----".PadLeft(5) + " " +
                        OutputCreator.PadRightFit("-----------------", 16) + " " +
                        OutputCreator.PadRightFit("--", 2) + " " +
                        OutputCreator.PadRightFit("--", 2) + " " +
                        OutputCreator.PadRightFit("--", 2) + " " +
                        (!isRanked ? OutputCreator.PadRightFit("--", 2) : OutputCreator.PadRightFit("------", 6)) +
                         OutputCreator.PadLeftFit("------------", 3) + " " +
                        OutputCreator.PadLeftFit("------------", 3) + " " + Environment.NewLine
                          ;
                    foreach (var row in qres.Rows)
                    {
                        reply.Description += row[qres.Cols["position"]].PadLeft(5) + " " +
                            OutputCreator.PadRightFit(row[qres.Cols["team_name"]], 16) + " " +
                            OutputCreator.PadLeftFit(row[qres.Cols["wins"]], 2) + " " +
                            OutputCreator.PadLeftFit(row[qres.Cols["draws"]], 2) + " " +
                            OutputCreator.PadLeftFit(row[qres.Cols["losses"]], 2) + " " +
                            (!isRanked ? OutputCreator.PadLeftFit(row[qres.Cols["points"]], 2) : OutputCreator.PadLeftFit(double.Parse(row[qres.Cols["ranking"]], CultureInfo.InvariantCulture).ToString("0.000").Replace(",", "."), 6)) +
                            OutputCreator.PadLeftFit(row[qres.Cols["td_diff"]], 3) + " " +
                            OutputCreator.PadLeftFit(row[qres.Cols["cas_diff"]], 3) + " " + Environment.NewLine
                            ;

                    }

                    reply.Description += "```";
                }
                else if (qres.Error != null)
                {
                    reply.Description += qres.Error;
                }
                else
                {
                    reply.Description += "No data found";
                }

            }
            catch (Exception ex)
            {
                reply.Description += "Something went terribly wrong...";
            }
            
        }
            
    }
}
