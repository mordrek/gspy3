﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common.Database;
using gSpy3_DiscordBot.Modules.competition.sched;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot.Modules.competition
{
   public class CompetitionScheduleCommand
    {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply, string compName, string compStringList)
        {
            msg = Command.NextWord(msg);
          
            if (msg.StartsWith("next"))
            {
                CompetitionScheduleNextCommand.Parse(dbManager, arg, msg, ref reply, compName, compStringList);
            }
            else
            {
                int colsize = 27;
                reply.Description = "" +
                    "```" +
                    "!gs comp (<id|name>) sched next (<X>)".PadRight(colsize) + " - Show next (X) scheduled games" + Environment.NewLine +
                    " Example: !gs comp sched next" + Environment.NewLine +
                    "```";
            }
            reply.Description += "";
        }

    }
}
