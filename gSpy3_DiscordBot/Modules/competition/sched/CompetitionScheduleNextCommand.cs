﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common.Database;
using gSpy3_Common.Database.Format;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot.Modules.competition.sched
{
   public class CompetitionScheduleNextCommand
    {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply, string compName, string compStringList)
        {
            msg = Command.NextWord(msg);
            int count = 10;
            if (msg!="") { count = int.Parse(msg); }

            // First get some competition info ,for format etc
            Dictionary<string, string> replacer = new Dictionary<string, string>();
            replacer["in:idcompetition"] = compStringList;

            var req = new FilteredQueryRequest();
            req.idmap = new Dictionary<string, string>()
            {
                {"idcompetition","0" }
            };
            req.limit = 1;
            req.id = "comp";
            var sqlC = FilteredQueryManager.Get().CreateSQL(req, replacer);
            var qresC = dbManager.StatsDB.Query(sqlC.Item1, sqlC.Item2, null, 10);
            var sformat = qresC.Rows[0][qresC.Cols["format"]];
            var ssorting = qresC.Rows[0][qresC.Cols["sorting"]];
            var idorigin = qresC.Rows[0][qresC.Cols["idorigin"]];
            bool isRanked = string.IsNullOrEmpty(ssorting) || ssorting.Contains("rank");

            if(sformat == "" || ((FormatID)int.Parse(sformat)) == FormatID.MatchMaking)
            {
                reply.Description += "This competition lacks schedule";
                return;
            }

            // And then the main data
            req.id = "compSchedules";
            req.order = null;
            req.ordercol = null;
            req.limit = count;
            var sql = FilteredQueryManager.Get().CreateSQL(req, replacer);
            var qres = dbManager.StatsDB.Query(sql.Item1, sql.Item2, null, 10);
            if (qres.Rows.Count > 0)
            {
                reply.Description += OutputCreator.GetScheduleLink(compName, compName,  (OriginID)int.Parse(idorigin), false);//, race != null ? req : null);
                reply.Description += Environment.NewLine + " ```";

                reply.Description += Environment.NewLine + 
                    OutputCreator.PadRightFit("Rnd", 3) + " " +
                    OutputCreator.PadLeftFit("Date", 16) + " " +
                    OutputCreator.PadRightFit("Home", 18) + " " +
                    OutputCreator.PadRightFit("vs", 2) + " " +
                    OutputCreator.PadRightFit("Away", 18);

                reply.Description += Environment.NewLine +
                OutputCreator.PadLeftFit("---", 3) + " " +
                OutputCreator.PadRightFit("----------------", 16) + " " +
                OutputCreator.PadRightFit("------------------", 18) + " " +
                OutputCreator.PadLeftFit("--", 2) + " " +
                OutputCreator.PadRightFit("------------------", 18);

                foreach(var row in qres.Rows)
                {
                    reply.Description += Environment.NewLine +
                    OutputCreator.PadLeftFit(row[qres.Cols["round"]], 3) + " " +
                    OutputCreator.PadRightFit(row[qres.Cols["userdate"]], 16) + " " +
                    OutputCreator.PadRightFit(row[qres.Cols["team_name_home"]], 18) + " " +
                    OutputCreator.PadLeftFit("vs", 2) + " " +
                    OutputCreator.PadRightFit(row[qres.Cols["team_name_away"]], 18);

                }
                reply.Description += "```";
            }
            else reply.Description += "Nothing scheduled";
        }

    }
}
