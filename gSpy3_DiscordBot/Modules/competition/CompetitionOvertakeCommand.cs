﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common;
using gSpy3_Common.Database;
using gSpy3_Common.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace gSpy3_DiscordBot.Modules.competition
{
    public class CompetitionOvertakeCommand
    {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply, string compName, string compStringList)
        {
            try
            {
                msg = Command.NextWord(msg);

                string[] splits = msg.Split(" ");
                if (splits.Length < 3)
                {
                    reply.Description += "Lacking arguments";
                    return;
                } else if (splits.Length > 3)
                {
                    reply.Description += "Too many arguments";
                    return;
                } else if (splits[1] != "from") 
                {
                    reply.Description += "Middle argument should be 'from'";
                    return;
                }
                int to = int.Parse(splits[0]);
                int from= int.Parse(splits[2]);

                if(to >= from)
                {
                    reply.Description += "'to' must be lower than 'from'";
                    return;
                }
                

            
                var replacer = new Dictionary<string, string>();
                replacer["in:idcompetition"] = compStringList;


                // Find the positions for the competition

                var req = new FilteredQueryRequest() { idmap = new Dictionary<string, string>() { { "idcompetition", "0" } } };

                // First get some competition info ,for format etc
                req.limit = 1;
                req.id = "comp";
                var sqlC = FilteredQueryManager.Get().CreateSQL(req, replacer);
                var qresC = dbManager.StatsDB.Query(sqlC.Item1, sqlC.Item2, null, 10);
                var sformat = qresC.Rows[0][qresC.Cols["format"]];
                var ssorting = qresC.Rows[0][qresC.Cols["sorting"]];
                bool isRanked = string.IsNullOrEmpty(ssorting) || ssorting.Contains("rank");

                if (!isRanked)
                {
                    reply.Description+=("This only works for competitions that uses ranking at the moment");
                    return;
                }

                // And then the standings
                req = new FilteredQueryRequest()
                {
                    idmap = new Dictionary<string, string>() { { "idcompetition", "0" } },
                    id = "compStandings",
                    limit = 2,
                    filters = new Dictionary<string, string>()
                    {
                        {"position","="+from+"|="+to }
                    },
                };
                var sql = FilteredQueryManager.Get().CreateSQL(req, replacer);
                var qres = dbManager.StatsDB.Query(sql.Item1, sql.Item2, null, 10);
                if (qres.Rows.Count >= 2)
                {
                    var targetRank = double.Parse(qres.Rows[0][qres.Cols["ranking"]], CultureInfo.InvariantCulture);
                    var curRank = double.Parse(qres.Rows[1][qres.Cols["ranking"]], CultureInfo.InvariantCulture);
                    var curWins = int.Parse(qres.Rows[1][qres.Cols["wins"]]);
                    var curDraws = int.Parse(qres.Rows[1][qres.Cols["draws"]]);
                    var curLosses = int.Parse(qres.Rows[1][qres.Cols["losses"]]);
                    var curConcedes = int.Parse(qres.Rows[1][qres.Cols["concedes"]]);
                    int wins = curWins;
                    double newRank = 0;
                    for (int i = 1; i < 100; i++) 
                    {
                        wins = curWins + i;
                        newRank = Ranking.CalculateRank(wins, curDraws, curLosses, curConcedes);
                        if(newRank > targetRank)
                        {
                            break;
                        }

                    }
                    if(newRank <= targetRank)
                    {
                        reply.Description += "More than you will ever win";
                        return;
                    }
                    reply.Description += OutputCreator.GetStandingsLink(compName, compName , null, null, false) + Environment.NewLine;
                    reply.Description += "Overtaking position " + to + " from " + from + Environment.NewLine;
                    reply.Description += "```" + OutputCreator.PadRightFit("Wins needed:", 13) + OutputCreator.PadLeftFit("" + (wins - curWins), 6) + Environment.NewLine;
                    reply.Description += OutputCreator.PadRightFit("Old rank:", 13) + OutputCreator.PadLeftFit(curRank.ToString("0.000").Replace(",", "."), 6) + Environment.NewLine;
                    reply.Description += OutputCreator.PadRightFit("Target rank:", 13) + OutputCreator.PadLeftFit(targetRank.ToString("0.000").Replace(",", "."), 6) + Environment.NewLine;
                    reply.Description += OutputCreator.PadRightFit("New rank:", 13) + OutputCreator.PadLeftFit(newRank.ToString("0.000").Replace(",", "."), 6) + Environment.NewLine;
                    reply.Description += "```";
                }
                else if (qres.Error != null)
                {
                    reply.Description += qres.Error;
                }
                else
                {
                    reply.Description += "No data found";
                }

            }
            catch (Exception ex)
            {
                reply.Description += "Something went terribly wrong...";
            }
        }

    }
}
