﻿using Discord.Commands;
using Renci.SshNet.Security.Cryptography;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace gSpy3_DiscordBot.Modules
{
	// Create a module with no prefix
	public class GoblinSpyModule : ModuleBase<SocketCommandContext>
	{
		// ~say hello world -> hello world
		[Command("gspy")]
		[Summary("Portal for all gspy commands")]
		public Task<Discord.IUserMessage> GspyAsync([Remainder][Summary("String of gspy commands ripe for parsing")] string cmd) {

		   string msg = cmd;
			string reply = "";
            if (msg == "" || msg=="?"||msg=="help")
            {
                reply = ("<b>gspy</b> bot v"+ typeof(Program).Assembly.GetName().Version);
            }


			return ReplyAsync(reply);
		}

		// ReplyAsync is a method on ModuleBase 
	}
}
