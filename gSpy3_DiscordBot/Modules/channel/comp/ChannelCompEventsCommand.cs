﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot.Modules.channel.comp
{
    public class ChannelCompEventsCommand
    {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply)
        {
            ulong channel_id = BotChannels.RequireChannel(dbManager, arg);
            if (channel_id < 0) reply.Description += "This command requires you to be in a channel";

            msg = Command.NextWord(msg);
            bool triggerEvent = msg.StartsWith("t") || msg.StartsWith("T");
            var curList = false;
            try
            {
                BotChannels.SetBotSetting(dbManager, arg.Author.Id, channel_id, "comp event",triggerEvent? "match":"");
                if (triggerEvent) reply.Description += "Match report events from competitions registered with '!gs channel comp add' will now be shown in this channel";
                else reply.Description += "Match report events will NOT be reported in this channel";
            }
            catch
            { // No setting previously
                reply.Description += "Oh no, I failed";
            }
        }

    }
}
