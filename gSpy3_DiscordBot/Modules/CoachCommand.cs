﻿using Discord;
using Discord.WebSocket;
using gSpy3_Common;
using gSpy3_Common.Database;
using gSpy3_Common.Database.Format;
using gSpy3_Common.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_DiscordBot.Modules
{

    public class CoachCommand {
        public static void Parse(DatabaseManager dbManager, SocketMessage arg, string msg, ref EmbedBuilder reply)
        {
            msg = Command.NextWord(msg);

            if (string.IsNullOrEmpty(msg))
            {
                reply.Description += "You must enter a coachname";
                return;
            }

            string coach = msg;
            var ambigous = new Dictionary<string, List<CoachWrapper>>();
            var coachId = InputParser.GetCoachIDFromIDOrName(dbManager, coach, null, out ambigous);
            if (ambigous.Count > 0)
            {
                reply.Description += OutputCreator.ShowAmbigousCoaches(ambigous);
                return;
            }
            if (coachId == null)
            {
                reply.Description += ("Unable to find any coach named '" + coach + "'");
                return;

            }
            // And then the data
            var req = new FilteredQueryRequest();
            req.id = "compCoachRaces";
            req.filters = new Dictionary<string, string>();
            req.filters["idcoach"] = "" + (int)coachId;
            req.order = null;
            req.ordercol = null;
            req.limit = 5;
            var sql = FilteredQueryManager.Get().CreateSQL(req, null);
            var qres = dbManager.StatsDB.Query(sql.Item1, sql.Item2, null, 10);
            if (qres.Rows.Count > 0)
            {
                reply.Description += OutputCreator.GetCoachLink(null, "" + coachId, coach, null, false);
                {
                    int wins = 0;
                    int draws = 0;
                    int losses = 0;
                    int concedes = 0;
                    foreach (var row in qres.Rows)
                    {
                        wins += int.Parse(row[qres.Cols["wins"]]);
                        draws += int.Parse(row[qres.Cols["draws"]]);
                        losses += int.Parse(row[qres.Cols["losses"]]);
                        concedes += int.Parse(row[qres.Cols["concedes"]]);
                    }
                    if ((wins + draws + losses) > 0)
                    {
                        double rank = Ranking.CalculateRank(wins, draws, losses, concedes);
                        reply.Description += Environment.NewLine + rank.ToString("0.0").Replace(",", ".") + " % winrate";

                    }
                }

                reply.Description += Environment.NewLine + " ```" +
                    OutputCreator.PadRightFit("Race", 12) + " " +
                   OutputCreator.PadLeftFit("Win%", 4) + " " +
                   OutputCreator.PadLeftFit("W", 4) + " " +
                   OutputCreator.PadLeftFit("D", 4) + " " +
                   OutputCreator.PadLeftFit("L", 4) + " " +
                      Environment.NewLine +

                    OutputCreator.PadRightFit("-----------------", 12) + " " +
                  OutputCreator.PadRightFit("----", 4) + " " +
                  OutputCreator.PadRightFit("----", 4) + " " +
                  OutputCreator.PadRightFit("----", 4) + " " +
                    OutputCreator.PadRightFit("----", 4) + " " +
                         Environment.NewLine
                      ;
                foreach (var row in qres.Rows)
                {
                    

                    reply.Description +=
                        OutputCreator.PadRightFit(((RaceEnum)(int.Parse(row[qres.Cols["idrace"]]))).ToString(), 12) + " " +
                        OutputCreator.PadLeftFit(double.Parse(row[qres.Cols["win_pct"]], System.Globalization.CultureInfo.InvariantCulture).ToString("0.0").Replace(",", ".") ,4)+ " " +
                        OutputCreator.PadLeftFit(row[qres.Cols["wins"]], 4) + " " +
                        OutputCreator.PadLeftFit(row[qres.Cols["draws"]], 4) + " " +
                        OutputCreator.PadLeftFit(row[qres.Cols["losses"]], 4) + " " +
                         Environment.NewLine
                        ;

                }

                reply.Description += "```";
            }
            else if (qres.Error != null)
            {
                reply.Description += qres.Error;
            }
            else
            {
                reply.Description += "No data found";
            }

        }
    }
}
