﻿using gSpy3_Common;
using gSpy3_Common.Database;
using System;

namespace gSpy3_DiscordBot
{
    class Program
    {
        static void Main(string[] args)
        {
            Log log = new Log();
            log.Info("gSpy3_DiscordBot v " + typeof(Program).Assembly.GetName().Version);
            try
            {
                var settings = PrivateSettings.Load();
                DatabaseManager dbManager = new DatabaseManager(settings);

                GoblinSpyDiscordBot bot = new GoblinSpyDiscordBot(dbManager, settings.BB2APIKey, settings.DiscordToken);
                bot.Run();
            }
            catch (Exception ex)
            {
                log.Error("Error", ex.ToString());
            }
            log.Info("Program exit");
        }
    }
}
