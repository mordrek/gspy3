﻿using AutoMapper;
using gSpy3_API.Services;
using gSpy3_API.Services.Contracts;
using gSpy3_API.Services.Services;
using gSpy3_Common.Database;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace gSpy3_API.IoC.Configuration.DI
{
    public static class ServiceCollectionExtensions
    {
        public static void ConfigureBusinessServices(this IServiceCollection services, IConfiguration configuration, DatabaseManager dbManager, string baseFilePath)
        {
            if (services != null)
            {
                services.AddTransient<IDatabaseService, DatabaseService>((sp)=>
                {
                    return new DatabaseService(dbManager);

                });
                services.AddTransient<IFileService, FileService>((sp) =>
                {
                    return new FileService(baseFilePath);

                });
                services.AddTransient<IUserService, UserService>();
            }
        }

        public static void ConfigureMappings(this IServiceCollection services)
        {
            if (services != null)
            {
                //Automap settings
                services.AddAutoMapper(Assembly.GetExecutingAssembly());
            }
        }
    }
}
