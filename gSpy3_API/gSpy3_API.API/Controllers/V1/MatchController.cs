﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gSpy3_API.API.Controllers.V1
{
    using global::gSpy3_API.API.DataContracts.Requests;
    using global::gSpy3_API.Services.Contracts;
    using gSpy3_Common;
    using gSpy3_Common.API.LegacyGoblinSpy.DataFormat;
    using gSpy3_Common.Database;
    using gSpy3_Common.Database.AccessInterface;
    using gSpy3_Common.Database.Format;
    using gSpy3_Common.Helpers;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    namespace gSpy3_API.API.Controllers.V1
    {
        [ApiVersion("1.0")]
        [Route("api/match")]//required for default versioning
        [Route("api/v{version:apiVersion}/match")]
        [ApiController]
        public class MatchController : Controller
        {
            Log log = new Log();
            private readonly IDatabaseService _db;

            public MatchController(IDatabaseService dbManager)
            {
                _db = dbManager;
            }


            [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<string>))]
            [HttpGet("replays")]
            public async Task<List<string>> GetMatchReplays(DateTime from, DateTime to)
            {
                var settings = PrivateSettings.Load();
                string baseURL = "https://www.mordrek.com/gspy/matches/";
                string fileURL = Path.Combine(settings.BaseFileFolder, "matches");
                var now = from;
                List<string> res = new List<string>();
                int i = 0;
                while(now.Date <= to.Date)
                {
                    var folder = Path.Combine(fileURL, now.Date.ToString("yyyy-MM-dd"));
                    if (Directory.Exists(folder))
                    {
                        var files = Directory.GetFiles(folder, "*.bbrz");
                        foreach (var file in files)
                        {
                            res.Add(baseURL+now.Date.ToString("yyyy-MM-dd")+"/"+ Path.GetFileName(file));
                            i++;
                        }
                        if (i > 100)
                        {
                            break;
                        }
                    }
                    now += new TimeSpan(1, 0, 0, 0);
                }


                return res;
            }


            [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
            [HttpGet("{idmatch}/url")]
            public async Task<string> GetMatchURL(string idmatch)
            {
                bool cyanideID = false;

                var settings = PrivateSettings.Load();
                string baseURL = "https://www.mordrek.com/gspy/matches/";
                string fileURL = Path.Combine(settings.BaseFileFolder, "matches");

                if (idmatch.Contains("_"))
                {
                    var sNonHex = Int64.Parse(idmatch.Substring(idmatch.IndexOf("_") + 1), System.Globalization.NumberStyles.HexNumber);
                    var res2 = _db.GetStatsConnection().Query("select finished from matches where match_origin_uid=@idmatch", new Dictionary<string, string>(){
                        { "idmatch", ""+sNonHex }  });
                    if (res2.Rows.Count > 0)
                    {
                        var filePath = Path.Combine(fileURL, res2.Rows[0][0].Substring(0, res2.Rows[0][0].IndexOf(" ")) + "/" + idmatch.Substring(idmatch.IndexOf("_") + 1) + ".bbrz");
                        if (System.IO.File.Exists(filePath) == false)
                        {
                            return null;
                        }
                        return baseURL + res2.Rows[0][0].Substring(0, res2.Rows[0][0].IndexOf(" ")) + "/" + idmatch.Substring(idmatch.IndexOf("_") + 1) + ".bbrz";
                    }
                }
                   
                var res = _db.GetStatsConnection().Query("select finished,match_origin_uid from matches where idmatch=@idmatch", new Dictionary<string, string>(){
                    { "idmatch", idmatch
                    } });
                if(res.Rows.Count > 0)
                {
                    var hexmatch = UInt64.Parse(res.Rows[0][1]).ToString("X");
                    var filePath = Path.Combine(fileURL, res.Rows[0][0].Substring(0, res.Rows[0][0].IndexOf(" ")) + "/" + hexmatch + ".bbrz");
                    if (System.IO.File.Exists(filePath) == false)
                    {
                        return null;
                    }
                    return baseURL + res.Rows[0][0].Substring(0, res.Rows[0][0].IndexOf(" ")) + "/" + hexmatch + ".bbrz";
                }
                
               
                return null;
            }


        }

    }

}
