﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gSpy3_API.API.Controllers.V1
{
    using global::gSpy3_API.API.DataContracts.Requests;
    using global::gSpy3_API.Services.Contracts;
    using gSpy3_Common;
    using gSpy3_Common.API.LegacyGoblinSpy.DataFormat;
    using gSpy3_Common.Database;
    using gSpy3_Common.Database.AccessInterface;
    using gSpy3_Common.Database.Format;
    using gSpy3_Common.Helpers;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    namespace gSpy3_API.API.Controllers.V1
    {
        [ApiVersion("1.0")]
        [Route("api/comp")]//required for default versioning
        [Route("api/v{version:apiVersion}/comp")]
        [ApiController]
        public class CompetitionController : Controller
        {
            Log log = new Log();
            private readonly IDatabaseService _db;

            public CompetitionController(IDatabaseService dbManager)
            {
                _db = dbManager;
            }

            /// <summary>
            /// Mark the given match for replay download (in case the original download failed)
            /// </summary>
            /// <param name="idcompinput">id or competition_name, see https://www.mordrek.com/gspy/help/idinput</param>
            /// <param name="sorting">W-D-L, TIE1 (, TIE2, TIE3)</param>
            /// <returns></returns>
            [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
            [HttpPost("{idcompinput}/match/{idmatch}/downloadreplay")]
            public async Task<bool> DownloadReplay(string idcompinput, UInt64 idmatch)
            {
                log.Info("DownloadReplay " + idcompinput+ " : "+ idmatch);

                return MatchManager.DownloadReplay(idcompinput, idmatch);
            }

            /// <summary>
            /// Change sorting for a competition
            /// Sorting follows the formula shown when you select to alter sorting of a competition on the gspy page
            /// </summary>
            /// <param name="idcompinput">id or competition_name, see https://www.mordrek.com/gspy/help/idinput</param>
            /// <param name="sorting">W-D-L, TIE1 (, TIE2, TIE3)</param>
            /// <returns></returns>
            [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
            [HttpPost("{idcompinput}/sorting/{sorting}")]
            public async Task<bool> AlterSorting(string idcompinput, string sorting)
            {
                log.Info("AlterSorting " + idcompinput, sorting);

                return LeagueManager.AlterCompetitionSorting(0, idcompinput, sorting);
            }

            /// <summary>
            /// Perform maintenance for competition.
            /// Mainy to adjust for issues due to buggy legacy import
            /// </summary>
            /// <param name="idcompinput">id or competition_name, see https://www.mordrek.com/gspy/help/idinput</param>
            /// <returns></returns>
            [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
            [HttpPost("{idcompinput}/maintenance")]
            public async Task<bool> PerformMaintenance(string idcompinput)
            {
                log.Info("Maintenance " + idcompinput);

                return LeagueManager.PerformMaintenance(idcompinput);
            }

            /// <summary>
            /// Requests a legacy export
            /// Returns link to where the exported data will be after finished export
            /// If an export already exists, a new export will only be made if a day has passed since last time
            /// </summary>
            /// <param name="idcompinput">id or competition_name, see https://www.mordrek.com/gspy/help/idinput</param>
            /// <returns></returns>
            [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
            [HttpPost("{idcompinput}/legacyexport")]
            public async Task<string> LegacyExport(string idcompinput)
            {
                log.Info("LegacyExport " + idcompinput);
                var idcompList = LeagueManager.FindCompetitionByIDCompInput(idcompinput);
                bool ok = true;
                string dbfile = "";

                bool cleanupDone = false;


                foreach (var comp in idcompList)
                {
                    var settings = PrivateSettings.Load();
                    dbfile = gSpy3_Maintenance.LegacyExporter.GetFilename(settings.BaseFileFolder,comp.idcompetition);

                    // Cleanup
                    if (cleanupDone == false)
                    {
                        cleanupDone = true;
                        var folder = System.IO.Path.GetDirectoryName(dbfile);
                        if (Directory.Exists(folder) == false)
                        {
                            Directory.CreateDirectory(folder);
                        }
                        foreach (var existingfile in Directory.GetFiles(folder,"*.sqlite"))
                        {
                            FileInfo fi = new FileInfo(existingfile);
                            if ((DateTime.Now - fi.CreationTime) > new TimeSpan(30, 0, 0, 0))
                            {
                                log.Info("Legacy database export removed due to age: " + existingfile);
                                System.IO.File.Delete(existingfile);
                            }
                        }
                    }

                    // Check if already exists
                    if (System.IO.File.Exists(dbfile))
                    {
                        FileInfo fi = new FileInfo(dbfile);
                        if((DateTime.Now-fi.CreationTime) < new TimeSpan(1, 0, 0, 0))
                        {
                            return "https://www.mordrek.com/gspy/export/database/" + System.IO.Path.GetFileName(dbfile);
                        }
                        log.Info("Removing existing export");
                        System.IO.File.Delete(dbfile);

                    }
                    
                }
                if (dbfile != "")
                {
                    string data = "{\"idcompname\":\"" + idcompinput + "\"}";
                    var res = _db.GetBackendConnection().Query("insert into work (idmodule,command,data,weight,added) values (@idmodule,@command,@data,@weight,@added)", new Dictionary<string, string>()
                    {
                        { "@idmodule", ""+(int)gSpy3_Common.Module.ModuleBase.ModuleEnum.gSpy3_Maintenance },
                        {"@command","LegacyExport" },
                        { "@data", data},
                        { "@weight", ""+100},
                        { "@added", DateTime.Now.ToDatabaseUniversalString()},
                    });
                }

                return "https://www.mordrek.com/gspy/export/database/" + System.IO.Path.GetFileName(dbfile);
            }


            /// <summary>
            /// Update user set schedules of a competitions
            /// </summary>
            /// <param name="idcompinput">id or competition_name, see https://www.mordrek.com/gspy/help/idinput</param>
            /// <param name="request"></param>
            /// <returns></returns>
            [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
            [HttpPost("{idcompinput}/schedules")]
            public async Task<bool> UpdateSchedules(string idcompinput, [FromBody] UpdateSchedulesRequest request)
            {
                log.Info("UpdateSchedules " + idcompinput, request);
                var idcompList = LeagueManager.FindCompetitionByIDCompInput(idcompinput);
                bool ok = true;
                foreach (var comp in idcompList)
                {
                    List<UserSchedule> list = new List<UserSchedule>();
                    foreach (var item in request.schedules)
                    {
                        list.Add(new UserSchedule()
                        {
                            schedule_origin_uid = item.idcontest,
                            idorigin = (int)comp.idorigin,
                            userdate_changed = DateTime.Now.ToDatabaseUniversalString(),
                            userdate = string.IsNullOrEmpty(item.datetime)==false ? DateTime.Parse(item.datetime).ToDatabaseUniversalString() : ""

                        });
                    }

                    ok &= LeagueManager.UpdateSchedules((int)comp.idorigin, list);
                }
                return ok;
            }

            /// <summary>
            /// Returns current standings
            /// </summary>
            /// <param name="idcompinput">id or competition_name, see https://www.mordrek.com/gspy/help/idinput</param>
            /// <returns></returns>
            [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
            [HttpGet("{idcompinput}/standings")]
            public async Task<QueryResult> GetStandings(string idcompinput)
            {
                return LeagueManager.GetStandings(idcompinput);

                

            }

            /// <summary>
            /// Get user defined schedule
            /// </summary>
            /// <param name="idcompinput">id or competition_name, see https://www.mordrek.com/gspy/help/idinput</param>
            /// <returns></returns>
            [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
            [HttpGet("{idcompinput}/userschedule")]
            public async Task<QueryResult> GetUserSchedule(string idcompinput)
            {
                return ScheduleManager.GetUserSchedule(idcompinput);

            }

            /// <summary>
            /// Get schedule
            /// </summary>
            /// <param name="idcompinput">id or competition_name, see https://www.mordrek.com/gspy/help/idinput</param>
            /// <returns></returns>
            [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
            [HttpGet("{idcompinput}/schedule")]
            public async Task<QueryResult> GetSchedule(string idcompinput)
            {
                return ScheduleManager.GetSchedule(idcompinput);

            }

            /// <summary>
            /// Returns top list for each race
            /// </summary>
            /// <param name="idcompinput">id or competition_name, see https://www.mordrek.com/gspy/help/idinput</param>
            /// <returns></returns>
            [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
            [HttpGet("{idcompinput}/standings/race")]
            public async Task<Dictionary<string, QueryResult>> GetStandingsPerRace(string idcompinput)
            {
                return LeagueManager.GetStandingsPerRace(idcompinput);
            }

        }
    }

}
