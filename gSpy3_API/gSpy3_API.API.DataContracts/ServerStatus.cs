﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_API.API.DataContracts
{
    public class ServerStatus
    {
        public List<ModuleStatus> Modules { get; set; }
    }
}
