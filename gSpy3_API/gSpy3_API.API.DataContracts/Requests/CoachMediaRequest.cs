﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_API.API.DataContracts.Requests
{
    public class CoachMediaRequest
    {
        public string youtube { get; set; }
        public string twitch { get; set; }
    }
}
