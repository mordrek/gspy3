﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_API.Services.Contracts
{
    public interface IFileService
    {
        string LoadFile(string path);
    }
}
