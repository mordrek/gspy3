﻿using gSpy3_Common;
using gSpy3_Common.API;
using gSpy3_Common.API.Cyanide;
using gSpy3_Common.Database;
using gSpy3_Common.Database.AccessInterface;
using gSpy3_Common.Database.Format;
using gSpy3_Common.Helpers;
using gSpy3_Common.Module;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using System.Timers;

namespace gSpy3_BB2MatchCollector
{
    public class BB2MatchCollector : ModuleBase
    {
        DatabaseManager dbManager;
        Timer heartbeatTimer;
        protected enum Work { Activate_Leagues, Update_Leagues , RequestUpdateLeagues }
        Log log = new Log();
        CyanideBBAPI cyanide;

        
        public BB2MatchCollector(DatabaseManager dbm, string bb2APIKey) : base(ModuleEnum.gSpy3_BB2MatchCollector, dbm)
        {
            cyanide = new CyanideBBAPI(bb2APIKey);
            dbManager = dbm;
            heartbeatTimer = new Timer();
            heartbeatTimer.Interval = 1;
            heartbeatTimer.Elapsed += HeartbeatTimer_Elapsed;
            heartbeatTimer.Start();

            LeagueManager.cyanideAPI = cyanide;
            LeagueManager.ModuleID = (int)ModuleEnum.gSpy3_BB2MatchCollector;
        }

        private void HeartbeatTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            heartbeatTimer.Enabled = false;
            heartbeatTimer.Interval = 1000 * 60;
            base.Tick();
            heartbeatTimer.Enabled = true;
        }

        

        public void Run()
        {



#if DEBUG
//            bb2.GetTeams(3, OriginID.BB3_pc, "DVLA BB3 League", "DVLA PRE-SEASON");

            RequestUpdateLeagues(new UpdateCompetitionWork()
            {
                idleagues = new List<int>() { 96 },// 703 },
                                                      from = "2000-06-19",
                                                        to = "2024-02-01",
                                                    // platform = OriginID.BB3_pc.ToString()
                platform = OriginID.BB2_pc.ToString()

            });
            /*
            ActivateLeagues(new UpdateCompetitionWork()
            {
                platform = OriginID.BB3_pc.ToString(),
                idleagues = new List<int>()
                  {
                       45
                  }
            });*

            /*
            var trans = dbManager.StatsDB.BeginTransaction();
            ImportSchedule(OriginID.BB2_pc, 12, trans);
            trans.Commit();
            */
           // GroupedStatsManager.CreateGroupedStats(4);
            /*

            */
            /*
            try
            {
                int idcomp = 907;
                Transaction trans = null;
                OriginID origin = OriginID.BB2_pc;
                var comp = LeagueManager.FindCompetition(idcomp, trans);
              //  if (comp.format != null && comp.format != (int)FormatID.MatchMaking)
                {
                    var league = LeagueManager.FindLeague(comp.idleague);
                    var sched = bb2.GetSchedule((int)ID, OriginConversion.ToLegacyGoblinSpy(origin), league.league_name, comp.competition_name);
                    var schedList = new List<Schedule>();
                    foreach (var s in sched.upcoming_matches)
                    {
                        if (s.opponents != null && s.opponents.Count >= 2)
                        {
                            schedList.Add(new Schedule(origin, s, (int)comp.idcompetition,
                                                                    (int)TeamManager.FindTeam(origin, s.opponents[0].team.id, trans).idteam,
                                                                    (int)TeamManager.FindTeam(origin, s.opponents[1].team.id, trans).idteam));
                        }

                    }

                    ScheduleManager.AddIfMissing(schedList, trans);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
            }
            */
#endif


            while (true)
            {
                try
                {
                    var numRemoved = base.RemoveOldWork();
                    if (numRemoved > 0)
                    {
                        log.Info("Removed " + numRemoved + " old work items");
                    }

                    // Plan new work, if we don't have enough
                    try
                    {
                        int workNeeded = base.GetNumWorkNeeded();
                        if (workNeeded > 0)
                        {
                            // Check what leagues have "collecting" status in the public db, and which are being collected as stated in the backed.
                            var activeLeagues = CollectionManager.GetActiveLeagues();
                            var collectedLeagues = CollectionManager.GetCollectedLeagues();
                            var officialLeagues = CollectionManager.GetOfficialLeagues();
                            string platform = "";
                            // Check if we have any active league that has no active collection
                            // If so, that should be prioritized
                            List<Tuple<int,OriginID>> unactivatedLeagues = new List<Tuple<int, OriginID>>();
                            foreach(var id in activeLeagues)
                            {
                                if (collectedLeagues.Contains(id.Item1)==false && (platform=="" || platform == id.Item2.ToString()))
                                {
                                    if (platform == "")
                                    {
                                        platform = id.Item2.ToString();
                                    }
                                    unactivatedLeagues.Add(id);
                                    workNeeded--;
                                    if(workNeeded <= 0) { break; }
                                }
                            }
                            if (unactivatedLeagues.Count > 0)
                            {
                                List<int> idleagues = new List<int>();
                                foreach(var tuple in unactivatedLeagues) { idleagues.Add(tuple.Item1); }

                                AddWork(Work.Activate_Leagues.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(new UpdateCompetitionWork()
                                { idleagues = idleagues, platform = platform }), 100);
                                CollectionManager.SetCollectedLeagues(unactivatedLeagues);

                               
                            }

                            // Then we add league update based on time since last update
                            var leaguesToUpdate = CollectionManager.GetNextLeaguesToCollect();
                            int maxLeaguesPerUpdate = 20;
                            
                            while(workNeeded > 0)
                            {
                                // Find the next items in leaguesToUpdate to send as work
                                UpdateCompetitionWork work = new UpdateCompetitionWork()
                                {
                                    idleagues = new List<int>()
                                };
                                platform = "";
                                bool gotOfficial = false;
                                foreach(var id in leaguesToUpdate)
                                {
                                    if ((platform == "" || platform == id.Item2.ToString()))
                                    {
                                        if (platform == "")
                                        {
                                            platform = id.Item2.ToString();
                                            work.platform = platform;
                                        }
                                        
                                        if (officialLeagues.Contains(id.Item1))
                                        {
                                            
                                            if (work.idleagues.Count == 0) // This is a new group, officials must be handled one by one due to the amount
                                            {
                                                work.idleagues.Add(id.Item1);
                                                gotOfficial = true;
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            work.idleagues.Add(id.Item1);
                                            if (work.idleagues.Count >= maxLeaguesPerUpdate)
                                            {
                                                break;
                                            }
                                        }
                                    }
                                }
                                // Remove the work order from the list so that we don't ask to update same leagues again next time
                                foreach(var id in work.idleagues)
                                {
                                    for(int i=0; i < leaguesToUpdate.Count; i++)
                                    {
                                        if(leaguesToUpdate[i].Item1 == id)
                                        {
                                            leaguesToUpdate.RemoveAt(i);
                                            i--;
                                        }
                                    }
                                }

                                if (work.idleagues.Count == 0)
                                {
                                    break;
                                }

                                // Create the work
                                AddWork(Work.Update_Leagues.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(work),10);
                                workNeeded--;
                                CollectionManager.SetLatestLeagueCheck(work.idleagues);

                            }


                        }
                    }
                    catch (Exception ex)
                    {
                        log.Info("Error in work planning", ex.ToString());
                    }

                    // Execute work
                    int idwork = 0;
                    try
                    {
                        
                        string command, data;
                        if (base.GetNextWork(out idwork, out command, out data))
                        {
                            log.Info("Work: " + command + " : " + data);
                            string result = "";
                            switch (command)
                            {
                                case "Activate_Leagues":
                                     result = ActivateLeagues(Newtonsoft.Json.JsonConvert.DeserializeObject<UpdateCompetitionWork>(data));
                                    break;
                                case "Update_Leagues":
                                    result = UpdateLeagues(Newtonsoft.Json.JsonConvert.DeserializeObject<UpdateCompetitionWork>(data));
                                    break;
                                case "RequestUpdateLeagues":
                                    result = RequestUpdateLeagues(Newtonsoft.Json.JsonConvert.DeserializeObject<UpdateCompetitionWork>(data));
                                    break;
                            }
                            log.Info(result);
                            base.FinishWork(idwork, result);
                        }

                    }
                    catch (Exception ex)
                    {
                        log.Error("Error in work execution", ex.ToString());
                        base.FailWork(idwork, ex.ToString());
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error in main loop: " + ex.ToString());
                }
                System.Threading.Thread.Sleep(500);
            }
        }

        /// <summary>
        /// Activating means we try to get data from early on until today
        /// </summary>
        /// <param name="work"></param>
        /// <returns></returns>
        string ActivateLeagues(UpdateCompetitionWork work)
        {
            OriginID origin = OriginConversion.FromLegacyGoblinSpy(work.platform);
            var logoMap = UniqueStringManager.GetLogoMap();
            var mottoMap = UniqueStringManager.GetMottoMap();
            var casComboMap = UniqueStringManager.GetCasComboMap();
            var casMap = UniqueStringManager.GetCasMap();
            var skComboMap = UniqueStringManager.GetSkillComboMap();
            var skillMap = UniqueStringManager.GetSkillMap();
            var plTypeMap = UniqueStringManager.GetPlayerTypeMap();
            var plNameMap = UniqueStringManager.GetPlayerNameMap();
            var raceNameMap = dbManager.StatsDB.GetIndexMap("idrace", new string[] { "race_name"}, "races", "",null,null);

            work.from = (DateTime.Now - new TimeSpan(30, 0, 0, 0)).ToDatabaseUniversalString();
            work.to = (DateTime.Now.ToDatabaseUniversalString());

            foreach (var item in work.idleagues) 
            {
                var league = LeagueManager.FindLeagueByID(item);
                if (league.league_name == "")
                {
                    continue;
                }
                UpdateCompetitionsAndTeams.UpdateCompetitionsInLeague(dbManager, cyanide, (int)ID, origin, (int)league.idleague);
            }

            return RequestUpdateLeagues(work);
        }

    

       



        /// <summary>
        /// Updates the leagues from a couple of hours ago until current time
        /// </summary>
        /// <param name="work"></param>
        /// <returns></returns>
        string UpdateLeagues(UpdateCompetitionWork work)
        {
            var res = dbManager.StatsDB.Query("select last_game from leagues as l inner join competitions as c on c.idleague = l.idleague where l.idleague in ("+string.Join(',',work.idleagues)+") order by last_game desc limit 1",null,null, 60 * 60 * 20);
            if (res != null && res.Success && res.Rows.Count > 0)
            {
                try
                {
                    work.from = (DateTime.Parse(res.Rows[0][0] + "Z") - new TimeSpan(0, 4, 0, 0)).ToDatabaseUniversalString();
                }
                catch
                {
                    work.from = (DateTime.Now - new TimeSpan(0, 4, 0, 0)).ToDatabaseUniversalString();
                }
            }
            else
            {
                work.from = (DateTime.Now - new TimeSpan(0, 4, 0, 0)).ToDatabaseUniversalString();
            }
            work.to = ((DateTime.Now+new TimeSpan(4,0,0)).ToDatabaseUniversalString());
            return RequestUpdateLeagues(work);

        }

        static Dictionary<int, DateTime> lastLeagueRequestMap = new Dictionary<int, DateTime>();

        /// <summary>
        /// Updates the leagues for the given period. If the max limit is reached when getting data, the rest is continued by adding work orders on the queue
        /// </summary>
        /// <param name="work"></param>
        /// <returns></returns>
        string RequestUpdateLeagues(UpdateCompetitionWork work)
        {
            OriginID origin = OriginConversion.FromLegacyGoblinSpy(work.platform);
            bool isBB2 = OriginConversion.IsBB2(origin);
            bool isBB3 = OriginConversion.IsBB3(origin);


            Transaction trans = null;
            string res = "Updated from "+work.from+" to "+work.to+" : ";
            try
            {
                int limit = 1000;

#if DEBUG
limit = 1;
#endif


                ResourceManager.CleanupResourceUse();

                // Fetch league names from the IDs given, since BB API queries are name-based
                List<string> leagues = new List<string>();
                foreach(int id in work.idleagues)
                {
                    var l = LeagueManager.FindLeagueByID(id);
                    if (l != null && l.league_name!="")
                    {
                        leagues.Add(l.league_name);

                        if (lastLeagueRequestMap.ContainsKey((int)l.idleague))
                        {
                            DateTime lastUpdateForLeague = lastLeagueRequestMap[(int)l.idleague];
                            while ((DateTime.Now-lastUpdateForLeague) < new TimeSpan(0, 5, 0))
                            {
                                log.Info("A little too soon to check this league again. Sleep a while");
                                System.Threading.Thread.Sleep(30000);
                                continue;
                            }
                        }
                        lastLeagueRequestMap[(int)l.idleague] = DateTime.Now;
                    }
                }

                if (leagues.Count > 0)
                {



                    // Grab list of match IDs
                    DateTime timeBegin = DateTime.Now;

                    List<string> newMatchesIDList = cyanide.GetMatchIDs((int)ID, origin, work.from, work.to, leagues, limit);

                    
                    List<string> matchesToRequest = new List<string>();
                    string firstDate = "";
                    foreach (var item in newMatchesIDList)
                    {
                        // Has the match already been registered?
                        var m = MatchManager.FindMatch(origin, isBB2 ? UInt64.Parse(item, NumberStyles.HexNumber).ToString() : item);
                        if (m == null)
                        {
                            // Not registered, so we should request match data
                            matchesToRequest.Add(item);
#if DEBUG
                            break;
#endif


                        }
                        else
                        {
                            try
                            {
                                if (firstDate == "" || DateTime.Parse(m.started) < DateTime.Parse(firstDate))
                                {
                                    firstDate = m.started;
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("huh?");
                            }
                        }



                    }

                    var Time_GettingMatchIDs = (int)(DateTime.Now - timeBegin).TotalMilliseconds;
                    Console.WriteLine("Getting match IDs: " + Time_GettingMatchIDs + "ms ("+newMatchesIDList.Count+" matches)");
                    timeBegin = DateTime.Now;

                    // Download matches
                    var bbMatches = new List<gSpy3_Common.API.Cyanide.DataFormat.MatchResult>();
                    Parallel.ForEach(matchesToRequest, (mid) =>  

                    //foreach (var mid in matchesToRequest)
                    {
                        try
                        {
                            lock (bbMatches)
                            {
                                var match = cyanide.GetMatch((int)ID, mid, origin);
                                bbMatches.Add(match);
#if DEBUG
  //  break;
#endif

                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error(ex.ToString());
                            throw ex;
                        }
                    }
                    );


                    var Time_DownloadMatches = (int)(DateTime.Now - timeBegin).TotalMilliseconds;
                    Console.WriteLine("Download " + matchesToRequest.Count + " matches: " + Time_DownloadMatches + "ms => " + (matchesToRequest.Count == 0 ? "" : ("" + (Time_DownloadMatches / matchesToRequest.Count) + "ms")));
                    timeBegin = DateTime.Now;

                    Dictionary<int, bool> competitionMap = new Dictionary<int, bool>();

                    trans = dbManager.StatsDB.BeginTransaction();
                    try
                    {
                        var logoMap = UniqueStringManager.GetLogoMap(trans);
                        var mottoMap = UniqueStringManager.GetMottoMap(trans);
                        var casComboMap = UniqueStringManager.GetCasComboMap(trans);
                        var casMap = UniqueStringManager.GetCasMap(trans);
                        var skComboMap = UniqueStringManager.GetSkillComboMap(trans);
                        var skillMap = UniqueStringManager.GetSkillMap(trans);
                        var stadiumMap = UniqueStringManager.GetStadiumMap(trans);
                        var plTypeMap = UniqueStringManager.GetPlayerTypeMap(trans);
                        var plNameMap = UniqueStringManager.GetPlayerNameMap(trans);

                        // Coaches
                        Dictionary<string, bool> coachMap = new Dictionary<string, bool>();
                        List<Coach> convertedCoaches = new List<Coach>();
                        if (bbMatches == null)
                        {
                            Console.WriteLine("!!! NULL MATCHES");
                        }
                        else
                        {
                            foreach (var m in bbMatches)
                            {
                                if (m?.match?.coaches != null)
                                {
                                    foreach (var c in m.match.coaches)
                                    {
                                        if (coachMap.ContainsKey(c.idcoach) == false)
                                        {
                                            convertedCoaches.Add(new Coach(c, origin));
                                            coachMap[c.idcoach] = true;
                                        }
                                    }
                                }
                            }
                        }
                        trans.Commit();
                        CoachManager.AddIfMissing(convertedCoaches, null);
                        trans = dbManager.StatsDB.BeginTransaction();

                        var Time_InsCoaches = (int)(DateTime.Now - timeBegin).TotalMilliseconds;
                        Console.WriteLine("Insert coaches: " + Time_InsCoaches + "ms");
                        timeBegin = DateTime.Now;

                        // Teams
                        Dictionary<string, bool> teamMap = new Dictionary<string, bool>();
                        List<Team> convertedTeams = new List<Team>();


                        foreach (var m in bbMatches)
                        {
                            try
                            {
                                var league = LeagueManager.FindLeagueByOrgID(origin, m.match.idleague, trans);
                                if (league == null)
                                {
                                    log.Info($"Unknown league: {m.match.leaguename} : {m.match.idleague}");
                                    continue;
                                }


                                var comp = LeagueManager.FindCompetitionByOrgID(origin, m.match.idcompetition, trans);

                                // another competition we are not aware of ? 
                                if (comp == null)
                                {
                                    log.Info($"Unknown compettion: {m?.match?.competitionname} : {m?.match?.idcompetition} @ {origin}");
                                   
                                    comp = LeagueManager.FindCompetitionByName(origin, (int)league.idleague, m.match.competitionname, trans);
                                    if (comp != null)
                                    {
                                        log.Info($"Found it by name: {m?.match?.competitionname}");
                                        comp.competition_origin_uid = m.match.idcompetition;
                                        comp.active = 1;
                                    }
                                    else
                                    {
                                        log.Info($"Creating new competition");
                                        comp = new Competition()
                                        {
                                            idleague = (int)league.idleague,
                                            competition_name = m.match.competitionname,
                                            competition_origin_uid = m.match.idcompetition,
                                            idorigin = (int)origin,
                                            active = 1
                                        };
                                    }
                                    // Grab uuid and other info from cyanide
                                    var bb2l = cyanide.GetCompetitions((int)ID, origin, league.league_name);
                                    foreach (var item in bb2l.competitions)
                                    {
                                        log.Info($"Comp from cyanide: {item.name}");
                                        if (item.name == comp.competition_name)
                                        {
                                            log.Info($"Found competition");
                                            if (item.id != null)
                                            {
                                                comp.competition_origin_uid = item.id;
                                            }
                                            try
                                            {
                                                comp.format = (int)FormatIDConversion.FromLegacyGoblinSpy(item.format);
                                            }
                                            catch { }
                                            int dur = 0;
                                            int.TryParse(item.turn_duration, out dur);
                                            comp.turn_duration = dur;
                                        }

                                    }

                                    // Register the competition
                                    trans.Commit();

                                    log.Info($"Register competition");
                                    LeagueManager.RegisterCompetition(new List<Competition>() { comp }, null);

                                    trans = dbManager.StatsDB.BeginTransaction();

                                    comp = LeagueManager.FindCompetitionByOrgID(origin, m.match.idcompetition, trans);

                                    if (comp.idcompetition == null)
                                    {
                                        log.Error("Unable to find competition id for " + comp.competition_name + "...");
                                        continue;
                                    }

                                }

                                if (competitionMap.ContainsKey((int)comp.idcompetition) == false)
                                {
                                    competitionMap[(int)comp.idcompetition] = true;
                                }




                                int i = 0;
                                foreach (var t in m.match.teams)
                                {
                                    if (teamMap.ContainsKey(t.idteamlisting) == false)
                                    {
                                        var curTeam = TeamManager.FindTeamByOrgID(origin, t.idteamlisting);
                                        if (curTeam == null)
                                        {
                                            // Missing team. Let's try to register all teams belonging to the league
                                            /*
                                            trans.Commit();
                                            try
                                            {
                                                trans = dbManager.StatsDB.BeginTransaction();
                                                var league = LeagueManager.FindLeagueByOrgID(origin, m.match.idleague, trans);
                                                UpdateCompetitionsAndTeams.UpdateTeamsInCompetition(dbManager, cyanide, (int)ID, origin, (int)league.idleague, (int)comp.idcompetition, trans);
                                                trans.Commit();
                                            }
                                            catch (Exception ex)
                                            {
                                                trans.Rollback();

                                                Console.WriteLine("Error updating teams in competition: " + ex);
                                            }
                                            trans = dbManager.StatsDB.BeginTransaction();
                                            */
                                        }

                                        var coach = CoachManager.FindCoachByOrgID(origin, m.match.coaches[i].idcoach, trans);
                                        var newTeam = new Team(t, origin, (int)coach.idcoach);
                                        newTeam.idlogo = UniqueStringManager.GetOrCreateLogo(logoMap, t.teamlogo == null ? "" : t.teamlogo);
                                        convertedTeams.Add(newTeam);
                                        teamMap[t.idteamlisting] = true;
                                    }
                                    i++;
                                }
                            }
                            catch(Exception ex)
                            {
                                log.Error(ex.Message.ToString());
                            }
                        }
                        trans.Commit();
                        TeamManager.AddIfMissing(convertedTeams, null);
                        trans = dbManager.StatsDB.BeginTransaction();

                        foreach (var m in bbMatches)
                        {
                            try
                            {
                                var league = LeagueManager.FindLeagueByOrgID(origin, m.match.idleague, trans);
                                if (league == null)
                                {
                                    log.Info($"Unknown league: {m.match.leaguename} : {m.match.idleague}");
                                    continue;
                                }

                                List<Standing> convertedStandings = new List<Standing>();
                                var comp = LeagueManager.FindCompetitionByOrgID(origin, m.match.idcompetition, trans);
                                if (comp == null)
                                {

                                    continue;
                                }
                                int i = 0;
                                foreach (var t in m.match.teams)
                                {

                                    var team = TeamManager.FindTeamByOrgID(origin, t.idteamlisting, trans);
                                    if (team != null)
                                    {
                                        convertedStandings.Add(new Standing()
                                        {
                                            idorigin = (int)origin,
                                            idteam = (int)team.idteam,
                                            idcompetition = (int)comp.idcompetition,
                                            team_origin_uid = t.idteamlisting,
                                            active = 1
                                        });
                                    }
                                }
                                LeagueManager.AddIfMissing(convertedStandings, (int)comp.idcompetition, null);
                            }
                            catch(Exception ex2)
                            {
                                log.Error(ex2.Message.ToString());
                            }
                        }



                        var Time_InsTeams = (int)(DateTime.Now - timeBegin).TotalMilliseconds;
                        Console.WriteLine("Insert teams: " + Time_InsTeams + "ms");
                        timeBegin = DateTime.Now;

                        // Convert to match info
                        List<gSpy3_Common.API.Cyanide.DataFormat.MatchResultTeam> teamsInvolved = new List<gSpy3_Common.API.Cyanide.DataFormat.MatchResultTeam>();
                        Dictionary<string, gSpy3_Common.API.Cyanide.DataFormat.CoachStats> coachesInvolved = new Dictionary<string,gSpy3_Common.API.Cyanide.DataFormat.CoachStats>();
                        List<Match> convertedMatches = new List<Match>();
                        foreach (var m in bbMatches)
                        {
                            try
                            {

                                if(m == null)
                                {
                                    Console.WriteLine("Null match, skip");
                                    continue;
                                }

                                var league = LeagueManager.FindLeagueByOrgID(origin, m.match.idleague, trans);
                                if (league == null)
                                {
                                    log.Info($"Unknown league: {m.match.leaguename} : {m.match.idleague}");
                                    continue;
                                }

                                teamsInvolved.Add(m.match.teams[0]);
                                teamsInvolved.Add(m.match.teams[1]);
                                if (m.match.teams[0] != null && m.match.coaches[0] != null)
                                {
                                    coachesInvolved[m.match.teams[0].idteamlisting] = m.match.coaches[0];
                                }
                                if (m.match.teams[1] != null && m.match.coaches[1] != null)
                                {
                                    coachesInvolved[m.match.teams[1].idteamlisting] = m.match.coaches[1];
                                }

                                var teams = new Team[2];
                                teams[0] = TeamManager.FindTeamByOrgID(origin, m.match.teams[0].idteamlisting, trans);
                                teams[1] = TeamManager.FindTeamByOrgID(origin, m.match.teams[1].idteamlisting, trans);
                                var comp = LeagueManager.FindCompetitionByOrgID(origin, m.match.idcompetition, trans);


                                try
                                {
                                    if (firstDate == "" || DateTime.Parse(m.match.started) < DateTime.Parse(firstDate))
                                    {
                                        firstDate = m.match.started;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine("huh?");
                                }
                                if (teams == null)
                                {
                                    log.Error("Teams is null");
                                }
                                if (teams[0] == null)
                                {
                                    log.Error("Teams[0] is null");
                                }
                                if (teams[1] == null)
                                {
                                    log.Error("Teams[1] is null");
                                }
                                if (m.match == null)
                                {
                                    log.Error("m.match is null");
                                }
                                if (comp == null)
                                {
                                    log.Error("comp is null");
                                }
                                

                                convertedMatches.Add(new Match(origin, m, (int)comp.idcompetition, (int)teams[0].idteam, (int)teams[1].idteam,
                                    UniqueStringManager.GetOrCreateStadium(stadiumMap, m.match.structstadium)
                                    ));
                            }catch(Exception ex2)
                            {
                                log.Error("Error trying to convert match, "+ex2.ToString());
                                log.Error($"Error for match {m.uuid} in leagues {work.idleagues.ToString()}");
                            }
                        }
                        var insertedMatches = MatchManager.AddIfMissing(convertedMatches, trans);
                        var Time_InsMatches = (int)(DateTime.Now - timeBegin).TotalMilliseconds;
                        Console.WriteLine("Insert matches: " + Time_InsMatches + "ms");
                        timeBegin = DateTime.Now;

                        // Players, PlayerStats, TeamStats
                        Dictionary<string, gSpy3_Common.API.Cyanide.DataFormat.MatchResultTeam> teamMatchStatMap = new Dictionary<string, gSpy3_Common.API.Cyanide.DataFormat.MatchResultTeam>();
                        List<TeamMatchStats> convertedTeamStats = new List<TeamMatchStats>();
                        foreach (var m in bbMatches)
                        {
                            try
                            {
                                if (m == null) { log.Error("Null match in game " + m.uuid); continue; }
                                if (m.match == null) { log.Error("Null match in game " + m.uuid); continue; }
                                if (m.match.teams == null || m.match.teams.Count < 2) { log.Error("Not full teams in game " + m.uuid); continue; }

                                var league = LeagueManager.FindLeagueByOrgID(origin, m.match.idleague, trans);
                                if (league == null)
                                {
                                    log.Info($"Unknown league: {m.match.leaguename} : {m.match.idleague}");
                                    continue;
                                }

                                var teams = new Team[2];
                                teams[0] = TeamManager.FindTeamByOrgID(origin, m.match.teams[0].idteamlisting, trans);
                                teams[1] = TeamManager.FindTeamByOrgID(origin, m.match.teams[1].idteamlisting, trans);
                                if (teams[0] == null || teams[1] == null)
                                {
                                    log.Error("Unable to find teams :(");
                                    continue;
                                }
                                var match = MatchManager.FindMatch(origin, isBB2 ? UInt64.Parse(m.uuid, System.Globalization.NumberStyles.HexNumber).ToString() : m.uuid, trans);
                                int iTeam = 0;
                                var comp = LeagueManager.FindCompetitionByOrgID(origin, m.match.idcompetition, trans);
                                foreach (var t in m.match.teams)
                                {
                                    if (t == null) { log.Error("Null team in game " + m.uuid); continue; }

                                    // Players
                                    List<Player> convertedPlayers = new List<Player>();
                                    List<PlayerMatchStats> convertedPlayerStats = new List<PlayerMatchStats>();

                                    if (t.roster != null)
                                    {
                                        foreach (var p in t.roster)
                                        {
                                            if (p != null && p.id != null) // only add real players, not borrowed ones without id
                                            {
                                                convertedPlayers.Add(new Player(p, origin, (int)league.idleague, (int)teams[iTeam].idteam,
                                                    UniqueStringManager.GetOrCreatePlayerName(plNameMap, p.name, trans),
                                                    UniqueStringManager.GetOrCreateSkillCombo(skComboMap, skillMap, p.skills_string, trans),
                                                    UniqueStringManager.GetOrCreateCasCombo(casComboMap, casMap, p.casualties_state_string, trans),
                                                    UniqueStringManager.GetOrCreateCasCombo(casComboMap, casMap, p.casualties_sustained_string, trans),
                                                    UniqueStringManager.GetOrCreatePlayerType(plTypeMap, p.type, trans)
                                                    ));
                                            }
                                        }
                                    }
                                    string statsorgid = isBB2 ? "" + UInt64.Parse("" + (iTeam + 1) + m.uuid, System.Globalization.NumberStyles.HexNumber) : (iTeam + 1) + "" + m.uuid;
                                    if (teamMatchStatMap.ContainsKey(statsorgid))
                                    {
                                        Console.WriteLine("How?");
                                    }
                                    else
                                    {
                                        teamMatchStatMap[statsorgid] = t;
                                        convertedTeamStats.Add(new TeamMatchStats(m.teams[iTeam], t, iTeam == 0 ? m.match.teams[1] : m.match.teams[0], statsorgid,
                                            origin, match.idcompetition, (int)teams[iTeam].idteam, (UInt64)match.idmatch, iTeam == 0)
                                        {

                                        });
                                    }
                                    if (t.roster != null)
                                    {
                                        trans.Commit();
                                        var playersAdded = TeamManager.AddIfMissing(convertedPlayers, null);
                                        trans = dbManager.StatsDB.BeginTransaction();





                                        // Player match stats
                                        foreach (var p in t.roster)
                                        {
                                            int? idplayer = null;
                                            if (p.id != null)
                                            {
                                                var player = TeamManager.FindPlayerByOrgID(origin, p.id, trans);
                                                idplayer = player.idplayer;


                                                convertedPlayerStats.Add(new PlayerMatchStats(p, idplayer, (UInt64)match.idmatch, match.idcompetition, (int)teams[iTeam].idteam,
                                                    UniqueStringManager.GetOrCreatePlayerName(plNameMap, p.name, trans),
                                                    UniqueStringManager.GetOrCreateSkillCombo(skComboMap, skillMap, p.skills_string, trans),
                                                    UniqueStringManager.GetOrCreateCasCombo(casComboMap, casMap, p.casualties_state_string, trans),
                                                    UniqueStringManager.GetOrCreateCasCombo(casComboMap, casMap, p.casualties_sustained_string, trans),
                                                    UniqueStringManager.GetOrCreatePlayerType(plTypeMap, p.type, trans)
                                                    ));
                                            }
                                            else // Mercenary. No id
                                            {

                                            }
                                        }

                                        MatchManager.AddIfMissing(convertedPlayerStats, trans);
                                    }
                                    iTeam++;


                                }
                            }
                            catch (Exception ex)
                            {
                                log.Error(ex.Message.ToString());
                                log.Error($"Error for match {m.uuid} in leagues {work.idleagues.ToString()}");
                            }

                        }

                        MatchManager.AddIfMissing(convertedTeamStats, trans);
                        var Time_InsStats = (int)(DateTime.Now - timeBegin).TotalMilliseconds;
                        Console.WriteLine("Insert stats: " + Time_InsStats + "ms");
                        timeBegin = DateTime.Now;

                        res += ". " + insertedMatches.Count + " new matches in [" + origin.ToString() + "] " + string.Join(",", leagues);
#region UPDATE CURRENT PLAYERS FOR EACH TEAM
                        foreach (var m in bbMatches)
                        {
                            var league = LeagueManager.FindLeagueByOrgID(origin, m.match.idleague, trans);
                            if (league == null)
                            {
                                log.Info($"Unknown league: {m.match.leaguename} : {m.match.idleague}");
                                continue;
                            }

                            var teams = new gSpy3_Common.Database.Format.Team[2];
                            teams[0] = TeamManager.FindTeamByOrgID(origin, m.match.teams[0].idteamlisting, trans);
                            teams[1] = TeamManager.FindTeamByOrgID(origin, m.match.teams[1].idteamlisting, trans);
                            var comp = LeagueManager.FindCompetitionByOrgID(origin, m.match.idcompetition, trans);
                            int iTeam = 0;
                            if (m.match.teams != null && m.match.teams.Count > 0)
                            {
                                foreach (var t in m.match.teams)
                                {
                                    var team = teams[iTeam];
                                    TeamManager.UpdatePlayersFromStats((int)league.idleague, (int)comp.idcompetition, (int)team.idteam, trans);
                                    iTeam++;
                                }
                            }
                        }
#endregion
                        var Time_UpPlayers = (int)(DateTime.Now - timeBegin).TotalMilliseconds;
                        Console.WriteLine("Update current players: " + Time_UpPlayers + "ms");
                        timeBegin = DateTime.Now;
#region Schedule
                        foreach (var idcomp in competitionMap.Keys)
                        {
                            ImportSchedule(origin,idcomp, trans);
                        }




#endregion

                        var TimeSched = (int)(DateTime.Now - timeBegin).TotalMilliseconds;
                        Console.WriteLine("Update schedule: " + TimeSched + "ms");
                        timeBegin = DateTime.Now;

#region Team activity
                        foreach (var idcomp in competitionMap.Keys)
                        {
                            try
                            {
                                var comp = LeagueManager.FindCompetitionByID(idcomp, trans);
                                var league = LeagueManager.FindLeagueByID(comp.idleague);
                                //var teams = bb2.GetTeams((int)ID, ((OriginID)comp.idorigin), league.league_name, comp.competition_name);
                                if (teamsInvolved.Count > 0)
                                {

                                    var curTeams = dbManager.StatsDB.Query("select t.idteam, t.team_origin_uid,t.active,s.active,idlogo,idcoach from teams as t inner join standings as s on s.idteam=t.idteam where s.idcompetition=@idcompetition", new Dictionary<string, string>()
                                    {
                                        {"@idcompetition", ""+comp.idcompetition }
                                    }, trans);
                                    List<gSpy3_Common.API.Cyanide.DataFormat.MatchResultTeam> toSetActive = new List<gSpy3_Common.API.Cyanide.DataFormat.MatchResultTeam>();
                                    List<gSpy3_Common.API.Cyanide.DataFormat.CoachResult> toSetActiveCoaches = new List<gSpy3_Common.API.Cyanide.DataFormat.CoachResult>();
                                    List<int> toSetInactive = new List<int>();
                                    foreach (var row in curTeams.Rows)
                                    {
                                        bool isActive = false;
                                        gSpy3_Common.API.Cyanide.DataFormat.MatchResultTeam activeteam = null;
                                        foreach (var team in teamsInvolved)
                                        {
                                            if (team != null && team.idteamlisting == row[1])
                                            {
                                                activeteam = team;
                                                isActive = true;
                                                break;
                                            }
                                        }
                                        if ((row[2] == "0" || row[3] == "0" || row[4] == "" || row[4] == "0") && isActive)
                                        {
                                            toSetActive.Add(activeteam);
                                        }
                                        if (row[5] == "0" || row[5] == "" || row[5] == null) // coachless teams
                                        {
                                            if (activeteam != null)
                                            {
                                                if (coachesInvolved.ContainsKey(activeteam.idteamlisting))
                                                {
                                                    var coach = coachesInvolved[activeteam.idteamlisting];
                                                    var coachRes = dbManager.StatsDB.Query("select idcoach from coaches where origin=" + comp.idorigin + " and coach_name=@coach_name", new Dictionary<string, string>()
                                                    {
                                                        {"@coach_name",coach.coachname }
                                                    }, trans);
                                                    string idcoach = null;
                                                    if (coachRes.Rows.Count > 0) { idcoach = coachRes.Rows[0][0]; }
                                                    if (idcoach != null)
                                                    {
                                                        dbManager.StatsDB.Query("update teams set idcoach=" + idcoach + " where  idorigin=" + comp.idorigin + " and team_origin_uid=" + activeteam.idteamlisting, null, trans);
                                                    }
                                                }

                                            }
                                        }
                                        if (row[2] == "1" && !isActive)
                                        {
                                            toSetInactive.Add(int.Parse(row[0]));
                                        }
                                    }
                                    var raceNameMap = dbManager.StatsDB.GetIndexMap("idrace", new string[] { "race_name" }, "races", "", null);
                                    foreach (var team in toSetActive)
                                    {
                                        //var idlogo = UniqueStringManager.GetOrCreateLogo(logoMap, team.teamlogo == null ? "" : team.teamlogo);
                                        gSpy3_Common.API.Cyanide.DataFormat.MatchResultTeam teamRes = null;
                                        if (teamRes == null)
                                        {
                                            teamMatchStatMap.TryGetValue("0" + team.idteamlisting, out teamRes);
                                        }
                                        if (teamRes == null)
                                        {
                                            teamMatchStatMap.TryGetValue("1" + team.idteamlisting, out teamRes);
                                        }

                                        var idlogo = UniqueStringManager.GetOrCreateLogo(logoMap, team.teamlogo == null ? "" : team.teamlogo);
                                        //dbManager.StatsDB.Query("update teams set active=1, idlogo=@idlogo  where idorigin=" + comp.idorigin + " and team_origin_uid='" + team.idteamlisting+"'", new Dictionary<string, string>()
                                        dbManager.StatsDB.Query("update teams set active=1, idlogo=@idlogo, idrace=" + team.idraces + ", team_name=@team_name where idorigin=" + comp.idorigin + " and team_origin_uid='" + team.idteamlisting + "'", new Dictionary<string, string>()

                                    {
                                       { "@idlogo", ""+idlogo},
                                       {"@team_name", team.teamname }
                                    }, trans);
                                        dbManager.StatsDB.Query("update standings set active=1 where team_origin_uid='" + team.idteamlisting + "' and idcompetition=" + comp.idcompetition, null, trans);

                                    }
                                    /*
                                    foreach (var id in toSetInactive)
                                    {
                                        dbManager.StatsDB.Query("update teams set active=0 where idteam=" + id, null, trans);
                                        dbManager.StatsDB.Query("update standings set active=0 where idorigin=" + comp.idorigin + " and idteam=" + id + " and idcompetition=" + comp.idcompetition, null, trans);
                                    }*/



                                }
                            }
                            catch (Exception ex)
                            {
                                log.Error(ex.Message.ToString());
                            }

                        }
#endregion
                        var TimeTeamActivity = (int)(DateTime.Now - timeBegin).TotalMilliseconds;
                        Console.WriteLine("Update team activity: " + TimeTeamActivity + "ms");
                        timeBegin = DateTime.Now;
                        trans.Commit();
                        trans = null;


                        foreach (var idl in work.idleagues)
                        {
                            dbManager.StatsDB.Query("update competitions set last_checked=@thismoment where idleague=" + idl, new Dictionary<string, string>()
                            {
                                {"@thismoment", DateTime.Now.ToDatabaseUniversalString() }
                            });
                        }

                        #region UPDATE STANDINGS AND ACTIVITY



                        foreach (var comp in competitionMap.Keys)
                        {
                            try
                            {
                                LeagueManager.UpdateStandings((int)ID, comp, trans);
                                var lastRes = dbManager.StatsDB.Query("select m.finished from teammatchstats as tm inner join matches as m on m.idmatch=tm.idmatch where m.idcompetition=" + comp + "  order by m.finished desc limit 1", null, null, 60 * 60 * 20);
                                var numGamesRes = dbManager.StatsDB.Query("select count(*) from matches where idcompetition=" + comp + " ", null, null, 60 * 60 * 20);
                                var numCoachRes = dbManager.StatsDB.Query("select count(*) from (select 1 from standings as s inner join teams as t on t.idteam = s.idteam where s.idcompetition=" + comp + " group by t.idcoach) as sub", null, null, 60 * 60 * 20);
                                var numTeamsRes = dbManager.StatsDB.Query("select count(*) from standings where idcompetition=" + comp + " ", null, null, 60 * 60 * 20);
                                var last_game = lastRes.Rows[0][0];
                                if (string.IsNullOrEmpty(last_game))
                                {
                                    last_game = null;
                                }
                                try
                                {
                                    dbManager.StatsDB.Query("update competitions set active=1"+(last_game!=null?",last_game=@last_game":"")+",num_coaches=@num_coaches, num_teams=@num_teams,num_games=@num_games where idcompetition=" + comp, new Dictionary<string, string>()
                        {
                            { "@last_game", last_game },
                            { "@num_coaches", numCoachRes.Rows[0][0] },
                            { "@num_teams", numTeamsRes.Rows[0][0] },
                            { "@num_games", numGamesRes.Rows[0][0] },
                        }, null, 60 * 60 * 20);


                                    var compdata = LeagueManager.FindCompetitionByID(comp, trans);
                                    CollectionManager.SetLatestGame(compdata.idleague, lastRes.Rows[0][0]);

                                    // Perhaps add some grouped stats?
                                    int hash = GroupedStatsManager.GetHash("" + (int)compdata.idcompetition);
                                    if (int.Parse(numGamesRes.Rows[0][0]) > 10000)
                                    {
                                        GroupedStatsManager.CreateGroupedTeamStats(hash, "" + (int)compdata.idcompetition);
                                        GroupedStatsManager.CreateGroupedCoachStats(hash, "" + (int)compdata.idcompetition);
                                        GroupedStatsManager.CreateGroupedRaceStats(hash, "" + (int)compdata.idcompetition);
                                    }
                                    if (int.Parse(numGamesRes.Rows[0][0]) > 1000)
                                    {
                                        GroupedStatsManager.CreateGroupedPlayerStats(hash, "" + (int)compdata.idcompetition);
                                    }

                                    DateTime earliest = DateTime.Now;
                                    foreach (var m in bbMatches)
                                    {
                                        if (m.match.idcompetition == compdata.competition_origin_uid)
                                        {
                                            DateTime mDate = DateTime.Parse(m.match.finished + "Z");
                                            if (mDate < earliest) earliest = mDate;
                                        }
                                    }

                                    // Also add work for the bot
                                    dbManager.BackendDB.Query("insert into work (idmodule,command, data, weight, added) values(@id, @command,@data, @weight, @added)",
                                          new Dictionary<string, string>()
                                          {
                                            {"@id", ((int)ModuleEnum.gSpy3_DiscordBot).ToString() },
                                            {"@data", "{\"idcomp\":"+compdata.idcompetition+",\"from\":\""+earliest.ToDatabaseUniversalString()+"\"}" },
                                            {"@weight", ""+10 },
                                            {"@added", DateTime.Now.ToDatabaseUniversalString() },
                                            {"@command", "TriggerMatchPlayed"},
                                          }, null, 60 * 60 * 20);
                                }
                                catch (Exception ex)
                                {
                                    log.Error(ex.ToString());
                                }
                            }
                            catch (Exception ex)
                            {
                                log.Error(ex.Message.ToString());
                            }
                        }
                        string oldest = (DateTime.Now - new TimeSpan(14, 0, 0, 0)).ToDatabaseUniversalString();
                        string oldestact = (DateTime.Now - new TimeSpan(14, 0, 0, 0)).ToDatabaseUniversalString();
                        foreach (var idleague in work.idleagues)
                        {
                            dbManager.StatsDB.Query("update competitions set active=0 where idleague=@idleague and last_game < @oldest", new Dictionary<string, string>()
                        {
                            { "@oldest", oldest },
                            { "@idleague", ""+idleague },
                        },null, 60 * 60 * 20);
                            dbManager.BackendDB.Query("update leagueupdates set collecting=0 where idleague=@idleague and activation_date < @oldestact and latest_game < @oldest", new Dictionary<string, string>()
                        {
                            { "@oldest", oldest },
                            { "@oldestact", oldestact },
                            { "@idleague", ""+idleague },
                        },null, 60 * 60 * 20);
                        }


#endregion
                        var TimeStandings = (int)(DateTime.Now - timeBegin).TotalMilliseconds;
                        Console.WriteLine("Update standings and activity: " + TimeStandings + "ms");
                        timeBegin = DateTime.Now;


                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.ToString());
                        if (trans != null)
                        {
                            trans.Rollback();
                        }
                        trans = null;
                    }

                    // If we got the exact amount of items as the limit, then we need to adjust the dates and continue as a new work
                    if (newMatchesIDList.Count >= limit && !string.IsNullOrEmpty(firstDate) )
                    {
                        AddWork(Work.RequestUpdateLeagues.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(
                            new UpdateCompetitionWork()
                            {
                                from = work.from,
                                to = (DateTime.Parse(firstDate + "Z") + new TimeSpan(1, 0, 0)).ToDatabaseUniversalString(),
                                idleagues = work.idleagues,
                                platform = work.platform
                            })

                             , 25);
                        res += " (Reached limit)";
                    }
                }
                else res = "No named leagues";


                return res;
            }
            catch(Exception ex)
            {
                log.Error("Error in updating leagues: " + ex.ToString());
                if(ex.ToString().Contains("There is already an open DataReader"))
                {
                    var info = new System.Diagnostics.ProcessStartInfo(Environment.GetCommandLineArgs()[0]);
                    System.Diagnostics.Process.Start(info);
                    Environment.Exit(0);
                   
                }
                if (trans != null)
                {
                    trans.Rollback();
                }
                trans = null;

                throw ex;
            }
        }
        void ImportSchedule(OriginID origin,int idcomp, Transaction trans)
        {
            try
            {
                var comp = LeagueManager.FindCompetitionByID(idcomp, trans);
                if (comp.format != null && comp.format != (int)FormatID.MatchMaking)
                {
                    var league = LeagueManager.FindLeagueByID(comp.idleague);
                    var sched = cyanide.GetSchedule((int)ID, origin, league.league_name, comp.competition_name);

                    foreach (var s in sched.upcoming_matches)
                    {
                        gSpy3_Common.Database.Format.Schedule newSched = null;
                        if (s.opponents != null && s.opponents.Count >= 2 && s.opponents[0].team != null && s.opponents[1].team != null)
                        {
                            var team1 = TeamManager.FindTeamByOrgID(origin, s.opponents[0].team.id, trans);
                            var team2 = TeamManager.FindTeamByOrgID(origin, s.opponents[1].team.id, trans);
                            if (team1 != null && team2 != null)
                            {

                                newSched = (new gSpy3_Common.Database.Format.Schedule(origin, s, (int)comp.idcompetition,
                                                                        (int)team1.idteam,
                                                                        (int)team2.idteam));
                            }
                        }
                        else
                        {
                            //    newSched = (new Schedule(origin, s, (int)comp.idcompetition,
                            //                                            (int)0,
                            //                                            (int)0));
                        }
                        if (newSched != null)
                        {
                            if (ScheduleManager.FindSchedule(origin, newSched.schedule_origin_uid) == null)
                            {
                                dbManager.StatsDB.Insert("schedules", newSched, trans);
                            }
                            else
                            {
                                dbManager.StatsDB.Update("schedules", newSched, "where idorigin=" + (int)origin + " and schedule_origin_uid=" + newSched.schedule_origin_uid, new string[] { "idschedule" }, trans);
                            }
                        }
                    }

                    // The we need to fill in played matches if not already done
                    var currScheds = dbManager.StatsDB.Query("select idschedule,match_origin_uid,idmatch from schedules where idorigin=" + (int)origin + " and idcompetition=" + idcomp, null, trans, 60 * 60 * 20);
                    foreach (var currSchedItem in currScheds.Rows)
                    {
                        string sIdSched = currSchedItem[0];
                        string match_origin_uid = currSchedItem[1];
                        string idmatch = currSchedItem[2];
                        if (idmatch == "0" || idmatch == "")
                        {
                            var matchPlayedRes = dbManager.StatsDB.Query("select idmatch from matches where idorigin=" + (int)origin + " and match_origin_uid=" + match_origin_uid, null, trans, 60 * 60 * 20);
                            if (matchPlayedRes.Rows.Count > 0)
                            {
                                dbManager.StatsDB.Query("update schedules set idmatch=" + matchPlayedRes.Rows[0][0] + " where idorigin=" + (int)origin + " and match_origin_uid=" + match_origin_uid, null, trans, 60 * 60 * 20);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                if (ex.InnerException != null)
                    log.Error(ex.InnerException.ToString());
            }
        }
    }

    
}
