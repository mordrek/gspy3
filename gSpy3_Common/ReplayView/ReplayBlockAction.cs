//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;


namespace BB2StatsLib.Views.Replay
{
	[Serializable]
	public class ReplayBlockAction : ReplayActionBase
	{
        public string TID { get; set; }// Target ID
		public List<int> Roll { get; set; } // BlockDice
        public int Dice { get; set; }// BlockDice
        public bool RR { get; set; }
        public bool Red { get; set; }

		public ReplayBlockAction ()
		{
		}
        public ReplayBlockAction(string rolled, string targetPlayer)
        {
            TID = targetPlayer;
            Roll = ParseDices(rolled);
            RR = false;
            Dice = -1;
        }
        
		public static new List<int> ParseDices(string diceString)
		{
            
			diceString = diceString.Substring (1, diceString.Length - 2);
			List<int> dices = new List<int> ();

            // For some reason only first half of block dices should be used
            string[] splits = diceString.Split(',');
            int num = splits.Length / 2;
			if (num == 0) num = 1;
            for(int i=0; i < num;i++)
			{
				dices.Add(int.Parse(splits[i]));
			}
            

            return dices;
		}
	}
	[Serializable]
	public enum BlockDice
	{
		BothDown=1, Push=2, Pow=4, Stumble=3, Skull=0, Unknown6 = 6, Unknown5 = 5
	}
}

