//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
using System;
namespace BB2StatsLib.Views.Replay
{
	[Serializable]
	public class ReplayItem
	{
        public int AID { get; set; } // Action ID
        public int I { get; set; } // Index
        public string PID { get; set; } // Player id

		public ReplayItem ()
		{
		}
		public ReplayItem(ReplayItemType action, int index, string pid)
		{
            this.AID = (int)action;
			this.I = index;
            this.PID = pid;
		}
	}
    [Serializable]
	public enum ReplayItemType
    {
        Unknown = -1,
        NoRoll = 0,
        GFI = 1,
        Dodge = 2,
        Armor = 3,
        Injury = 4,
        Block = 5,
        StandUp = 6,
        PickUp = 7,
        Casualty = 8,
        Catch = 9,
        Scatter = 10,
        ThrowIn = 11,
        Pass = 12,
        PushBack = 13,
        FollowUp = 14,
        DeceiveReferee = 15,
        Interception = 16,
        WakeUp = 17,
        PitchInv = 18,
        TouchBack = 19,
        BoneHead = 20,
        ReallyStupid = 21,
        WildAnimal = 22,
        Loner = 23,
        Land = 24,
        Regeneration = 25,
        ScatterPlayer = 26,
        Hungry = 27,
        EatTeamMate = 28,
        Dauntless = 29,
        SafeThrow = 30,
        JumpUp = 31,
        Shadowing = 32,
        DumpOff = 33,
        Stab = 34,
        FrenzyStab = 35,
        Leap = 36,
        FoulAppearance = 37,
        Tentacles = 38,
        Chainsaw = 39,
        TakeRoot = 40,
        BallAndChain = 41,
        HailMaryPass = 42,
        PilingOn = 43,
        DivingTackle = 44,
        Pro = 45,
        HypnoticGaze = 46,
        KickOffReturn = 47,
        PassBlock = 48,
        Animosity = 49,
        BloodLust = 50,
        Feed = 51,
        Bribe = 52,
        HalflingChef = 53,
        FireBallHit = 54,
        LightningBolt = 55,
        ThrowTeamMate = 56,
        AdditionalBlock = 57,
        WeatherScatter = 58,
        PilingOnArmor = 59,
        PilingOnInjury = 60,
        BlockWrestle = 61,
        BlockDodge = 62,
        BlockStandFirm = 63,
        BlockJuggernaut = 64,
        PushStandFirm = 65,
        Necromancer = 66,
        NurglesRot = 67,
        PushSideStep = 68,
        FansNumber = 69,
        InitialWeather = 70,
        SwelteringHeat = 71,
        BombHit = 72,
        ChainsawArmor = 73,
        HailMaryBomb = 74,
        Nb = 75,





        Foul = 2000,
        HandOff = 2001,

        EndTurn = 1000,
        Touchdown = 1001,
        KickoffEvent = 1002,
        Blitz = 1003,
        KickBall = 1004,
        BallAction = 1005,
    }
    
}

