//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using BB2StatsLib.BB2Replay;
using BB2StatsLib.Views;
using gSpy3_BB2MatchParser.BB2Replay;
using gSpy3_Common.BB2Replay;

namespace BB2StatsLib
{
  


	[Serializable]
	public class PlayerStats
	{
		public string Name {get;set;}
		public string PlayerId{ get; set; }


		public int Number { get; set; }
		public int Level {get;set;}
		public int XP {get;set;}
		public int GamesPlayed { get; set; }
		
		public string PlayerType {get;set;}
		public string PlayerTypeName { get; set; }

		public List<string> Skills {get;set;}

		public int Passes{ get; set; }
		public int MetersPassed {get;set;}
		public int Blocks{get;set;}
		public int BlocksAgainst{ get; set; }
		public int Breaks{get;set;}
		public int BreaksAgainst{get;set;}
		public int Knockouts{get;set;}
		public int KnockoutsAgainst{get;set;}
		public int Stuns{get;set;}
		public int StunsAgainst{get;set;}
		public int Casualties{get;set;}
		public int CasualtiesAgainst{get;set;}
		public int Kills{get;set;}
		public int KillsAgainst{get;set;}
		public int MetersRun{ get; set; }

		public int Catches {get;set;}
		public int Interceptions {get;set;}
		public int Touchdowns {get;set;}
		public int MVP {get;set;}

        public int Dodges { get; set; }
		public int Expulsions { get; set; }
		public int Fouls { get; set; }
		public int Completions { get; set; }
        public int GFIs { get; set; }
        public int Pickups { get; set; }

        public int Sacks { get; set; }
        public int Turnovers { get; set; }

		public int Boneheads { get; set; }


		public bool IsMercenary { get; set; }
        public bool IsActive { get; set; }

		public int Ma { get; set; }
		public int St { get; set; }
		public int Av { get; set; }
		public int Ag { get; set; }


		public List<KilledVictimView> KillList { get; set; }
        public KilledVictimView KilledBy { get; set; }

		public PlayerStats ()
		{
            IsActive = true;

        }
        /// <summary>
        /// Called during compilation when adding players from pre-parsed games
        /// </summary>
        /// <param name="game"></param>
        /// <param name="stats"></param>
        public void AddFromPlayer(PlayerStats stats)
		{
            Name = stats.Name;
            PlayerId = stats.PlayerId;
                
            Number = stats.Number;
            Level = stats.Level;
            Skills = stats.Skills;
			PlayerType = stats.PlayerType;
			PlayerTypeName = ((PlayerTypeEnum)int.Parse(stats.PlayerType)).ToString();
            IsActive = stats.IsActive;

            if (KillList == null && stats.KillList != null)
            {
                KillList = new List<KilledVictimView>();
                foreach (var item in stats.KillList)
                {
                    if(!KillList.Contains(item))
                         KillList.Add(item);
                }
            }

            if (KilledBy == null && stats.KilledBy != null)
            KilledBy = stats.KilledBy;

            

			XP += stats.XP;

			MVP += stats.MVP;
			GamesPlayed += stats.GamesPlayed;
			Passes += stats.Passes;
			MetersPassed += stats.MetersPassed;
			MetersRun += stats.MetersRun;
			Touchdowns += stats.Touchdowns;
			Blocks += stats.Blocks;
			BlocksAgainst += stats.BlocksAgainst;
			Breaks += stats.Breaks;
			BreaksAgainst += stats.BreaksAgainst;
			Stuns += stats.Stuns;
			StunsAgainst += stats.StunsAgainst;
			Knockouts += stats.Knockouts;
			KnockoutsAgainst += stats.KnockoutsAgainst;
			Casualties += stats.Casualties;
			CasualtiesAgainst += stats.CasualtiesAgainst;
			Kills += stats.Kills;
			KillsAgainst += stats.KillsAgainst;
			Catches += stats.Catches;
			Interceptions += stats.Interceptions;
            Dodges += stats.Dodges;
			Expulsions += stats.Expulsions;
            GFIs += stats.GFIs;
            Pickups += stats.Pickups;
            Sacks += stats.Sacks;
            Turnovers += stats.Turnovers;
			Ma = stats.Ma;
			St = stats.St;
			Ma = stats.Ma;
			Ag = stats.Ag;
		}

        /// <summary>
        /// Parsed client side when traversing the original game file
        /// </summary>
        /// <param name="res"></param>
        /// <param name="teamId"></param>
		public void ParsePlayerResult(PlayerResult res, int teamId)
		{
			if (res.PlayerData != null) 
			{
				PlayerId = res.Statistics.IdPlayerListing;
                
                
				Name = res.PlayerData.Name;

                if (String.IsNullOrEmpty(PlayerId) || PlayerId == "0")
                {
                    string lonerId = PlayerId;
                    if (Name == null)
                        Name = "unknown";
                    PlayerId = lonerId+Math.Abs(Name.GetHashCode()) + teamId;
                    IsMercenary = true;
                }
                else
                    IsMercenary = false;

				Ma = res.PlayerData.Ma;
                St = res.PlayerData.St;
				Av = res.PlayerData.Av;
				Ag = res.PlayerData.Ag;
				Number = res.PlayerData.Number;
				Level = res.PlayerData.Level;
				GamesPlayed = res.Statistics.MatchPlayed;
				PlayerType = ""+(res.PlayerData.IdPlayerTypes);
				PlayerTypeName = ((PlayerTypeEnum)res.PlayerData.IdPlayerTypes).ToString();
				if(res.PlayerData.ListSkills!=null)
				{
					Skills = new List<string> ();
					string commalist = res.PlayerData.ListSkills.Substring(1,res.PlayerData.ListSkills.Length-2);
					string[] items = commalist.Split(',');
					foreach(string skillitem in items)
					{
						Skills.Add(((SkillEnum)int.Parse(skillitem)).ToString());
					}
				}

				Casualties = res.Statistics.InflictedCasualties;
				CasualtiesAgainst = res.Statistics.SustainedCasualties;
				Kills = res.Statistics.InflictedDead;
				KillsAgainst = res.Statistics.SustainedDead;
				Knockouts = res.Statistics.InflictedKO;
				KnockoutsAgainst = res.Statistics.SustainedKO;
				Breaks = res.Statistics.InflictedInjuries;
				BreaksAgainst = res.Statistics.SustainedInjuries;
				Blocks = res.Statistics.InflictedTackles;
				BlocksAgainst = res.Statistics.SustainedTackles;
				Passes = res.Statistics.InflictedPasses;
				MetersPassed = res.Statistics.InflictedMetersPassing;
				MetersRun = res.Statistics.InflictedMetersRunning;
				Catches = res.Statistics.InflictedCatches;
				Interceptions = res.Statistics.InflictedInterceptions;
				Touchdowns = res.Statistics.InflictedTouchdowns;
				MVP = res.Statistics.MVP;
				XP = res.Xp;

				Stuns = Breaks-Knockouts-Casualties-Kills;
				StunsAgainst = BreaksAgainst-KnockoutsAgainst-CasualtiesAgainst-KillsAgainst;


			}
			XP = res.Xp;
		}
        /// <summary>
        /// Parsed client side when traversing the original game file
        /// </summary>
        /// <param name="state"></param>
		public void ParsePlayerState(PlayerState state)
		{

		}
	}


    [Serializable]
    public class PlayerStatsOverview : PlayerStats
    {
        public string TeamId { get; set; }
        public string TeamName { get; set; }
        public PlayerStatsOverview(PlayerStats org, string aTeamId, string aTeamName)
        {
            Type psType = typeof(PlayerStats);

            foreach (var pi in psType.GetProperties())
            {
                object val = pi.GetValue(org, null);
                pi.SetValue(this, val, null);
            }
            TeamId = aTeamId;
            TeamName = aTeamName;
            Skills = null;
            KillList = null;
            KilledBy = null;

        }
    }
}

