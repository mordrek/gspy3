﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BB2StatsLib.Views.Replay;
using gSpy3_BB2MatchParser.BB2Replay;

namespace BB2StatsLib.Views.CalculatedStats
{
    public enum RiskCalculatorType { All, Passing, Movement, Bashing }
    public class RiskCalculator : IStatCalculator
    {
        public double Risk = 0;
        bool lastPassIsScattered = true;
        bool lastCatchWasTurnover = false;
        List<RiskPath> _riskPaths = new List<RiskPath>();
        RiskCalculatorType _riskType = RiskCalculatorType.All;

        ReplayView _replayView;

        static double[] _blockRiskWithoutBlock = new double[] { 15200.0/216.0, 2000.0/36.0, 1200.0/36.0, 400.0/36.0, 800.0/216.0 };
        static double[] _blockRiskWithBlock = new double[] { 9100.0/216.0, 1100.0/36.0, 100.0/6.0, 100.0/36.0, 100.0/216.0 };
        static double[] _roll1DRisk = new double[] { 100.0 / 6.0, 100.0 / 6.0, 200.0 / 6.0, 300.0 / 6.0, 400.0 / 6.0, 500.0 / 6.0 };
        bool _isRerollUsed = false;
        bool _hasTeamReroll = false;
       

        int turnovers = 0;
        int sack = 0;

        public RiskCalculator() { }
        public RiskCalculator(RiskCalculatorType aType)
        {
            _riskType = aType;
            _riskPaths.Add(new RiskPath());
        }

        public static double GetBlockRisk(int dices, List<string> blockerSkills, List<string> targetSkills)
        {
            if (dices > 0) // White dices
                dices += 1;
            if (dices < 0) // Red dices
                dices = 3 + dices;
            double chance = 0;
            if (blockerSkills != null && (blockerSkills.Contains(SkillConsts.Block) ||
                                        blockerSkills.Contains(SkillConsts.Wrestle) ||
                                        blockerSkills.Contains(SkillConsts.Juggernaut)))
                chance = (_blockRiskWithBlock[dices]);
            else
                chance = (_blockRiskWithoutBlock[dices]);
            return chance;
        }

        public static double GetRollRisk(int required)
        {
            int index = required - 1;
            if (index < 0) index = 0;
            if (index >= 6) index = 5;
            return _roll1DRisk[index];
        }

        public void TurnStarted(ReplayView aReplayView)
        {
            _replayView = aReplayView;
            lastPassIsScattered = true;
            // New turn, so store risk an clear risk paths
            double successChance = 0;
            foreach (RiskPath path in _riskPaths)
            {
                successChance += path.SuccessChance;
            }
            Risk += (1.0 - successChance);



            _riskPaths.Clear();
            _riskPaths.Add(new RiskPath());
            _isRerollUsed = false;
        }

        /// <summary>
        /// Adds calculations based on a single replay item (a move, a block etc)
        /// Each turn is a list of ReplayItems.
        /// We look for replay items that can cause turnovers and add them together on a list
        /// Example: Human thrower makes a (4+) pass to a catcher (3+)
        /// This becomes a chain of (3/6)*(4/6) => OK (12/36) to succeed ; or 24/36 to fail
        /// To take skill reroll into account, this simple throw can either become rerolled at pass or catch
        /// resulting in three different possible chains of success
        /// 1) (3/6) * (4/6) => OK (12/36)
        /// 2) (3/6)RR * (3/6) * (4/6) => OK (12*3/36*6 == 6/36)
        /// 3) (3/6) * (2/6) RR * (4/6) => OK (12*2/36*6 == 4/36)
        /// Total chance of success is 12/36 + 6/36 + 4/36 => OK (22/36)
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="index"
        public void AddFromReplayItem(ReplayItem item, int index, TeamStats ownTeam, TeamStats otherTeam, int turn)
        {
            try
            {
                if (item.AID == (int)ReplayItemType.EndTurn)
                {
                    ReplayEndTurnAction endTurnAction = (ReplayEndTurnAction)_replayView.EndTurns[item.I];
                    _hasTeamReroll = endTurnAction.NextTeamRerolls > 0;
                }


                PlayerStats player = ownTeam.FindUniquePlayer(item.PID);
                if (player == null)
                    return;

                bool skillRerollFound = false;
                double riskOfFailure = 0;
                bool wasSuccess = false;
                bool wasFailure = false;
                switch ((ReplayItemType)item.AID)
                {
                    case ReplayItemType.Block:
                        if (_riskType == RiskCalculatorType.Bashing ||
                            _riskType == RiskCalculatorType.All)
                        {
                            ReplayBlockAction blockAction = _replayView.Blocks[item.I];
                            PlayerStats targetPlayer = otherTeam.FindUniquePlayer(blockAction.TID);
                            riskOfFailure = GetBlockRisk(blockAction.Roll.Count, player.Skills, targetPlayer == null ? null : targetPlayer.Skills);

                            // Did the block succeed ?
                            for (int i = index+1; i < _replayView.Items.Count; i++)
                            {
                                if (_replayView.Items[i].AID == (int)ReplayItemType.Armor || _replayView.Items[i].AID == (int)ReplayItemType.ChainsawArmor)
                                {
                                    wasSuccess = wasSuccess || (_replayView.Items[i].PID != player.PlayerId);
                                    wasFailure = wasFailure || (_replayView.Items[i].PID == player.PlayerId);
                                  
                                }
                                // break on some stuff
                                if (_replayView.Items[i].AID == (int)ReplayItemType.Block ||
                                    _replayView.Items[i].AID == (int)ReplayItemType.NoRoll|| // move
                                    _replayView.Items[i].AID == (int)ReplayItemType.Leap||
                                    _replayView.Items[i].AID == (int)ReplayItemType.GFI||
                                    _replayView.Items[i].AID == (int)ReplayItemType.Dodge)
                                {
                                    break;
                                }
                            }
                            if(_riskType == RiskCalculatorType.All)
                            { 
                                PlayerStats ps = ownTeam.FindUniquePlayer(item.PID);
                                bool targetHasSureHands = targetPlayer != null && targetPlayer.Skills != null && targetPlayer.Skills.Contains(SkillConsts.SureHands);
                                bool blockerHasStripBall = ps != null && ps.Skills != null && ps.Skills.Contains(SkillConsts.StripBall);
                                if (wasSuccess || (blockAction.Dice == (int)BlockDice.Push || blockAction.Dice == (int)BlockDice.Stumble) && blockerHasStripBall && !targetHasSureHands)
                                {
                                    // Did he carry the ball?
                                    try
                                    {
                                        string lastPlayerHoldingBall = "-1";
                                        for (int i = 0; i < index; i++)
                                        {
                                           if (_replayView.Items[i].AID == (int)ReplayItemType.BallAction)
                                            {
                                                ReplayBallAction rba = _replayView.Balls[_replayView.Items[i].I];
                                                if (rba.IsBallHeld)
                                                    lastPlayerHoldingBall = rba.PID;
                                                else
                                                    lastPlayerHoldingBall = "-1";
                                            }
                                           else if (_replayView.Items[i].AID == (int)ReplayItemType.Armor || _replayView.Items[i].AID == (int)ReplayItemType.ChainsawArmor)
                                           {
                                               if (_replayView.Items[i].PID == lastPlayerHoldingBall)
                                               {
                                                   lastPlayerHoldingBall = "-1";
                                               }
                                           }
                                           else if (_replayView.Items[i].AID == (int)ReplayItemType.Catch || _replayView.Items[i].AID == (int)ReplayItemType.PickUp)
                                           {
                                               ReplayRollAction cra = _replayView.Rolls[_replayView.Items[i].I]; 
                                               if (LuckCalculator.IsRollSuccessful(cra.Roll[0], cra.Req))
                                               {
                                                   lastPlayerHoldingBall = _replayView.Items[i].PID;
                                               }
                                           }
                                           else if (_replayView.Items[i].AID == (int)ReplayItemType.EndTurn)
                                           {
                                               ReplayEndTurnAction eta = _replayView.EndTurns[_replayView.Items[i].I];
                                               if (eta.IsBallHeld)
                                               {
                                                   if (eta.Players != null)
                                                   {
                                                       for (int p = 0; p < eta.Players.Count; p++)
                                                       {
                                                           ReplayPlayer rp = eta.Players[p];
                                                           if (rp.Pos[0] == eta.BallPosition.X && rp.Pos[1] == eta.BallPosition.Y)
                                                           {
                                                               lastPlayerHoldingBall = rp.PID;
                                                               break;
                                                           }
                                                       }
                                                   }
                                                   else
                                                       lastPlayerHoldingBall = "-1";
                                                   
                                               }
                                               else
                                               {
                                                   lastPlayerHoldingBall = "-1";
                                               }
                                           }
                                        }
                                        if (lastPlayerHoldingBall != "-1" && lastPlayerHoldingBall == blockAction.TID)
                                        {
                                            sack++;
                                            if (ps != null)
                                                ps.Sacks++;
                                        }

                                    }
                                    catch (Exception e) { Console.WriteLine(e); }
                                }
                            }
                        }
                        break;
                    case ReplayItemType.Dodge:
                        if (_riskType == RiskCalculatorType.Movement ||
                            _riskType == RiskCalculatorType.All)
                        {
                            riskOfFailure = GetRollRisk(_replayView.Rolls[item.I].Req);
                            skillRerollFound = player.Skills != null && player.Skills.Contains(SkillConsts.Dodge);
                            wasSuccess = LuckCalculator.IsRollSuccessful(_replayView.Rolls[item.I].Roll.Sum(), _replayView.Rolls[item.I].Req);
                            wasFailure = !wasSuccess;
                        }
                        break;
                    case ReplayItemType.Leap:
                        if (_riskType == RiskCalculatorType.Movement ||
                            _riskType == RiskCalculatorType.All)
                        {
                            if (item.I >= 0 && item.I < _replayView.Rolls.Count)
                            {
                                riskOfFailure = GetRollRisk(_replayView.Rolls[item.I].Req);
                                wasSuccess = LuckCalculator.IsRollSuccessful(_replayView.Rolls[item.I].Roll.Sum(), _replayView.Rolls[item.I].Req);
                                wasFailure = !wasSuccess;
                            }
                            else Console.WriteLine("LEAP ROLL NOT FOUND");
                        }
                        break;
                    case ReplayItemType.GFI:
                        if (_riskType == RiskCalculatorType.Movement ||
                            _riskType == RiskCalculatorType.All)
                        {
                            riskOfFailure = GetRollRisk(_replayView.Rolls[item.I].Req);
                            skillRerollFound = player.Skills != null && player.Skills.Contains(SkillConsts.SureFeet);
                            wasSuccess = LuckCalculator.IsRollSuccessful(_replayView.Rolls[item.I].Roll.Sum(), _replayView.Rolls[item.I].Req);
                            wasFailure = !wasSuccess;
                        }
                        break;
                    case ReplayItemType.Pass:
                        if (_riskType == RiskCalculatorType.Passing ||
                            _riskType == RiskCalculatorType.All)
                        {
                            riskOfFailure = GetRollRisk(_replayView.Rolls[item.I].Req);
                            skillRerollFound = player.Skills != null && player.Skills.Contains(SkillConsts.Pass);
                            wasSuccess = LuckCalculator.IsRollSuccessful(_replayView.Rolls[item.I].Roll.Sum(), _replayView.Rolls[item.I].Req);
                            wasFailure = !wasSuccess;
                            lastPassIsScattered = wasFailure;
                        }
                        break;
                    case ReplayItemType.PickUp:
                        if (_riskType == RiskCalculatorType.Passing ||
                            _riskType == RiskCalculatorType.All)
                        {
                            riskOfFailure = GetRollRisk(_replayView.Rolls[item.I].Req);
                            skillRerollFound = player.Skills != null && player.Skills.Contains(SkillConsts.SureHands);
                            wasSuccess = LuckCalculator.IsRollSuccessful(_replayView.Rolls[item.I].Roll.Sum(), _replayView.Rolls[item.I].Req);
                            wasFailure = !wasSuccess;
                        }
                        break;
                    case ReplayItemType.Catch:
                        if (_riskType == RiskCalculatorType.Passing ||
                            _riskType == RiskCalculatorType.All)
                        {
                            riskOfFailure = GetRollRisk(_replayView.Rolls[item.I].Req);
                            skillRerollFound = player.Skills != null && player.Skills != null && player.Skills.Contains(SkillConsts.Catch);
                            wasSuccess = LuckCalculator.IsRollSuccessful(_replayView.Rolls[item.I].Roll.Sum(), _replayView.Rolls[item.I].Req);
                            wasFailure = !wasSuccess;
                            lastCatchWasTurnover = (lastPassIsScattered == false);
                            lastPassIsScattered = true;
                        }
                        break;
                    default:
                        return;
                }

                // If the roll failed we should see if it was rerolled or not
                bool wasRerolled = false;
                if (wasFailure)
                {
                    for (int i = index + 1; i < _replayView.Items.Count; i++)
                    {
                        if (_replayView.Items[i].AID == item.AID && _replayView.Items[i].PID == item.PID)
                            wasRerolled = true;

                        // First item for another player
                        if (_replayView.Items[i].PID != item.PID)
                        {
                            break;
                        }
                    }
                }

                if (wasRerolled) // If it was rerolled, then we skip this roll and calculate on the next reroll instead
                    return;

                // Ok, now get got all info we need
                bool rerollPerformed = false;
                List<RiskPath> newBranches = new List<RiskPath>();
                foreach (RiskPath path in _riskPaths)
                {

                    // And add failure roll as a branch (but only if reroll is possible)
                    if (!_isRerollUsed && (skillRerollFound || _hasTeamReroll))
                    {
                        if (skillRerollFound == false)
                            _hasTeamReroll = false;

                        rerollPerformed = true;
                        RiskPath newBranch = new RiskPath();
                        newBranch.AddPath(path);
                        newBranch.AddFailureRoll(riskOfFailure / 100.0, true);
                        newBranch.AddSuccessRoll(1.0 - riskOfFailure / 100.0, _isRerollUsed);
                        newBranches.Add(newBranch);
                        path.AddSuccessRoll(1.0 - riskOfFailure / 100.0, _isRerollUsed);
                    }
                    else
                    {
                        // Add successfull roll to all paths
                        path.AddSuccessRoll(1.0 - riskOfFailure / 100.0, _isRerollUsed);
                    }

                }
                _riskPaths.AddRange(newBranches);

                if (rerollPerformed)
                    _isRerollUsed = true;

                if (_riskType == RiskCalculatorType.All)
                {
                    if (wasFailure && !(((ReplayItemType)item.AID) == ReplayItemType.Catch && lastCatchWasTurnover==false))
                    { 
                        PlayerStats ps = ownTeam.FindUniquePlayer(item.PID);
                        if (ps != null)
                            ps.Turnovers++;
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error grabbing risk for item " + e.ToString());
            }

        }

        public void StoreResults(GameStats game, TeamStats stats, TeamStats otherTeam)
        {
            double risk = 0;
            risk = (100.0*Risk) / 16;

            switch (_riskType)
            {
                case RiskCalculatorType.All:
                    stats.Risk = (int)risk;
                    stats.Sacks = sack;
                    stats.Turnovers = turnovers;
                    break;
                case RiskCalculatorType.Bashing:
                    stats.BlockRisk = (int)risk;
                    break;
                case RiskCalculatorType.Movement:
                    stats.MoveRisk = (int)risk;
                    break;
                case RiskCalculatorType.Passing:
                    stats.PassRisk = (int)risk;
                    break;
            }

        }
    }
}
