﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BB2StatsLib.Views.Replay;

namespace BB2StatsLib.Views.CalculatedStats
{
    public class KillCalculator : IStatCalculator
    {
        ReplayView _replayView;
        Dictionary<string, Tuple<string, ReplayInjuryAction,int,int>> _casRollAgainst = new Dictionary<string, Tuple<string, ReplayInjuryAction, int, int>>();

        public KillCalculator()
        {
           
        }

        public void TurnStarted(ReplayView aReplayView)
        {
            _replayView = aReplayView;
        }

        /// <summary>
        /// Adds calculations based on a single replay item (a move, a block etc)
        /// </summary>
        /// <param name="item"></param>
        /// <param name="index"
        public void AddFromReplayItem(ReplayItem item, int index, TeamStats ownTeam, TeamStats otherTeam, int turn)
        {
            if (item.AID != (int)ReplayItemType.Casualty)
                return;

            PlayerStats player = ownTeam.FindUniquePlayer(item.PID);
            if (player != null)
            {
                ReplayInjuryAction inj = _replayView.Casualties[item.I];
                {
                    // Find a "killer", if it exists
                    ReplayItem killItem = _replayView.Items[inj.Cause];
                    if (killItem.AID == (int)ReplayItemType.Block || killItem.AID == (int)ReplayItemType.Foul) // blocked or fouled
                    {
                        PlayerStats killer = otherTeam.FindUniquePlayer(killItem.PID);
                        if (killer != null)
                        {
                            _casRollAgainst[player.PlayerId] = new Tuple<string, ReplayInjuryAction,int,int>(
                                killer.PlayerId, inj,turn, index);
                        }
                        else if(killItem.PID == player.PlayerId && killItem.AID == (int)ReplayItemType.Block) // Perhaps killer is not the last blocker, but the last blocker target (if we fall due to skull or both down)
                        {
                            ReplayBlockAction rba = _replayView.Blocks[killItem.I];
                            killer = otherTeam.FindUniquePlayer(rba.TID);
                            if (killer != null)
                            {
                                _casRollAgainst[player.PlayerId] = new Tuple<string, ReplayInjuryAction,int,int>(
                                killer.PlayerId, inj, turn, index);
                            }
                        }
                    }
                    else
                    {
                        _casRollAgainst[player.PlayerId] = new Tuple<string, ReplayInjuryAction,int,int>(
                                "-1", inj, turn, index);

                    }
                }
            }
            else
            {
                PlayerStats ps = otherTeam.FindUniquePlayer(item.PID);
                if (ps != null)
                {
                    ReplayInjuryAction inj = _replayView.Casualties[item.I];
                
                    // Find a "killer", if it exists
                    ReplayItem killItem = _replayView.Items[inj.Cause];
                    PlayerStats killer = null;
                    if (killItem.AID == (int)ReplayItemType.Block) // blocked to death
                    {
                        killer = ownTeam.FindUniquePlayer(killItem.PID);
                    }

                    {
                        if (killItem.AID == (int)ReplayItemType.Block) // blocked to death
                        {

                            if (killer != null)
                            {
                                _casRollAgainst[ps.PlayerId] = new Tuple<string, ReplayInjuryAction,int,int>(
                                    killer.PlayerId, inj, turn, index);
                            } // No need to add "accidental" injuries now, it will be taken care when going through the other team
                            else if(killItem.PID == ps.PlayerId) // Perhaps killer is not the last blocker, but the last blocker target (if we fall due to skull or both down)
                            {
                                ReplayBlockAction rba = _replayView.Blocks[killItem.I];
                                killer = ownTeam.FindUniquePlayer(rba.TID);
                                if (killer != null)
                                {
                                    _casRollAgainst[ps.PlayerId] = new Tuple<string, ReplayInjuryAction,int,int>(
                                    killer.PlayerId, inj, turn, index);
                                }
                            }
                        }

                    }
                }
            }
        }



        public void StoreResults(GameStats game,TeamStats stats, TeamStats otherTeam)
        {
            foreach (string victimId in _casRollAgainst.Keys)
            {
                ReplayInjuryAction casAction = _casRollAgainst[victimId].Item2;
                if (casAction.Roll[0] >= 61) // DEATH
                {
                    string killerId = _casRollAgainst[victimId].Item1;
                    int turn = _casRollAgainst[victimId].Item3;
                    int index = _casRollAgainst[victimId].Item4;
                    ReplayItem killItem = _replayView.Items[casAction.Cause];
                    if (otherTeam.FindUniquePlayer(victimId) != null) // victim is on other team
                    {
                        PlayerStats ps = otherTeam.FindUniquePlayer(victimId);
                        PlayerStats killer = stats.FindUniquePlayer(killerId);
                        if (killer.KillList == null)
                            killer.KillList = new List<KilledVictimView>();
                        KilledVictimView item = new KilledVictimView(ps.PlayerId, ps.Name, int.Parse(otherTeam.TeamId), otherTeam.TeamName, game.GameId, (ReplayItemType)killItem.AID, game.Date, killer==null?null:killer.PlayerId, killer==null?"":killer.Name, turn, index);
                        if(killer.KillList.Contains(item)== false)
                         killer.KillList.Add(item);
                        killer.KillList.Sort((a, b) => b.KillTime.CompareTo(a.KillTime));
                    }
                    else // We got killed
                    {
                        PlayerStats ps = stats.FindUniquePlayer(victimId);
                        PlayerStats killer = otherTeam.FindUniquePlayer(killerId);
                        if (killer != null)
                        {
                            ps.KilledBy = new KilledVictimView(victimId, ps.Name, int.Parse(otherTeam.TeamId), otherTeam.TeamName, game.GameId, (ReplayItemType)killItem.AID, game.Date, killer == null ? null : killer.PlayerId, killer == null ? "" : killer.Name, turn, index);
                        }
                        else
                        {
                            ps.KilledBy = new KilledVictimView(victimId, "KilledBy_" + killItem.AID, int.Parse(otherTeam.TeamId), otherTeam.TeamName, game.GameId, (ReplayItemType)killItem.AID, game.Date, killer == null ? null : killer.PlayerId, killer == null ? "" : killer.Name, turn, index);
                        }
                    }
                }
            }
        }
    }
}
