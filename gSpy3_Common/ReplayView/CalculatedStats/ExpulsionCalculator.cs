﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BB2StatsLib.Views.Replay;

namespace BB2StatsLib.Views.CalculatedStats
{
    public class ExpulsionCalculator : IStatCalculator
    {
        ReplayView _replayView;
        int expulsions = 0;

        public void AddFromReplayItem(ReplayItem item, int index, TeamStats ownTeam, TeamStats otherTeam, int turn)
        {
            if (item.AID == (int)ReplayItemType.DeceiveReferee && item.I >= 0 && item.I < _replayView.Rolls.Count)
            {
                ReplayRollAction ra = _replayView.Rolls[item.I];
                if (ra.Roll != null && ra.Roll.Count >= 2)
                {
                    expulsions++;
                }
            }
           
        }

        public void StoreResults(GameStats game, TeamStats stats, TeamStats otherTeam)
        {
            stats.Expulsions = expulsions;
        }

        public void TurnStarted(ReplayView aReplayView)
        {
            _replayView = aReplayView;
            
        }
    }
}
