//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using BB2StatsLib.BB2Replay;
using System.Globalization;
using BB2StatsLib.Views.Replay;
using BB2StatsLib.Views;
using gSpy3_Common.ReplayView;
using BB2StatsLib.Views.CalculatedStats;

namespace BB2StatsLib
{
	[Serializable]
	public class TeamStats
	{
        public string CoachId { get; set; }
        public string CoachName { get; set; }
		public string TeamName {get;set;}
		public string TeamId{ get; set; }
        public string LeagueName { get; set; }
        public string CompetitionName { get; set; }
		public int Value {get;set;}
		public string Race { get; set; }
		public bool IsHome { get; set; }
        public int GamesPlayed { get; set; }
        public int RealGamesPlayed { get; set; }

        public int Passes{ get; set; }
        public int PassesFailed { get; set; }
		public int MetersPassed {get;set;}
		public int MetersRun{ get; set; }
        public int Touchdowns { get; set; }
        public int TouchdownsAgainst { get; set; }
        public int Score { get; set; }
        public int ScoreAgainst { get; set; }

		public int Blocks{get;set;}
        public int BlocksFailed { get; set; }
        public int BlocksNeutral { get; set; }
		public int BlocksAgainst{ get; set; }

        public int BlocksGood { get; set; }
		public int Breaks{get;set;}
		public int BreaksAgainst{get;set;}
		public int Knockouts{get;set;}
		public int KnockoutsAgainst{get;set;}
		public int Stuns{get;set;}
		public int StunsAgainst{get;set;}
		public int Casualties{get;set;}
		public int CasualtiesAgainst{get;set;}
		public int Kills{get;set;}
		public int KillsAgainst{get;set;}

		public int Catches{get;set;}
        public int CatchesFailed { get; set; }

        public int Interceptions{get;set;}
        
        public int Dodges { get; set; }
        public int Expulsions { get; set; }
        public int Fouls { get; set; }
        public int DodgesFailed { get; set; }
        public int GFIs { get; set; }
        public int GFIsFailed { get; set; }
        public int Pickups { get; set; }
        public int PickupsFailed { get; set; }

        public int BlockRisk { get; set; }
        public int BlockLuck { get; set; }
        public int MoveRisk { get; set; }
        public int MoveLuck { get; set; }
        public int PassRisk { get; set; }
        public int PassLuck { get; set; }
        public int Risk { get; set; }
        public int Luck { get; set; }
        public int BallPossession { get; set; }

        public int Completions { get; set; }
        public int Sacks { get; set; }
        public int Turnovers { get; set; }
        public int Boneheads { get; set; }

        public int XP { get; set; }

        public List<PlayerStats> PlayerStats { get; set; }
        public List<RollStats> Rolls { get; set; }

		private Dictionary<string,int> _idToIndexMap = new Dictionary<string, int>();

		public TeamStats ()
		{
			PlayerStats = new List<PlayerStats> ();
            Luck = 100;
            BlockLuck = 100;
            MoveLuck = 100;
            PassLuck = 100;
            BallPossession = 0;
		}
        /// <summary>
        /// Add stats to this team from the given game.
        /// </summary>
        /// <param name="gstats">The game to add stats from</param>
        /// <param name="stats">The teamstats in the game that corresponds to this team</param>
        /// <param name="otherTeam">The teamstats in the game that corresponds to the opposing team</param>
        /// <param name="settings">League settings, including disabled games</param>
		public void AddFromGame(GameStats gstats, TeamStats stats, TeamStats otherTeam,  bool onlyCoreStats=false)
		{
            



            #region Calculate averages for parameters that should not just add the stats
            if (!gstats.IsManual)
            {
                Luck = (Luck * RealGamesPlayed + stats.Luck) / (RealGamesPlayed + 1);
                Risk = (Risk * RealGamesPlayed + stats.Risk) / (RealGamesPlayed + 1);
                BlockLuck = (BlockLuck * RealGamesPlayed + stats.BlockLuck) / (RealGamesPlayed + 1);
                BlockRisk = (BlockRisk * RealGamesPlayed + stats.BlockRisk) / (RealGamesPlayed + 1);
                MoveLuck = (MoveLuck * RealGamesPlayed + stats.MoveLuck) / (RealGamesPlayed + 1);
                MoveRisk = (MoveRisk * RealGamesPlayed + stats.MoveRisk) / (RealGamesPlayed + 1);
                PassLuck = (PassLuck * RealGamesPlayed + stats.PassLuck) / (RealGamesPlayed + 1);
                PassRisk = (PassRisk * RealGamesPlayed + stats.PassRisk) / (RealGamesPlayed + 1);
                BallPossession = (BallPossession * RealGamesPlayed + stats.BallPossession) / (RealGamesPlayed + 1);
            }

            if ( gstats.IsManual == false)
            {
                Dodges += stats.Dodges;
                GFIs += stats.GFIs;
                Pickups += stats.Pickups;
                Fouls += stats.Fouls;

                Completions += stats.Completions;
                Turnovers += stats.Turnovers;
                Sacks += stats.Sacks;

                BlocksGood += stats.BlocksGood;
                BlocksFailed += stats.BlocksFailed;
                BlocksNeutral += stats.BlocksNeutral;
                DodgesFailed += stats.DodgesFailed;
                GFIsFailed += stats.GFIsFailed;
                PickupsFailed += stats.PickupsFailed;
                CatchesFailed += stats.CatchesFailed;
                PassesFailed += stats.PassesFailed;
            }
            #endregion


            // Adjust stats from given teamstats
            GamesPlayed++;

            if (!gstats.IsManual)
                RealGamesPlayed++;

            // Add all included stats
            Passes += stats.Passes;
			MetersPassed += stats.MetersPassed;
			MetersRun += stats.MetersRun;
			Touchdowns += stats.Touchdowns;
			TouchdownsAgainst += otherTeam.Touchdowns;
            Score += stats.Score;
            ScoreAgainst += stats.ScoreAgainst;
			Blocks += stats.Blocks;
			BlocksAgainst += stats.BlocksAgainst;
			Breaks += stats.Breaks;
			BreaksAgainst += stats.BreaksAgainst;
			Stuns += stats.Stuns;
			StunsAgainst += stats.StunsAgainst;
			Knockouts += stats.Knockouts;
			KnockoutsAgainst += stats.KnockoutsAgainst;
			Casualties += stats.Casualties;
			CasualtiesAgainst += stats.CasualtiesAgainst;
			Kills += stats.Kills;
			KillsAgainst += stats.Kills;
			Catches += stats.Catches;
			Interceptions += stats.Interceptions;
            XP += stats.XP;

            if(onlyCoreStats == false)
                Rolls = stats.Rolls; // We don't add up , just use latest

            if (onlyCoreStats == false)
                AddPlayersFromGame(stats);


		}

        public PlayerStats FindUniquePlayer(string  id)
        {
            foreach (PlayerStats ps in PlayerStats)
            {
                if (ps.PlayerId == id)
                    return ps;
            }
    
            return null;
        }

		public void ParseCoachResult(CoachResult res, CoachInfos infos)
		{
		
			
			foreach (PlayerResult pres in res.TeamResult.PlayerResults) 
			{
				int index=0;
				if(_idToIndexMap.TryGetValue(pres.PlayerData.Id, out index)==false)
				{
					PlayerStats.Add(new PlayerStats());
					index = PlayerStats.Count-1;
					_idToIndexMap[pres.PlayerData.Id] = index;
				}
				PlayerStats[index].ParsePlayerResult(pres,(res.TeamResult.IdTeam));
			}

			if (res.TeamResult.TeamData != null) 
			{
				TeamName = res.TeamResult.TeamData.Name;
				TeamId = res.TeamResult.IdTeam.ToString();
				Value = res.TeamResult.TeamData.Value;
				Race = ""+ (res.TeamResult.TeamData.IdRace);
			}

            if (infos != null)
            {
                CoachId = infos.UserId;
                CoachName = infos.Login;
            }
           
		}


		public void ParseTeamState(TeamState ts)
		{
			ParsePlayersState (ts.ListPitchPlayers);

		}
		void ParsePlayersState(List<PlayerState> players)
		{
			foreach (PlayerState state in players) 
			{
				int index=0;
				if(_idToIndexMap.TryGetValue(state.Id, out index)==false)
				{
					PlayerStats.Add(new PlayerStats());
					index = PlayerStats.Count-1;
					_idToIndexMap[state.Id] = index;
				}
				PlayerStats[index].ParsePlayerState(state);
			}
		}

        public void SumStatsFromPlayers(bool calculateTouchdowns = true)
        {
            Passes = 0;
            MetersPassed = 0;
            Blocks = 0;
            BlocksAgainst = 0;
            BreaksAgainst = 0;
            Breaks = 0;
            Stuns = 0;
            StunsAgainst = 0;
            Knockouts = 0;
            KnockoutsAgainst = 0;
            Casualties = 0;
            CasualtiesAgainst = 0;
            Kills = 0;
            KillsAgainst = 0;
            Interceptions = 0;
            Catches = 0;
            MetersRun = 0;
            if (calculateTouchdowns)
                Touchdowns = 0;
            Dodges = 0;
            Fouls = 0;
            GFIs = 0;
            Pickups = 0;
            Passes = 0;
            Catches = 0;
            XP = 0;
            Sacks = 0;
            Turnovers = 0;

            /* These doesn't exist in players, so don't add from players or reset
            BlocksFailed = 0;
            BlocksNeutral = 0;
            CatchesFailed = 0;
            DodgesFailed = 0;
            GFIsFailed = 0;
            PickupsFailed = 0;
            PassesFailed = 0;
            Completions = 0;
            */

            foreach (PlayerStats stats in PlayerStats)
            {
                XP += stats.XP;
                Passes += stats.Passes;
                MetersPassed += stats.MetersPassed;
                Blocks += stats.Blocks;
                BlocksAgainst += stats.BlocksAgainst;
                Breaks += stats.Breaks;
                BreaksAgainst += stats.BreaksAgainst;
                Knockouts += stats.Knockouts;
                KnockoutsAgainst += stats.KnockoutsAgainst;
                Stuns += stats.Stuns;
                StunsAgainst += stats.StunsAgainst;
                Kills += stats.Kills;
                KillsAgainst += stats.KillsAgainst;
                Casualties += stats.Casualties;
                CasualtiesAgainst += stats.CasualtiesAgainst;
                Interceptions += stats.Interceptions;
                Catches += stats.Catches;
                MetersRun += stats.MetersRun;
                if (calculateTouchdowns)
                    Touchdowns += stats.Touchdowns;

                Dodges += stats.Dodges;
                Expulsions += stats.Expulsions;
                Fouls += stats.Fouls;
                GFIs += stats.GFIs;
                Pickups += stats.Pickups;
                Sacks += stats.Sacks;
                Turnovers += stats.Turnovers;
            }

        }
        void AddPlayersFromGame(TeamStats stats)
        {
      
                #region Add players and player stats
                // Find or create basic player stats
                foreach (PlayerStats p in stats.PlayerStats)
                {
                    // First try to find a player based on player ID
                    PlayerStats pThis = null;
                    foreach (PlayerStats p2 in PlayerStats)
                    {
                        if (p2.PlayerId == p.PlayerId)
                        {
                            pThis = p2;
                            break;
                        }
                    }
                    // If this fails, it might be a loner
                    if (pThis == null)
                    {
                        // Match by name since that is almost unique while loners can share player id
                        foreach (PlayerStats p2 in PlayerStats)
                        {
                            if (p2.Name == p.Name)
                            {
                                pThis = p2;

                                break;
                            }
                        }
                    }
                    // If we didn't find the player on this team, then we need to create the player
                    if (pThis == null)
                    {
                        pThis = new BB2StatsLib.PlayerStats();

                        PlayerStats.Add(pThis);
                        pThis.AddFromPlayer(p);
                        if (string.IsNullOrEmpty(pThis.PlayerId) ||
                            pThis.PlayerId=="0") // A loner, we must give him a new unique ID
                        {
                            string plonerId = pThis.PlayerId;
                            if (p.Name == null)
                                p.Name = "unknown";
                            pThis.PlayerId = plonerId + Math.Abs(p.Name.GetHashCode()) + int.Parse(TeamId);
                        }
                    }
                    else
                        pThis.AddFromPlayer( p);
                }
                #endregion
            
        }
        /// <summary>
        /// Calculate advanced stats from the replay file
        /// </summary>
        /// <param name="gstats"></param>
        /// <param name="otherTeam"></param>
     public void CalculateAdvancedStats(GameStats gstats, TeamStats otherTeam)
     { 
            


        // Calculate advanced stats
        // Parse additional stats based on the actions performed
        if (gstats.Replay != null)
        {

            foreach (PlayerStats ps in PlayerStats)
            {
                ps.Dodges = 0;
                ps.GFIs = 0;
                ps.Pickups = 0;
                ps.Passes = 0;
                ps.Catches = 0;
                ps.Turnovers = 0;
                ps.Sacks = 0;
                    ps.Expulsions = 0;
            }
            Sacks = 0;
            Turnovers = 0;
            Completions = 0;
            BlocksGood = 0;
            BlocksFailed = 0;
            BlocksNeutral = 0;
            CatchesFailed = 0;
            DodgesFailed = 0;
            GFIsFailed = 0;
            PickupsFailed = 0;
            PassesFailed = 0;

            Dictionary<RiskCalculatorType, RiskCalculator> risks = new Dictionary<RiskCalculatorType, RiskCalculator>();
            risks[RiskCalculatorType.All] = (new RiskCalculator(RiskCalculatorType.All));
            risks[RiskCalculatorType.Bashing] = (new RiskCalculator(RiskCalculatorType.Bashing));
            risks[RiskCalculatorType.Movement] = (new RiskCalculator(RiskCalculatorType.Movement));
            risks[RiskCalculatorType.Passing] = (new RiskCalculator(RiskCalculatorType.Passing));

            Dictionary<LuckCalculatorType, LuckCalculator> lucks = new Dictionary<LuckCalculatorType, LuckCalculator>();
            lucks[LuckCalculatorType.All] = (new LuckCalculator(LuckCalculatorType.All));
            lucks[LuckCalculatorType.Bashing] = (new LuckCalculator(LuckCalculatorType.Bashing));
            lucks[LuckCalculatorType.Movement] = (new LuckCalculator(LuckCalculatorType.Movement));
            lucks[LuckCalculatorType.Passing] = (new LuckCalculator(LuckCalculatorType.Passing));

            Dictionary<ReplayItemType, SimpleActionCalculator> simpleActions = new Dictionary<ReplayItemType, SimpleActionCalculator>();
            simpleActions[ReplayItemType.Dodge] = (new SimpleActionCalculator(ReplayItemType.Dodge, "Dodges"));
            simpleActions[ReplayItemType.GFI] = (new SimpleActionCalculator(ReplayItemType.GFI, "GFIs"));
            simpleActions[ReplayItemType.PickUp] = (new SimpleActionCalculator(ReplayItemType.PickUp, "Pickups"));
            simpleActions[ReplayItemType.Catch] = (new SimpleActionCalculator(ReplayItemType.Catch, "Catches"));
            simpleActions[ReplayItemType.Pass] = (new SimpleActionCalculator(ReplayItemType.Pass, "Passes"));
            simpleActions[ReplayItemType.Foul] = (new SimpleActionCalculator(ReplayItemType.Foul, "Fouls"));


            List<IStatCalculator> calculators = new List<IStatCalculator>();
            calculators.AddRange(risks.Values);
            calculators.AddRange(lucks.Values);
            calculators.AddRange(simpleActions.Values);
            calculators.Add(new KillCalculator());
            calculators.Add(new BallPossessionCalculator());
                calculators.Add(new CompletionCalculator());
                calculators.Add(new ExpulsionCalculator());

                int itemIndex = -1;
            int turn = -1;

            foreach (ReplayItem item in gstats.Replay.Items)
            {
                itemIndex++;;

                if (item.AID == (int)ReplayItemType.EndTurn)
                {
                        turn++;

                    foreach (IStatCalculator calc in calculators)
                        calc.TurnStarted(gstats.Replay);
                }

                foreach (IStatCalculator calc in calculators)
                {
                    try
                    {
                        calc.AddFromReplayItem(item, itemIndex, this, otherTeam, turn);
                    }
                    catch(Exception ex)
                    {
                            Console.WriteLine("Error, out of index in "+calc.GetType().Name);
                    }
                }

            }
            foreach (IStatCalculator calc in calculators)
                calc.StoreResults(gstats, this, otherTeam);


            SumStatsFromPlayers();
            }
        else
        {

        }
            



    }
        


		public static TeamStats CreateFromGames(string teamId, List<GameStats> reports)
		{
			TeamStats teamStats = new TeamStats ();
			foreach (GameStats gs in reports) 
			{
                if (gs.TeamStats == null)
                    continue;
				try{
					if(gs.TeamStats[0].TeamId == teamId || 
					   gs.TeamStats[1].TeamId == teamId)
					{

						if(gs.Date != null && gs.Date!="" && gs.Time!=null && gs.Time != "")
						{
                            DateTime dt = DateTime.MinValue;
                            try
                            {
                                dt = DateTime.Parse(gs.Date + " " + gs.Time+"Z");
                            }
                            catch(Exception e) {
                                Console.WriteLine();
                            }
				
							teamStats.AddFromGame(gs,
							                      gs.TeamStats[0].TeamId == teamId ? gs.TeamStats[0] : gs.TeamStats[1],
												  gs.TeamStats[0].TeamId != teamId ? gs.TeamStats[0] : gs.TeamStats[1]);

						}
					}
				}catch{}
			}

      
			return teamStats;
		}


	}
}

