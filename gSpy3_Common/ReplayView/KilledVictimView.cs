﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BB2StatsLib.Views.Replay;
using System.Collections;

namespace BB2StatsLib.Views
{
    [Serializable]
    public class KilledVictimView : IComparable, IEqualityComparer, IEquatable<KilledVictimView>
    {
        public string KillPlayerId { get; set; }
        public string KillerPlayerId { get; set; }
        public string KillerName { get; set; }
        public int KillTeamId { get; set; }
        public string KillTime { get; set; }
        public string KillTeamName { get; set; }
        public string KillGameId { get; set; }
        public string KillPlayerName { get; set; }
        public int KillCausedBy { get; set; }  // ReplayItemType
        public int Turn { get; set; }
        public int Index { get; set; }

        public KilledVictimView()
        { }
        public KilledVictimView(string pid, string pname,int teamId,string sTeam, string gid, ReplayItemType aCause, string aKillTime, string killer, string killerName, int turn,int index)
        {
            KillerName = killerName;
            KillerPlayerId = killer;
            KillTime = aKillTime;
            KillTeamId = teamId;
            KillCausedBy = (int)aCause;
            KillPlayerId = pid;
            KillPlayerName = pname;
            KillGameId = gid;
            KillTeamName = sTeam;
            Turn = turn;
            Index = index;
        }

        public int CompareTo(object obj)
        {
            return this.ToString().CompareTo((obj as KilledVictimView).ToString() );
        }
        public override string ToString()
        {
            return KillGameId + "," + KillPlayerId + "," + KillCausedBy;
        }

        public new bool Equals(object x, object y)
        {
            return (x as KilledVictimView).CompareTo(y) == 0;
        }

        public int GetHashCode(object obj)
        {
            return ToString().GetHashCode();
        }

        public bool Equals(KilledVictimView other)
        {
            return other.CompareTo(this) == 0;
        }
    }
}
