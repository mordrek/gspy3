//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
namespace BB2StatsLib.BB2Replay
{
	[Serializable]
	public class Order
	{
		public CellTo CellTo {get;set;}
        public Location CellFrom { get; set; }
		public Order ()
		{
		}
	}
}

