//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;


namespace BB2StatsLib.BB2Replay
{
	[Serializable]
	public class InducementsCategory
	{
		public int Max{ get; set; }
		public int Cost {get;set;}
		public int Type { get; set; }
		public List<PlayerData> Players{ get; set; }

		public InducementsCategory ()
		{
		}
	}
}

