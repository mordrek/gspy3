//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
using System;
namespace BB2StatsLib.BB2Replay
{
	[Serializable]
	public class RulesEventBoardAction
	{
		public string PlayerId { get; set; }
		public int RequestType{ get; set; }
		public int ActionType{get;set;}
		public Order Order {get;set;} 
		public Results Results { get; set; }

		public RulesEventBoardAction ()
		{
		}
	}
}

