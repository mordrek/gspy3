//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
namespace BB2StatsLib.BB2Replay
{
	[Serializable]
	public class SkillInfo
	{
		public int PlayerId { get; set; }
		public int SkillId { get; set;}


		public SkillInfo ()
		{
		}
	}
}

