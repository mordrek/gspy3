//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;


namespace BB2StatsLib.BB2Replay
{
	[Serializable]
	public class CampaignSpecifics
	{
		public List<string> ListDangerousCells { get; set; } 
		public List<string> ListPoisonnedPlayers { get; set; } 

		public CampaignSpecifics ()
		{
		}
	}
}

