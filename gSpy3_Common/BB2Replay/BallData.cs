﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BB2StatsLib.BB2Replay
{
    [Serializable]
    public class BallData : ActorMove
    {
        public int IsHeld { get; set; }
    }
}
