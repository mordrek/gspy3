//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using System.Xml;



namespace BB2StatsLib.BB2Replay
{

	[Serializable]
	public class Replay
	{
		static XmlSerializer _serializer = null;
		public string ClientVersion { get; set; }
		[XmlElement]
		public List<ReplayStep> ReplayStep { get; set; }

		public Replay ()
		{
		}

		public static Replay CreateFromString(string txt)
		{
			if (_serializer == null)
				_serializer = new XmlSerializer (typeof(Replay));
		
			var settings = new XmlReaderSettings ();
			settings.CheckCharacters = false;
			settings.ConformanceLevel = ConformanceLevel.Auto;
			settings.DtdProcessing = DtdProcessing.Ignore;
			settings.ValidationFlags = System.Xml.Schema.XmlSchemaValidationFlags.None;
			settings.ValidationType = ValidationType.None;

			var root = XmlReader.Create (new StringReader (txt), settings);
			return (Replay)_serializer.Deserialize(root);
		}
	}

}

