﻿using goblinSpy.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace gSpy3_Common.API
{
    public class API
    {

        private class MyWebClient : WebClient
        {
            protected override WebRequest GetWebRequest(Uri uri)
            {
                WebRequest w = base.GetWebRequest(uri);
                w.Timeout = 20 * 60 * 1000;
                return w;
            }
        }

        
        public T Get<T>(string url)
        {
#if DEBUG
            var folder = Path.Combine(Path.GetTempPath(), "gspy3", "cache");
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            var fileURL =  url.ToLower().Replace("https://", "").Replace("http://", "");
            fileURL = fileURL.Substring(fileURL.LastIndexOf("/"))+".json";
            var file = Path.Combine(folder, SaneFilePath.CreateSafePath(fileURL));
            if (File.Exists(file))
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>
                    (File.ReadAllText(file));
            }
#endif

            using (MyWebClient wc = new MyWebClient())
            {
                var str = wc.DownloadString(url);
                var firstCurly = str.IndexOf("{");
                if (firstCurly > 0)
                {
                    str = str.Substring(firstCurly);
                }

#if DEBUG
                bool storeAsFile = str != "false";
               

                if (storeAsFile)
                {
                try{
                    str = str.Replace("\"id\": []", "\"id\": null").Replace("\"name\": []", "\"name\": null");
                    File.WriteAllText(file, str);
                    }catch{
                        Console.WriteLine("Unable to write temp file");
                    }
                }
#endif
                str = str.Replace("\"id\": []", "\"id\": null")
                    .Replace("\"name\": []", "\"name\": null")
                    .Replace("\"match\": []", "\"match\": null");
                try
                {
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<T>
                        (str);
                }catch(Exception ex)
                {
                    try
                    {
                        File.WriteAllText(Path.GetTempFileName(), str);
                    }
                    catch
                    {
                        Console.WriteLine("Unable to write to tmp file");
                    }
                    throw ex;
                }
            }
        }
        public string GetRaw(string url)
        {
            using (MyWebClient wc = new MyWebClient())
            {
                return (wc.DownloadString(url));
            }
        }
    }
}
