BEGIN TRANSACTION;
DROP TABLE IF EXISTS "coachmatchstats";
CREATE TABLE IF NOT EXISTS "coachmatchstats" (
	"cmsv"	TEXT,
	"platform"	TEXT,
	"leaguename"	TEXT,
	"competitionname"	TEXT,
	"activecoach"	INT,
	"idcoach"	INT,
	"coach"	TEXT,
	"winpct"	TEXT,
	"win"	TEXT,
	"draw"	TEXT,
	"loss"	TEXT,
	"gp"	TEXT,
	"conceded"	TEXT
);
DROP TABLE IF EXISTS "leaguestandings";
CREATE TABLE IF NOT EXISTS "leaguestandings" (
	"lsrver"	TEXT,
	"lsb"	TEXT,
	"youtube"	TEXT,
	"twitch"	TEXT,
	"teaminfo"	TEXT,
	"compteamname"	TEXT,
	"idcompetition"	INT,
	"platform"	TEXT,
	"leaguename"	TEXT,
	"competitionname"	TEXT,
	"coachname"	TEXT,
	"idcoach"	INT,
	"activeteam"	INT,
	"idteamlisting"	INT,
	"logo"	TEXT,
	"idracesimg"	INT,
	"idraces"	INT,
	"teamname"	TEXT,
	"rank"	REAL,
	"points"	INT,
	"sort"	REAL,
	"gamesplayed"	TEXT,
	"wins"	INT,
	"draws"	INT,
	"losses"	INT,
	"td"	INT,
	"tdopp"	INT,
	"tddiff"	INT,
	"cas"	INT,
	"casopp"	INT,
	"concedes"	INT,
	"concedesreceived"	INT,
	"value"	INT,
	"kills"	INT,
	"rankoppavg"	TEXT,
	"rankoppmax"	TEXT
);
DROP TABLE IF EXISTS "leaguestandingsbase";
CREATE TABLE IF NOT EXISTS "leaguestandingsbase" (
	"lsb"	TEXT,
	"youtube"	TEXT,
	"twitch"	TEXT,
	"teaminfo"	TEXT,
	"compteamname"	TEXT,
	"idcompetition"	INT,
	"platform"	TEXT,
	"leaguename"	TEXT,
	"competitionname"	TEXT,
	"coachname"	TEXT,
	"idcoach"	INT,
	"activeteam"	INT,
	"idteamlisting"	INT,
	"logo"	TEXT,
	"idracesimg"	INT,
	"idraces"	INT,
	"teamname"	TEXT,
	"rank"	REAL,
	"points"	INT,
	"sort"	REAL,
	"gamesplayed"	INT,
	"wins"	INT,
	"draws"	INT,
	"losses"	INT,
	"td"	INT,
	"tdopp"	INT,
	"tddiff"	INT,
	"cas"	INT,
	"casopp"	INT,
	"concedes"	INT,
	"concedesreceived"	INT,
	"value"	INT,
	"kills"	INT
);
DROP TABLE IF EXISTS "matchplayers";
CREATE TABLE IF NOT EXISTS "matchplayers" (
	"mpver"	TEXT,
	"date"	TEXT,
	"platform"	TEXT,
	"leaguename"	TEXT,
	"competitionname"	TEXT,
	"idteamlisting"	INT,
	"idracesimg"	INT,
	"teamname"	TEXT,
	"teamlogo"	TEXT,
	"idraces"	INT,
	"idracesimg:1"	INT,
	"idmatch"	TEXT,
	"type"	TEXT,
	"level"	INT,
	"xp"	INT,
	"skills_string"	TEXT,
	"casualties_state_string"	TEXT,
	"casualties_sustained_string"	TEXT,
	"ma"	INT,
	"ag"	INT,
	"av"	INT,
	"st"	INT,
	"xp_gain"	INT,
	"matchplayed"	INT,
	"mvp"	INT,
	"uuid"	TEXT,
	"inflictedcasualties"	INT,
	"inflictedstuns"	INT,
	"inflictedpasses"	INT,
	"inflictedmeterspassing"	INT,
	"inflictedtackles"	INT,
	"inflictedko"	INT,
	"inflicteddead"	INT,
	"inflictedinterceptions"	INT,
	"inflictedpushouts"	INT,
	"inflictedcatches"	INT,
	"inflictedinjuries"	INT,
	"inflictedmetersrunning"	INT,
	"inflictedtouchdowns"	INT,
	"sustainedinterceptions"	INT,
	"sustainedtackles"	INT,
	"sustainedinjuries"	INT,
	"sustaineddead"	INT,
	"sustainedko"	INT,
	"sustainedcasualties"	INT,
	"sustainedstuns"	INT,
	"muuid"	TEXT,
	"name"	TEXT,
	"idteam"	INT,
	"idteamstats"	TEXT,
	"puuid"	TEXT
);
DROP TABLE IF EXISTS "teammatches";
CREATE TABLE IF NOT EXISTS "teammatches" (
	"tmver"	TEXT,
	"tmb"	TEXT,
	"rerolls"	TEXT,
	"cheerleaders"	TEXT,
	"apothecary"	TEXT,
	"assistantcoaches"	TEXT,
	"rerollsopp"	TEXT,
	"idteamlisting"	INT,
	"idteamlistingopp"	INT,
	"ver"	TEXT,
	"date"	TEXT,
	"uuid"	TEXT,
	"idcompetition"	INT,
	"duration"	TEXT,
	"finished"	TEXT,
	"platform"	TEXT,
	"leaguename"	TEXT,
	"competitionname"	TEXT,
	"idteamstats1"	TEXT,
	"idteamstats2"	TEXT,
	"idcoachstats1"	TEXT,
	"idcoachstats2"	TEXT,
	"externalstats"	TEXT,
	"vs"	TEXT,
	"compteamname"	TEXT,
	"duration:1"	TEXT,
	"idcoach"	INT,
	"coach"	TEXT,
	"activecoach"	INT,
	"idraceimg"	INT,
	"idrace"	INT,
	"logo"	TEXT,
	"teamname"	TEXT,
	"score"	INT,
	"coachopp"	TEXT,
	"logoopp"	TEXT,
	"teamnameopp"	TEXT,
	"idraceimgopp"	INT,
	"idraceopp"	INT,
	"scoreopp"	INT,
	"win"	TEXT,
	"draw"	TEXT,
	"loss"	TEXT,
	"tvdiff"	TEXT,
	"tvdiffabs"	TEXT,
	"conceded"	TEXT,
	"concedereceived"	TEXT,
	"sustainedtouchdowns"	INT,
	"sustainedtackles"	INT,
	"idmatch"	TEXT,
	"idteamstats"	TEXT,
	"idteamlisting:1"	INT,
	"idraces"	INT,
	"teamname:1"	TEXT,
	"teamlogo"	TEXT,
	"value"	INT,
	"score:1"	INT,
	"cashbeforematch"	INT,
	"popularitybeforematch"	INT,
	"popularitygain"	INT,
	"cashspentinducements"	INT,
	"cashearned"	INT,
	"cashearnedbeforeconcession"	INT,
	"winningsdice"	INT,
	"spirallingexpenses"	INT,
	"nbsupporters"	INT,
	"possessionball"	INT,
	"occupationown"	INT,
	"occupationtheir"	INT,
	"mvp"	INT,
	"inflictedpasses"	INT,
	"inflictedcatches"	INT,
	"inflictedinterceptions"	INT,
	"inflictedtouchdowns"	INT,
	"inflictedcasualties"	INT,
	"inflictedtackles"	INT,
	"inflictedko"	INT,
	"inflictedinjuries"	INT,
	"inflicteddead"	INT,
	"inflictedmetersrunning"	INT,
	"inflictedmeterspassing"	INT,
	"inflictedpushouts"	INT,
	"sustainedexpulsions"	INT,
	"sustainedcasualties"	INT,
	"sustainedko"	INT,
	"sustainedinjuries"	INT,
	"sustaineddead"	INT,
	"active"	INT,
	"cheerleaders:1"	TEXT,
	"apothecary:1"	TEXT,
	"rerolls:1"	TEXT,
	"assistantcoaches:1"	TEXT,
	"rank"	TEXT,
	"rankopp"	TEXT
);
DROP TABLE IF EXISTS "leaguematches";
CREATE TABLE IF NOT EXISTS "leaguematches" (
	"lmver"	TEXT,
	"rerolls"	TEXT,
	"apothecary"	TEXT,
	"cheerleaders"	TEXT,
	"assistantcoaches"	TEXT,
	"tvabs"	TEXT,
	"tvhome"	INT,
	"tvaway"	INT,
	"pool"	TEXT,
	"uuid"	TEXT,
	"idcompetition"	INT,
	"leaguename"	TEXT,
	"competitionname"	TEXT,
	"stadium"	TEXT,
	"levelstadium"	INT,
	"structstadium"	TEXT,
	"started"	TEXT,
	"finished"	TEXT,
	"idcoachstatshome"	TEXT,
	"idcoachstatsaway"	TEXT,
	"idteamstatshome"	TEXT,
	"idteamstatsaway"	TEXT,
	"externalstats"	TEXT,
	"platform"	TEXT,
	"checkedplayerstats"	INT,
	"vs"	TEXT,
	"duration"	TEXT,
	"date"	TEXT,
	"hour"	TEXT,
	"weekday"	TEXT,
	"coachhome"	TEXT,
	"idraceimghome"	INT,
	"idteamlistinghome"	INT,
	"idracehome"	INT,
	"logohome"	TEXT,
	"teamhome"	TEXT,
	"scorehome"	INT,
	"coachaway"	TEXT,
	"idraceimgaway"	INT,
	"idraceaway"	INT,
	"idteamlistingaway"	INT,
	"logoaway"	TEXT,
	"teamaway"	TEXT,
	"scoreaway"	INT,
	"conceded"	TEXT,
	"concededhome"	TEXT,
	"concededaway"	TEXT
);
DROP TABLE IF EXISTS "coaches";
CREATE TABLE IF NOT EXISTS "coaches" (
	"chiver"	INTEGER,
	"id"	INTEGER,
	"name"	TEXT,
	"twitch"	TEXT,
	"youtube"	TEXT,
	"country"	TEXT,
	"lang"	TEXT,
	"active"	INTEGER,
	"platform"	TEXT,
	"competition"	TEXT,
	"league"	TEXT,
	PRIMARY KEY("id")
);
DROP TABLE IF EXISTS "userschedule";
CREATE TABLE IF NOT EXISTS "userschedule" (
	"contest_id"	INTEGER,
	"date"	TEXT,
	"date_changed"	TEXT,
	PRIMARY KEY("contest_id")
);
DROP TABLE IF EXISTS "teams";
CREATE TABLE IF NOT EXISTS "teams" (
	"id"	INTEGER,
	"team"	TEXT,
	"logo"	TEXT,
	"value"	TEXT,
	"motto"	TEXT,
	"race"	TEXT,
	"coach"	TEXT,
	"active"	INTEGER,
	"platform"	TEXT,
	"competition"	TEXT,
	"league"	TEXT,
	PRIMARY KEY("id")
);
DROP TABLE IF EXISTS "currentplayers";
CREATE TABLE IF NOT EXISTS "currentplayers" (
	"name"	TEXT,
	"idteam"	INTEGER,
	"idteamstats"	TEXT,
	"puuid"	TEXT,
	PRIMARY KEY("puuid")
);
DROP TABLE IF EXISTS "playerstats";
CREATE TABLE IF NOT EXISTS "playerstats" (
	"type"	TEXT,
	"level"	INTEGER,
	"xp"	INTEGER,
	"skills_string"	TEXT,
	"casualties_state_string"	TEXT,
	"casualties_sustained_string"	TEXT,
	"ma"	INTEGER,
	"ag"	INTEGER,
	"av"	INTEGER,
	"st"	INTEGER,
	"xp_gain"	INTEGER,
	"matchplayed"	INTEGER,
	"mvp"	INTEGER,
	"uuid"	TEXT,
	"inflictedcasualties"	INTEGER,
	"inflictedstuns"	INTEGER,
	"inflictedpasses"	INTEGER,
	"inflictedmeterspassing"	INTEGER,
	"inflictedtackles"	INTEGER,
	"inflictedko"	INTEGER,
	"inflicteddead"	INTEGER,
	"inflictedinterceptions"	INTEGER,
	"inflictedpushouts"	INTEGER,
	"inflictedcatches"	INTEGER,
	"inflictedinjuries"	INTEGER,
	"inflictedmetersrunning"	INTEGER,
	"inflictedtouchdowns"	INTEGER,
	"sustainedinterceptions"	INTEGER,
	"sustainedtackles"	INTEGER,
	"sustainedinjuries"	INTEGER,
	"sustaineddead"	INTEGER,
	"sustainedko"	INTEGER,
	"sustainedcasualties"	INTEGER,
	"sustainedstuns"	INTEGER,
	"muuid"	TEXT,
	"name"	TEXT,
	"idteam"	INTEGER,
	"idteamstats"	TEXT,
	"puuid"	TEXT,
	PRIMARY KEY("muuid")
);
DROP TABLE IF EXISTS "schedule";
CREATE TABLE IF NOT EXISTS "schedule" (
	"smver"	INTEGER,
	"league"	TEXT,
	"competition"	TEXT,
	"platform"	TEXT,
	"competition_id"	INTEGER,
	"contest_id"	INTEGER,
	"format"	TEXT,
	"competition_round"	INTEGER,
	"type"	TEXT,
	"status"	TEXT,
	"stadium"	TEXT,
	"match_uuid"	TEXT,
	"match_id"	TEXT,
	"idteam1"	TEXT,
	"idteam2"	TEXT,
	"date"	TEXT,
	PRIMARY KEY("contest_id")
);
DROP TABLE IF EXISTS "teamstats";
CREATE TABLE IF NOT EXISTS "teamstats" (
	"idmatch"	TEXT,
	"idteamstats"	TEXT,
	"idteamlisting"	INTEGER,
	"idraces"	INTEGER,
	"teamname"	TEXT,
	"teamlogo"	TEXT,
	"value"	INTEGER,
	"score"	INTEGER,
	"cashbeforematch"	INTEGER,
	"popularitybeforematch"	INTEGER,
	"popularitygain"	INTEGER,
	"cashspentinducements"	INTEGER,
	"cashearned"	INTEGER,
	"cashearnedbeforeconcession"	INTEGER,
	"winningsdice"	INTEGER,
	"spirallingexpenses"	INTEGER,
	"nbsupporters"	INTEGER,
	"possessionball"	INTEGER,
	"occupationown"	INTEGER,
	"occupationtheir"	INTEGER,
	"mvp"	INTEGER,
	"inflictedpasses"	INTEGER,
	"inflictedcatches"	INTEGER,
	"inflictedinterceptions"	INTEGER,
	"inflictedtouchdowns"	INTEGER,
	"inflictedcasualties"	INTEGER,
	"inflictedtackles"	INTEGER,
	"inflictedko"	INTEGER,
	"inflictedinjuries"	INTEGER,
	"inflicteddead"	INTEGER,
	"inflictedmetersrunning"	INTEGER,
	"inflictedmeterspassing"	INTEGER,
	"inflictedpushouts"	INTEGER,
	"sustainedexpulsions"	INTEGER,
	"sustainedcasualties"	INTEGER,
	"sustainedko"	INTEGER,
	"sustainedinjuries"	INTEGER,
	"sustaineddead"	INTEGER,
	"active"	INTEGER,
	"cheerleaders"	TEXT,
	"apothecary"	TEXT,
	"rerolls"	TEXT,
	"assistantcoaches"	TEXT,
	PRIMARY KEY("idteamstats")
);
DROP TABLE IF EXISTS "coachstats";
CREATE TABLE IF NOT EXISTS "coachstats" (
	"idcoachstats"	TEXT,
	"idcoach"	INTEGER,
	"coachname"	TEXT,
	"coachcyanearned"	INTEGER,
	"coachxpearned"	INTEGER,
	"active"	INTEGER,
	PRIMARY KEY("idcoachstats")
);
DROP TABLE IF EXISTS "matches";
CREATE TABLE IF NOT EXISTS "matches" (
	"uuid"	TEXT,
	"idcompetition"	INTEGER,
	"leaguename"	TEXT,
	"competitionname"	TEXT,
	"stadium"	TEXT,
	"levelstadium"	INTEGER,
	"structstadium"	TEXT,
	"started"	TEXT,
	"finished"	TEXT,
	"idcoachstatshome"	TEXT,
	"idcoachstatsaway"	TEXT,
	"idteamstatshome"	TEXT,
	"idteamstatsaway"	TEXT,
	"externalstats"	TEXT,
	"platform"	TEXT,
	"checkedplayerstats"	INTEGER,
	PRIMARY KEY("uuid")
);
DROP INDEX IF EXISTS "leaguestandings_platform";
CREATE INDEX IF NOT EXISTS "leaguestandings_platform" ON "leaguestandings" (
	"platform"
);
DROP INDEX IF EXISTS "leaguestandings_leaguename";
CREATE INDEX IF NOT EXISTS "leaguestandings_leaguename" ON "leaguestandings" (
	"leaguename"
);
DROP INDEX IF EXISTS "leaguestandings_competitionname";
CREATE INDEX IF NOT EXISTS "leaguestandings_competitionname" ON "leaguestandings" (
	"competitionname"
);
DROP INDEX IF EXISTS "leaguestandingsbase_compteamname";
CREATE INDEX IF NOT EXISTS "leaguestandingsbase_compteamname" ON "leaguestandingsbase" (
	"compteamname"
);
DROP INDEX IF EXISTS "leaguestandingsbase_idteamlisting";
CREATE INDEX IF NOT EXISTS "leaguestandingsbase_idteamlisting" ON "leaguestandingsbase" (
	"idteamlisting"
);
DROP INDEX IF EXISTS "teammatches_idcoach";
CREATE INDEX IF NOT EXISTS "teammatches_idcoach" ON "teammatches" (
	"idcoach"
);
DROP INDEX IF EXISTS "teammatches_idteamlisting";
CREATE INDEX IF NOT EXISTS "teammatches_idteamlisting" ON "teammatches" (
	"idteamlisting"
);
DROP INDEX IF EXISTS "matchplayers_puuid";
CREATE INDEX IF NOT EXISTS "matchplayers_puuid" ON "matchplayers" (
	"puuid"
);
DROP INDEX IF EXISTS "matchplayers_date";
CREATE INDEX IF NOT EXISTS "matchplayers_date" ON "matchplayers" (
	"date"
);
DROP INDEX IF EXISTS "matchplayers_idteamlisting";
CREATE INDEX IF NOT EXISTS "matchplayers_idteamlisting" ON "matchplayers" (
	"idteamlisting"
);
DROP INDEX IF EXISTS "matchplayers_idteamstats";
CREATE INDEX IF NOT EXISTS "matchplayers_idteamstats" ON "matchplayers" (
	"idteamstats"
);
DROP INDEX IF EXISTS "matchplayers_platform";
CREATE INDEX IF NOT EXISTS "matchplayers_platform" ON "matchplayers" (
	"platform"
);
DROP INDEX IF EXISTS "matchplayers_leaguename";
CREATE INDEX IF NOT EXISTS "matchplayers_leaguename" ON "matchplayers" (
	"leaguename"
);
DROP INDEX IF EXISTS "matchplayers_competitionname";
CREATE INDEX IF NOT EXISTS "matchplayers_competitionname" ON "matchplayers" (
	"competitionname"
);
DROP INDEX IF EXISTS "teammatches_coach";
CREATE INDEX IF NOT EXISTS "teammatches_coach" ON "teammatches" (
	"coach"
);
DROP INDEX IF EXISTS "teammatches_date";
CREATE INDEX IF NOT EXISTS "teammatches_date" ON "teammatches" (
	"date"
);
DROP INDEX IF EXISTS "teammatches_idteamstats";
CREATE INDEX IF NOT EXISTS "teammatches_idteamstats" ON "teammatches" (
	"idteamstats"
);
DROP INDEX IF EXISTS "teammatches_uuid";
CREATE INDEX IF NOT EXISTS "teammatches_uuid" ON "teammatches" (
	"uuid"
);
DROP INDEX IF EXISTS "teammatches_leaguename";
CREATE INDEX IF NOT EXISTS "teammatches_leaguename" ON "teammatches" (
	"leaguename"
);
DROP INDEX IF EXISTS "teammatches_competitionname";
CREATE INDEX IF NOT EXISTS "teammatches_competitionname" ON "teammatches" (
	"competitionname"
);
DROP INDEX IF EXISTS "teammatches_platform";
CREATE INDEX IF NOT EXISTS "teammatches_platform" ON "teammatches" (
	"platform"
);
DROP INDEX IF EXISTS "leaguematches_date";
CREATE INDEX IF NOT EXISTS "leaguematches_date" ON "leaguematches" (
	"date"
);
DROP INDEX IF EXISTS "leaguematches_uuid";
CREATE INDEX IF NOT EXISTS "leaguematches_uuid" ON "leaguematches" (
	"uuid"
);
DROP INDEX IF EXISTS "leaguematches_leaguename";
CREATE INDEX IF NOT EXISTS "leaguematches_leaguename" ON "leaguematches" (
	"leaguename"
);
DROP INDEX IF EXISTS "leaguematches_competitionname";
CREATE INDEX IF NOT EXISTS "leaguematches_competitionname" ON "leaguematches" (
	"competitionname"
);
DROP INDEX IF EXISTS "leaguematches_platform";
CREATE INDEX IF NOT EXISTS "leaguematches_platform" ON "leaguematches" (
	"platform"
);
DROP VIEW IF EXISTS "teamplayers";
CREATE VIEW teamplayers as select 3 as tmp,c.puuid , mp.* from currentplayers as c inner join matchplayers as mp on mp.idteamstats = c.idteamstats and mp.puuid=c.puuid;
DROP VIEW IF EXISTS "scheduled";
CREATE VIEW scheduled as select 2 as scver,s.*,t1.coach as coachname1, t1.logo as logo1, t1.team as team1, t1.value as tv1,t1.race as racename1,t1.race as race1,t2.coach as coachname2,t2.value as tv2,  t2.team as team2, t2.logo as logo2, t2.race as race2, t2.race as racename2 from schedule as s inner join teams as t1 on t1.id=s.idteam1 inner join teams as t2 on t2.id=s.idteam2;
DROP VIEW IF EXISTS "teammatchesbase";
CREATE VIEW teammatchesbase as select 8 as tmb,ts1.rerolls as rerolls, ts1.cheerleaders as cheerleaders, ts1.apothecary as apothecary, ts1.assistantcoaches as assistantcoaches, ts2.rerolls as rerollsopp, ts1.idteamlisting as idteamlisting, ts2.idteamlisting as idteamlistingopp, m.*,("t1="||ts1.idteamstats||"&t2="||ts2.idteamstats||"&m="||m.uuid) as vs,(ts1.idteamlisting||ts1.teamname||idcompetition    ) as compteamname, duration,cs1.idcoach as idcoach, cs1.coachname as coach, cs1.active as activecoach, ts1.idraces as idraceimg, ts1.idraces as idrace,ts1.teamlogo as logo, ts1.teamname as teamname, ts1.score as score, cs2.coachname as coachopp, ts2.teamlogo as logoopp, ts2.teamname as teamnameopp, ts2.idraces as idraceimgopp,ts2.idraces as idraceopp, ts2.score as scoreopp, (case when ts1.score>ts2.score then 1 else 0 end) as win, (case when ts1.score=ts2.score then 1 else 0 end) as draw,(case when ts1.score<ts2.score then 1 else 0 end) as loss,(ts1.value-ts2.value) as tvdiff, abs(ts1.value-ts2.value) as tvdiffabs ,((case when ts1.mvp = 0 then 1 else 0 end)) as conceded, ((case when ts2.mvp=0 then 1 else 0 end)) as concedereceived,ts2.inflictedtouchdowns as sustainedtouchdowns, ts2.inflictedtackles as sustainedtackles,ts1.* from teammatchesraw  as m  inner join coachstats as cs1 on cs1.idcoachstats=m.idcoachstats1 inner join coachstats as cs2 on cs2.idcoachstats=idcoachstats2 inner join teamstats as ts1 on ts1.idteamstats = idteamstats1 inner join teamstats as ts2 on ts2.idteamstats=idteamstats2;
DROP VIEW IF EXISTS "teammatchesraw";
CREATE VIEW teammatchesraw as select 6 as ver, date(m.finished) as date,* from(select uuid, idcompetition,round((julianday(finished)-julianday(started))*24*60,0) as duration, finished, platform, leaguename, competitionname, idteamstatshome as idteamstats1, idteamstatsaway as idteamstats2, idcoachstatshome as idcoachstats1, idcoachstatsaway as idcoachstats2, externalstats from matches  WHERE date(finished) >= '0001-01-01' union select uuid,idcompetition,round((julianday(finished)-julianday(started))*24*60,0) as duration, finished, platform,  leaguename, competitionname, idteamstatsaway as idteamstats1, idteamstatshome as idteamstats2, idcoachstatsaway as idcoachstats1, idcoachstatshome as idcoachstats2, externalstats from matches  WHERE date(finished) >= '0001-01-01') as m;
COMMIT;
