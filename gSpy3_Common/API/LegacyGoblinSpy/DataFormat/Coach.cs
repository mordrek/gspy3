﻿using goblinSpy.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace goblinSpy.DataFormat
{
    public class Coach
    {
        public int chiver { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string twitch { get; set; }
        public string youtube { get; set; }
        public string country { get; set; }
        public string lang { get; set; }
        public int active { get; set; }

        public string platform { get; set; }
        public string competition { get; set; }
        public string league { get; set; }

        public static Coach From(gSpy3_Common.Database.Format.Coach c, string league_name, string competition_name)
        {
            return new Coach()
            {
                chiver = 0,
                id = c.coach_origin_uid,
                name = c.coach_name,
                twitch = c.twitch,
                youtube = c.youtube,
                country = c.country,
                lang = c.lang,
                active = 1,
                platform = gSpy3_Common.Database.Format.OriginConversion.ToLegacyGoblinSpy((gSpy3_Common.Database.Format.OriginID)c.idorigin),
                competition = competition_name,
                league = league_name
            };
        }
        public GlobalCoach ToGlobal()
        {
            
            var c = this;
            if(c.platform == "")
                platform = "pc";
            return new GlobalCoach()
            {
            idcoach = c.platform + c.id,
            coach = c.name
            };
        
     

        }
        /*
        public void Import(Database.DatabaseManager db, Database.DatabaseManager dbHub) 
        {
            try
            {
                chiver = 3;
                active = 1;
                db.Query("update coachmatchstats set activecoach = 1 where idcoach=" + this.id, null);

                var res = dbHub.Query("select youtube,twitch from coaches where id = " + this.id,null);
                if (res == null || res.rows == null || res.rows.Count == 0)
                {
                    dbHub.WriteClass(this);
                }
                else
                {
                    youtube = res.rows[0][0];
                    twitch = res.rows[0][1];
                }
                db.WriteClass(this);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }*/
    }

    public class Coaches
    {
        public List<Coach> coaches;
        /*
        public static int Import(Database.DatabaseManager db, string storageFolder, string specificFile, string platform, string league, string competition, Database.DatabaseManager dbHub)
        {
            int count = 0;
            foreach (string file in Directory.GetFiles(storageFolder, "coaches.*.zip"))
            {
                if (specificFile != null && Path.GetFileName(specificFile) != Path.GetFileName(file))
                    continue;

                if (File.Exists(file))
                {

                    // Remove old coaches, since coaches is always the latest
                    Dictionary<string, object> parameters = new Dictionary<string, object>();
                    parameters["p1"] = platform;
                    parameters["c1"] = competition;
                    parameters["l1"] = league;
                    try
                    {
                        // Alter table in case it has the old coach id....
                        var res = db.Query("select chiver from coaches limit 1", parameters);
                        if(res == null || res.rows == null || res.rows.Count  == 0 || res.rows[0][0]!="3")
                        {

                            Console.WriteLine("Recreating coaches");
                            db.Query("drop table coaches", parameters);
                            db.RegisterClass<Coach>("coaches", "id");
                        }
                    }
                    catch
                    {
                        Console.WriteLine("Recreating coaches");
                        db.Query("drop table coaches", parameters);
                        db.RegisterClass<Coach>("coaches", "id");
                    }
                    try
                    {
                        // Reset to inactive and update from the data given
                        db.Query("update coaches set active = 0 where platform=@p1 and competition=@c1 and league=@l1", parameters);
                        db.Query("update coachmatchstats set activecoach = 0", parameters);
                        
                    }
                    catch (Exception e3)
                    {
                        Console.WriteLine(e3.ToString() + " when deleting old coaches");
                    }
                   // db.StartTransaction();
                    try
                    {
                       
                        string[] rawFiles = Zipper.Unzip(file);
                        if (rawFiles != null)
                        {
                            foreach (string f in rawFiles)
                            {
                                using (var sr = File.OpenText(f))
                                {
                                    string datastring = sr.ReadToEnd();
                                    Coaches ms = SimpleJson.SimpleJson.DeserializeObject<Coaches>(datastring);

                                    if (ms.coaches != null)
                                    {

                                        foreach (Coach m in ms.coaches)
                                        {
                                            

                                            count++;
                                            try
                                            {
                                                parameters["cid"] = m.id;

                                                m.platform = platform;
                                                m.competition = competition;
                                                m.league = league;

                                                m.Import(db, dbHub);

                                            }
                                            catch (Exception e4)
                                            {
                                                Console.WriteLine("Exception while importing coaches: " + e4.ToString());
                                            }
                                        }
                                    }
                                }
                                File.Delete(f);
                            }
                            if (rawFiles.Length > 0)
                                Directory.Delete(Path.GetDirectoryName(rawFiles[0]), true);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                   // db.StopTransaction();
                    try
                    {
                        db.Query("update coachstats set active = 0 where exists (select 1 FROM coaches as c WHERE c.id = idcoach and c.active=0 and c.platform=@p1 and c.competition=@c1 and c.league=@l1)", parameters);
                        db.Query("update coachstats set active = 1 where exists (select 1 FROM coaches as c WHERE c.id = idcoach and c.active=1 and c.platform=@p1 and c.competition=@c1 and c.league=@l1)", parameters);
                    }
                    catch (Exception e) {
                        Console.WriteLine("Couldn't update coachstats, probably because no coaches existed: "+e.ToString());
                    }
                    Console.WriteLine("Replaced " + count + " coaches");

                }
                else Console.WriteLine("Unable to find the stored file " + file);
            }
            return count;
        }*/
    }
}
