﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace goblinSpy.DataFormat
{
    public class MatchResult
    {
        public string uuid { get; set; }

     //   [Database.DatabaseManager.DBIgnore]
        public MatchResultData match { get; set; }

        // Given from bb api json, but without db equivalent
       // [Database.DatabaseManager.DBIgnore]
        public List<CoachStats> coaches { get; set; }
      //  [Database.DatabaseManager.DBIgnore]
        public List<TeamStats_Current> teams { get; set; }
    }

    public class MatchResultData
    {
        public int idcompetition { get; set; }
        public string leaguename { get; set; }
        public string competitionname { get; set; }
        public string platform { get; set; }
        public string stadium { get; set; }
        public int levelstadium { get; set; }
        public string structstadium { get; set; }
        public string started { get; set; }
        public string finished { get; set; }
      // [Database.DatabaseManager.DBIgnore]
        public List<CoachStats> coaches { get; set; }
       // [Database.DatabaseManager.DBIgnore]
        public List<MatchResultTeam> teams { get; set; }
    }
    
    public class MatchResultTeam : TeamStats
    {
     //   [Database.DatabaseManager.DBIgnore]
        public List<MatchResultPlayer> roster { get; set; }

    }

    public class CurrentPlayer
    {
        public string? name { get; set; }
        public int idteam { get; set; }
        public string idteamstats { get; set; }
        public string puuid { get; set; }
    }

    public class MatchResultPlayer : CurrentPlayer
    {
        public string type { get; set; }
        public int level { get; set; }
        public int xp { get; set; }

        // generated stats based on the lists and other info
        public string skills_string { get; set; }
        public string casualties_state_string { get; set; }
        public string casualties_sustained_string { get; set; }
        public int ma { get; set; }
        public int ag { get; set; }
        public int av { get; set; }
        public int st { get; set; }

        public int xp_gain { get; set; }
        public int matchplayed { get; set; }
        public int mvp { get; set; }

     //   [Database.DatabaseManager.DBIgnore]
        public MatchResultPlayerAttributes attributes { get; set; }
      //  [Database.DatabaseManager.DBIgnore]
        public MatchResultPlayerStats stats { get; set; }
     //   [Database.DatabaseManager.DBIgnore]
        public List<string> skills { get; set; }


     //   [Database.DatabaseManager.DBIgnore]
        public List<string> casualties_state { get; set; }
      //  [Database.DatabaseManager.DBIgnore]
        public List<string> casualties_sustained { get; set; }
        


        public string uuid { get; set; }

        public int inflictedcasualties { get; set; }
        public int inflictedstuns { get; set; }
        public int inflictedpasses { get; set; }
        public int inflictedmeterspassing { get; set; }
        public int inflictedtackles { get; set; }
        public int inflictedko { get; set; }
        public int inflicteddead { get; set; }
        public int inflictedinterceptions { get; set; }
        public int inflictedpushouts { get; set; }
        public int inflictedcatches { get; set; }
        public int inflictedinjuries { get; set; }
        public int inflictedmetersrunning { get; set; }
        public int inflictedtouchdowns { get; set; }
        public int sustainedinterceptions { get; set; }
        public int sustainedtackles { get; set; }
        public int sustainedinjuries { get; set; }
        public int sustaineddead { get; set; }
        public int sustainedko { get; set; }
        public int sustainedcasualties { get; set; }
        public int sustainedstuns { get; set; }


        public string muuid { get; set; }

        /// <summary>
        /// Initialize serializable data from non-serializable data
        /// </summary>
        public void Initialize(string aUuid, int aTeamID, string aTeamStatsID)
        {
            idteam = aTeamID;
            idteamstats = aTeamStatsID;
            uuid = aUuid;
            muuid = uuid + name;
            skills_string = Newtonsoft.Json.JsonConvert.SerializeObject(skills);// SimpleJson.SimpleJson.SerializeObject(skills);
            casualties_state_string = Newtonsoft.Json.JsonConvert.SerializeObject(skills);//SimpleJson.SimpleJson.SerializeObject(casualties_state);
            casualties_sustained_string = Newtonsoft.Json.JsonConvert.SerializeObject(skills);//SimpleJson.SimpleJson.SerializeObject(casualties_sustained);

            puuid = idteam + name;
            ma = attributes.ma;
            ag = attributes.ag;
            av = attributes.av;
            st = attributes.st;

            inflictedcasualties = stats.inflictedcasualties;
            inflictedstuns = stats.inflictedstuns;
            inflictedpasses = stats.inflictedpasses;
            inflictedmeterspassing = stats.inflictedmeterspassing;
            inflictedtackles = stats.inflictedtackles;
            inflictedko = stats.inflictedko;
            inflicteddead = stats.inflicteddead;
            inflictedinterceptions = stats.inflictedinterceptions;
            inflictedpushouts = stats.inflictedpushouts;
            inflictedcatches = stats.inflictedcatches;
            inflictedinjuries = stats.inflictedinjuries;
            inflictedmetersrunning = stats.inflictedmetersrunning;
            inflictedtouchdowns = stats.inflictedtouchdowns;
            sustainedinterceptions = stats.sustainedinterceptions;
            sustainedtackles = stats.sustainedtackles;
            sustainedinjuries = stats.sustainedinjuries;
            sustaineddead = stats.sustaineddead;
            sustainedko = stats.sustainedko;
            sustainedcasualties = stats.sustainedcasualties;
            sustainedstuns = stats.sustainedstuns;
        }

    }
    public class MatchResultPlayerAttributes
    {
        public int ma { get; set; }
        public int ag { get; set; }
        public int av { get; set; }
        public int st { get; set; }
    }
    public class MatchResultPlayerStats
    {
        public int inflictedcasualties { get; set; }
        public int inflictedstuns { get; set; }
        public int inflictedpasses { get; set; }
        public int inflictedmeterspassing { get; set; }
        public int inflictedtackles { get; set; }
        public int inflictedko { get; set; }
        public int inflicteddead { get; set; }
        public int inflictedinterceptions { get; set; }
        public int inflictedpushouts { get; set; }
        public int inflictedcatches { get; set; }
        public int inflictedinjuries { get; set; }
        public int inflictedmetersrunning { get; set; }
        public int inflictedtouchdowns { get; set; }
        public int sustainedinterceptions { get; set; }
        public int sustainedtackles { get; set; }
        public int sustainedinjuries { get; set; }
        public int sustaineddead { get; set; }
        public int sustainedko { get; set; }
        public int sustainedcasualties { get; set; }
        public int sustainedstuns { get; set; }

    }
    

}
