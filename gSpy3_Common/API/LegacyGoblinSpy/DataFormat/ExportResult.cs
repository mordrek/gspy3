﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.API.LegacyGoblinSpy.DataFormat
{
    public class ExportResult
    {
        public bool IsSuccess { get; set; }
        public ExportResultData Data { get; set; }

        public void BuildColMap()
        {
            ColMap = new Dictionary<string, int>();
            for (int i = 0; i < Data.cols.Length; i++) 
            {
                ColMap[Data.cols[i]] = i;
            }
        }
        public Dictionary<string,int> ColMap { get; set; }
    }


    public class ExportResultData
    {
        public int time { get; set; }
        public int from { get; set; }
        public int num { get; set; }
        public string[] cols { get; set; }
        public string[][] rows { get; set; }
    }
}
