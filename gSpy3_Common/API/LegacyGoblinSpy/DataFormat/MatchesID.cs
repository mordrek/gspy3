﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace goblinSpy.DataFormat
{
    public class MatchesID
    {
        public List<MatchID> matches { get; set; }
    }


    public class MatchID
    {
        public string uuid;
        public int id;
        public int idleague;
    }
}
