﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace goblinSpy.DataFormat
{
    public class CoachHistory
    {
        public string id { get; set; }
        public int idteamlisting { get; set; }
        public string teamname { get; set; }
        public string teamlogo { get; set; }
        public string idracesimg { get; set; }
        public int idraces { get; set; }

        public string league { get; set; }
        public string competition { get; set; }
        public string platform { get; set; }
        public string lastgame { get; set; }

        public int wins { get; set; }
        public int draws { get; set; }
        public int losses { get; set; }

        public int concedes { get; set; }

        public string rank { get; set; }
        public string rankoppavg { get; set; }
        public string rankoppmax { get; set; }

        public string name { get; set; }


    }
}
