﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gSpy3_Common.API.LegacyGoblinSpy.DataFormat
{
    public class StatsCompetition
    {
        public const int DBVERSION= 2;
        public string id { get; set; }
        public string league { get; set; }
        public string competition { get; set; }
        public string earliestgame { get; set; }
        public string lastgame { get; set; }
        public int numcoaches { get; set; }
        public int nummatches { get; set; }
        public int numteams { get; set; }
        
        /// <summary>
        /// Last time the competition has been checked for updated data
        /// </summary>
        public string lastupdate { get; set; }
        /// <summary>
        /// Last time a competition has been rebuilt and checked for coaches/teams
        /// </summary>
        public string lastCoachImport { get; set; }


        /// <summary>
        /// != 0 if a user has activated collection for this league at any time
        /// </summary>
        public int activated { get; set; }

        public string bb2statsid { get; set; }

        /// <summary>
        /// != 0 if the competition should collect data
        /// </summary>
        public int collect { get; set; }

        public int history { get; set; }

        public string platform { get; set; }

        public string sorting { get; set; }
        public int dbversion { get; set; }
        public string format { get; set; }

      
    }

    public class StatsPublicLeagues
    {
        public List<StatsPublicLeague> leagues { get; set; }
        public StatsPublicLeagues()
        {
            leagues = new List<StatsPublicLeague>();
        }
    }

    public class StatsPublicLeague
    {
        public string name { get; set; }
        public string platform { get; set; }
        public UInt64 id { get; set; }

        public List<StatsPublicCompetition> list { get; set; }
        public StatsPublicLeague()
        {
            list = new List<StatsPublicCompetition>();
        }
     
    }
    public class StatsPublicCompetition
    {
        public string name { get; set; }
        public bool active { get; set; }
        public string sorting { get; set; }
        public string lastgame { get; set; }
        public int numcoaches { get; set; }
        public int numteams { get; set; }
        public int numgames { get; set; }
        public string format { get; set; }
        public string id { get; set; }
    }
}
