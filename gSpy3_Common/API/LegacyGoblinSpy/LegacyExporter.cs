﻿using goblinSpy.Database;
using gSpy3_Common;
using gSpy3_Common.Database;
using gSpy3_Common.Database.AccessInterface;
using gSpy3_Common.Database.Format;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace gSpy3_Maintenance
{
    public class LegacyExporter
    {
        public static string GetFilename(string baseFolder, int idcompetition)
        {
            var comp = LeagueManager.FindCompetitionByID(idcompetition);
            var league = LeagueManager.FindLeagueByID(comp.idleague);
            string dbPath = Path.Combine(baseFolder, "export", Storage.GetDatabaseName(league.league_name, comp.competition_name, gSpy3_Common.Database.Format.OriginConversion.ToLegacyGoblinSpy((gSpy3_Common.Database.Format.OriginID)comp.idorigin)).Replace("Database","database"));

            return dbPath;
        }

        public static void Export(DatabaseManager dbManager,string baseFolder, int idcompetition)
        {
            Log log = new Log();

            var comp = LeagueManager.FindCompetitionByID(idcompetition);
            var league = LeagueManager.FindLeagueByID(comp.idleague);

            log.Info("Export " + league.league_name + " ; " + comp.competition_name);

            string dbPath = Path.Combine(baseFolder, "export", Storage.GetDatabaseName(league.league_name, comp.competition_name, gSpy3_Common.Database.Format.OriginConversion.ToLegacyGoblinSpy((gSpy3_Common.Database.Format.OriginID)comp.idorigin)).Replace("Database", "database"));
            string dbFolder = Path.GetDirectoryName(dbPath);
            if (Directory.Exists(dbFolder) == false)
            {
                Directory.CreateDirectory(dbFolder);
            }
            if (!File.Exists(dbPath))
            {
                System.Data.SQLite.SQLiteConnection.CreateFile(dbPath);
            }
            using (var db = new DatabaseConnectionSQLite("Data Source=" + dbPath))
            {
                const int maxsize = 1024 * 100;

                // Create the db schema
                var exefolder = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                var q = File.ReadAllText(exefolder + "\\API\\LegacyGoblinSpy\\legacyGoblinSpy.sqlite.sql");
                db.Query(q);

                // Start filling it in
             /*   #region PLAYERSTATS
                {
                    log.Info("Adding playerstats");
                    var list = dbManager.StatsDB.Select<gSpy3_Common.Database.Format.Coach>("select s.* from playermatchstats inner join players as p on p.idplayer = s.idplayer inner join playernames as pn on pn.idplayername = p.idplayername inner join teams as t on t.idteam=s.idteam", new Dictionary<string, string>());
                    var trans = db.BeginTransaction();
                    foreach (var row in list)
                    {
                        var res = db.Query("insert into playerstats (" +
                            "type,level,xp,skills_string,casualties_state_string,casualties_sustained_string,ma,ag,av,st,xp_gain,matchplayed,"+
                            "mvp,uuid,inflictedcasualties,inflictedstuns,inflictedpasses,inflictedmeterspass,platform,competition_id,contest_id,format,competition_round,type,status,stadium, match_uuid, match_id,idteam1,idteam2,date"
                            ") values ("+
                            ")", new Dictionary<string, string>()
                            {
                            });
                        if (res.Success == false)
                        {
                            throw new Exception("Failed to add entry");
                        }
                    }
                    trans.Commit();
                }
                #endregion*/

                // Add all basic standings stats
                #region STANDINGS
                {
                    log.Info("Adding standings and teams");
                    var standings = dbManager.StatsDB.Query("select s.*,ch.coach_origin_uid,t.team_origin_uid from standingsview as s inner join coaches as ch on ch.idcoach = s.idcoach inner join teams as t on t.idteam=s.idteam where s.idcompetition=" + comp.idcompetition + " ", new Dictionary<string, string>());

                    var trans = db.BeginTransaction();
                    foreach (var row in standings.Rows)
                    {
                        var tres = db.Query("insert into teams (" +
                            "id,team,logo,value,race,coach,active,platform,competition,league" +
                            "" +
                            ") values (" +
                            "@id, @team, @logo, @value,  @race, @coach, @active, @platform, @competition, @league" +
                            ")", new Dictionary<string, string>()
                            {
                                {"@id",row[standings.Cols["team_origin_uid"]] },
                               {"@team", row[standings.Cols["team_name"]] },
                          {"@logo", row[standings.Cols["logo"]] },
                               {"@value", row[standings.Cols["team_value"]] },
                               
                               {"@coach", row[standings.Cols["coach_name"]] },
                               {"@active", row[standings.Cols["active"]] },
                          {"@race", ((RaceEnum)(int.Parse(row[standings.Cols["idrace"]]))).ToString() },
                                {"@platform",  gSpy3_Common.Database.Format.OriginConversion.ToLegacyGoblinSpy((gSpy3_Common.Database.Format.OriginID)comp.idorigin) },
                        {"@competition", comp.competition_name },
                        {"@league", league.league_name },
                                              });
                        if (tres.Success == false)
                        {
                            throw new Exception("Failed to add entry");
                        }



                        var res = db.Query("insert into leaguestandings (youtube,twitch,teaminfo,compteamname,idcompetition,platform,leaguename,competitionname,coachname,idcoach,activeteam,idteamlisting,logo,idracesimg,idraces,teamname,rank,points,sort,gamesplayed,wins,draws,losses,td,tdopp,tddiff,cas,casopp,concedes,concedesreceived,value,kills,rankoppavg,rankoppmax) values " +
                            "(@youtube,@twitch,@teaminfo,@compteamname,@idcompetition,@platform,@leaguename,@competitionname,@coachname,@idcoach,@activeteam,@idteamlisting,@logo,@idracesimg,@idraces,@teamname,@rank,@points,@sort,@gamesplayed,@wins,@draws,@losses" +
                            ",@td,@tdopp,@tddiff,@cas,@casopp,@concedes,@concedesreceived,@value,@kills,@rankoppavg,@rankoppmax)",
                            new Dictionary<string, string>()
                            {
                                {"@youtube", row[standings.Cols["youtube"]]},
                                {"@twitch", row[standings.Cols["twitch"]]},
                                {"@teaminfo", ""},
                                {"@compteamname", row[standings.Cols["team_origin_uid"]]+row[standings.Cols["team_name"]]+comp.competition_origin_uid}, // ts1.idteamlisting||ts1.teamname||idcompetition
                                {"@idcompetition", ""+comp.competition_origin_uid },
                                {"@platform",  gSpy3_Common.Database.Format.OriginConversion.ToLegacyGoblinSpy((gSpy3_Common.Database.Format.OriginID)comp.idorigin) },
                                {"@leaguename", league.league_name },
                                {"@competitionname", comp.competition_name },
                                {"@coachname", row[standings.Cols["coach_name"]] },

                                {"@idcoach", row[standings.Cols["coach_origin_uid"]] },
                                {"@activeteam", row[standings.Cols["active"]] },
                                {"@idteamlisting", row[standings.Cols["team_origin_uid"]] },
                                {"@logo", row[standings.Cols["logo"]] },
                                {"@idracesimg", row[standings.Cols["idrace"]] },
                                {"@idraces", row[standings.Cols["idrace"]] },
                                {"@teamname", row[standings.Cols["team_name"]] },
                                {"@rank", row[standings.Cols["ranking"]] },
                                {"@points", row[standings.Cols["points"]] },
                                {"@sort", row[standings.Cols["sorting"]] },
                                {"@gamesplayed", row[standings.Cols["gp"]] },
                                {"@wins", row[standings.Cols["wins"]] },
                                {"@draws", row[standings.Cols["draws"]] },
                              {"@losses", row[standings.Cols["losses"]] },
                              {"@td", row[standings.Cols["td"]] },
                              {"@tdopp", row[standings.Cols["td_opp"]] },
                              {"@tddiff", row[standings.Cols["td_diff"]] },
                              {"@cas", row[standings.Cols["cas"]] },
                              {"@casopp", row[standings.Cols["cas_opp"]] },
                              {"@concedes", row[standings.Cols["concedes"]] },
                              {"@concedesreceived", "" },
                              {"@value", row[standings.Cols["team_value"]] },
                              {"@kills", row[standings.Cols["kills"]] },
                              {"@rankoppavg", "" },
                              {"@rankoppmax", "" },
                            }, trans);
                        if (res.Success == false)
                        {
                            throw new Exception("Failed to add entry");
                        }
                        res = db.Query("insert into leaguestandingsbase (youtube,twitch,teaminfo,compteamname,idcompetition,platform,leaguename,competitionname,coachname,idcoach,activeteam,idteamlisting,logo,idracesimg,idraces,teamname,rank,points,sort,gamesplayed,wins,draws,losses,td,tdopp,tddiff,cas,casopp,concedes,concedesreceived,value,kills) values " +
                            "(@youtube,@twitch,@teaminfo,@compteamname,@idcompetition,@platform,@leaguename,@competitionname,@coachname,@idcoach,@activeteam,@idteamlisting,@logo,@idracesimg,@idraces,@teamname,@rank,@points,@sort,@gamesplayed,@wins,@draws,@losses" +
                            ",@td,@tdopp,@tddiff,@cas,@casopp,@concedes,@concedesreceived,@value,@kills)",
                            new Dictionary<string, string>()
                            {
                                {"@youtube", row[standings.Cols["youtube"]]},
                                {"@twitch", row[standings.Cols["twitch"]]},
                                {"@teaminfo", ""},
                                {"@compteamname", row[standings.Cols["team_origin_uid"]]+row[standings.Cols["team_name"]]+comp.competition_origin_uid}, // ts1.idteamlisting||ts1.teamname||idcompetition
                                {"@idcompetition", ""+comp.competition_origin_uid },
                                {"@platform",  gSpy3_Common.Database.Format.OriginConversion.ToLegacyGoblinSpy((gSpy3_Common.Database.Format.OriginID)comp.idorigin) },
                                {"@leaguename", league.league_name },
                                {"@competitionname", comp.competition_name },
                                {"@coachname", row[standings.Cols["coach_name"]] },

                                {"@idcoach", row[standings.Cols["coach_origin_uid"]] },
                                {"@activeteam", row[standings.Cols["active"]] },
                                {"@idteamlisting", row[standings.Cols["team_origin_uid"]] },
                                {"@logo", row[standings.Cols["logo"]] },
                                {"@idracesimg", row[standings.Cols["idrace"]] },
                                {"@idraces", row[standings.Cols["idrace"]] },
                                {"@teamname", row[standings.Cols["team_name"]] },
                                {"@rank", row[standings.Cols["ranking"]] },
                                {"@points", row[standings.Cols["points"]] },
                                {"@sort", row[standings.Cols["sorting"]] },
                                {"@gamesplayed", row[standings.Cols["gp"]] },
                                {"@wins", row[standings.Cols["wins"]] },
                                {"@draws", row[standings.Cols["draws"]] },
                              {"@losses", row[standings.Cols["losses"]] },
                              {"@td", row[standings.Cols["td"]] },
                              {"@tdopp", row[standings.Cols["td_opp"]] },
                              {"@tddiff", row[standings.Cols["td_diff"]] },
                              {"@cas", row[standings.Cols["cas"]] },
                              {"@casopp", row[standings.Cols["cas_opp"]] },
                              {"@concedes", row[standings.Cols["concedes"]] },
                              {"@concedesreceived", "" },
                              {"@value", row[standings.Cols["team_value"]] },
                              {"@kills", row[standings.Cols["kills"]] },
                            }, trans);
                        if (res.Success == false)
                        {
                            throw new Exception("Failed to add entry");
                        }

                    }

                    trans.Commit();
                }
                #endregion

                #region MATCHES
                {
                    log.Info("Adding matches");
                    var res = dbManager.StatsDB.Query("select r.*,t.team_origin_uid as team_origin_uid,t.team_name as team_name,t2.team_name as team_name_opp,"+
                        "ts2.rerolls as rerollsopp,t.idrace as idrace_home,t2.idrace as idrace_away,t.team_origin_uid,t2.team_origin_uid as team_origin_uid_away,m.duration,"+
                        "ts.teammatchstat_origin_uid,ts2.teammatchstat_origin_uid as teammatchstat_origin_uid_away,stm.stadium,m.match_origin_uid,ts.team_value,"+
                        "ts.conceded,ts2.conceded as conceded_away,ts.td,ts.td_opp,"+
                        "ts.blocks_against as sustainedtackles,ts.cash_spent,ts2.cash_spent as cash_spent_away,ts.supporters,ts.possession,ts2.possession as possession_away,ts.occ_own,ts.occ_their,ts2.occ_own as occ_own_away,ts.occ_their,ts2.occ_their as occ_their_away," +
                        "ts.mvp,ts2.mvp as mvp_away,"+
                        "ts.passes as inflictedpasses,ts2.passes as inflictedpasses_away,ts.catches as inflictedcatches,ts2.catches as inflictedcatches_away," +
                        "ts.interceptions as inflictedinterceptions,ts2.interceptions as inflictedinterceptions_away," +
                        "ts.casualties_for as inflictedcasualties,ts2.casualties_for as inflictedcasualties_away," +
                        "ts.blocks_for as inflictedtackles,ts2.blocks_for as inflictedtackles_away," +
                        "ts.kos_for as inflictedko,ts2.kos_for as inflictedko_away," +
                       "ts.breaks_for as inflictedinjuries,ts2.breaks_for as inflictedinjuries_away," +
                        "ts.kills_for as inflicteddead,ts2.kills_for as inflicteddead_away," +
                       "ts.run_meters as inflictedmetersrunning,ts2.run_meters as inflictedmetersrunning_away," +
                       "ts.pass_meters as inflictedmeterspassing,ts2.pass_meters as inflictedmeterspassing_away," +
                     "ts.pushouts as inflictedpushouts,ts2.pushouts as inflictedpushouts_away," +
                     "ts.expulsions as sustainedexpulsions,ts2.expulsions as sustainedexpulsions_away," +
                   "ts.casualties_against as sustainedcasualties,ts2.casualties_against as sustainedcasualties_away," +
                   "ts.kos_against as sustainedko,ts2.kos_against as sustainedko_away," +
                  "ts.breaks_against as sustainedinjuries,ts2.breaks_against as sustainedinjuries_away," +
                  "ts.kills_against as sustaineddead,ts2.kills_against as sustaineddead_away," +
                        "ts2.team_value as team_value_away,ts.rerolls,ts.apo,ts2.apo as apoopp ,ts.asscoaches,ts2.asscoaches as asscoachesopp,ts.cheerleaders,ts2.cheerleaders as cheerleadersopp from resultsview as r inner join matches as m on m.idmatch=r.idmatch " +
                        "inner join stadiums as stm on stm.idstadium=m.idstadium inner join teammatchstats as ts on ts.idmatch=r.idmatch and ts.idteam=r.idteam_home  "+
                        "inner join teammatchstats as ts2 on ts2.idmatch=r.idmatch and ts2.idteam=r.idteam_away inner join teams as t on t.idteam=r.idteam_home "+
                        "inner join teams as t2 on t2.idteam=r.idteam_away where r.idcompetition=" + comp.idcompetition);


                    var trans = db.BeginTransaction();
                    foreach (var row in res.Rows)
                    {
                        for(int i=0; i < 2;i++)
                        {
                            
                            var qres = db.Query("insert into teamstats (" +
                                "idmatch,idteamstats,idteamlisting,idraces,teamname,teamlogo,value,score,cashspentinducements,nbsupporters," +
                                "possessionball,occupationown,occupationtheir,mvp," +
                                "inflictedpasses,inflictedcatches,inflictedinterceptions,inflictedtouchdowns," +
                                "inflictedcasualties,inflictedtackles,inflictedko,inflictedinjuries,inflicteddead," +
                                "inflictedmetersrunning,inflictedmeterspassing,sustainedexpulsions,sustainedko,"+
                                "sustainedinjuries,sustaineddead,sustainedcasualties,active,cheerleaders,apothecary,rerolls,assistantcoaches"+
                                ") values (" +
                                "@idmatch,@idteamstats,@idteamlisting,@idraces,@teamname,@teamlogo,@value,@score,@cashspentinducements,@nbsupporters," +
                                "@possessionball,@occupationown,@occupationtheir,@mvp," +
                                "@inflictedpasses,@inflictedcatches,@inflictedinterceptions,@inflictedtouchdowns," +
                                "@inflictedcasualties,@inflictedtackles,@inflictedko,@inflictedinjuries,@inflicteddead," +
                                "@inflictedmetersrunning,@inflictedmeterspassing,@sustainedexpulsions,@sustainedko," +
                                "@sustainedinjuries,@sustaineddead,@sustainedcasualties,@active,@cheerleaders,@apothecary,@rerolls,@assistantcoaches" +
                                ")"
                                , new Dictionary<string, string>()
                                {
                                    #region row1
                                    {"@idmatch",UInt64.Parse(row[res.Cols["match_origin_uid"]]).ToString("x") },
                                    {"@idteamstats",row[res.Cols[i==0?"teammatchstat_origin_uid":"teammatchstat_origin_uid_away"]] },
                                       {"@idteamlisting",row[i==0?res.Cols["team_origin_uid"]:res.Cols["team_origin_uid_away"]] },
                                      {"@idraces",row[res.Cols[i==0?"idrace_home":"idrace_away"]] },
                                                      {"@teamname",row[res.Cols[i==0?"team_name_home":"team_name_away"]] },
                                                     {"@teamlogo",row[res.Cols[i==0?"logo_home":"logo_away"]] },
                                                          {"@value",row[res.Cols[i==0?"team_value":"team_value_away"]] },
                                                           {"@score",row[res.Cols[i==0?"score_home":"score_away"]] },
                                                         {"@cashspentinducements",row[res.Cols[i==0?"cash_spent":"cash_spent_away"]] },
                                                                {"@nbsupporters",row[res.Cols["supporters"]] },

                                    #endregion
                                    #region row2
{"@possessionball",row[res.Cols[i==0?"possession":"possession_away"]] },
                                                                {"@occupationtheir",row[res.Cols[i==0?"occ_their":"occ_their_away"]] },
                                                                {"@occupationown",row[res.Cols[i==0?"occ_own":"occ_own_away"]] },
                                                                {"@mvp",row[res.Cols[i==0?"mvp":"mvp_away"]] },
                                    #endregion
                                    #region row3
                                                                {"@inflictedpasses",row[res.Cols[i==0?"inflictedpasses":"inflictedpasses_away"]] },
                                                                {"@inflictedcatches",row[res.Cols[i==0?"inflictedcatches":"inflictedcatches_away"]] },
                                                                {"@inflictedinterceptions",row[res.Cols[i==0?"inflictedinterceptions":"inflictedinterceptions_away"]] },
                                                                {"@inflictedtouchdowns",row[res.Cols[i==0?"td":"td_opp"]] },

                                    #endregion
                                    #region row4
                                    {"@inflictedcasualties",row[res.Cols[i==0?"inflictedcasualties":"inflictedcasualties_away"]] },
                                                               {"@inflictedtackles",row[res.Cols[i==0?"inflictedtackles":"inflictedtackles_away"]] },
                                                               {"@inflictedko",row[res.Cols[i==0?"inflictedko":"inflictedko_away"]] },
                                                               {"@inflictedinjuries",row[res.Cols[i==0?"inflictedinjuries":"inflictedinjuries_away"]] },
                                                               {"@inflicteddead",row[res.Cols[i==0?"inflicteddead":"inflicteddead_away"]] },
                                    #endregion
                                    #region row5
                                    {"@inflictedmetersrunning",row[res.Cols[i==0?"inflictedmetersrunning":"inflictedmetersrunning_away"]] },
                                                               {"@inflictedmeterspassing",row[res.Cols[i==0?"inflictedmeterspassing":"inflictedmeterspassing_away"]] },
                                                                  {"@inflictedpushouts",row[res.Cols[i==0?"inflictedpushouts":"inflictedpushouts_away"]] },
                                                                  {"@sustainedexpulsions",row[res.Cols[i==0?"sustainedexpulsions":"sustainedexpulsions_away"]] },
                                                                  {"@sustainedko",row[res.Cols[i==0?"sustainedko":"sustainedko_away"]] },
                                                                     #endregion
                                                                     #region row 6
                                                                  
                                                                    {"@sustainedinjuries",row[res.Cols[i==0?"sustainedinjuries":"sustainedinjuries_away"]] },
                                                                  {"@sustaineddead",row[res.Cols[i==0?"sustaineddead":"sustaineddead_away"]] },
                                                                  {"@sustainedcasualties",row[res.Cols[i==0?"sustainedcasualties":"sustainedcasualties_away"]] },
{"@rerolls",row[res.Cols[i==0?"rerolls":"rerollsopp"]] },
                                                                 {"@apothecary",row[res.Cols[i==0?"apo":"apoopp"]] },
                                                                 {"@cheerleaders",row[res.Cols[i==0?"cheerleaders":"cheerleadersopp"]] },
                                                                 {"@assistantcoaches",row[res.Cols[i==0?"asscoaches":"asscoachesopp"]] },
                                                                  {"@active","1" },
                                                                
                                    #endregion
                                });
                            if (!qres.Success)
                            {
                                throw new Exception("Failed to add entry");
                            }
                        }

            



                        for (int i = 0; i < 2; i++)
                        {
                            bool win = false;
                            bool draw = false;
                            bool loss = false;
                            if (i == 0)
                            {
                                win = int.Parse(row[res.Cols["score_home"]]) > int.Parse(row[res.Cols["score_away"]]);
                                draw = int.Parse(row[res.Cols["score_home"]]) == int.Parse(row[res.Cols["score_away"]]);
                                loss = int.Parse(row[res.Cols["score_home"]]) < int.Parse(row[res.Cols["score_away"]]);
                            }
                            else
                            {
                                win = int.Parse(row[res.Cols["score_home"]]) < int.Parse(row[res.Cols["score_away"]]);
                                draw = int.Parse(row[res.Cols["score_home"]]) == int.Parse(row[res.Cols["score_away"]]);
                                loss = int.Parse(row[res.Cols["score_home"]]) > int.Parse(row[res.Cols["score_away"]]);
                            }
                            var qres = db.Query("insert into teammatches (rerolls,cheerleaders,apothecary,assistantcoaches,rerollsopp,idteamlisting,idteamlistingopp," +
                                                                         "uuid,idcompetition,duration,finished,platform,leaguename,competitionname," +
                                                                         "idteamstats1,idteamstats2,compteamname,idcoach,coach,idraceimg,idrace,logo," +
                                                                         "teamname,score,coachopp,logoopp,teamnameopp,idraceimgopp,idraceopp,scoreopp," +
                                                                         "win,draw,loss,tvdiff,tvdiffabs,conceded,concedereceived,sustainedtouchdowns," +
                                                                         "sustainedtackles,idmatch,idteamstats,idraces,teamlogo,value,cashbeforematch," +
                                                                         "cashspentinducements,nbsupporters,possessionball,occupationtheir,occupationown,mvp," +
                                                                         "inflictedpasses,inflictedcatches,inflictedinterceptions,inflictedtouchdowns," +
                                                                         "inflictedcasualties,inflictedtackles, inflictedko,inflictedinjuries, " +
                                                                         "inflicteddead, inflictedmetersrunning,inflictedmeterspassing," +
                                                                         "inflictedpushouts,sustainedexpulsions,sustainedcasualties, sustainedko," +
                                                                         "sustainedinjuries,sustaineddead) values (" +
                                                                         "@rerolls, @cheerleaders, @apothecary, @assistantcoaches, @rerollsopp, @idteamlisting, @idteamlistingopp, " +
                                                                         "@uuid,@idcompetition,@duration,@finished,@platform,@leaguename,@competitionname," +
                                                                         "@idteamstats1,@idteamstats2,@compteamname,@idcoach,@coach,@idraceimg,@idrace,@logo," +
                                                                         "@teamname,@score,@coachopp,@logoopp,@teamnameopp,@idraceimgopp,@idraceopp,@scoreopp," +
                                                                         "@win,@draw,@loss,@tvdiff,@tvdiffabs,@conceded,@concedereceived,@sustainedtouchdowns," +
                                                                         "@sustainedtackles,@idmatch,@idteamstats,@idraces,@teamlogo,@value,@cashbeforematch," +
                                                                         "@cashspentinducements,@nbsupporters,@possessionball,@occupationtheir,@occupationown,@mvp," +
                                                                         "@inflictedpasses,@inflictedcatches,@inflictedinterceptions,@inflictedtouchdowns," +
                                                                         "@inflictedcasualties,@inflictedtackles, @inflictedko,@inflictedinjuries, " +
                                                                         "@inflicteddead, @inflictedmetersrunning,@inflictedmeterspassing," +
                                                                         "@inflictedpushouts,@sustainedexpulsions,@sustainedcasualties, @sustainedko," +
                                                                         "@sustainedinjuries,@sustaineddead)",
                                                                 new Dictionary<string, string>()
                                                                 {
                                                                 #region row1
                                                                 {"@rerolls",row[res.Cols[i==0?"rerolls":"rerollsopp"]] },
                                                                 {"@apothecary",row[res.Cols[i==0?"apo":"apoopp"]] },
                                                                 {"@cheerleaders",row[res.Cols[i==0?"cheerleaders":"cheerleadersopp"]] },
                                                                 {"@assistantcoaches",row[res.Cols[i==0?"asscoaches":"asscoachesopp"]] },
                                                                 {"@rerollsopp",row[res.Cols[i==0?"rerollsopp":"rerolls"]] },
                                                                 {"@idteamlisting",row[i==0?res.Cols["team_origin_uid"]:res.Cols["team_origin_uid_away"]] },
                                                                 {"@idteamlistingopp",row[i==0?res.Cols["team_origin_uid_away"]:res.Cols["team_origin_uid"]] },
                                                                 #endregion
                                                                 #region row 2
                                                                 {"@uuid",UInt64.Parse(row[res.Cols["match_origin_uid"]]).ToString("x") },
                                                                 {"@idcompetition",""+comp.competition_origin_uid },
                                                                 {"@duration",row[res.Cols["duration"]] },
                                                                 {"@finished",row[res.Cols["finished"]] },
                                                                 {"@platform",gSpy3_Common.Database.Format.OriginConversion.ToLegacyGoblinSpy((gSpy3_Common.Database.Format.OriginID)comp.idorigin) },
                                                                 {"@leaguename",league.league_name },
                                                                 {"@competitionname",comp.competition_name },
                                                                 #endregion
                                                                 #region row 3
                                                                 {"@idteamstats1",row[res.Cols[i==0?"teammatchstat_origin_uid":"teammatchstat_origin_uid_away"]] },
                                                                 {"@idteamstats2",row[res.Cols[i==0?"teammatchstat_origin_uid_away":"teammatchstat_origin_uid"]] },
                                                                 {"@compteamname", row[res.Cols[i==0?"team_origin_uid":"team_origin_uid_away"]]+row[res.Cols[i==0?"team_name":"team_name_away"]]+comp.competition_origin_uid}, // ts1.idteamlisting||ts1.teamname||idcompetition
                                                                 {"@idcoach",row[res.Cols[i==0?"idcoach_home":"idcoach_away"]] },
                                                                 {"@coach",row[res.Cols[i==0?"coach_name_home":"coach_name_away"]] },
                                                                 {"@idraceimg",row[res.Cols[i==0?"idrace_home":"idrace_away"]] },
                                                                 {"@idrace",row[res.Cols[i==0?"idrace_home":"idrace_away"]] },
                                                                 {"@logo",row[res.Cols[i==0?"logo_home":"logo_away"]] },
                                                                 #endregion
                                                                 #region row4
                                                                 {"@teamname",row[res.Cols[i==0?"team_name_home":"team_name_away"]] },
                                                                 {"@score",row[res.Cols[i==0?"score_home":"score_away"]] },
                                                                 {"@coachopp",row[res.Cols[i==0?"coach_name_away":"coach_name_home"]] },
                                                                 {"@logoopp",row[res.Cols[i==0?"logo_away":"logo_home"]] },
                                                                 {"@teamnameopp",row[res.Cols[i==0?"team_name_away":"team_name_home"]] },
                                                                 {"@idraceimgopp",row[res.Cols[i==0?"idrace_away":"idrace_home"]] },
                                                                 {"@idraceopp",row[res.Cols[i==0?"idrace_away":"idrace_home"]] },
                                                                 {"@scoreopp",row[res.Cols[i==0?"score_away":"score_home"]] },
                                                                     #endregion
                                                                 #region row5
                                                                  {"@win",win ? "1":"0"},
                                                                  {"@draw",draw ? "1":"0"},
                                                                  {"@loss",loss ? "1":"0"},
                                                                  {"@tvdiff",""+(i==0?(int.Parse(row[res.Cols["team_value"]])-int.Parse(row[res.Cols["team_value_away"]])):
                                                                                  (int.Parse(row[res.Cols["team_value_away"]])-int.Parse(row[res.Cols["team_value"]]))) },
                                                                  {"@tvdiffabs",""+Math.Abs(int.Parse(row[res.Cols["team_value"]])-int.Parse(row[res.Cols["team_value_away"]])) },
                                                                  {"@conceded",row[res.Cols[i==0?"conceded":"conceded_away"]] },
                                                                  {"@concedereceived",row[res.Cols[i==0?"conceded_away":"conceded"]] },
                                                                  {"@sustainedtouchdowns",row[res.Cols[i==0?"td_opp":"td"]] },
                                                                #endregion
                                                                 #region row 6
                                                                 {"@sustainedtackles",row[res.Cols[i==0?"sustainedtackles":"inflictedtackles"]] },
                                                                 {"@idmatch",UInt64.Parse(row[res.Cols["match_origin_uid"]]).ToString("x") },
                                                                 {"@idteamstats",row[res.Cols[i==0?"teammatchstat_origin_uid":"teammatchstat_origin_uid_away"]] },
                                                                 {"@idraces",row[res.Cols[i==0?"idrace_home":"idrace_away"]] },
                                                                 {"@teamlogo",row[res.Cols[i==0?"logo_home":"logo_away"]] },
                                                                 {"@value",row[res.Cols[i==0?"team_value":"team_value_away"]] },
                                                                 {"@cashbeforematch","" },
                                                                     #endregion
                                                                #region row7
                                                                {"@cashspentinducements",row[res.Cols[i==0?"cash_spent":"cash_spent_away"]] },
                                                                {"@nbsupporters",row[res.Cols["supporters"]] },
                                                                {"@possessionball",row[res.Cols[i==0?"possession":"possession_away"]] },
                                                                {"@occupationtheir",row[res.Cols[i==0?"occ_their":"occ_their_away"]] },
                                                                {"@occupationown",row[res.Cols[i==0?"occ_own":"occ_own_away"]] },
                                                                {"@mvp",row[res.Cols[i==0?"mvp":"mvp_away"]] },
                                                                    #endregion
                                                                    #region row 8
                                                                {"@inflictedpasses",row[res.Cols[i==0?"inflictedpasses":"inflictedpasses_away"]] },
                                                                {"@inflictedcatches",row[res.Cols[i==0?"inflictedcatches":"inflictedcatches_away"]] },
                                                                {"@inflictedinterceptions",row[res.Cols[i==0?"inflictedinterceptions":"inflictedinterceptions_away"]] },
                                                                {"@inflictedtouchdowns",row[res.Cols[i==0?"td":"td_opp"]] },
                                                                #endregion
                                                                     #region row 9
                                                                {"@inflictedcasualties",row[res.Cols[i==0?"inflictedcasualties":"inflictedcasualties_away"]] },
                                                               {"@inflictedtackles",row[res.Cols[i==0?"inflictedtackles":"inflictedtackles_away"]] },
                                                               {"@inflictedko",row[res.Cols[i==0?"inflictedko":"inflictedko_away"]] },
                                                               {"@inflictedinjuries",row[res.Cols[i==0?"inflictedinjuries":"inflictedinjuries_away"]] },
                                                               
                                                                     #endregion
                                                                     #region row 10
                                                               {"@inflicteddead",row[res.Cols[i==0?"inflicteddead":"inflicteddead_away"]] },
                                                               {"@inflictedmetersrunning",row[res.Cols[i==0?"inflictedmetersrunning":"inflictedmetersrunning_away"]] },
                                                               {"@inflictedmeterspassing",row[res.Cols[i==0?"inflictedmeterspassing":"inflictedmeterspassing_away"]] },
                                                                     #endregion
                                                                     #region row 11
                                                                  {"@inflictedpushouts",row[res.Cols[i==0?"inflictedpushouts":"inflictedpushouts_away"]] },
                                                                  {"@sustainedexpulsions",row[res.Cols[i==0?"sustainedexpulsions":"sustainedexpulsions_away"]] },
                                                                  {"@sustainedcasualties",row[res.Cols[i==0?"sustainedcasualties":"sustainedcasualties_away"]] },
                                                                  {"@sustainedko",row[res.Cols[i==0?"sustainedko":"sustainedko_away"]] },
                                                                     #endregion
                                                                     #region row 12
                                                                    {"@sustainedinjuries",row[res.Cols[i==0?"sustainedinjuries":"sustainedinjuries_away"]] },
                                                                  {"@sustaineddead",row[res.Cols[i==0?"sustaineddead":"sustaineddead_away"]] },
                                                               
                                                                     #endregion


                                                                 });
                            if (!qres.Success)
                            {
                                throw new Exception("Failed to add entry");
                            }
                        }
                    }

                    trans.Commit();

                    trans = db.BeginTransaction();
                    foreach (var row in res.Rows)
                    {

                        var qres = db.Query("insert into matches (uuid,idcompetition,leaguename,competitionname,stadium,started,finished,idteamstatshome,idteamstatsaway,platform) " +
                                                                    " values "+
                                                             "(@uuid, @idcompetition, @leaguename, @competitionname, @stadium, @started, @finished, @idteamstatshome, @idteamstatsaway, @platform) " ,
                                                             new Dictionary<string, string>()
                                                             {
                                                                 {"@uuid",UInt64.Parse(row[res.Cols["match_origin_uid"]]).ToString("x") },
                                                                 {"@idcompetition",""+comp.competition_origin_uid },
                                                                 {"@leaguename",league.league_name },
                                                                 {"@competitionname",comp.competition_name },

                                                                 {"@stadium",row[res.Cols["stadium"]] },
                                                                 {"@started",row[res.Cols["started"]] },
                                                                 {"@finished",row[res.Cols["finished"]] },
                                                                 {"@idteamstatshome",row[res.Cols["teammatchstat_origin_uid"]] },
                                                                 {"@idteamstatsaway",row[res.Cols["teammatchstat_origin_uid_away"]] },
                                                                 {"@platform",gSpy3_Common.Database.Format.OriginConversion.ToLegacyGoblinSpy((gSpy3_Common.Database.Format.OriginID)comp.idorigin) },
                                                                 

                                                             });
                        if (!qres.Success)
                        {
                            throw new Exception("Failed to add entry");
                        }
                    }

                    trans.Commit();


                    trans = db.BeginTransaction();
                    foreach (var row in res.Rows)
                    {
                        var qres = db.Query("insert into leaguematches (rerolls,apothecary,cheerleaders,assistantcoaches,tvabs,tvhome,tvaway,uuid,idcompetition,leaguename,competitionname," +
                                                             "stadium,levelstadium,structstadium,started,finished,idteamstatshome,idteamstatsaway,platform,duration,date,hour," +
                                                             "weekday,coachhome,idraceimghome,idteamlistinghome,idracehome,logohome,teamhome,scorehome,coachaway,idraceimgaway," +
                                                             "idraceaway,idteamlistingaway,logoaway,teamaway,scoreaway,conceded,concededhome,concededaway) values " +
                                                             "(@rerolls, @apothecary, @cheerleaders, @assistantcoaches, @tvabs, @tvhome, @tvaway, @uuid, @idcompetition, @leaguename, @competitionname, " +
                                                             "@stadium,@levelstadium,@structstadium,@started,@finished,@idteamstatshome,@idteamstatsaway,@platform,@duration,@date,@hour," +
                                                             "@weekday,@coachhome,@idraceimghome,@idteamlistinghome,@idracehome,@logohome,@teamhome,@scorehome,@coachaway,@idraceimgaway," +
                                                             "@idraceaway,@idteamlistingaway,@logoaway,@teamaway,@scoreaway,@conceded,@concededhome,@concededaway)",
                                                             new Dictionary<string, string>()
                                                             {
                                                                 {"@rerolls",row[res.Cols["rerolls"]] },
                                                                 {"@apothecary",row[res.Cols["apo"]] },
                                                                 {"@cheerleaders",row[res.Cols["cheerleaders"]] },
                                                                 {"@assistantcoaches",row[res.Cols["asscoaches"]] },
                                                                 {"@tvabs",""+Math.Abs(int.Parse(row[res.Cols["team_value"]])-int.Parse(row[res.Cols["team_value_away"]])) },
                                                                 {"@tvhome",row[res.Cols["team_value"]] },
                                                                 {"@tvaway",row[res.Cols["team_value_away"]] },
                                                                 {"@uuid",UInt64.Parse(row[res.Cols["match_origin_uid"]]).ToString("x") },
                                                                 {"@idcompetition",""+comp.competition_origin_uid },
                                                                 {"@leaguename",league.league_name },
                                                                 {"@competitionname",comp.competition_name },

                                                                 {"@stadium",row[res.Cols["stadium"]] },
                                                                 {"@levelstadium",""},
                                                                 {"@structstadium","" },
                                                                 {"@started",row[res.Cols["started"]] },
                                                                 {"@finished",row[res.Cols["finished"]] },
                                                                 {"@idteamstatshome",row[res.Cols["teammatchstat_origin_uid"]] },
                                                                 {"@idteamstatsaway",row[res.Cols["teammatchstat_origin_uid_away"]] },
                                                                 {"@platform",gSpy3_Common.Database.Format.OriginConversion.ToLegacyGoblinSpy((gSpy3_Common.Database.Format.OriginID)comp.idorigin) },
                                                                 {"@duration",row[res.Cols["duration"]] },
                                                                 {"@date","" },
                                                                 {"@hour","" },

                                                                 {"@weekday","" },
                                                                 {"@coachhome",row[res.Cols["coach_name_home"]] },
                                                                 {"@idraceimghome",row[res.Cols["idrace_home"]] },
                                                                 {"@idteamlistinghome",row[res.Cols["team_origin_uid"]] },
                                                                 {"@idracehome",row[res.Cols["idrace_home"]] },
                                                                 {"@logohome",row[res.Cols["logo_home"]] },
                                                                 {"@teamhome",row[res.Cols["team_name_home"]] },
                                                                 {"@scorehome",row[res.Cols["score_home"]] },
                                                                 {"@coachaway",row[res.Cols["coach_name_away"]] },
                                                                 {"@idraceimgaway",row[res.Cols["idrace_away"]] },

                                                                 {"@idraceaway",row[res.Cols["idrace_away"]] },
                                                                 {"@idteamlistingaway",row[res.Cols["team_origin_uid_away"]] },
                                                                 {"@logoaway",row[res.Cols["logo_away"]] },
                                                                 {"@teamaway",row[res.Cols["team_name_away"]] },
                                                                 {"@scoreaway",row[res.Cols["score_away"]] },
                                                                 {"@conceded",row[res.Cols["conceded_home"]] },
                                                                 {"@concededhome",row[res.Cols["conceded_home"]] },
                                                                 {"@concededaway",row[res.Cols["conceded_away"]] },

                                                             });
                        if (!qres.Success)
                        {
                            throw new Exception("Failed to add entry");
                        }
                    }

                    trans.Commit();

                }
                #endregion
                #region COACHES
                {
                    log.Info("Adding coaches");
                    var coachlist = dbManager.StatsDB.Select<gSpy3_Common.Database.Format.Coach>("select c.* from standings as s inner join teams as t on t.idteam=s.idteam inner join coaches as c on c.idcoach=t.idcoach where idcompetition=" + comp.idcompetition+" group by idcoach", new Dictionary<string, string>());
                    var coachOldList = new List<goblinSpy.DataFormat.Coach>();
                    var trans = db.BeginTransaction();
                    foreach (var coach in coachlist)
                    {
                        coachOldList.Add(goblinSpy.DataFormat.Coach.From(coach, league.league_name, comp.competition_name));
                    }
                    db.InsertBatch("coaches", coachOldList, new string[0], trans, maxsize);
                    trans.Commit();
                }
                #endregion
                #region COACHMATCHSTATS
                {
                    log.Info("Adding coachmatchstats");
                    var coachlist = dbManager.StatsDB.Query("select s.idcompetition,ch.coach_origin_uid,ch.idorigin,ch.coach_name,ch.idcoach,c.last_game, c.competition_name,l.idleague, l.league_name, CAST(FORMAT(100*(sum(wins)+sum(draws)/2)/(sum(wins)+sum(draws)+sum(losses)),4) as DECIMAL) as win_pct,sum(wins) as wins, sum(draws) as draws, sum(losses) as losses,sum(concedes) as concedes,sum(gp) as gp  from teams as t inner join standings as s on s.idteam = t.idteam inner join coaches as ch on ch.idcoach = t.idcoach inner join competitions as c on c.idcompetition=s.idcompetition inner join leagues as l on l.idleague = c.idleague where c.idcompetition="+comp.idcompetition+" group by ch.idcoach", new Dictionary<string, string>());
                   
                    var trans = db.BeginTransaction();
                    foreach (var row in coachlist.Rows)
                    {
                        var res =db.Query("insert into coachmatchstats (platform, leaguename, competitionname,activecoach,idcoach,coach,winpct,win,draw,loss,gp,conceded) values (@pl,@ln,@cn,@ac,@id,@c,@wp,@w,@d,@l,@gp,@conc)",
                            new Dictionary<string, string>()
                            {
                                {"@pl", gSpy3_Common.Database.Format.OriginConversion.ToLegacyGoblinSpy((gSpy3_Common.Database.Format.OriginID)int.Parse(row[coachlist.Cols["idorigin"]]))},
                                {"@ln", league.league_name},
                                {"@cn", comp.competition_name},
                                {"@ac", "1"},
                                {"@id", row[coachlist.Cols["coach_origin_uid"]] },
                                {"@c", row[coachlist.Cols["coach_name"]] },
                                {"@wp", row[coachlist.Cols["win_pct"]] },
                                {"@w", row[coachlist.Cols["wins"]] },
                                {"@d", row[coachlist.Cols["draws"]] },
                                {"@l", row[coachlist.Cols["losses"]] },
                                {"@gp", row[coachlist.Cols["gp"]] },
                                {"@conc", row[coachlist.Cols["concedes"]] },
                            }, trans);
                        if (res.Success == false)
                        {
                            throw new Exception("Failed to add entry");
                        }
                       
                    }

                    trans.Commit();
                }
                #endregion
                {
                    // Skipping coachmatchstats since we haven't got it
                }

                

              

            }
        }
    }
}
