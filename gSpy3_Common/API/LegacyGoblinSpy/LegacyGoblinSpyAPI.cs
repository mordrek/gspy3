﻿using gSpy3_Common.API.LegacyGoblinSpy.DataFormat;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace gSpy3_Common.API.LegacyGoblinSpy
{
    public class LegacyGoblinSpyAPI : API
    {
        const string hostWeb = "http://www.mordrek.com:80/goblinSpy/";
        const string hostAPI = "http://www.mordrek.com:8888/";
        const string queryCompetitions = hostWeb + "Overview/competitions.json";
        const string queryExport = hostAPI + "Export?league={league}&comp={comp}&platform={platform}&table={table}";
        const string queryGetDBName = hostAPI + "GetDBName?league={league}&comp={comp}&platform={platform}";
        public StatsPublicLeagues GetCompetitions()
        {
            return base.Get<StatsPublicLeagues>(queryCompetitions);
        }

        public ExportResult GetExport(string platform, string league, string comp, string table)
        {
            var res = base.Get<ExportResult>(queryExport.Replace("{league}", HttpUtility.UrlEncode(league)).Replace("{comp}", HttpUtility.UrlEncode(comp)).Replace("{platform}", platform).Replace("{table}", table));
            res.BuildColMap();
            return res;
        }
        public string GetDBName(string platform, string league, string comp)
        {
            var res = base.GetRaw(queryGetDBName.Replace("{league}", HttpUtility.UrlEncode(league)).Replace("{comp}", HttpUtility.UrlEncode(comp)).Replace("{platform}", platform));
            return res;
        }
        public string GetDBURL(string name)
        {
            return (hostWeb + name).Replace("\\","/");
        }
        public string GetDBPath(string name)
        {
            return ("E:\\Data\\www\\goblinSpy" + name);
        }
    }
}
