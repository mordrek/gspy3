﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.API.Cyanide.DataFormat
{
    public class Team
    {
        public string id { get; set; }
        public string team { get; set; }
        public string logo { get; set; }
        public string value { get; set; }
        public string motto { get; set; }
        public string race { get; set; }
        public int? race_id { get; set; } // Only BB3
        public string coach { get; set; }
        public string coach_id { get; set; } // Only BB3

        public int active { get; set; }

        public string platform { get; set; }
        public string competition { get; set; }
        public string league { get; set; }


   
    }


    public class Teams
    {
        public List<Team> teams;

    }
}
