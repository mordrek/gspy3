﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.API.Cyanide.DataFormat
{
    public class LadderRanking
    {
        public LadderRankingTeam team { get; set; }
        public LadderRankingCoach coach { get; set; }
    }
}
