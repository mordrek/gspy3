﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.API.Cyanide.DataFormat
{
    public class LadderRankingCoach
    {
        public string name { get; set; }
        public string id { get; set; }
        public string lang { get; set; }
        public string country { get; set; }
    }
}
