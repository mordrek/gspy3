﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.API.Cyanide.DataFormat
{
    public class MatchResult
    {
        public string uuid { get; set; } 

        public MatchResultData match { get; set; }

        // Given from bb api json, but without db equivalent
        
        public List<TeamStats_Current> teams { get; set; }

    }

    public class MatchResultData
    {
        public string idleague { get; set; }
        public string idcompetition { get; set; }
        public string leaguename { get; set; }
        public string competitionname { get; set; }
        public string platform { get; set; }
        public string stadium { get; set; }
        public int levelstadium { get; set; }
        public string structstadium { get; set; }
        public string started { get; set; }
        public string finished { get; set; }
        public List<CoachStats> coaches { get; set; }
        public List<MatchResultTeam> teams { get; set; }

 
    }

    public class MatchResultTeam : TeamStats
    {
        public List<MatchResultPlayer> roster { get; set; }

        public int popularity { get; set; }
        public int cash { get; set; }
        public int cheerleaders { get; set; }
        public int balms { get; set; }
        public int apothecary { get; set; }
        public int rerolls { get; set; }
        public int assistantcoaches { get; set; }

    }

    public class CurrentPlayer
    {
        public string name { get; set; }
        public int idteam { get; set; }
        public string idteamstats { get; set; }
        public string puuid { get; set; }
    }

    public class MatchResultPlayer : CurrentPlayer
    {
        public string? id { get; set; }
        public int number { get; set; }
        public string type { get; set; }
        public int level { get; set; }
        public int xp { get; set; }

        // generated stats based on the lists and other info
        public string skills_string { get { return Newtonsoft.Json.JsonConvert.SerializeObject(skills ?? new List<string>()); } }
        public string casualties_state_string { get { return Newtonsoft.Json.JsonConvert.SerializeObject(casualties_state ?? new List<string>()); } }
        public string casualties_sustained_string { get { return Newtonsoft.Json.JsonConvert.SerializeObject(casualties_sustained??new List<string>()); } }


        public int xp_gain { get; set; }
        public int? matchplayed { get; set; } = 1;
        public bool mvp { get; set; }

        public MatchResultPlayerAttributes attributes { get; set; }
        public MatchResultPlayerStats stats { get; set; }
        public List<string> skills { get; set; }


        public List<string> casualties_state { get; set; }
        public List<string> casualties_sustained { get; set; }



        public string uuid { get; set; }

        public int inflictedcasualties { get; set; }
        public int inflictedstuns { get; set; }
        public int inflictedpasses { get; set; }
        public int inflictedmeterspassing { get; set; }
        public int inflictedtackles { get; set; }
        public int inflictedko { get; set; }
        public int inflicteddead { get; set; }
        public int inflictedinterceptions { get; set; }
        public int inflictedpushouts { get; set; }
        public int inflictedcatches { get; set; }
        public int inflictedinjuries { get; set; }
        public int inflictedmetersrunning { get; set; }
        public int inflictedtouchdowns { get; set; }
        public int sustainedinterceptions { get; set; }
        public int sustainedtackles { get; set; }
        public int sustainedinjuries { get; set; }
        public int sustaineddead { get; set; }
        public int sustainedko { get; set; }
        public int sustainedcasualties { get; set; }
        public int sustainedstuns { get; set; }


        public string muuid { get; set; }

      

    }
    public class MatchResultPlayerAttributes
    {
        public int ma { get; set; }
        public int ag { get; set; }
        public int av { get; set; }
        public int st { get; set; }
        public int pa { get; set; }
    }
    public class MatchResultPlayerStats
    {
        public int inflictedcasualties { get; set; }
        public int inflictedstuns { get; set; }
        public int inflictedpasses { get; set; }
        public int inflictedmeterspassing { get; set; }
        public int inflictedtackles { get; set; }
        public int inflictedko { get; set; }
        public int inflicteddead { get; set; }
        public int inflictedinterceptions { get; set; }
        public int inflictedpushouts { get; set; }
        public int inflictedcatches { get; set; }
        public int inflictedinjuries { get; set; }
        public int inflictedmetersrunning { get; set; }
        public int inflictedtouchdowns { get; set; }
        public int sustainedinterceptions { get; set; }
        public int sustainedtackles { get; set; }
        public int sustainedinjuries { get; set; }
        public int sustaineddead { get; set; }
        public int sustainedko { get; set; }
        public int sustainedcasualties { get; set; }
        public int sustainedstuns { get; set; }
        public int sustainedexpulsions { get; set; }

    }
}
