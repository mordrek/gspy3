﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.API.Cyanide.DataFormat
{
    public class Match
    {
        public const string Platform_PC = "pc";
        public const string Platform_XBOX1 = "xb1";
        public const string Platform_PS4 = "ps4";

        public string uuid { get; set; }
        public string idcompetition { get; set; }
        public string leaguename { get; set; }
        public string competitionname { get; set; }
        public string stadium { get; set; }
        public int levelstadium { get; set; }
        public string structstadium { get; set; }
        public string started { get; set; }
        public string finished { get; set; }

        // Given from bb api json, but without db equivalent
        public List<CoachStats> coaches { get; set; }
        public List<TeamStats> teams { get; set; }

    
        // platform
        public string platform { get; set; }



   
     }
}
