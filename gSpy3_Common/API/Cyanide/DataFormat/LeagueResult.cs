﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.API.Cyanide.DataFormat
{
    public class LeagueResult
    {
        public LeagueResultItem league { get; set; }

    }
    public class LeagueResultItem
    {
        public string id { get; set; }
        public string name { get; set; }
    }
}
