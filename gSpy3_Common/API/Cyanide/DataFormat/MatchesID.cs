﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.API.Cyanide.DataFormat
{
    public class MatchesID
    {
        public List<MatchID> matches { get; set; }
    }


    public class MatchID
    {
        public string uuid;     // bb2. bb3?
        public int? id; 
        public string gameId;   // bb3
        public string idleague; // bb3 is string, bb2 is int
    }
}
