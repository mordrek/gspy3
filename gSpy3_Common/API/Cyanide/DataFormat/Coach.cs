﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.API.Cyanide.DataFormat
{
    public class Coach
    {
        public int chiver { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string twitch { get; set; }
        public string youtube { get; set; }
        public string country { get; set; }
        public string lang { get; set; }
        public int active { get; set; }

        public string platform { get; set; }
        public string competition { get; set; }
        public string league { get; set; }

   

     }

    public class Coaches
    {
        public List<Coach> coaches;
    }
  }
