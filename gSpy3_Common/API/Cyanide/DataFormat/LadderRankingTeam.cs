﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.API.Cyanide.DataFormat
{
    public class LadderRankingTeam
    {
        public int rank { get; set; }
        public string score { get; set; }
        public int tv { get; set; }
        public string id { get; set; }
        public int race_id { get; set; }
        public string race { get; set; }
        public string logo { get; set; }
        [JsonProperty("w\\/d\\/\\/l")]
        public string wdl { get; set; }
    }
}
