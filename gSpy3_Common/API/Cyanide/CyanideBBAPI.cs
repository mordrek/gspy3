﻿using gSpy3_Common.API.Cyanide.DataFormat;
using gSpy3_Common.Database.AccessInterface;
using gSpy3_Common.Database.Format;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace gSpy3_Common.API.Cyanide
{
    public class CyanideBBAPI : API 
    {
        const string QueryURL_Matches = "http://web.cyanide-studio.com/ws/bb2/matches/?key={{key}}&league={{league}}&competition={{competition}}&limit=3000&start={{start}}&end={{end}}&platform={{platform}}";
        const string QueryURL_MatchesID = "http://web.cyanide-studio.com/ws/bb2/matches/?key={{key}}&league={{league}}&limit={{limit}}&start={{start}}&end={{end}}&platform={{platform}}&id_only=1";
        const string QueryURL_Match = "https://web.cyanide-studio.com/ws/bb2/match/?key={{key}}&uuid={{uuid}}";
        const string QueryURL_Replay = "https://web.cyanide-studio.com/ws/bb2/replay/?key={{key}}&uuid={{uuid}}&format=zip";
        const string QueryURL_Schedule = "http://web.cyanide-studio.com/ws/bb2/contests/?key={{key}}&league={{league}}&competition={{competition}}&limit=500&platform={{platform}}";
        const string QueryURL_SingleMatch = "http://web.cyanide-studio.com/ws/bb2/match/?key={{key}}&uuid={{uuid}}";
        const string QueryURL_Coaches = "http://web.cyanide-studio.com/ws/bb2/coaches/?key={{key}}&league={{league}}&competition={{competition}}&limit=100000&platform={{platform}}";
        const string QueryURL_Teams = "http://web.cyanide-studio.com/ws/bb2/teams/?key={{key}}&league={{league}}&competition={{competition}}&limit=100000&platform={{platform}}";
        const string QueryURL_League = "http://web.cyanide-studio.com/ws/bb2/league/?key={{key}}&league_name={{league}}&platform={{platform}}";
        const string QueryURL_Competitions = "http://web.cyanide-studio.com/ws/bb2/competitions/?key={{key}}&league_name={{league}}&platform={{platform}}";
        const string QueryURL_Ladder = "http://web.cyanide-studio.com/ws/bb2/ladder/?key={{key}}&league={{league}}&competition_name={{competition}}&platform={{platform}}";

        string key;
        public CyanideBBAPI(string aKey)
        {
            key = aKey;
        }

        private string GetBBURL(string url, OriginID originID)
        {
            string platform = OriginConversion.ToBBAPIPlatform(originID);
            string bbver = OriginConversion.GetBBVer(originID);
            return url
                .Replace("/bb2/",$"/{bbver}/")
                .Replace("{{key}}", key)
                .Replace("{{platform}}", platform);

        }

        public List<string> GetMatchIDs(int module, OriginID platform, string from, string to, List<string> leagues,int limit)
        {
            while (!ResourceManager.UseBB2API(module, 1))
            {
                System.Threading.Thread.Sleep(1000);
            }
            List<string> leagues2 = new List<string>();
            foreach(var l in leagues)
            {
                leagues2.Add(HttpUtility.UrlEncode(l));
            }
            List<string> ret = new List<string>();

            Parallel.ForEach(leagues, (league) =>
            {
                var matchesIDs = Get<MatchesID>(
                    GetBBURL(QueryURL_MatchesID, platform)
                        .Replace("{{league}}", HttpUtility.UrlEncode(league))
                        .Replace("{{limit}}", "" + limit)
                        .Replace("{{start}}", from)
                        .Replace("{{end}}", to));
                if (matchesIDs.matches != null)
                {
                    lock (ret)
                    {
                        foreach (var m in matchesIDs.matches)
                        {
                            if (m.uuid != null)
                            {
                                ret.Add(m.uuid);
                            }
                            else
                            {
                                ret.Add(m.gameId);
                            }
                        }
                    }
                }
            });

            return ret;
            

        }
        /// <summary>
        /// Get match result
        /// </summary>
        /// <param name="module"></param>
        /// <param name="matchId">UUID for bb3, Hex representation for bb2</param>
        /// <param name="platform"></param>
        /// <returns></returns>
        public MatchResult GetMatch(int module,string matchId, OriginID platform)
        {
            while (!ResourceManager.UseBB2API(module, 1))
            {
                System.Threading.Thread.Sleep(1000);
            }
            

            return Get<MatchResult>(
                GetBBURL(QueryURL_Match, platform)
                    .Replace("{{uuid}}", matchId));

        }


        public LeagueResult GetLeague(int module, OriginID platform, string league_name)
        {
            while (!ResourceManager.UseBB2API(module, 1))
            {
                System.Threading.Thread.Sleep(1000);
            }


            return Get<LeagueResult>(
                GetBBURL(QueryURL_League, platform)
                .Replace("{{league}}", HttpUtility.UrlEncode(league_name)));

        }
        public CompetitionsResult GetCompetitions(int module, OriginID platform, string league_name)
        {
            while (!ResourceManager.UseBB2API(module, 1))
            {
                System.Threading.Thread.Sleep(1000);
            }

            return Get<CompetitionsResult>(
                GetBBURL(QueryURL_Competitions, platform)
                    .Replace("{{league}}", HttpUtility.UrlEncode(league_name))
                    +"&limit=1000");

        }
        public CoachResult GetCoaches(int module, OriginID platform, string league_name, string competition_name)
        {
            while (!ResourceManager.UseBB2API(module, 1))
            {
                System.Threading.Thread.Sleep(1000);
            }


            return Get<CoachResult>(
                GetBBURL(QueryURL_Coaches, platform)
                    .Replace("{{competition}}", HttpUtility.UrlEncode(competition_name))
                    .Replace("{{league}}", HttpUtility.UrlEncode(league_name)));

        }

        public ScheduleResult GetSchedule(int module, OriginID platform, string league, string competition)
        {
            while (!ResourceManager.UseBB2API(module, 1))
            {
                System.Threading.Thread.Sleep(1000);
            }


            var res1 = Get<ScheduleResult>(
                GetBBURL(QueryURL_Schedule, platform)
                    .Replace("{{league}}", HttpUtility.UrlEncode(league))
                    .Replace("{{competition}}", HttpUtility.UrlEncode(competition))
                    + "&status=played");
            var res2 = Get<ScheduleResult>(
                GetBBURL(QueryURL_Schedule, platform)
                    .Replace("{{league}}", HttpUtility.UrlEncode(league))
                    .Replace("{{competition}}", HttpUtility.UrlEncode(competition))
                    + "&status=scheduled");
            foreach (var item in res2.upcoming_matches) {
                res1.upcoming_matches.Add(item);
            }
            return res1;

        }

        public TeamsResult GetTeams(int module, OriginID platform, string league_name, string competition_name)
        {
            while (!ResourceManager.UseBB2API(module, 1))
            {
                System.Threading.Thread.Sleep(1000);
            }


            return Get<TeamsResult>(
                GetBBURL(QueryURL_Teams, platform)
                    .Replace("{{league}}", HttpUtility.UrlEncode(league_name))
                    .Replace("{{competition}}", competition_name) );

        }

        public Ladder GetLadder(int module, OriginID originID, string league_name, string competition_name)
        {
            while (!ResourceManager.UseBB2API(module, 1))
            {
                System.Threading.Thread.Sleep(1000);
            }


            return Get<Ladder>(
                GetBBURL(QueryURL_Ladder, originID)
                    .Replace("{{league}}", HttpUtility.UrlEncode(league_name))
                    .Replace("{{competition}}", competition_name));
        }

        public void DownloadMatch(int module, string uuid, string target, OriginID platform)
        {
            string dir = Path.GetDirectoryName(target);
            if (Directory.Exists(dir) == false)
            {
                Directory.CreateDirectory(dir);
            }

            while (!ResourceManager.UseBB2API(module, 1))
            {
                System.Threading.Thread.Sleep(1000);
            }
            string url = GetBBURL(QueryURL_Replay, platform).Replace("{{uuid}}", uuid);

            /*
            Process proc = new Process();
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.StartInfo.FileName = "curl";
            proc.StartInfo.Arguments = "-o \"" + target +" \"" + url + "\"";
            proc.StartInfo.RedirectStandardError = false;
            proc.StartInfo.RedirectStandardOutput = false;
            proc.StartInfo.UseShellExecute = true;
            proc.StartInfo.CreateNoWindow = true;
            proc.Start();
            proc.WaitForExit(5000);
            */
            /*
            using (var wc = new WebClient())
            {
                wc.DownloadFile(url, target);
            }*/

            var client = new HttpClient();

            // Create the HttpContent for the form to be posted.
            // Get the response.
            HttpResponseMessage response = client.GetAsync(url).Result;

            using (var stream = response.Content.ReadAsStreamAsync().Result)
            {
                var fileInfo = new FileInfo(target);
                using (var fileStream = fileInfo.OpenWrite())
                {
                    stream.CopyToAsync(fileStream).Wait();
                }
                stream.Flush();
            }
        }

    }
}
