﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Helpers
{
    public static class DateTimeToDatabaseStringClass
    {
        public static string ToDatabaseUniversalString(this DateTime timestamp)
        {
            return timestamp.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss");
        }
        public static string ToStandardUniversalString(this DateTime timestamp)
        {
            return timestamp.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
        }
    }
}
