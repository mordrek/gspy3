﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Helpers
{
    public class Ranking
    {
        public static double CalculateRank(int wins,int draws,int losses, int concedes)
        {
            int played = wins + draws + losses;
            double CrossPoint = 0.2;
            double Limit = 42;
            //double NonConcedeBonus = (played - concedes) * 0.02;
            double NonConcedeBonus = (played) * 0.02;
            double a = 0.05;
            double Target = 28;
            double x = Math.Log10(a / (1 - CrossPoint)) / Math.Log10(1 - (Target / Limit));

            double winPct = 100.0 * (wins + draws / 2.0) / played;

            double RankPoints = winPct * (CrossPoint + (1 - CrossPoint) * (1 - Math.Pow((1 - Math.Min(Limit, played) / Limit), x))) + NonConcedeBonus;


            return  Math.Round(RankPoints, 2);
            
        }
    }
}
