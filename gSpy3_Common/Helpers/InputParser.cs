﻿using gSpy3_Common.Database;
using gSpy3_Common.Database.Format;
using gSpy3_Common.Helpers;
using Org.BouncyCastle.Asn1.Crmf;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common
{
    public static class InputParser
    {
        public static string ToJSONString(this Dictionary<string, List<CompetitionWrapper>> map)
        {
            string ret = "{";
            bool first = true;
            foreach(var kvp in map)
            {
                if (!first) ret += ",";
                ret += "\""+kvp.Key+"\"=[";
                bool firstArr = true;
                foreach(var wr in kvp.Value)
                {
                    if (!firstArr) ret += ",";
                    ret += "\""+wr.ToString()+ "\"";
                    firstArr = false;
                }
                ret += "]";
                first = false;
            }
            return ret + "}";
            
        }

        public static string ToJSONString(this List<CompetitionWrapper> arr)
        {
            string ret = "[";
            bool firstArr = true;
            foreach (var wr in arr)
            {
                if (firstArr) ret += ",";
                ret += "\"" + wr.ToString() + "\"";
                firstArr = false;
            }
            ret += "]";
            return ret;

        }

        static readonly char[] triggerChars = new char[] { '=', '&', '*', '|', originSeparator };
        static readonly char[] nonCollectionTriggerChars = new char[] { '=', '&', '*', '|' };
        static readonly char originSeparator = '§';
        public static bool IsIDCompetitionValue(string idcompvalue)
        {
            return idcompvalue.IndexOfAny(triggerChars) >= 0 || (idcompvalue.Length > 0 && char.IsNumber(idcompvalue[0])==false);
        }
        public static List<int> GetCompetitionIDListFromIDNameOrAliasList(DatabaseManager dbManager, List<string> ids, out List<CompetitionWrapper> results, out Dictionary<string, List<CompetitionWrapper>> unknowns, bool needOnlyID = true)
        {
            results = new List<CompetitionWrapper>();
            unknowns = new Dictionary<string, List<CompetitionWrapper>>();
            List<int> idlist = new List<int>();
            foreach(var id in ids)
            {
                var amb = new Dictionary<string, List<CompetitionWrapper>>();
                var res = new List<CompetitionWrapper>();
                GetCompetitionIDListFromIDNameOrAlias(dbManager, id, out res, out amb, needOnlyID);
                foreach (var r in res)
                {
                    results.Add(r);
                    idlist.Add(r.idcompetition);
                }
                foreach (var r in amb)
                {
                    unknowns[r.Key] = r.Value;
                }
            }
            return idlist;
        }
        public static string GetCompetitionIDListFromIDNameOrAlias(DatabaseManager dbManager, string id, out List<CompetitionWrapper> results, out Dictionary<string, List<CompetitionWrapper>> unknowns, bool needOnlyID=true)
        {
            string overrideName = null;
            var filters = new Dictionary<string, string>();
            int? overrideLimit = null;
            string overrideOrder = null;
            string overrideOrderCol = null;
            filters["competition_name"] = id;
            List<int> res = new List<int>();
            int idout;
            DateTime t1 = DateTime.Now;
            
            // Single cyanide id
            if(id.StartsWith("pc_") || id.StartsWith("xb1_") || id.StartsWith("ps4_"))
            {
                int splitter = id.IndexOf("_");
                var compRes = dbManager.StatsDB.Query("select idcompetition, competition_name, idorigin from competitions where idorigin=@idorigin and competition_name=@idcomp",
                    new Dictionary<string, string>() { {"@idorigin",""+ (int)OriginConversion.FromLegacyGoblinSpy(id.Substring(0,splitter)) },{ "@idcomp", id.Substring(splitter+1) } });

                unknowns = new Dictionary<string, List<CompetitionWrapper>>();
                results = new List<CompetitionWrapper>();
                foreach (var cid in compRes.Rows)
                {
                    results.Add(new CompetitionWrapper(int.Parse(cid[compRes.Cols["idcompetition"]]), overrideName != null ? overrideName : cid[compRes.Cols["competition_name"]], (OriginID)int.Parse(cid[compRes.Cols["idorigin"]])));
                    res.Add(int.Parse(cid[compRes.Cols["idcompetition"]]));
                }
                if(compRes.Rows.Count > 0)
                {
                    return (compRes.Rows[0][0]);
                }
            }
            // Or single platformname:
            if (id.EndsWith("§pc") || id.EndsWith("§xb1") || id.EndsWith("§ps4"))
            {
                int splitter = id.IndexOf("§");
                var compRes = dbManager.StatsDB.Query("select idcompetition, competition_name, idorigin from competitions where idorigin=@idorigin and competition_name=@compname",
                    new Dictionary<string, string>() { { "@idorigin", "" + (int)OriginConversion.FromLegacyGoblinSpy(id.Substring(splitter+1)) }, { "@compname", id.Substring(0,splitter) } });

                unknowns = new Dictionary<string, List<CompetitionWrapper>>();
                results = new List<CompetitionWrapper>();
                foreach (var cid in compRes.Rows)
                {
                    results.Add(new CompetitionWrapper(int.Parse(cid[compRes.Cols["idcompetition"]]), overrideName != null ? overrideName : cid[compRes.Cols["competition_name"]], (OriginID)int.Parse(cid[compRes.Cols["idorigin"]])));
                    res.Add(int.Parse(cid[compRes.Cols["idcompetition"]]));
                }
                if (compRes.Rows.Count > 0)
                {
                    return (compRes.Rows[0][0]);
                }
            }


            // Is it a simple number?
            if ((needOnlyID || int.TryParse(id,out idout)) && !IsIDCompetitionValue(id))
            {
                if (needOnlyID)
                {
                    results = new List<CompetitionWrapper>() { new CompetitionWrapper() { idcompetition = int.Parse(id) } };
                    unknowns = new Dictionary<string, List<CompetitionWrapper>>();
                    return id;
                }
                else
                {
                    results = new List<CompetitionWrapper>();
                    unknowns = new Dictionary<string, List<CompetitionWrapper>>();
                    var compRes = dbManager.StatsDB.Query("select idcompetition, competition_name, idorigin from competitions where idcompetition=@idcomp", new Dictionary<string, string>() { { "@idcomp", id } });
                    foreach (var cid in compRes.Rows)
                    {
                        results.Add(new CompetitionWrapper(int.Parse(cid[compRes.Cols["idcompetition"]]), overrideName != null ? overrideName : cid[compRes.Cols["competition_name"]], (OriginID)int.Parse(cid[compRes.Cols["idorigin"]])));
                        res.Add(int.Parse(cid[compRes.Cols["idcompetition"]]));
                    }
                    return id;
                }
            }

            // If it's a single name then it could be a collection
            if (id.IndexOfAny(nonCollectionTriggerChars) < 0)
            {
                var collRes = dbManager.StatsDB.Query("select idcollection,idleague,collection_name,filter from collections where collection_name=@col", new Dictionary<string, string>()
                {
                    {"@col",id }
                });
                if (collRes.Rows.Count > 0)
                {
                    var collectionFilters = Newtonsoft.Json.JsonConvert.DeserializeObject<FilteredQueryRequest>(collRes.Rows[0][collRes.Cols["filter"]]);
                    if (collectionFilters.filters != null)
                    {
                        foreach (var kvp in collectionFilters.filters)
                        {
                            filters[kvp.Key] = kvp.Value;
                        }
                    }
                    if (collectionFilters.limit != null)
                    {
                        overrideLimit = collectionFilters.limit;
                    }
                    if (collectionFilters.order != null)
                    {
                        overrideOrder = collectionFilters.order;
                    }
                    if (collectionFilters.ordercol != null)
                    {
                        overrideOrderCol = collectionFilters.ordercol;
                    }
                    overrideName = "@" + collRes.Rows[0][collRes.Cols["collection_name"]];
                }




            } 
            // Perhaps a simplified query with just numbers in an or-list?
            else if(id.Contains("|") && !id.Contains("!") && !id.Contains("&") && !id.Contains("(") && !id.Contains(")") && !id.Contains("="))
            {
                //then we must make a complex query
                var found = new List<CompetitionWrapper>();
                var unknown = new Dictionary<string, List<CompetitionWrapper>>();
                string[] parts = FilteredQueryManager.Split(id, '|');
                foreach (var spart in parts)
                {
                    string part = spart;
                    OriginID idorg = OriginID.BB2_pc;

                    if (part.Contains(originSeparator))
                    {
                        idorg = OriginConversion.FromLegacyGoblinSpy(part.Substring(part.LastIndexOf(originSeparator) + 1));
                        part = part.Substring(0, part.LastIndexOf(originSeparator));
                    }

                    int iId = 0;
                    if (int.TryParse(part, out iId))
                    {
                        res.Add(iId);
                    }
                    else
                    {
                        QueryResult r;

                        if (part.StartsWith("=") == false)
                        {
                            r = dbManager.StatsDB.Query("select idcompetition,competition_name,idorigin from competitions where competition_name like @name", new Dictionary<string, string>()
                        {
                            {"@name","%"+part+"%" }
                        });
                        }
                        else
                        {
                            string name = part.Substring(1);
                            r = dbManager.StatsDB.Query("select idcompetition,competition_name,idorigin from competitions where competition_name=@name", new Dictionary<string, string>()
                        {
                            {"@name",name }
                        });

                        }
                        foreach (var row in r.Rows)
                        {
                            int rowid = int.Parse(r.Rows[0][0]);
                            if(res.Contains(rowid) ==false)
                                res.Add(rowid);
                        }

                    }
                }
                foreach (var cid in res)
                {
                    bool gotIt = false;
                    foreach (var item in found)
                    {
                        if (item.idcompetition == cid)
                        {
                            gotIt = true;
                            break;
                        }
                    }
                    if (gotIt)
                    {
                        continue;
                    }

                    var r = dbManager.StatsDB.Query("select idcompetition,competition_name,idorigin from competitions where idcompetition=" + cid);
                    
                    found.Add(new CompetitionWrapper(cid, r.Rows[0][1], (OriginID)int.Parse(r.Rows[0][2])));
                }

                if (res.Count > 1)
                {
                    overrideName = "@";
                    foreach (var row in found)
                    {
                        if (overrideName != "@") overrideName += ", ";
                        overrideName += row.name;
                    }
                }
                else if (res.Count > 3)
                {
                    overrideName = "@" + id;
                }


                if (overrideName != null)
                {
                    foreach (var row in found)
                    {
                        row.name = overrideName;
                    }
                }

                if (id.Contains("|") == false && found.Count > 1 && overrideName == null) // If we have more than a single row from a single word query and it is NOT a collection, THEN it's ambigous
                {
                    unknown[id] = found;
                }

                DateTime t2 = DateTime.Now;
                System.Diagnostics.Debug.WriteLine((t2 - t1).TotalMilliseconds + "ms");
                unknowns = unknown;
                results = found;
                return string.Join(',', res);
            }


            // Normal case, but we should still try to handle origin specificiation in the name
            {
                string part = id;
                OriginID idorg = OriginID.BB2_pc;

                if (part.Contains(originSeparator))
                {
                    idorg = OriginConversion.FromLegacyGoblinSpy(part.Substring(part.LastIndexOf(originSeparator) + 1));
                    part = part.Substring(0, part.LastIndexOf(originSeparator));
                    filters["competition_name"] = part;
                    filters["idorigin"] = "" + (int)idorg;
                }
            }


            {

                var tmpReqSql = FilteredQueryManager.Get().CreateSQL(new FilteredQueryRequest()
                {
                    id = "comp",
                    filters = filters,
                    limit = overrideLimit,
                    order = overrideOrder,
                    ordercol = overrideOrderCol
                });

                var compRes = dbManager.StatsDB.Query(tmpReqSql.Item1, tmpReqSql.Item2);
                var found = new List<CompetitionWrapper>();
                var unknown = new Dictionary<string, List<CompetitionWrapper>>();


                if (overrideName==null)
                {
                    if (compRes.Rows.Count > 1 && compRes.Rows.Count <= 3)
                    {
                        overrideName = "@";
                        foreach (var row in compRes.Rows)
                        {
                            if (overrideName != "@") overrideName += ", ";
                            overrideName += row[compRes.Cols["competition_name"]];
                        }
                    } else if(compRes.Rows.Count > 1)
                    {
                        overrideName = "@"+id;
                    }
                }
                

                foreach (var cid in compRes.Rows)
                {
                    found.Add(new CompetitionWrapper(int.Parse(cid[compRes.Cols["idcompetition"]]), overrideName != null ? overrideName : cid[compRes.Cols["competition_name"]], (OriginID)int.Parse(cid[compRes.Cols["idorigin"]])));
                    res.Add(int.Parse(cid[compRes.Cols["idcompetition"]]));
                }

                if (id.Contains("|") == false && compRes.Rows.Count > 1 && overrideName == null) // If we have more than a single row from a single word query and it is NOT a collection, THEN it's ambigous
                {
                    unknown[id] = found;
                }


                unknowns = unknown;
                results = found;
                DateTime t2 = DateTime.Now;
                System.Diagnostics.Debug.WriteLine((t2 - t1).TotalMilliseconds + "ms");
            }
            return string.Join(',', res);
        }

        public static int? GetCoachIDFromIDOrName(DatabaseManager dbManager, string id,string compStringList, out Dictionary<string, List<CoachWrapper>> unknowns)
        {
            string overrideName = null;
            var filters = new Dictionary<string, string>();
            int? overrideLimit = null;
            string overrideOrder = null;
            string overrideOrderCol = null;
            filters["competition_name"] = id;
            List<int> res = new List<int>();
            Dictionary<string, List<CoachWrapper>> amb = new Dictionary<string, List<CoachWrapper>>();
            unknowns = amb;

            DateTime t1 = DateTime.Now;
            // Is it a simple number?
            if (id.Length > 0 && char.IsNumber(id[0]))
            {
                unknowns = new Dictionary<string, List<CoachWrapper>>();
                return int.Parse(id);
            }

            var req = new FilteredQueryRequest()
            {
                id = "coach",
                filters = new Dictionary<string, string>()
            };
            req.filters["coach_name"] = id;
            req.limit = 10;
            Dictionary<string, string> replacer = new Dictionary<string, string>();
            if (compStringList != null)
            {
                string repl = compStringList;
                replacer["in:idcompetition"] = repl;
            }
            var sqlC = FilteredQueryManager.Get().CreateSQL(req, replacer);
            var qresC = dbManager.StatsDB.Query(sqlC.Item1, sqlC.Item2, null, 10);

            
            if (qresC.Rows.Count > 1)
            {
                List<CoachWrapper> list = new List<CoachWrapper>();
                foreach (var row in qresC.Rows)
                {
                    list.Add(new CoachWrapper(int.Parse(row[qresC.Cols["idcoach"]]), row[qresC.Cols["coach_name"]], (OriginID)Enum.Parse(typeof(OriginID), row[qresC.Cols["idorigin"]])));
                }
                amb[id]=(list);
            }

            if(qresC.Rows.Count==0)
            {
                return null;
            }

            DateTime t2 = DateTime.Now;
            System.Diagnostics.Debug.WriteLine((t2 - t1).TotalMilliseconds + "ms");

            return (int)int.Parse(qresC.Rows[0][qresC.Cols["idcoach"]]);
        }

        public static int? GetTeamIDFromIDOrName(DatabaseManager dbManager, string id, List<TeamWrapper> comps, out Dictionary<string, List<TeamWrapper>> unknowns)
        {
            string overrideName = null;
            var filters = new Dictionary<string, string>();
            int? overrideLimit = null;
            string overrideOrder = null;
            string overrideOrderCol = null;
            filters["team_name"] = id;
            List<int> res = new List<int>();
            Dictionary<string, List<TeamWrapper>> amb = new Dictionary<string, List<TeamWrapper>>();
            unknowns = amb;

            DateTime t1 = DateTime.Now;
            // Is it a simple number?
            if (id.Length > 0 && char.IsNumber(id[0]))
            {
                unknowns = new Dictionary<string, List<TeamWrapper>>();
                return int.Parse(id);
            }

            var req = new FilteredQueryRequest()
            {
                id = "team",
                filters = new Dictionary<string, string>()
            };
            req.filters["team_name"] = id;
            req.limit = 10;
            Dictionary<string, string> replacer = new Dictionary<string, string>();
            if (comps != null)
            {
                string repl = "";
                foreach (var comp in comps) { if (repl != "") repl += ","; repl += comp.idcompetition; }
                replacer["in:idcompetition"] = repl;
            }
            var sqlC = FilteredQueryManager.Get().CreateSQL(req, replacer);
            var qresC = dbManager.StatsDB.Query(sqlC.Item1, sqlC.Item2, null, 10);


            if (qresC.Rows.Count > 1)
            {
                List<TeamWrapper> list = new List<TeamWrapper>();
                foreach (var row in qresC.Rows)
                {
                    list.Add(new TeamWrapper(int.Parse(row[qresC.Cols["idteam"]]), row[qresC.Cols["team_name"]], (OriginID)Enum.Parse(typeof(OriginID), row[qresC.Cols["idorigin"]])));
                }
                amb[id] = (list);
            }

            if (qresC.Rows.Count == 0)
            {
                return null;
            }

            DateTime t2 = DateTime.Now;
            System.Diagnostics.Debug.WriteLine((t2 - t1).TotalMilliseconds + "ms");

            return (int)int.Parse(qresC.Rows[0][qresC.Cols["idteam"]]);
        }

    }
}
