﻿using gSpy3_Common.API.Cyanide;
using gSpy3_Common.Database;
using gSpy3_Common.Database.AccessInterface;
using gSpy3_Common.Database.Format;
using System;
using System.Collections.Generic;

namespace gSpy3_Common.Helpers
{
    public static class UpdateCompetitionsAndTeams
    {

        public static void UpdateCompetitionsInLeague(DatabaseManager dbManager, CyanideBBAPI cyanide, int module, OriginID origin, int idleague)
        {
            var league = LeagueManager.FindLeagueByID(idleague);
            var comps = cyanide.GetCompetitions(module, origin, league.league_name);
            List<Competition> newComps = new List<Competition>();
            foreach (var compitem in comps.competitions)
            {
                if (compitem.id == null) { continue; }
                if (string.IsNullOrEmpty(compitem.date_created)) { continue; }
                var createdDate = DateTime.Parse(compitem.date_created + "Z");
                if ((DateTime.Now - createdDate) > new TimeSpan(30, 0, 0, 0))
                {
                    continue;
                }

                var foundComp = LeagueManager.FindCompetitionByName(origin, (int)league.idleague, compitem.name);
                if (foundComp == null)
                {

                    newComps.Add(new Competition()
                    {
                        active = 0,
                        competition_name = compitem.name,
                        competition_origin_uid = compitem.id,
                        idorigin = league.idorigin,
                        idleague = (int)league.idleague
                    }
                    );
                }
            }

            LeagueManager.RegisterCompetition(newComps);
            foreach (var compitem in comps.competitions)
            {
                if (compitem.id == null) { continue; }
                if (string.IsNullOrEmpty(compitem.date_created)) { continue; }
                var createdDate = DateTime.Parse(compitem.date_created + "Z");
                if ((DateTime.Now - createdDate) > new TimeSpan(30, 0, 0, 0))
                {
                    continue;
                }

                var comp = LeagueManager.FindCompetitionByOrgID((OriginID)league.idorigin, compitem.id);

                Console.WriteLine("Getting coaches");
                if (comp.num_coaches == 0)
                {
                    var coachresult = cyanide.GetCoaches(module, origin, league.league_name, comp.competition_name);
                    List<Coach> coachlist = new List<Coach>();
                    foreach (var coach in coachresult.coaches)
                    {
                        coachlist.Add(new Coach()
                        {
                            coach_origin_uid = coach.id,
                            idorigin = league.idorigin,
                            coach_name = coach.name,
                            country = coach.country,
                            lang = coach.lang,
                        });
                    }
                    CoachManager.AddIfMissing(coachlist);
                }

                Console.WriteLine("Getting teams");
                if (comp.num_teams == 0)
                {
                    UpdateTeamsInCompetition(dbManager, cyanide,module,  origin, (int)league.idleague, (int)comp.idcompetition);
                }

            }
        }

        public static void UpdateTeamsInCompetition(DatabaseManager dbManager, CyanideBBAPI cyanide, int module, OriginID origin, int idleague, int idcompetition, Transaction trans = null)
        {
            var logoMap = UniqueStringManager.GetLogoMap(null);
            var mottoMap = UniqueStringManager.GetMottoMap(null);
            var raceNameMap = dbManager.StatsDB.GetIndexMap("idrace", new string[] { "race_name" }, "races", "", null, null);

            var league = LeagueManager.FindLeagueByID(idleague);
            var comp = LeagueManager.FindCompetitionByID(idcompetition);


            var coaches = cyanide.GetCoaches(module, origin, league.league_name, comp.competition_name);
            List<Coach> coachlist = new List<Coach>();
            foreach (var coach in coaches.coaches)
            {
                coachlist.Add(new Coach()
                {
                 coach_name = coach.name,
                  coach_origin_uid = coach.id,
                   country = coach.country,
                    idorigin = (int)origin,
                     lang = coach.lang,
                      twitch = coach.twitch,
                       youtube = coach.youtube,
                });
            }
            CoachManager.AddIfMissing(coachlist, trans);

            var teamresult = cyanide.GetTeams(module, origin, league.league_name, comp.competition_name);
            List<Team> teamlist = new List<Team>();
            foreach (var team in teamresult.teams)
            {
                var coach = CoachManager.FindCoachByName(origin, team.coach, trans);
                if (coach == null)
                {
                    // NO coach
                    Console.WriteLine("Missing coach " + team.coach);
                    continue;
                    
                }

                int idrace = 0;
                if (!raceNameMap.TryGetValue(team.race, out idrace) && team.race_id != null)
                {
                    raceNameMap[team.race] = (int)team.race_id;
                    idrace = (int)team.race_id;
                    var res = dbManager.StatsDB.Query("select * from races where idrace=@idrace", new Dictionary<string, string>() { { "@idrace", ""+(int)team.race_id } });
                    if (res.Rows.Count <= 0)
                    {
                        dbManager.StatsDB.Query("insert into races set idrace=@idrace,race_name=@race_name", new Dictionary<string, string>()
                            {
                                { "@idrace", ""+(int)team.race_id },
                                { "@race_name", team.race }
                            });
                    }
                }

                teamlist.Add(new Team()
                {
                    active = 1,
                    idcoach = (int)coach.idcoach,
                    idlogo = UniqueStringManager.GetOrCreateLogo(logoMap, team.logo == null ? "" : team.logo),
                    idmotto = UniqueStringManager.GetOrCreateMotto(mottoMap, team.motto == null ? "" : team.motto),
                    idorigin = (int)origin,
                    idrace = idrace,
                    team_name = team.team,
                    team_origin_uid = team.id
                });
            }
            var added = TeamManager.AddIfMissing(teamlist, trans);

            Console.WriteLine("Getting Standings");
            List<Standing> standingList = new List<Standing>();
            foreach (var team in teamresult.teams)
            {
                var t = TeamManager.FindTeamByOrgID(origin, team.id, trans);
                if (t != null)
                {
                    standingList.Add(new Standing()
                    {
                        idteam = (int)t.idteam,
                        team_origin_uid = t.team_origin_uid,
                        idorigin = (int)origin,
                        idcompetition = (int)comp.idcompetition,
                        active = 1
                    });
                }
                else
                {
                    Console.WriteLine("Missing team " + team.team);
                }
            }
            LeagueManager.AddIfMissing(standingList, (int)comp.idcompetition, trans);
        }
    }
}
