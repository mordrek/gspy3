﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database
{
    public class QueryResult
    {
        public string FileData { get; set; }
        public Dictionary<string,int> Cols { get; set; }
        public List<string[]> Rows { get; set; }
        public int Affected { get; set; }
        public int TimeMs { get; set; }
        public string? Error { get; set; }
        public bool Success { get; set; }

        /// <summary>
        /// Set manually
        /// </summary>
        public int? From { get; set; }
        /// <summary>
        /// Set manually
        /// </summary>
        public int? Limit { get; set; }
        /// <summary>
        /// Set manually
        /// </summary>
        public int? Page { get; set; }




        public QueryResult()
        {
            Cols = new Dictionary<string, int>();
            Rows = new List<string[]>();
        }
    }
}
