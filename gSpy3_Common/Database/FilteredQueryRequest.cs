﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database
{
    [Serializable]
    public class FilteredQueryRequest
    {
        public string id { get; set; }
        public Dictionary<string, string> idmap { get; set; }
        public Dictionary<string, string> filters { get; set; }
        public string ordercol { get; set; }
        public string order { get; set; }
        public int? limit { get; set; }
        public int? from { get; set; }
        public string group { get; set; }
        public string aggr { get; set; }

        public void ApplyDefaults(FilteredQueryRequest req)
        {
            if (req == null) { return; }
            if ((idmap == null || idmap.Count==0) && req.idmap != null) { idmap = req.idmap; }
            if ((filters == null || filters.Count==0) && req.filters != null) { filters = req.filters; }
            if (ordercol == null && req.ordercol != null) { ordercol = req.ordercol; }
            if (order == null && req.order != null) { order = req.order; }
            if (limit == null && req.limit != null) { limit = req.limit; }
            if (from == null && req.from != null) { from = req.from; }
            if (group == null && req.group != null) { group = req.group; }
            if (aggr == null && req.aggr != null) { aggr = req.aggr; }
        }
    }
}
