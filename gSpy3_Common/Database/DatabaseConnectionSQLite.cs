﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;

namespace gSpy3_Common.Database
{
    public class DatabaseConnectionSQLite : DatabaseConnection, IDisposable
    {

        SQLiteConnection connection = null;

        public DatabaseConnectionSQLite(string connectionString)
        {
            connection = new SQLiteConnection(connectionString);
           
        }

        public override ConnectionState State { get { return connection.State; } }
        public override bool IsPasswordExpired { get { return false; } }
        protected override void Open() { connection.Open(); }
        protected override void Close() { connection.Close(); }
        protected override ITransactionObject BeginConnectionTransaction() { return new TransactionObjectSQLite(connection.BeginTransaction()); }

        protected override QueryResult QueryRaw(string sql, System.Collections.Generic.Dictionary<string, string> parameters, Transaction transaction, int? overrideTimeout = null)
        {
            DateTime start = DateTime.Now;


            QueryResult res = new QueryResult();
            try
            {
                using (var command = new SQLiteCommand(sql, connection, transaction == null ? null : (SQLiteTransaction)transaction.TransactionObject))
                {
                    if (parameters != null)
                    {
                        foreach (var kvp in parameters)
                        {
                            command.Parameters.AddWithValue(kvp.Key, kvp.Value);
                        }
                    }

                    using (var reader = command.ExecuteReader())
                    {
                        res.Affected = reader.RecordsAffected;
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            res.Cols[reader.GetName(i)] = i;
                        }
                        while (reader.Read())
                        {
                            string[] row = new string[reader.FieldCount];
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                var obj = reader.GetValue(i);
                                if (obj is float)
                                {
                                    row[i] = ((float)obj).ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                                }
                                else if (obj is double)
                                {
                                    row[i] = ((double)obj).ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                                }
                                else
                                {
                                    row[i] = obj.ToString();
                                }

                            }
                            res.Rows.Add(row);
                        }
                    }
                }
                res.Success = true;
            }
            catch (Exception ex)
            {
                res.Success = false;
                res.Error = ex.ToString();
            }
            res.TimeMs = (int)(DateTime.Now - start).TotalMilliseconds;
            return res;
        }

        public override void Dispose()
        {
            if (connection != null)
            {
                connection.Close();
                connection.Dispose();
                connection = null;
            }
        }

    }

    public class TransactionObjectSQLite : ITransactionObject
    {
        SQLiteTransaction transObject;
        public TransactionObjectSQLite(SQLiteTransaction trans)
        {
            transObject = trans;
        }

        public void Commit()
        {
            transObject.Commit();
        }
        public void Rollback()
        {
            transObject.Rollback();

        }
        public object GetObject()
        {
            return transObject;
        }


    }
}
