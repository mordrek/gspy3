﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    public class UserSchedule : DatabaseItem
    {
        public int? iduser_schedule { get; set; }
        public UInt64? idschedule { get; set; }
        public string userdate { get; set; }
        public string userdate_changed { get; set; }
        public UInt64 schedule_origin_uid { get; set; }
        public int idorigin { get; set; }

        public override string GetIndexColumn()
        {
            return "iduser_schedule";
        }
        public override string GetIndexColumnValue()
        {
            return "" + iduser_schedule;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "idorigin", "idcontest" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + idorigin ,""+ schedule_origin_uid };
        }
    }
}
