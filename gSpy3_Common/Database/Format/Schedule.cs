﻿using gSpy3_Common.API.Cyanide.DataFormat;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    public class Schedule : DatabaseItem
    {
        public UInt64? idschedule { get; set; }
        public int idorigin { get; set; }
        public string schedule_origin_uid { get; set; }
        public int idteam_home { get; set; }
        public int idteam_away { get; set; }
        public int round { get; set; }
        public int idcompetition { get; set; }
        public int? idscheduletype { get; set; }

        public UInt64? idmatch { get; set; }
        public string? match_origin_uid { get; set; }

        public Schedule() { }
        public Schedule(OriginID idorigin, ScheduledMatch s, int idcompetition, int idteam_home, int idteam_away)
        {
            this.idorigin = (int)idorigin;
            schedule_origin_uid = s.contest_id;
            this.idteam_home = idteam_home;
            this.idteam_away = idteam_away;
            round = (int)s.round;
            this.idcompetition = idcompetition;
            this.idscheduletype = (int)ScheduleTypeConversion.FromLegacyGoblinSpy(s.type);
            this.idmatch = null;
            if (string.IsNullOrEmpty(s.match_uuid) == false)
                this.match_origin_uid = OriginConversion.IsBB2(idorigin) ? ""+UInt64.Parse(s.match_uuid, NumberStyles.HexNumber) : s.match_uuid;
            else
                match_origin_uid = null;
        }

        public override string GetIndexColumn()
        {
            return "idschedule";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idschedule;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "idorigin", "schedule_origin_uid" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + idorigin, "" + schedule_origin_uid };
        }
    }
   }
