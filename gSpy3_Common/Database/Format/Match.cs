﻿using gSpy3_Common.API.Cyanide.DataFormat;
using gSpy3_Common.Database.AccessInterface;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
   public class Match : DatabaseItem
    {
        public UInt64? idmatch { get; set; }
        public int idorigin { get; set; }
        public string match_origin_uid { get; set; }
        public int idcompetition { get; set; }
        public string started { get; set; }
        public string finished { get; set; }
        public int duration { get; set; }

        public int idteam_home { get; set; }
        public int idteam_away { get; set; }
        public int idstadium { get; set; }

        public Match() { }
        public Match(OriginID originID, MatchResult m, int idcompetition, int idteam_home, int idteam_away, int idstadium)
        {
            idorigin =(int)originID;
            match_origin_uid = OriginConversion.IsBB2(originID)? ""+ UInt64.Parse(m.uuid, System.Globalization.NumberStyles.HexNumber) : m.uuid;
            started = m.match.started;
            finished = m.match.finished;
            duration = (int)(DateTime.Parse(m.match.finished) - DateTime.Parse(m.match.started)).TotalMinutes;
            this.idcompetition = idcompetition;
            this.idteam_home = idteam_home;
            this.idteam_away = idteam_away;
            this.idstadium = idstadium;

        }



        public override string GetIndexColumn()
        {
            return "idmatch";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idmatch;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "idorigin", "match_origin_uid" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + idorigin, "" + match_origin_uid };
        }
    }
}
