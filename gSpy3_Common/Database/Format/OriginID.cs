﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace gSpy3_Common.Database.Format
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum OriginID
    {
        BB2_pc = 1,
        BB2_xb1 = 2,
        BB2_ps4 = 3,
        BB3_pc = 4,
        BB3_xb = 5,
        BB3_ps = 6,

    }

    public static class OriginConversion
    {
        /// <summary>
        /// Convert legacy (bb2) platform to origin id
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static OriginID FromLegacyGoblinSpy(string platform)
        {
            object origin = null;
            if(Enum.TryParse(typeof(OriginID), platform, out origin))
            {
                return (OriginID)origin;
            }

            switch (platform)
            {
                case "ps4": return OriginID.BB2_ps4;
                case "xb1": return OriginID.BB2_xb1;
            }
            return OriginID.BB2_pc;
        }
        /// <summary>
        /// Return platform component of origin ID
        /// </summary>
        /// <param name="idorigin"></param>
        /// <returns></returns>
        public static string ToLegacyGoblinSpy(OriginID idorigin)
        {
            switch (idorigin)
            {
                case OriginID.BB2_ps4:
                case OriginID.BB3_ps:
                    return "ps4";
                case OriginID.BB2_xb1:
                case OriginID.BB3_xb:
                    return "xb1";
            }
            return "pc";
        }


        public static string ToBBAPIPlatform(OriginID idorigin)
        {
            switch (idorigin)
            {
                case OriginID.BB2_pc:
                case OriginID.BB3_pc:
                    return "pc";
                case OriginID.BB2_ps4:
                case OriginID.BB3_ps:
                    return "playstation";
                case OriginID.BB2_xb1:
                case OriginID.BB3_xb:
                    return "xbox";
            }
            return "pc";
        }

        /// <summary>
        /// Return version string for API based on origin
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetBBVer(OriginID id)
        {
            switch (id)
            {
                case OriginID.BB3_pc:
                case OriginID.BB3_ps:
                case OriginID.BB3_xb:
                    return "bb3";
            }
            return "bb2";
        }
        public static bool IsBB2(OriginID id)
        {
            return GetBBVer(id) == "bb2";
        }
        public static bool IsBB3(OriginID id)
        {
            return GetBBVer(id) == "bb3";
        }
    }


}
