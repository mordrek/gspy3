﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    public class Standing : DatabaseItem
    {
        public int? idstanding { get; set; }
        public int idcompetition { get; set; }
        public int idteam { get; set; }
        public float ranking { get; set; }
        public float points { get; set; }
        public float sorting { get; set; }
        public int active { get; set; }
        public int wins { get; set; }
        public int draws { get; set; }
        public int losses { get; set; }
        public int td { get; set; }
        public int td_opp { get; set; }
        public int td_diff { get; set; }
        public int cas { get; set; }
        public int cas_opp { get; set; }
        public int cas_diff { get; set; }
        public int concedes { get; set; }
        public int team_value { get; set; }
        public int kills { get; set; }
        public int position { get; set; }
        public int gp { get; set; }
        // public int team_origin_uid { get; set; } // Deprecated
        public string team_origin_uid { get; set; }
        public int idorigin { get; set; }
        public override string GetIndexColumn()
        {
            return "idstanding";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idstanding;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "idorigin", "team_origin_uid" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + idorigin, "" + team_origin_uid };
        }
    }
}
