﻿using gSpy3_Common.API.Cyanide.DataFormat;
using System;

namespace gSpy3_Common.Database.Format
{
    public class PlayerMatchStats : DatabaseItem
    {
        public UInt64? idplayermatchstat { get; set; }
        public int? idplayer{ get; set; }
        public UInt64 idmatch { get; set; }
        public int idteam { get; set; }

        public int passes { get; set; }
        public int catches { get; set; }
        public int completions { get; set; }
        public int pushouts { get; set; }
        public int blocks_for { get; set; }
        public int breaks_for { get; set; }
        public int stuns_for { get; set; }
        public int kos_for { get; set; }
        public int casualties_for { get; set; }
        public int kills_for { get; set; }
        public int blocks_against { get; set; }
        public int breaks_against { get; set; }
        public int stuns_against { get; set; }
        public int kos_against { get; set; }
        public int casualties_against { get; set; }
        public int kills_against { get; set; }
        public int turnovers { get; set; }
        public int level { get; set; }
        public int xp { get; set; }
        public int idplayertype { get; set; }
        public int idplayername { get; set; }
        public int idskillcombo { get; set; }
        public int idcasstate { get; set; }
        public int idcassustained { get; set; }
        public int ma { get; set; }
        public int ag { get; set; }
        public int st { get; set; }
        public int av { get; set; }
        public int pa { get; set; }

        public int dodges { get; set; }
        public int xp_gain { get; set; }
        public int pass_meters { get; set; }
        public int run_meters { get; set; }
        public int td { get; set; }
        public int interceptions { get; set; }
        public int deflections { get; set; }
        public int fouls { get; set; }
        public int expulsions { get; set; }
        public int number { get; set; }
        public int idcompetition { get; set; }
        public int game_played { get; set; }
        public int mvp { get; set; }



        public int? sacks { get; set; }
        public int? pickups { get; set; }
        public int? rushes { get; set; }
        public int? boneheads { get; set; }

        public PlayerMatchStats() { }
        public PlayerMatchStats(MatchResultPlayer p,int? idplayer, UInt64 idmatch,int idcompetition, int idteam, int name, int skills, int casstate, int cassus, int playertype)
        {
            this.idplayer = idplayer;
            this.idmatch = idmatch;
            this.idteam = idteam;
            if (p.stats != null)
            {
                passes = p.stats.inflictedpasses;
                catches = p.stats.inflictedcatches;
                pushouts = p.stats.inflictedpushouts;
                stuns_for = p.stats.inflictedstuns;
                blocks_for = p.stats.inflictedtackles;
                breaks_for = p.stats.inflictedinjuries;
                kos_for = p.stats.inflictedko;
                casualties_for = p.stats.inflictedcasualties;
                kills_for = p.stats.inflicteddead;


                if (stuns_for == 0) // Seems stuns are missing from data
                {
                    stuns_for = breaks_for - (kos_for + casualties_for);
                    if (stuns_for < 0) stuns_for = 0;
                }
                if (stuns_against == 0)
                {
                    stuns_against = breaks_against - (kos_against + casualties_against);
                    if (stuns_against < 0) stuns_against = 0;
                }

                blocks_against = p.stats.sustainedtackles;
                breaks_against = p.stats.sustainedinjuries;
                stuns_against = p.stats.sustainedstuns;
                kos_against = p.stats.sustainedko;
                casualties_against = p.stats.sustainedcasualties;
                kills_against = p.stats.sustaineddead;
                expulsions = p.stats.sustainedexpulsions;
            }


            level = p.level;
            xp = p.xp;
            xp_gain = p.xp_gain;

            this.idplayertype = playertype;
            this.idplayername = name;
            this.idskillcombo = skills;
            this.idcasstate = casstate;
            this.idcassustained = cassus;
            ma = p.attributes.ma;
            av = p.attributes.av;
            st = p.attributes.st;
            ag = p.attributes.ag;
            pa = p.attributes.pa;
            if (p.stats != null)
            {
                pass_meters = p.stats.inflictedmeterspassing;
                run_meters = p.stats.inflictedmetersrunning;

                td = p.stats.inflictedtouchdowns;
                interceptions = p.stats.inflictedinterceptions;
            }

            number = p.number;
            mvp = p.mvp?1:0;

            this.idcompetition = idcompetition;
            game_played = p.matchplayed == null ? 1 : (int)p.matchplayed;

    }

        public override string GetIndexColumn()
        {
            return "idplayermatchstat";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idplayermatchstat;
        }
        public override string[] GetInsertIndexColumns()
        {
            throw new Exception("No insert columns exist, only a index column.");
        }
        public override string[] GetInsertIndexColumnValues()
        {
            throw new Exception("No insert columns exist, only a index column.");
        }
    }
}
