﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    public class Motto : DatabaseItem
    {
        public int? idmotto { get; set; }
        public string motto { get; set; }

        public override string GetIndexColumn()
        {
            return "idmotto";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idmotto;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "motto" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + motto };
        }
    }
}
