﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    class PlayerType : DatabaseItem
    {
        public int? idplayertype { get; set; }
        public string playertype_name { get; set; }
        public int ma { get; set; }
        public int st { get; set; }
        public int ag { get; set; }
        public int av { get; set; }
        public int pa { get; set; }
        public int price { get; set; }
        public int max { get; set; }

        public override string GetIndexColumn()
        {
            return "idplayertype";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idplayertype;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "playertype_name" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + playertype_name };
        }
    }
}
