﻿using gSpy3_Common.API.Cyanide.DataFormat;
using Org.BouncyCastle.Utilities.Encoders;
using System.Text;

namespace gSpy3_Common.Database.Format
{
    public class Player : DatabaseItem
    {
        public int? idplayer { get; set; }
        public int idorigin { get; set; }
        // public int? player_origin_uid { get; set; } // Deprecated
        public string player_origin_uid { get; set; }
        public int idplayername { get; set; }
        public int idteam { get; set; }
        public int idleague { get; set; }
        public int ma { get; set; }
        public int ag { get; set; }
        public int av { get; set; }
        public int st { get; set; }
        public int pa { get; set; }
        public int idskillcombo { get; set; }
        public int idcasstate { get; set; }
        public int idcassustained { get; set; }
        public int xp { get; set; }
        public int xp_gain { get; set; }
        public int level { get; set; }
        public int idplayertype { get; set; }
        public int active { get; set; }
        public int number { get; set; }

        public Player() { }
        public Player(PlayerMatchStats p, int idleague) 
        {
            idplayer = p.idplayer;
            idplayername = p.idplayername;
            idteam = p.idteam;
            this.idleague = idleague;
            ma = p.ma;
            ag = p.ag;
            av = p.av;
            st = p.st;
            pa = p.pa;
            idskillcombo = p.idskillcombo;
            idcasstate = p.idcasstate;
            idcassustained = p.idcassustained;
            xp = p.xp;
            xp_gain = p.xp_gain;
            level = p.level;
            idplayertype = p.idplayertype;
            active = p.game_played;
            number = p.number;
    }
        public Player(MatchResultPlayer p, OriginID idorigin, int idleague,int idteam, int name, int skills,int casstate,int cassus,int playertype)
        {
            this.idorigin = (int)idorigin;
            if(p.id.Length > 20 && p.id.Contains("-")==false)
            {
                try
                {
                    p.id = UTF8Encoding.UTF8.GetString(Base64.Decode(p.id));
                }
                catch
                {

                }
            }
            player_origin_uid = p.id;
            idplayername = name;
            this.idteam = idteam;
            this.idleague = idleague;
            ma = p.attributes.ma;
            st = p.attributes.st;
            ag = p.attributes.ag;
            av = p.attributes.av;
            pa = p.attributes.pa;
            this.idskillcombo = skills;
            this.idcasstate = casstate;
            this.idcassustained = cassus;
            xp = p.xp;
            xp_gain = p.xp_gain;
            level = p.level;
            this.idplayertype = playertype;
            active = p.matchplayed == null?1: (int)p.matchplayed;
            number = p.number;
            

        }

        public override string GetIndexColumn()
        {
            return "idplayer";
        }
        public override string GetIndexColumnValue()
        {
            return "" + idplayer;
        }
        public override string[] GetInsertIndexColumns()
        {
            return new string[] { "idorigin", "player_origin_uid" };
        }
        public override string[] GetInsertIndexColumnValues()
        {
            return new string[] { "" + idorigin, "" + player_origin_uid };
        }
    }
}
