﻿using gSpy3_Common.Database.Format;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.AccessInterface
{
    public class ScheduleManager
    {
        /// <summary>
        /// Must be set before using league manager
        /// </summary>
        public static DatabaseManager DBManager = null;

        public static void AddIfMissing(List<Schedule> schedules, Transaction transaction = null)
        {
            if (schedules.Count == 0) return;
            int idorigin = schedules[0].idorigin;
            var trans = transaction == null ? DBManager.StatsDB.BeginTransaction() : transaction;
            DBManager.StatsDB.InsertIfMissing("schedules", schedules, transaction);

        }

        public static Schedule FindSchedule(OriginID idorigin, string schedule_origin_uid)
        {
            var res = DBManager.StatsDB.Select<Schedule>("select * from schedules where idorigin=@idorigin and schedule_origin_uid=@schedule_origin_uid", new Dictionary<string, string>()
            {
                {"@idorigin", ((int)idorigin).ToString() },
                {"@schedule_origin_uid", (schedule_origin_uid).ToString() },

            });
            if (res != null && res.Count > 0)
            {
                return res[0];
            }
            return null;
        }

        public static QueryResult GetUserSchedule(string idcompinput)
        {
            List<CompetitionWrapper> results;
            Dictionary<string, List<CompetitionWrapper>> ambigous;
            var list = InputParser.GetCompetitionIDListFromIDNameOrAlias(DBManager, idcompinput, out results, out ambigous);
            if (ambigous != null && ambigous.Count > 0)
            {
                throw new Exception("Ambigous competition input: " + Environment.NewLine + string.Join(Environment.NewLine, ambigous.ToJSONString()));
            }
            if (results.Count == 0)
            {
                throw new Exception("No matching competition");
            }

            return DBManager.StatsDB.Query("select * from userschedules where idcompetition in (@list) order by userdate desc limit 1000", new Dictionary<string, string>() {
                    {
                        "@list", ""+list
                    } });
        }

        public static QueryResult GetSchedule(string idcompinput)
        {
            List<CompetitionWrapper> results;
            Dictionary<string, List<CompetitionWrapper>> ambigous;
            var list = InputParser.GetCompetitionIDListFromIDNameOrAlias(DBManager, idcompinput, out results, out ambigous);
            if (ambigous != null && ambigous.Count > 0)
            {
                throw new Exception("Ambigous competition input: " + Environment.NewLine + string.Join(Environment.NewLine, ambigous.ToJSONString()));
            }
            if (results.Count == 0)
            {
                throw new Exception("No matching competition");
            }

            return DBManager.StatsDB.Query("select * from schedulesview where idcompetition in (@list) order by round asc limit 1000", new Dictionary<string, string>() {
                    {
                        "@list", ""+list
                    } });
        }
    }
}
