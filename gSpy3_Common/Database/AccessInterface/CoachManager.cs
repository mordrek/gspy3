﻿using gSpy3_Common.Database.Format;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.AccessInterface
{
    public class CoachManager
{       /// <summary>
        /// Must be set before using league manager
        /// </summary>
        public static DatabaseManager DBManager = null;

        public static List<Coach> AddIfMissing(List<Coach> coaches,Transaction transaction = null)
        {
            if (coaches.Count == 0) return new List<Coach>();
            int idorigin = coaches[0].idorigin;
            var trans = transaction;

            var inserted= DBManager.StatsDB.InsertIfMissing("coaches", coaches, "where idorigin=@idorigin", new Dictionary<string, string>() { { "@idorigin", "" + idorigin } }, transaction);
//            var rows = DBManager.StatsDB.Select<Coach>("select * from coaches where idorigin=@idorigin", new Dictionary<string, string>() { { "@idorigin", "" + idorigin } }, transaction);
            foreach(var row in coaches)
            {
                bool wasInserted = false;
                foreach(var c in inserted)
                {
                    if(c.coach_origin_uid == row.coach_origin_uid)
                    {
                        wasInserted = true;
                        break;
                    }
                }
                if (!wasInserted && !string.IsNullOrEmpty(row.coach_origin_uid) && row.coach_origin_uid != "0") 
                { 

                    var res = DBManager.StatsDB.Update<Coach>("coaches", row, $"where coach_origin_uid='{row.coach_origin_uid}' and idorigin=" + idorigin, new string[] { "idcoach", "coach_origin_uid", "idorigin" }, transaction);
                    if (!res.Success)
                    {
                        Console.WriteLine("!?");
                    }
                }
            }
            return inserted;


        }

        public static Dictionary<string, int> GetCoachMap(Transaction transaction = null)
        {
            return DBManager.StatsDB.GetIndexMap<Coach>("coaches","", new Dictionary<string,string>(), transaction);
        }

        public static Coach FindCoachByID(int idcoach, Transaction trans=null)
        {
            var res = DBManager.StatsDB.Select<Coach>("select * from coaches where idcoach=@idcoach", new Dictionary<string, string>()
            {
                {"@idcoach", ((int)idcoach).ToString() },

            },trans);
            if (res != null && res.Count > 0)
            {
                return res[0];
            }
            return null;
        }

        public static Coach FindCoachByOrgID(OriginID origin, string coach_origin_uid, Transaction trans = null)
        {
            var res = DBManager.StatsDB.Select<Coach>("select * from coaches where idorigin=@idorigin and coach_origin_uid=@coach_origin_uid", new Dictionary<string, string>()
            {
                {"@idorigin", ((int)origin).ToString() },
                {"@coach_origin_uid", ""+coach_origin_uid }

            }, trans);
            if (res != null && res.Count > 0)
            {
                return res[0];
            }
            return null;
        }
        public static Coach FindCoachByName(OriginID origin, string coachname, Transaction trans = null)
        {
            var res = DBManager.StatsDB.Select<Coach>("select * from coaches where idorigin=@idorigin and coach_name=@coachname", new Dictionary<string, string>()
            {
                {"@idorigin", ((int)origin).ToString() },
                {"@coachname", ""+coachname }

            }, trans);
            if (res != null && res.Count > 0)
            {
                return res[0];
            }
            return null;
        }

        public static bool AlterMedia(int idcoach, string youtube, string twitch)
        {
            if (string.IsNullOrEmpty(youtube)==false && youtube.ToLowerInvariant().StartsWith("https://www.youtube.com/") == false &&
               youtube.ToLowerInvariant().StartsWith("http://www.youtube.com/") == false &&
               youtube.ToLowerInvariant().StartsWith("https://youtube.com/") == false &&
               youtube.ToLowerInvariant().StartsWith("http://youtube.com/") == false)
                throw new Exception("Must be valid youtube link");
            if (string.IsNullOrEmpty(twitch) == false && twitch.ToLowerInvariant().StartsWith("https://www.twitch.tv/") == false &&
               twitch.ToLowerInvariant().StartsWith("http://www.twitch.tv/") == false &&
               twitch.ToLowerInvariant().StartsWith("https://twitch.tv/") == false &&
               twitch.ToLowerInvariant().StartsWith("http://twitch.tv/") == false)
                throw new Exception("Must be valid twitch link");

            var res = DBManager.StatsDB.Query("update coaches set youtube=@youtube,twitch=@twitch where idcoach=@idcoach", new Dictionary<string, string>()
            {
                {"@youtube", youtube },
                {"@twitch", twitch },
                {"@idcoach", ""+idcoach },
            });
            return res.Success;
        }

    }
}
