﻿using gSpy3_Common.Database.Format;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace gSpy3_Common.Database.AccessInterface
{
    public class MatchManager
    {
        /// <summary>
        /// Must be set before using league manager
        /// </summary>
        public static DatabaseManager DBManager = null;
        public static List<PlayerMatchStats> AddIfMissing(List<PlayerMatchStats> playermatchstats, Transaction transaction = null)
        {
            if (playermatchstats.Count == 0) return new List<PlayerMatchStats>();
            return DBManager.StatsDB.InsertIfMissing("playermatchstats", playermatchstats, transaction);

        }
        public static List<Match> AddIfMissing(List<Match> matches, Transaction transaction = null)
        {
            if (matches.Count == 0) return new List<Match>();
            return DBManager.StatsDB.InsertIfMissing("matches", matches, transaction);

        }
        public static List<TeamMatchStats> AddIfMissing(List<TeamMatchStats> matches, Transaction transaction = null)
        {
            if (matches.Count == 0) return new List<TeamMatchStats>();
            return DBManager.StatsDB.InsertIfMissing("teammatchstats", matches, transaction);

        }
        public static bool DownloadReplay(string idcompname, UInt64 idmatch)
        {
            DBManager.StatsDB.Query("update matches set workstatus=0 where idmatch=" + idmatch);
            return true;
        }
        /// <summary>
        /// Find match
        /// </summary>
        /// <param name="idorigin"></param>
        /// <param name="match_origin_uid">For bb2, this is Uint64 representation (not hex). For bb3 this is guid</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public static Match FindMatch(OriginID idorigin, string match_origin_uid, Transaction trans = null)
        {

            var res = DBManager.StatsDB.Select<Match>("select * from matches where idorigin=@idorigin and match_origin_uid=@match_origin_uid", new Dictionary<string, string>()
            {
                {"@idorigin", ((int)idorigin).ToString() },
                {"@match_origin_uid", match_origin_uid },

            }, trans);
            if (res != null && res.Count > 0)
            {
                return res[0];
            }
            return null;
        }
    }
}
