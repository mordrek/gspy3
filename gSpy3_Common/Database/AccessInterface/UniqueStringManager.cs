﻿using gSpy3_Common.Database.Format;
using Org.BouncyCastle.Utilities.Encoders;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.AccessInterface
{
    public class UniqueStringManager
    {       /// <summary>
            /// Must be set before using league manager
            /// </summary>
        public static DatabaseManager DBManager = null;

        public static Dictionary<string,int> GetLogoMap(Transaction transaction = null)
        {
            return DBManager.StatsDB.GetIndexMap<Logo>("logos", "", new Dictionary<string, string>(), transaction);
        }

        public static int GetOrCreateLogo(Dictionary<string,int> map, string logo, Transaction transaction =null)
        {
            int index = 0;
            if(map.TryGetValue(logo, out index))
            {
                return index;
            }
            DBManager.StatsDB.Query("insert into logos set logo=@logo", new Dictionary<string, string>() { { "@logo", logo } },transaction);
            var res = DBManager.StatsDB.Query("select idlogo from logos where logo=@logo", new Dictionary<string, string>() { { "@logo", logo } }, transaction);
            int ilogo = int.Parse(res.Rows[0][0]);
            map[logo] = ilogo;
            return ilogo;
        }

        public static Dictionary<string, int> GetMottoMap(Transaction transaction = null)
        {
            return DBManager.StatsDB.GetIndexMap<Motto>("mottos", "", new Dictionary<string, string>(), transaction);
        }

        public static int GetOrCreateMotto(Dictionary<string, int> map, string motto, Transaction transaction = null)
        {
            int index = 0;
            if (map.TryGetValue(motto, out index))
            {
                return index;
            }
            DBManager.StatsDB.Query("insert into mottos set motto=@motto", new Dictionary<string, string>() { { "@motto", motto } }, transaction);
            var res = DBManager.StatsDB.Query("select idmotto from mottos where motto=@motto", new Dictionary<string, string>() { { "@motto", motto } }, transaction);
            int iMotto = int.Parse(res.Rows[0][0]);
            map[motto] = iMotto;
            return iMotto;
        }
        public static Dictionary<string, int> GetPlayerNameMap(Transaction transaction = null)
        {
            return DBManager.StatsDB.GetIndexMap<PlayerName>("playernames", "", new Dictionary<string, string>(), transaction);
        }

        public static int GetOrCreatePlayerName(Dictionary<string, int> map, string playername, Transaction transaction = null)
        {
            if (playername.EndsWith("=") || playername.Contains("+") || (playername.Length > 15 && playername.Contains(" ") == false))
            {
                try
                {
                    string decodedName = UTF8Encoding.UTF8.GetString(Base64.Decode(playername));
                    playername = decodedName;
                }
                catch
                {

                }
            }
            if (playername.Length > 50) playername = playername.Substring(0, 50);
            int index = 0;
            if (map.TryGetValue(playername, out index))
            {
                return index;
            }
            DBManager.StatsDB.Query("insert into playernames set player_name=@playername", new Dictionary<string, string>() { { "@playername", playername } }, transaction);
            var res = DBManager.StatsDB.Query("select idplayername from playernames where player_name=@playername", new Dictionary<string, string>() { { "@playername", playername } }, transaction);
            int iMotto = int.Parse(res.Rows[0][0]);
            map[playername] = iMotto;
            return iMotto;
        }

        public static Dictionary<string, int> GetCasComboMap(Transaction transaction = null)
        {
            return DBManager.StatsDB.GetIndexMap<CasualtyCombo>("casualtycombos", "", new Dictionary<string, string>(), transaction);
        }

        public static Dictionary<string, int> GetCasMap(Transaction transaction = null)
        {
            var res = DBManager.StatsDB.Query("select idcasualty, casualty from casualties", new Dictionary<string, string>(), transaction);
            var dic = new Dictionary<string, int>();
            foreach (var row in res.Rows)
            {
                dic[row[1]] = int.Parse(row[0]);
            }
            return dic;
        }

        public static int GetOrCreateCasCombo(Dictionary<string, int> comboMap, Dictionary<string, int> casMap,string casString, Transaction transaction = null)
        {
            casString = casString.Replace("\r\n", "").Replace("\n","");

            int index = 0;
            if (comboMap.TryGetValue(casString, out index))
            {
                return index;
            }
            DBManager.StatsDB.Query("insert into casualtycombos set cas=@cas", new Dictionary<string, string>() { { "@cas", casString } }, transaction);
            var res = DBManager.StatsDB.Query("select idcasualtycombo from casualtycombos where cas=@cas", new Dictionary<string, string>() { { "@cas", casString } }, transaction);
            int iMotto = int.Parse(res.Rows[0][0]);
            comboMap[casString] = iMotto;
            if (casString != "" )
            {
                var casStringMap = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(casString);
                if (casStringMap != null)
                {
                    foreach (var cas in casStringMap)
                    {
                        if (cas != null && casMap.ContainsKey(cas) == false)
                        {
                            DBManager.StatsDB.Query("insert into casualties set casualty=@casualty", new Dictionary<string, string>() { { "@casualty", cas } }, transaction);
                            var res2 = DBManager.StatsDB.Query("select idcasualty from casualties where casualty=@casualty", new Dictionary<string, string>() { { "@casualty", cas } }, transaction);
                            casMap[cas] = int.Parse(res2.Rows[0][0]);
                        }

                    }
                }
            }

            return iMotto;
        }
        public static Dictionary<string, int> GetPlayerTypeMap(Transaction transaction = null)
        {
            return DBManager.StatsDB.GetIndexMap<PlayerType>("playertypes", "", new Dictionary<string, string>(), transaction);
        }

        public static int GetOrCreatePlayerType(Dictionary<string, int> map, string plTypeName, Transaction transaction = null)
        {
            int index = 0;
            if (map.TryGetValue(plTypeName, out index))
            {
                return index;
            }
            DBManager.StatsDB.Query("insert into playertypes set playertype_name=@playertype_name", new Dictionary<string, string>() { { "@playertype_name", plTypeName } }, transaction);
            var res = DBManager.StatsDB.Query("select idplayertype from playertypes where playertype_name=@playertype_name", new Dictionary<string, string>() { { "@playertype_name", plTypeName } }, transaction);
            int iMotto = int.Parse(res.Rows[0][0]);
            map[plTypeName] = iMotto;
            return iMotto;
        }
       
        public static Dictionary<string, int> GetSkillComboMap(Transaction transaction = null)
        {
            return DBManager.StatsDB.GetIndexMap<SkillCombo>("skillcombos", "", new Dictionary<string, string>(), transaction);
        }
        public static Dictionary<string, int> GetSkillMap(Transaction transaction = null)
        {
            var res = DBManager.StatsDB.Query("select idskill, skill from skills", new Dictionary<string, string>(), transaction);
            var dic = new Dictionary<string, int>();
            foreach(var row in res.Rows)
            {
                dic[row[1]] = int.Parse(row[0]);
            }
            return dic;
        }

        public static int GetOrCreateSkillCombo(Dictionary<string, int> map,Dictionary<string,int> skillDictionary, string skCombo, Transaction transaction = null)
        {
            skCombo = skCombo.Replace("\r\n", "");

            int index = 0;
            if (map.TryGetValue(skCombo, out index))
            {
                return index;
            }
            DBManager.StatsDB.Query("insert into skillcombos set skills=@skills", new Dictionary<string, string>() { { "@skills", skCombo } }, transaction);
            var res = DBManager.StatsDB.Query("select idskillcombo from skillcombos where skills=@skills", new Dictionary<string, string>() { { "@skills", skCombo } }, transaction);
            int iMotto = int.Parse(res.Rows[0][0]);
            map[skCombo] = iMotto;
            if (skCombo != "" )
            {
                var skillMap = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(skCombo);
                if (skillMap != null)
                {
                    foreach (var skill in skillMap)
                    {
                        if (skillDictionary.ContainsKey(skill) == false)
                        {
                            DBManager.StatsDB.Query("insert into skills set skill=@skill", new Dictionary<string, string>() { { "@skill", skill } }, transaction);
                            res = DBManager.StatsDB.Query("select idskill from skills where skill=@skill", new Dictionary<string, string>() { { "@skill", skill } }, transaction);
                            skillDictionary[skill] = int.Parse(res.Rows[0][0]);
                        }

                    }
                }
            }
            return iMotto;
        }



        public static Dictionary<string, int> GetStadiumMap(Transaction transaction = null)
        {
            return DBManager.StatsDB.GetIndexMap<Stadium>("stadiums", "", new Dictionary<string, string>(), transaction);
        }

        public static int GetOrCreateStadium(Dictionary<string, int> map, string stadium, Transaction transaction = null)
        {
            if(stadium == null)
            {
                return 0;
            }

            stadium = stadium?.Replace("\r\n", "");

            int index = 0;
            if (map.TryGetValue(stadium, out index))
            {
                return index;
            }
            DBManager.StatsDB.Query("insert into stadiums set stadium=@stadium", new Dictionary<string, string>() { { "@stadium", stadium } }, transaction);
            var res = DBManager.StatsDB.Query("select idstadium from stadiums where stadium=@stadium", new Dictionary<string, string>() { { "@stadium", stadium } }, transaction);
            int iMotto = int.Parse(res.Rows[0][0]);
            map[stadium] = iMotto;
            return iMotto;
        }


    }
}
