﻿using gSpy3_Common.Database.Format;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_Common.Database.AccessInterface
{
    public class TeamManager
    {
        /// <summary>
        /// Must be set before using league manager
        /// </summary>
        public static DatabaseManager DBManager = null;


     

        public static void UpdatePlayersFromStats(int idleague,int idcomp, int idteam, Transaction trans = null)
        {
            try
            {
                var lastGame = DBManager.StatsDB.Query("select idmatch from matches where (idteam_home=@idteam or idteam_away=@idteam) and idcompetition=@idcomp order by finished desc limit 1",
                    new Dictionary<string, string>() { { "@idteam", "" + idteam } , { "@idcomp", "" + idcomp } }, trans);
                if (lastGame.Rows.Count > 0)
                {
                    var idmatch = int.Parse(lastGame.Rows[0][0]);
                    DBManager.StatsDB.Query("update players set active=0 where idteam=@idteam",
                    new Dictionary<string, string>() { { "@idteam", "" + idteam } }, trans);
                    var players = DBManager.StatsDB.Select<PlayerMatchStats>("select pms.* from playermatchstats as pms inner join players as p on p.idplayer=pms.idplayer where p.idteam=@idteam and pms.idmatch=@idmatch", new Dictionary<string, string>()
                    {
                        {"@idteam", ""+idteam }, {"@idmatch", ""+idmatch }
                    }, trans);
                    foreach(var playerStats in players)
                    {
                        var newPlayer= new Player(playerStats, idleague);
                        newPlayer.active = 1;
                        DBManager.StatsDB.Update("players", newPlayer, "where idplayer=" + playerStats.idplayer, new string[] { "idorigin", "player_origin_uid", "idplayer" }, trans);
                    }

                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        public static Team FindTeamByOrgID(OriginID origin, string team_origin_uid, Transaction trans = null)
        {
            var res = DBManager.StatsDB.Select<Team>("select * from teams where idorigin=@idorigin and team_origin_uid=@team_origin_uid", new Dictionary<string, string>()
            {
                {"@idorigin", ((int)origin).ToString() },
                {"@team_origin_uid", (team_origin_uid).ToString()  }

            }, trans);
                if (res != null && res.Count > 0)
                {
                    return res[0];
                }
                return null;
        }
        public static Team FindTeamByID(int idteam, Transaction trans = null)
        {
            var res = DBManager.StatsDB.Select<Team>("select * from teams where idteam=@idteam", new Dictionary<string, string>()
            {
                {"@idteam", ((int)idteam).ToString() },
                

            }, trans);
            if (res != null && res.Count > 0)
            {
                return res[0];
            }
            return null;
        }
        public static Player FindPlayerByOrgID(OriginID origin, string player_origin_uid, Transaction trans = null)
        {
            var res = DBManager.StatsDB.Select<Player>("select * from players where idorigin=@idorigin and player_origin_uid=@player_origin_uid", new Dictionary<string, string>()
            {
                {"@idorigin", ((int)origin).ToString() },
                {"@player_origin_uid", (player_origin_uid).ToString()  }

            }, trans);
            if (res != null && res.Count > 0)
            {
                return res[0];
            }
            return null;
        }
        public static List<Player> AddIfMissing(List<Player> players,Transaction transaction = null)
        {
            if (players.Count > 0)
            {
                // Try as a batch
                return DBManager.StatsDB.InsertIfMissing("players", players, transaction);

            }
            return players;
        }
        public static List<Team> AddIfMissing(List<Team> teams,Transaction transaction = null)
        {
            return DBManager.StatsDB.InsertIfMissing("teams", teams, transaction);
        }
        public static List<Team> AddIfMissing(List<Team> teams, int idcompetition, Transaction transaction = null)
        {
            var map = GetTeamMap(idcompetition, transaction);
            List<Team> addToAll = new List<Team>();
            foreach (var team in teams)
            {
                string id = team.GetInsertIndexID();
                if (!map.ContainsKey(id))
                {
                    // ok, not in the "standings" map.
                    var idteam = DBManager.StatsDB.Find("teams", team, transaction);
                    if (idteam == null)
                    {
                        addToAll.Add(team);
                    }
                }
            }
            if (addToAll.Count > 0) {
                // Try as a batch
                return DBManager.StatsDB.InsertIfMissing("teams", addToAll, transaction);
                
            }
            return addToAll;
        }

        public static Dictionary<string, int> GetTeamMap(int idcompetition, Transaction transaction = null)
        {
            return DBManager.StatsDB.GetIndexMap("t.idteam", new string[] {"t.idorigin", "t.team_origin_uid"}, "standings as s", "inner join teams t on t.idteam=s.idteam where s.idcompetition=@idcompetition", new Dictionary<string, string>() { { "@idcompetition", "" + idcompetition } }, transaction);
        }

    }
}
