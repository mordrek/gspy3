﻿using BB2StatsLib.BB2Replay;
using gSpy3_Common.API.Cyanide;
using gSpy3_Common.Database.Format;
using gSpy3_Common.Helpers;
using Org.BouncyCastle.Bcpg;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace gSpy3_Common.Database.AccessInterface
{
    public static class LeagueManager
    {
        /// <summary>
        /// Must be set before using league manager
        /// </summary>
        public static DatabaseManager DBManager = null;
        /// <summary>
        /// Must be set before using league manager
        /// </summary>
        public static CyanideBBAPI cyanideAPI = null;
        public static int ModuleID; // owner of the api

        public static List<Standing> AddIfMissing(List<Standing> standings, int idcompetition, Transaction transaction = null)
        {
            var map = TeamManager.GetTeamMap(idcompetition, transaction);
            List<Standing> addToAll = new List<Standing>();
            foreach (var standing in standings)
            {
                string id = standing.idorigin + ";" + standing.team_origin_uid;
                if (!map.ContainsKey(id))
                {
                    // ok, not in the "standings" map.
                    addToAll.Add(standing);
                    
                }
            }
            if (addToAll.Count > 0)
            {
                DBManager.StatsDB.InsertIfMissing("standings", addToAll,  transaction);
            }
            return addToAll;
        }

        public static League FindLeagueByID(int idleague, Transaction trans = null)
        {
            var res = DBManager.StatsDB.Select<League>("select * from leagues where idleague=@idleague", new Dictionary<string, string>()
            {
                {"@idleague", ((int)idleague).ToString() },

            }, trans);
            if (res != null && res.Count > 0)
            {
                return res[0];
            }
            return null;
        }
        public static League FindLeagueByName(OriginID origin, string league_name, Transaction trans=null)
        {
            var res = DBManager.StatsDB.Select<League>("select * from leagues where idorigin=@idorigin and league_name=BINARY @league_name", new Dictionary<string, string>()
            {
                {"@idorigin", ((int)origin).ToString() },
                {"@league_name", ""+league_name }

            }, trans);
            if (res != null && res.Count > 0)
            {
                return res[0];
            }
            return null;
        }
        public static League FindLeagueByOrgID(OriginID origin, string league_origin_uid, Transaction trans = null)
        {
            var res = DBManager.StatsDB.Select<League>("select * from leagues where idorigin=@idorigin and league_origin_uid=BINARY @league_origin_uid", new Dictionary<string, string>()
            {
                {"@idorigin", ((int)origin).ToString() },
                {"@league_origin_uid", ""+league_origin_uid }

            }, trans);
            if(res!=null && res.Count > 0)
            {
                return res[0];
            }
            return null;
        }

       
        public static List<CompetitionWrapper> FindCompetitionByIDCompInput(string idcompinput, Transaction trans = null)
        {
            List<CompetitionWrapper> results;
            Dictionary<string, List<CompetitionWrapper>> ambigous;
            InputParser.GetCompetitionIDListFromIDNameOrAlias(DBManager, idcompinput, out results, out ambigous, false);
            return results;
        }

        public static Competition FindCompetitionByName(OriginID origin, int league_id,string competition_name, Transaction trans = null)
        {
            var res = DBManager.StatsDB.Select<Competition>("select * from competitions where idorigin=@idorigin and idleague=@league_id and competition_name=BINARY @competition_name", new Dictionary<string, string>()
            {
                {"@league_id", league_id.ToString() },
                {"@idorigin", ((int)origin).ToString() },
                {"@competition_name", competition_name }

            }, trans);
            if (res != null && res.Count > 0)
            {
                return res[0];
            }
            return null;
        }

        public static Competition FindCompetitionByID(int idcompetition, Transaction trans = null)
        {
            var res = DBManager.StatsDB.Select<Competition>("select * from competitions where idcompetition=@idcompetition", new Dictionary<string, string>()
            {
                {"@idcompetition", (idcompetition).ToString() },

            }, trans);
            if (res != null && res.Count > 0)
            {
                return res[0];
            }
            return null;
        }
        public static Competition FindCompetitionByOrgID(OriginID origin, string competition_origin_uid, Transaction trans = null)
        {
            var res = DBManager.StatsDB.Select<Competition>("select * from competitions where idorigin=@idorigin and competition_origin_uid=@competition_origin_uid", new Dictionary<string, string>()
            {
                {"@idorigin", ((int)origin).ToString() },
                {"@competition_origin_uid", ""+competition_origin_uid }

            }, trans);
            if (res != null && res.Count > 0)
            {
                return res[0];
            }
            return null;
        }

        /// <summary>
        /// Register leagues
        /// </summary>
        public static RegisterResult RegisterLeague(List<League> leagues, Transaction trans=null)
        {
            RegisterResult regResult = new RegisterResult();
            regResult.Leagues = new List<League>();
            Dictionary<string, bool> duplicationMap = new Dictionary<string, bool>();

            int index = 0;
            foreach (var league in leagues)
            {
                bool success = false;
                string error = "";
                try
                {
                    var res = DBManager.StatsDB.Query("select * from leagues where idorigin=@idorigin and league_name=BINARY @league_name",
                            new Dictionary<string, string>()
                        {
                            {"@idorigin", ""+league.idorigin },
                            {"@league_name", league.league_name }
                        });
                    if(res.Rows.Count > 0 && league.collecting>0) // Only update if we choose to activate it.
                    {
                        DBManager.StatsDB.Update("leagues", league, "where idorigin=@idorigin and league_name=BINARY @league_name", new string[] {"idorigin","league_name" }, trans);
                        regResult.Updated++;
                        regResult.Leagues.Add(FindLeagueByName((OriginID)league.idorigin, league.league_name, trans));
                    }
                    else if(res.Rows.Count==0)
                    {
                        string uid = league.idorigin + "#" + league.league_name;
                        if (duplicationMap.ContainsKey(uid) == false && cyanideAPI!=null)
                        {
                            // Must fetch league id from cyanide....
                            var bb2l = cyanideAPI.GetLeague((int)ModuleID, (OriginID)league.idorigin, league.league_name);
                            if (bb2l.league?.name != null && bb2l.league.name.Equals(league.league_name, StringComparison.InvariantCultureIgnoreCase))
                            {
                                league.league_name = bb2l.league.name;
                                league.league_origin_uid = bb2l.league.id;
                                duplicationMap[uid] = true;
                                DBManager.StatsDB.Insert("leagues", league, trans);
                                regResult.Added++;
                                var localLeague = FindLeagueByName((OriginID)league.idorigin, league.league_name, trans);
                                regResult.Leagues.Add(localLeague);


                                UpdateCompetitionsAndTeams.UpdateCompetitionsInLeague(DBManager, cyanideAPI, 0, (OriginID)league.idorigin, (int)localLeague.idleague);
                            }
                            else
                            {
                                regResult.Skipped++;
                            }
                        }
                        else
                        {
                            regResult.Skipped++;
                        }
                    }
                    else
                    {
                        regResult.Skipped++;
                    }

                    
                    success = res.Success;
                    error = res.Error;
                }
                catch(Exception ex)
                {
                    error = ex.ToString();
                }
                if (success == false)
                {
                    throw new Exception(error);
                }
                index++;
            }
     
            return regResult;
        }

        public static bool PerformMaintenance(string idcompinput)
        {
            List<CompetitionWrapper> results;
            Dictionary<string, List<CompetitionWrapper>> ambigous;
            InputParser.GetCompetitionIDListFromIDNameOrAlias(DBManager, idcompinput, out results, out ambigous);
            if (ambigous != null && ambigous.Count > 0)
            {
                throw new Exception("Ambigous competition input: " + Environment.NewLine + string.Join(Environment.NewLine, ambigous.ToJSONString()));
            }
            if (results.Count == 0)
            {
                throw new Exception("No matching competition");
            }
            foreach (var compWrapper in results)
            {
                int idcomp = compWrapper.idcompetition;
                var comp = LeagueManager.FindCompetitionByID(idcomp);
                if (comp == null) throw new Exception("Unable to find the given competition: " + idcomp);

                // 1. Fix where teammatchstats have the wrong idcompetition
                DBManager.StatsDB.Query("update matches as m inner join teammatchstats as tms on tms.idmatch=m.idmatch set tms.idcompetition = @idcomp where m.idcompetition = @idcomp ",
                    new Dictionary<string, string>()
                    {
                        {"@idcomp",""+comp.idcompetition }
                    });
                // 2. Fix where players are not the latest version of the player
                var teamMap = TeamManager.GetTeamMap((int)comp.idcompetition);
                if(teamMap.Count < 10000)
                {
                    foreach(var idteam in teamMap.Values)
                    {
                        TeamManager.UpdatePlayersFromStats(comp.idleague, (int)comp.idcompetition, idteam);
                    }
                }

                

              
            }
            return true;
        }

        public static bool AlterCompetitionSorting(int moduleID,  string idcompinput, string newSorting)
        {
            List<CompetitionWrapper> results;
            Dictionary<string, List<CompetitionWrapper>> ambigous;
            InputParser.GetCompetitionIDListFromIDNameOrAlias(DBManager, idcompinput, out results, out ambigous);
            if (ambigous != null && ambigous.Count > 0)
            {
                throw new Exception("Ambigous competition input: " + Environment.NewLine + string.Join(Environment.NewLine, ambigous.ToJSONString()));
            }
            if (results.Count == 0)
            {
                throw new Exception("No matching competition");
            }
            foreach (var compWrapper in results)
            {
                int idcomp = compWrapper.idcompetition;
                var comp = LeagueManager.FindCompetitionByID(idcomp);
                if (comp == null) throw new Exception("Unable to find the given competition: " + idcomp);

#if DEBUG
#else

                var list = CollectionManager.GetOfficialLeagues();
                if (list.Contains(comp.idleague))
                {
                    throw new Exception("Not allowed to alter the official leagues");
                }
#endif

                var res = DBManager.StatsDB.Query("update competitions set sorting=@sorting where idcompetition=@idcomp", new Dictionary<string, string>()
                    {
                        {"@sorting", newSorting },
                        {"@idcomp", ""+idcomp }
                    });
                if (!res.Success) return false;
                UpdateStandings(moduleID, idcomp);
            }
            return true;
        }

        public static void UpdateStandings(int moduleID, int idcomp, Transaction transaction = null)
        {
            try
            {
                // Update team value
                //DBManager.StatsDB.Query("update standings set team_value=sub.team_value inner join (select tm.team_value from teammatchstats as tm inner join matches as m on m.idmatch=tm.idmatch where tm.idteam=standings.idteam order by m.finished desc limit 1) as sub where idcompetition=" + idcomp, null);
                DBManager.StatsDB.Query("update standings set team_value=(select tm.team_value from teammatchstats as tm inner join matches as m on m.idmatch=tm.idmatch where tm.idteam=standings.idteam and m.idcompetition=\"" + idcomp + "\" order by m.finished desc limit 1)",null, transaction);


                // comp.sort is either null/"", "rank" or "W-D-L,Tie1, Tie2, Tie3 ... "
                var res = DBManager.StatsDB.Query("select sorting,league_name from competitions as c inner join leagues as l on c.idleague =l.idleague where idcompetition=@idcompetition", new Dictionary<string, string>() { { "@idcompetition", "" + idcomp } });
                string sorting = res.Rows[0][0];
                string league_name = res.Rows[0][1];



                var teamstats = DBManager.StatsDB.Query("select s.idteam, sum(tm.win), sum(tm.draw), sum(tm.loss), sum(tm.conceded), sum(tm.td), sum(tm.td_opp), sum(tm.casualties_for), sum(tm.casualties_against), sum(tm.kills_for) from standings as s inner join teammatchstats as tm on tm.idteam=s.idteam and tm.idcompetition=s.idcompetition where s.idcompetition=@idcompetition group by s.idteam", new Dictionary<string, string>() { { "@idcompetition", "" + idcomp } });
                foreach (var row in teamstats.Rows)
                {
                    // Update ranking
                    try
                    {
                        int idteam = int.Parse(row[0]);
                        int wins = int.Parse(row[1].ToString());
                        int draws = int.Parse(row[2].ToString());
                        int losses = int.Parse(row[3].ToString());
                        int concedes = int.Parse(row[4].ToString());
                        int played = wins + draws + losses;
                        int td = int.Parse(row[5].ToString());
                        int td_opp = int.Parse(row[6].ToString());
                        int cas = int.Parse(row[7].ToString());
                        int cas_opp = int.Parse(row[8].ToString());
                        int kills = int.Parse(row[9].ToString());

                        DBManager.StatsDB.Query("update standings set wins=@wins, draws=@draws, losses=@losses, concedes=@concedes, td = @td, td_opp=@td_opp, td_diff=@td_diff, cas=@cas,cas_opp=@cas_opp, cas_diff=@cas_diff, kills=@kills, gp=@gp, points=(3*@wins+@draws)  where idcompetition=" + idcomp + " and idteam=" + (idteam),
                            new Dictionary<string, string>()
                            {
                            { "@wins", ""+wins},
                            { "@draws", ""+draws},
                            { "@losses", ""+losses},
                            { "@concedes", ""+concedes},
                            { "@td", ""+td},
                            { "@td_opp", ""+td_opp},
                            { "@td_diff", ""+(td-td_opp)},
                            { "@cas", ""+cas},
                            { "@cas_opp", ""+cas_opp},
                            { "@cas_diff", ""+(cas-cas_opp)},
                            { "@kills", ""+kills},
                            { "@gp", ""+played}
                            }, transaction);

                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex);
                    }
                }

                // Update sorting value
                if (!(league_name.Contains("Cabalvision Official") && !(league_name.Contains("Official League")) || string.IsNullOrEmpty(sorting) || sorting == "rank"))
                {
                    string sort = sorting;
                    string[] splits = sort.Split(',');
                    if (splits.Length == 0)
                        return;

                    string WDL = splits[0];
                    string[] WDLSplits = WDL.Split('-');
                    if (WDLSplits.Length < 3) return;

                    int W = int.Parse(WDLSplits[0].Trim());
                    int D = int.Parse(WDLSplits[1].Trim());
                    int L = int.Parse(WDLSplits[2].Trim());

                    string pointsValue = "(wins * " + W + " + draws * " + D + " + losses * " + L + ")";
                    string sortValue = pointsValue + "* 10000000000 + 5000000000";
                    string multiplier = "10000000";
                    string h2h = null;
                    for (int i = 1; i < splits.Length; i++)
                    {
                        try
                        {
                            switch (splits[i].Trim())
                            {
                                case "TD": sortValue += "+td*" + multiplier + ".0"; break;
                                case "TDD": sortValue += "+td_diff*" + multiplier + ".0"; break;
                                case "CAS": sortValue += "+cas*" + multiplier + ".0"; break;
                                case "CASD": sortValue += "+(CAST(cas as FLOAT)-CAST(cas_opp as FLOAT))*" + multiplier + ".0"; break;
                                case "KILL": sortValue += "+kills*" + multiplier + ".0"; break;
                                case "CW": sortValue += "+(CAST(cas as FLOAT)/2+CAST(td as FLOAT))*" + multiplier + ".0"; break;
                                case "H2H": sortValue += ""; h2h = multiplier + ".0"; break;
                            }
                            multiplier = multiplier.Substring(0, multiplier.Length - 3);
                        }
                        catch { }
                    }
                    Console.WriteLine(sortValue);
                    DBManager.StatsDB.Query("update standings set points=" + pointsValue + "  where idcompetition=" + idcomp, null, transaction);
                    DBManager.StatsDB.Query("update standings set sorting=(" + sortValue + ")  where idcompetition=" + idcomp, null, transaction);

                    if (h2h != null)
                    {
                        var resGroups = DBManager.StatsDB.Query("select points,count(points) from standings  where idcompetition=" + idcomp + " group by points", null);
                        foreach (var resGroupsRow in resGroups.Rows)
                        {
                            try
                            {
                                int points = int.Parse(resGroupsRow[0]);
                                int count = int.Parse(resGroupsRow[1]);

                                if (count > 1)
                                {
                                    var teamsInvolved = DBManager.StatsDB.Query("select idteam,sorting from standings where points=" + points + " and idcompetition=" + idcomp, null);
                                    // Go through the teams and check wins over the others, adding to the h2hpoints
                                    foreach (var teamrow in teamsInvolved.Rows)
                                    {
                                        int h2hPoints = 30;
                                        var gamesPlayed = DBManager.StatsDB.Query("select idteam,idteam_home,idteam_away,home, win, draw , loss from teammatchstats as tm inner join matches as m on m.idmatch=tm.idmatch where idteam=" + teamrow[0] + " and m.idcompetition=" + idcomp, null);
                                        foreach (var gamerow in gamesPlayed.Rows)
                                        {
                                            var idteam = gamerow[0];
                                            var idteam_home = gamerow[1];
                                            var idteam_away = gamerow[2];
                                            var home = gamerow[3];
                                            var win = gamerow[4];
                                            var draw = gamerow[5];
                                            var loss = gamerow[6];
                                            string gameopp = home == "1" ? idteam_away : idteam_home;
                                            foreach (var opprow in teamsInvolved.Rows)
                                            {
                                                if (opprow[0] == gameopp)
                                                {
                                                    h2hPoints += W * int.Parse(win) + D * int.Parse(draw) + L * int.Parse(loss);
                                                    if (h2hPoints < 0) h2hPoints = 0;

                                                    break;
                                                }
                                            }
                                        }
                                        string teamsort = teamrow[1];
                                        DBManager.StatsDB.Query("update standings set sorting=(" + teamsort + "+(" + h2h + "*" + h2hPoints + ")) where idteam=" + teamrow[0] + " and idcompetition=" + idcomp, null, transaction);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.ToString());
                            }
                        }
                    }


                    // Update position
                    DateTime t1 = DateTime.Now;
                    #region Updating standings position #
                    var stres = DBManager.StatsDB.Query("select idstanding,ROW_NUMBER() OVER (ORDER BY sorting desc) as position from gspy3.standings where idcompetition=" + idcomp);
                    /* var sb = new StringBuilder();
                     sb.Append("insert into standings (idstanding, position) values ");
                     bool first = true;
                     foreach (var row in stres.Rows)
                     {
                         if (!first) sb.Append(",");
                         first = false;
                         sb.Append("(");
                         sb.Append(row[0]);
                         sb.Append(",");
                         sb.Append(row[1]);
                         sb.Append(")");
                     }
                     sb.Append(" on duplicate key update position= values(position)");*/
                    foreach (var row in stres.Rows)
                    {
                        DBManager.StatsDB.Query("update standings set position=" + row[stres.Cols["position"]] + " where idstanding=" + row[stres.Cols["idstanding"]], null, transaction);
                    }

                    int timems = (int)(DateTime.Now - t1).TotalMilliseconds;
                    Console.WriteLine("position update in " + timems + " ms");

                }
                else // Updating ranking base competitions here by using /ladder/
                {
                    DateTime t1 = DateTime.Now;


                    var comp = LeagueManager.FindCompetitionByID(idcomp);
                    if (comp != null)
                    {
                        var ladder = cyanideAPI.GetLadder(moduleID, (OriginID)comp.idorigin, league_name, comp.competition_name);
                        foreach(var item in ladder.ranking)
                        {
                            var team = TeamManager.FindTeamByOrgID((OriginID)comp.idorigin, item.team.id);
                            if (team != null)
                            {
                                DBManager.StatsDB.Query("update standings set position="+item.team.rank+", sorting=" + item.team.score + ", ranking=" + item.team.score + "  where idteam=" + team.idteam + " and idcompetition=" + idcomp, null, transaction);
                            }
                            else
                            {
                                // Skip for now, they will appear later when they are part of a match
                            }
                        }

                    }

                    int timems = (int)(DateTime.Now - t1).TotalMilliseconds;
                    Console.WriteLine("position update in " + timems + " ms");
                }



              

                #endregion
            }catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
        }


        /// <summary>
        /// Register competitions of an existing league
        /// </summary>
        public static RegisterResult RegisterCompetition(List<Competition> competitions, Transaction trans = null)
        {
            RegisterResult regResult = new RegisterResult();
            Dictionary<string, bool> duplicationMap = new Dictionary<string, bool>();

            int index = 0;
            foreach (var comp in competitions)
            {
                Console.WriteLine("Register competition " + comp.competition_name);
                bool success = false;
                string error = "";
                try
                {
                    var res = DBManager.StatsDB.Query("select * from competitions where idorigin=@idorigin and competition_name=BINARY @competition_name and idleague=@idleague",
                            new Dictionary<string, string>()
                        {
                            {"@idorigin", ""+comp.idorigin },
                            {"@competition_name", comp.competition_name },
                            {"@idleague", ""+comp.idleague }
                        }, trans);
                    if (res.Rows.Count > 0 && comp.active > 0) // Already have it
                    {
                        string idcomp = res.Rows[0][res.Cols["idcompetition"]];
                        Console.WriteLine("Already have it, just update it");
                        if(comp.last_game == "")
                        {
                            comp.last_game = null;
                        }
                        var updateRes = DBManager.StatsDB.Update("competitions", comp, "where idcompetition="+ idcomp +"", new string[] { "idleague","idorigin", "competition_name"}, trans);
                        if (updateRes.Success == false)
                        {
                            success = false;
                            throw new Exception(updateRes.Error);
                        }
                        regResult.Updated++;
                    }
                    else if(res.Rows.Count == 0)
                    {
                        Console.WriteLine("New one, is it in map?");
                        string uid = comp.idorigin + "#" + comp.competition_name;
                        if (duplicationMap.ContainsKey(uid) == false) 
                        {
                            Console.WriteLine("New one, let's add it");
                            duplicationMap[uid] = true;

                            var insertRes = DBManager.StatsDB.Insert("competitions", comp, trans);
                            if (insertRes.Success == false)
                            {
                                Console.WriteLine("Failed to insert");
                                success = false;
                                throw new Exception(insertRes.Error);
                            }

                            regResult.Added++;
                        }
                        else
                        {
                            Console.WriteLine("Already in map, skip it");
                            regResult.Skipped++;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Skipped due to already existing, and we're not activating");
                        regResult.Skipped++;
                    }


                    success = res.Success;
                    error = res.Error;
                }
                catch (Exception ex)
                {
                    error = ex.ToString();
                    Console.WriteLine("Error: "+ex.ToString());
                }
                if (success == false)
                {
                    throw new Exception(error);
                }
                index++;
            }
     

            return regResult;
        }


        public static bool UpdateSchedules(int idorigin, List<UserSchedule> newSchedules)
        {
            bool ok = true;
            foreach(var sched in newSchedules)
            {
                if (string.IsNullOrEmpty(sched.userdate))
                {
                    var res = DBManager.StatsDB.Query("delete from userschedules  where schedule_origin_uid=@id", new Dictionary<string, string>()
                    {
                        {"@id",""+(UInt64)sched.schedule_origin_uid},
                    });
                    ok &= (res.Success);

                }
                else
                {
                    sched.idschedule = 0;
                    var inserted = DBManager.StatsDB.InsertIfMissing("userschedules", new List<UserSchedule>() { sched });
                    var schedres = DBManager.StatsDB.Query("select idschedule from schedules where idorigin=" + idorigin + " and schedule_origin_uid=" + sched.schedule_origin_uid);
                    if (schedres.Success == false)
                    {
                        return false;
                    }
                    var usersched = DBManager.StatsDB.Query("select iduser_schedule from userschedules where idorigin=" + idorigin + " and schedule_origin_uid=" + sched.schedule_origin_uid);
                    sched.idschedule = UInt64.Parse(schedres.Rows[0][0]);
                    sched.iduser_schedule = int.Parse(usersched.Rows[0][0]);
                    var res = DBManager.StatsDB.Query("update userschedules set idschedule=@idschedule,userdate=@userdate, userdate_changed=@userdate_changed where iduser_schedule=@id", new Dictionary<string, string>()
                {
                    {"@idschedule",""+(UInt64)sched.idschedule },
                    {"@userdate",sched.userdate },
                    {"@userdate_changed",sched.userdate_changed },
                    {"@id",""+(UInt64)sched.iduser_schedule},
                });
                    ok &= (res.Success);

                }

            }
            return ok;
        }

        public static QueryResult GetCompetitions(OriginID idorigin, string league)
        {

            return DBManager.StatsDB.Query("select * from leagues as l inner join competitions as c on c.idleague=l.idleague where l.idorigin=@idorigin and l.league_name=@league", new Dictionary<string, string>() {
                    {"@idorigin", ""+(int)idorigin } ,{"@league", league }
            });
        }

        public static QueryResult GetStandings(string idcompinput)
        {
            List<CompetitionWrapper> results;
            Dictionary<string, List<CompetitionWrapper>> ambigous;
            var list=InputParser.GetCompetitionIDListFromIDNameOrAlias(DBManager, idcompinput, out results, out ambigous);
            if (ambigous != null && ambigous.Count > 0)
            {
                throw new Exception("Ambigous competition input: " + Environment.NewLine + string.Join(Environment.NewLine, ambigous.ToJSONString()));
            }
            if (results.Count == 0)
            {
                throw new Exception("No matching competition");
            }

            return DBManager.StatsDB.Query("select * from standingsview where idcompetition in (@list) and active=1 order by sorting desc limit 50", new Dictionary<string, string>() {
                    {
                        "@list", list
                    } });
        }

        public static Dictionary<string, QueryResult> GetStandingsPerRace(string idcompinput)
        {
            List<CompetitionWrapper> results;
            Dictionary<string, List<CompetitionWrapper>> ambigous;
            var list = InputParser.GetCompetitionIDListFromIDNameOrAlias(DBManager, idcompinput, out results, out ambigous);
            if (ambigous != null && ambigous.Count > 0)
            {
                throw new Exception("Ambigous competition input: " + Environment.NewLine + string.Join(Environment.NewLine, ambigous.ToJSONString()));
            }
            if (results.Count == 0)
            {
                throw new Exception("No matching competition");
            }

            Dictionary<string, QueryResult> map = new Dictionary<string, QueryResult>();
            foreach (var race in Enum.GetNames(typeof(RaceEnum)))
            {
                map[race.ToString()] = DBManager.StatsDB.Query("select * from standingsview where idcompetition in (@list) and active=1 and idrace=@idrace order by sorting desc limit 50", new Dictionary<string, string>() {
                    { "@list", ""+list },
                    { "@idrace", ""+(int)Enum.Parse(typeof(RaceEnum),race) },
                    });
            }
            return map;
        }
    }

    public class RegisterResult
    {
        public int Added { get; set; }
        public int Updated { get; set; }
        public int Skipped { get; set; }
        public List<League> Leagues { get; set; }
    }

}
