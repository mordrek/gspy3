
export class UserLoginData {
	userInfo : Userinfoplus|undefined
	provider : string = ""
	access    : AuthToken | undefined           
}

export class AuthToken {
	token  : string =""
	expires : Date |undefined
}

export class Userinfoplus{
    id : string = ""
    picture : string = ""
    name : string = ""
    given_name : string = ""
    family_name : string = ""
    locale : string = ""
}

export default UserLoginData