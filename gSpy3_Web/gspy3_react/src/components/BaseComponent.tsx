import React  from 'react';
import DataView from "../api/views/dataView"
import { withRouter,RouteComponentProps } from "react-router-dom";
import { PageSelectionManager } from "../managers/pageSelectionManager";
import { QueryPageResponseView} from '../api/queryPageRequests';

export interface BaseComponentProps extends RouteComponentProps {
    selections : PageSelectionManager 
    gspyLocation : string 
};
export type BaseComponentState = {
 
};

class  BaseComponent<P  extends BaseComponentProps,S extends BaseComponentState> extends React.Component<P,S> {

    views : DataView[]  = []

    constructor(props : P){ 
        super(props);

        this.onSuccess = this.onSuccess.bind(this);
        this.onFail = this.onFail.bind(this);
        this.onLoad = this.onLoad.bind(this);
        this.onUpdate = this.onUpdate.bind(this);
        this.getData = this.getData.bind(this);
    }

    getData(){
        this.onLoad();
        this.props.selections.ensureViews(this.views).then((d)=>{
          this.onSuccess(d);
        }).catch((e)=>{ 
            this.onFail(e)
        });
        
   
      }
      onLoad(){
        this.onUpdate();
      }

      onUpdate(){

      }

      onSuccess(d : QueryPageResponseView){
      this.onUpdate();
      }

      onFail(e : any){
        this.onUpdate();
      }
    
      componentDidUpdate(prevProps : BaseComponentProps) {
        if(this.props.gspyLocation !== "" && this.props.gspyLocation !== prevProps.gspyLocation){
          console.log("Location has changed");
         this.getData();
        
        }
     }
    
      componentDidMount(){
       
        this.getData();
      }



    
}


export default BaseComponent
