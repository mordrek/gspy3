
import React, { CSSProperties } from 'react';
import { QueryPageResponse} from "../api/queryPageRequests"
import { ColumnID } from '../api/views/columns';
import {Link} from "react-router-dom"
import cogimg from "../gfx/cog.png"
import closeimg from "../gfx/close.png"
import HelpPanel from './HelpPanel';
import {Userinfoplus} from "../auth/UserLoginData"
import UserManager, {UserConfig} from "../managers/userManager"
import { PageSelectionManager } from "../managers/pageSelectionManager";
import messageManager, { PopupTypeEnum } from '../managers/messageManager';
import GSpy3_api from "../api/gSpy3_api"
import youtubeImg from "../gfx/youtube.png"
import twitchImg from "../gfx/twitch.png"


const hiddenStyle : CSSProperties= {
    display: "none"
  };
  

type CoachPanelState = {
    isCoachMenuOpen : boolean
    isFollowing : boolean
    userConfig : UserConfig
    isAlterMediaOpen: boolean
    youtubeLink :string
    twitchLink :string
};
type CoachPanelProps = {
    coach : QueryPageResponse
    global : boolean
    userInfo : Userinfoplus|undefined
    selections : PageSelectionManager 
};

class  CoachPanel extends React.Component<CoachPanelProps,CoachPanelState> {

  timer : NodeJS.Timeout|undefined;

  constructor(props : CoachPanelProps){
    super(props);

    this.onCoachMenu = this.onCoachMenu.bind(this);
    this.onFollowCoach = this.onFollowCoach.bind(this);
    this.onUnFollowCoach = this.onUnFollowCoach.bind(this);
    this.onAlterMedia = this.onAlterMedia.bind(this);
    this.onSaveMediaLinks = this.onSaveMediaLinks.bind(this)
    this.createState = this.createState.bind(this)

    this.state = this.createState()

  }

  onSaveMediaLinks(){
    this.setState({isAlterMediaOpen:false})  


    let youtube = (document.getElementById("youtubeInput") as HTMLInputElement).value;
    let twitch = (document.getElementById("twitchInput") as HTMLInputElement).value;
    this.props.selections.messages.addMessage(<span>Altering media links...</span>, 10000, PopupTypeEnum.Neutral);
    
    let idcoach = +this.props.coach.result.rows[0][this.props.coach.result.cols[ColumnID[ColumnID.idcoach]]];
    GSpy3_api.getAPI().postCoachMediaLinks(idcoach, youtube, twitch).then((res)=>{ 
        this.props.selections.messages.addMessage(<span>Media links have been altered. </span>, 5000, PopupTypeEnum.Success);
        setTimeout(function () {
              document.location.reload();
              }, 1000);
        
          }).catch((e : Error)=>{
      let sError = ""+(e)
      this.props.selections.messages.addMessage(<span>Altering media links has FAILED ({sError})</span>, 3000, PopupTypeEnum.Failure);
    });
  }

  onAlterMedia(){
    this.setState({isAlterMediaOpen:true, isCoachMenuOpen:false})
  }

  onFollowCoach(){
    this.setState({isCoachMenuOpen:false})  
    let row = this.props.coach.result.rows[0];
    let cols = this.props.coach.result.cols;
    let idcoach = +row[cols[ColumnID[ColumnID.idcoach]]]
    if(this.state.userConfig.follows.coaches.includes(idcoach)==false){
      this.state.userConfig.follows.coaches.push(idcoach);
      UserManager.saveConfig(this.props.userInfo?.id,this.state.userConfig);
      this.setState({userConfig:this.state.userConfig, isFollowing:true})
      this.props.selections.messages.addMessage(<span>Coach followed...</span>, 3000, PopupTypeEnum.Success);
    
    }else{
      this.props.selections.messages.addMessage(<span>Coach already followed...</span>, 3000, PopupTypeEnum.Neutral);

    }
  }
  onUnFollowCoach(){
    this.setState({isCoachMenuOpen:false}) 
    let row = this.props.coach.result.rows[0];
    let cols = this.props.coach.result.cols;
    let idcoach = +row[cols[ColumnID[ColumnID.idcoach]]]
    if(this.state.userConfig.follows.coaches.includes(idcoach)){
      this.state.userConfig.follows.coaches.splice(this.state.userConfig.follows.coaches.indexOf(idcoach),1);
      UserManager.saveConfig(this.props.userInfo?.id,this.state.userConfig);
      this.setState({userConfig:this.state.userConfig, isFollowing:false})
      this.props.selections.messages.addMessage(<span>Coach unfollowed...</span>, 3000, PopupTypeEnum.Success);
    
    }else{
      this.props.selections.messages.addMessage(<span>Coach not followed...</span>, 3000, PopupTypeEnum.Neutral);

    } 
  }

  onCoachMenu()
  {
    this.setState({isCoachMenuOpen:true})
  }

  componentDidUpdate(prevProps : CoachPanelProps)
  {
    if(prevProps.coach != this.props.coach){
    this.setState(this.createState())
    }
  }    
  
  createState() : CoachPanelState{
    if(!this.props.coach ||!this.props.coach.result ||this.props.coach.result.rows.length <= 0) return {isAlterMediaOpen:false,isCoachMenuOpen:false,   isFollowing :false , userConfig:new UserConfig(), youtubeLink:"", twitchLink:""};
    let row = this.props.coach.result.rows[0];
    let cols = this.props.coach.result.cols;
    let name = row[cols[ColumnID[ColumnID.coach_name]]]
    let idcoach = +row[cols[ColumnID[ColumnID.idcoach]]]
    let youtube = row[cols[ColumnID[ColumnID.youtube]]]
    let twitch = row[cols[ColumnID[ColumnID.twitch]]]
    console.log(row);
    let ucfg = UserManager.getConfig(this.props.userInfo?.id)
    return({isAlterMediaOpen:false,isCoachMenuOpen:false,   isFollowing : ucfg.follows.coaches.includes(idcoach) , userConfig:ucfg, youtubeLink:youtube, twitchLink:twitch});
  }

  render () : React.ReactElement {


    if(!this.state || !this.props.coach || !this.props.coach.result?.rows || this.props.coach.result?.rows.length==0){
        return <div></div>
    }
    let row = this.props.coach.result.rows[0];
    let cols = this.props.coach.result.cols;
    let name = row[cols[ColumnID[ColumnID.coach_name]]]
    let idcoach = row[cols[ColumnID[ColumnID.idcoach]]]

   return <div className="CoachPanel panel bordered">
       <div className="menu" style={{position:"absolute", right:"1pt",top:"0pt"}}>
        <a title="Competition settings"  onClick={this.onCoachMenu} className="imageButton"><img src={cogimg} className="iconSmall"></img></a>
        {this.state.isCoachMenuOpen&&<HelpPanel left={true} title="Coach menu" onClose={()=>{this.setState({isCoachMenuOpen:false})}}>
            <div className="menu padded">
                {this.state.isFollowing&&<button onClick={this.onUnFollowCoach}>Unfollow coach</button>}
                {!this.state.isFollowing&&<button onClick={this.onFollowCoach}>Follow coach</button>}
                <br/><button onClick={this.onAlterMedia}>Alter media links</button>
            </div>
            </HelpPanel>}
        {this.state.isAlterMediaOpen&& <HelpPanel left={true} title="Alter media links" onClose={()=>{this.setState({isAlterMediaOpen:false})}}>
            <div className="padded">
                <table>
                    <tbody>
                    <tr>
                        <td>Youtube</td><td><input style={{width:"200pt"}} type="url" id="youtubeInput" defaultValue={this.state.youtubeLink}></input></td>
                    </tr>
                    <tr>
                        <td>Twitch</td><td><input style={{width:"200pt"}} type="url"  id="twitchInput" defaultValue={this.state.twitchLink}></input></td>
                    </tr>
                    <tr>
                    <td></td><td><button onClick={this.onSaveMediaLinks}>Save</button></td>

                    </tr>
                    </tbody>
                </table>

            </div>


            </HelpPanel>}
        </div>
        <h2>
        {this.state.youtubeLink&&<a className="imageButton" target="_blank" href={this.state.youtubeLink}><img src={youtubeImg}></img></a>}
        {this.state.twitchLink&&<a className="imageButton" target="_blank" href={this.state.twitchLink}><img src={twitchImg}></img></a>}
        {(this.state.twitchLink||this.state.youtubeLink)&&<span> </span>}
        {name} {this.props.global&&<span>- Global</span>}
        </h2>

        {!this.props.global&&<div className="menu" style={{width:"100pt"}}><Link to={"/coach/"+idcoach}>Global data</Link></div>}
       </div>
   
  }
}

export default CoachPanel