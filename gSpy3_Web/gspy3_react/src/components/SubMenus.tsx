
import React from 'react';

import {Link} from "react-router-dom"


type SubMenusState = {


};
type SubMenusProps = {
  path : string
};

class MenuItem {
  key:string=""
  value:string = ""
  selected: boolean = false
}

class  SubMenus extends React.Component<SubMenusProps,SubMenusState> {
/*
  constructor(props : SubMenusProps){
    super(props);
  }*/

  render () : React.ReactElement {

    let path : MenuItem[] = [];
    if(this.props.path)
    {

      let loc = this.props.path;
      if(loc.indexOf("?") >= 0){
        loc = loc.substr(0,loc.indexOf("?"));
      }
      if(loc=="/" || loc=="/gspy/" || loc.startsWith("/user/"))
      {
        path.push({key:"User home", value:"/",selected:loc=="/" || loc=="/gspy/"});
        path.push({key:"Settings", value:"/user/settings/",selected:loc.startsWith("/user/")});
    
      }

      if(loc.startsWith("/collection/"))
      {
        let collm = loc.match("\/collection\/([^\/]*)\/")
        if(!collm || collm.length === 0){
          collm = loc.match("\/collection\/([^\/]*)")
        }
        if(collm){
        let idcoll = collm[1]

        path.push({key:"Collection", value:"/collection/"+idcoll, selected:(loc === "/collection/"+idcoll)});
        path.push({key:"Teams", value:"/collection/"+idcoll+"/teams", selected:(loc === "/collection/"+idcoll+"/teams")});
        }

      }

      if(loc.startsWith("/league/"))
      {
        let leaguematching = loc.match("\/league\/([^\/]*)\/")
        if(!leaguematching || leaguematching.length === 0){
          leaguematching = loc.match("\/league\/([^\/]*)")
        }
        if(leaguematching){
          let idleague = leaguematching[1];

          path.push({key:"Competitions", value:"/league/"+idleague, selected:(loc === "/league/"+idleague)});
          path.push({key:"Collections", value:"/league/"+idleague+"/collections", selected:(loc === "/league/"+idleague+"/collections")});
            
        }
      }

      
      

      if(loc.startsWith("/comp/"))
      {
        let compMatch = loc.match("\/comp\/([^\/]*)\/")
        if(!compMatch || compMatch.length === 0){
          compMatch = loc.match("\/comp\/([^\/]*)")
        }
        
        if(compMatch)
        {
          let compId = compMatch[1];
          if(compId.endsWith("/")) compId = compId.substr(0,compId.length-1);

          let compTeamMatch = loc.match("\/comp\/([^\/]*)\/team\/([^\/]*)\/")
          if(!compTeamMatch || compTeamMatch.length === 0){
            compTeamMatch = loc.match("\/comp\/([^\/]*)\/team\/([^\/]*)")
          }

          let compCoachMatch = loc.match("\/comp\/([^\/]*)\/coach\/([^\/]*)\/")
          if(!compCoachMatch || compCoachMatch.length === 0){
            compCoachMatch = loc.match("\/comp\/([^\/]*)\/coach\/([^\/]*)")
          }

          let compGameMatch = loc.match("\/comp\/([^\/]*)\/match\/([^\/]*)\/")
          if(!compGameMatch || compGameMatch.length === 0){
            compGameMatch = loc.match("\/comp\/([^\/]*)\/match\/([^\/]*)")
          }
  
          if(compTeamMatch)
          {
            let teamId = compTeamMatch[2];
            path.push({key:"Overview", value:"/comp/"+compId+"/team/"+teamId, selected:(loc === "/comp/"+compId+"/team/"+teamId)});
            path.push({key:"Standing", value:"/comp/"+compId+"/team/"+teamId+"/standing", selected:(loc === "/comp/"+compId+"/team/"+teamId+"/standing")});
            path.push({key:"Results", value:"/comp/"+compId+"/team/"+teamId+"/results", selected:(loc === "/comp/"+compId+"/team/"+teamId+"/results")});
            path.push({key:"Schedule", value:"/comp/"+compId+"/team/"+teamId+"/schedule", selected:(loc === "/comp/"+compId+"/team/"+teamId+"/schedule")});
            path.push({key:"Graveyard", value:"/comp/"+compId+"/team/"+teamId+"/graveyard", selected:(loc === "/comp/"+compId+"/team/"+teamId+"/graveyard")});
          }
          else if(compGameMatch)
          {
            let matchId = compGameMatch[2];
            path.push({key:"Overview", value:"/comp/"+compId+"/match/"+matchId, selected:(loc === "/comp/"+compId+"/match/"+matchId)});
            path.push({key:"Players", value:"/comp/"+compId+"/match/"+matchId+"/players", selected:(loc === "/comp/"+compId+"/match/"+matchId+"/players")});
            path.push({key:"Dice", value:"/comp/"+compId+"/match/"+matchId+"/dice", selected:(loc === "/comp/"+compId+"/match/"+matchId+"/dice")});
            path.push({key:"Replay", value:"/comp/"+compId+"/match/"+matchId+"/replay", selected:(loc === "/comp/"+compId+"/match/"+matchId+"/replay")});
            //path.push({key:"Players", value:"/comp/"+compId+"/match/"+matchId+"/players", selected:(loc === "/comp/"+compId+"/match/"+matchId+"/players")});
          }
         
          else if(loc.indexOf("/stats")>=0 || loc.indexOf("/stats/players") >= 0 || loc.indexOf("/stats/coaches")>0|| loc.indexOf("/stats/races")>0){
            path.push({key:"Teams", value:"/comp/"+compId+"/stats", selected:(loc === "/comp/"+compId+"/stats")});
            path.push({key:"Coaches", value:"/comp/"+compId+"/stats/coaches", selected:(loc === "/comp/"+compId+"/stats/coaches")});
            path.push({key:"Races", value:"/comp/"+compId+"/stats/races", selected:(loc === "/comp/"+compId+"/stats/races")});
            path.push({key:"Players", value:"/comp/"+compId+"/stats/players", selected:(loc === "/comp/"+compId+"/stats/players")});
          }
          else if(loc.indexOf("/coach/")>=0 && compCoachMatch)
            {
              let coachId = compCoachMatch[2];
              path.push({key:"Teams", value:"/comp/"+compId+"/coach/"+coachId, selected:(loc === "/comp/"+compId+"/coach/"+coachId)});
              path.push({key:"Races", value:"/comp/"+compId+"/coach/"+coachId+"/races", selected:(loc === "/comp/"+compId+"/coach/"+coachId+"/races")});
        
            }
          else
          {
            path.push({key:"Standings", value:"/comp/"+compId, selected:(loc === "/comp/"+compId)});
            path.push({key:"Results", value:"/comp/"+compId+"/results", selected:(loc === "/comp/"+compId+"/results")});
            path.push({key:"Schedule", value:"/comp/"+compId+"/schedule", selected:(loc === "/comp/"+compId+"/schedule")});
            path.push({key:"Activity", value:"/comp/"+compId+"/activity", selected:(loc === "/comp/"+compId+"/activity")});
            path.push({key:"Stats", value:"/comp/"+compId+"/stats", selected:(loc === "/comp/"+compId+"/stats")});
            if(loc.indexOf("/overtake/") > 0){
              path.push({key:"Overtake", value:loc, selected:true});
            }

          }
        //  path.push({key:"Top lists", value:"/comp/"+compId+"/top", selected:(loc === "/comp/"+compId+"/top")});
        //  path.push({key:"Coaches", value:"/comp/"+compId+"/coaches", selected:(loc === "/comp/"+compId+"/coaches")});
        }
      }
      else if(loc.indexOf("/coach/")>=0)
      {
        let coachMatch = loc.match("\/coach\/([^\/]*)")
        if(!coachMatch || coachMatch.length === 0){
          coachMatch = loc.match("\/coach\/([^\/]*)\/")
        }
        if(coachMatch){
        let coachId = coachMatch[1];
        path.push({key:"Teams", value:"/coach/"+coachId, selected:(loc === "/coach/"+coachId)});
        path.push({key:"Races", value:"/coach/"+coachId+"/races", selected:(loc === "/coach/"+coachId+"/races")});
        }
  
      }
      
     if(loc.startsWith("/leagues")){
        path.push({key:"Leagues", value:"/leagues", selected:(loc === "/leagues")});
        path.push({key:"Activate", value:"/leagues/activate", selected:(loc === "/leagues/activate")});
      } 

      if(loc.startsWith("/help")){
        path.push({key:"GoblinSpy", value:"/help", selected:(loc === "/help")});
        path.push({key:"Discord bot", value:"/help/bot", selected:(loc === "/help/bot")});
      } 
    }

    const items = path.map((item : MenuItem, index : number) =>{
        let className= (item.selected ? "selected":"")+" "+item.key;
        return  <Link className={className} key={item.key} to={item.value}>{item.key}</Link>;
        });

        return <div className="SubMenus menu">
                  {items}
            </div>
  }
  /* previously
  return <div className="SubMenus">
                  {items.length>0&&<div className="menuHeader break">Context links</div>}
                  <div className="menu">
                  {items}
                  </div>
            </div>*/
}

export default SubMenus