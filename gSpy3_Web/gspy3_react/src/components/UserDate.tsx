
import React from 'react';
import { withRouter,RouteComponentProps } from "react-router-dom";
import HelpPanel from "./HelpPanel"
import CalendarImg from "../gfx/calendar.png"
import ReactDateTime from "react-datetime"
import messageManager, { PopupTypeEnum } from '../managers/messageManager';
import GSpy3_api from "../api/gSpy3_api";


type UserDateState = {
    IsEditOpen : boolean
    editDate : Date|undefined
    shownDate: Date|undefined
};
interface UserDateProps extends 
RouteComponentProps{
    userDate : string,
    idcontest: number,
    idcomp : number
}



class  UserDate extends React.Component<UserDateProps,UserDateState> {


  constructor(props : UserDateProps){
    super(props);
    this.onEdit = this.onEdit.bind(this);
    this.onDateChange = this.onDateChange.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onRemove = this.onRemove.bind(this);
    let dateTime = props.userDate

    this.state={
        IsEditOpen : false,
        editDate : (!dateTime)?undefined:new Date(dateTime),
        shownDate : (!dateTime)?undefined:new Date(dateTime)
    }
  }
  componentDidUpdate(){
    
    
 }
  onEdit(event : React.MouseEvent)
  {
    this.setState({IsEditOpen: !this.state.IsEditOpen});
  }

  static DateToYYYYMMDD(Date: Date): string {
    let DS: string = Date.getFullYear()
        + '-' + ('0' + (Date.getMonth() + 1)).slice(-2)
        + '-' + ('0' + Date.getDate()).slice(-2)
    return DS
}
static DateToYYYYMMDD_HHMM(Date: Date): string {
    let DS: string = Date.getFullYear()
        + '-' + ('0' + (Date.getMonth() + 1)).slice(-2)
        + '-' + ('0' + Date.getDate()).slice(-2)
        + ' ' + ('0' + Date.getHours()).slice(-2)
        + ':' + ('0' + Date.getMinutes()).slice(-2)
    return DS
}
static DateToDDMM_HHMM(Date: Date): string {
    let DS: string =  ('0' + Date.getDate()).slice(-2)
        + '/' + ('0' + (Date.getMonth() + 1)).slice(-2)
        + ' ' + ('0' + Date.getHours()).slice(-2)
        + ':' + ('0' + Date.getMinutes()).slice(-2)
    return DS
}

  onDateChange(date : string|any)
  {
    this.setState({editDate : date});
    
  }
  onRemove()
  {
    let newUTCDate = "";
    let nowTime = (new Date()).toISOString();
    let mm = messageManager.getMessageManager();
    this.setState({shownDate: undefined, IsEditOpen:false})
    mm.addMessage(<span>Removing user schedule date...</span>, 10000, PopupTypeEnum.Neutral);
    GSpy3_api.getAPI().postCompMatchSchedule(this.props.idcomp,  [{idcontest:this.props.idcontest,datetime:newUTCDate}]).then((res)=>{ 
      mm.addMessage(<span>Removing schedule data succeeded.</span>, 3000, PopupTypeEnum.Success);
            
          }).catch((e : Error)=>{
      let sError = JSON.stringify(e)
      mm.addMessage(<span>Removing user schedule FAILED ({sError})</span>, 3000, PopupTypeEnum.Failure);
    });


  }
  onSave()
  {
    let newUTCDate = "";
    if (this.state.editDate){
        newUTCDate = this.state.editDate?.toISOString();
    }
    let nowTime = (new Date()).toISOString();
    let mm = messageManager.getMessageManager();
    this.setState({shownDate: this.state.editDate, IsEditOpen:false})
    mm.addMessage(<span>Saving user schedule date...</span>, 10000, PopupTypeEnum.Neutral);
    GSpy3_api.getAPI().postCompMatchSchedule(this.props.idcomp,  [{idcontest:this.props.idcontest,datetime:newUTCDate}]).then((res)=>{ 
      mm.addMessage(<span>Setting user schedule data succeeded.</span>, 3000, PopupTypeEnum.Success);
        
          }).catch((e : Error)=>{
      let sError = JSON.stringify(e)
      mm.addMessage(<span>Setting user schedule FAILED ({sError})</span>, 3000, PopupTypeEnum.Failure);
    });


  }

  render () : React.ReactElement {
      let dateText : string= "";
      let longDateText : string = "";
      
      if(!this.state.shownDate) {
          longDateText = "Click to edit"
      } else{
          dateText = UserDate.DateToDDMM_HHMM(new Date(this.state.shownDate))
          longDateText = UserDate.DateToYYYYMMDD_HHMM(new Date(this.state.shownDate))+", Click to edit"
      }
      let extraClass = "higher";
      return <div><button title={longDateText} onClick={this.onEdit}><img className="iconSmall" alt="Date" src={CalendarImg}></img>
            <span >{dateText}</span></button>
                {this.state.IsEditOpen&&<HelpPanel extraClass={extraClass} left={false} title="User date">
                Enter date and time in local time. Each user will see it in his/her local time.<br/>
                <ReactDateTime
                        value={this.state.editDate}
                        dateFormat="YYYY-MM-DD"
                        timeFormat="HH:mm"
                        onChange = {this.onDateChange}
                    /><br/>
                <button className="datesavebutton" onClick={this.onSave} >Save this date</button>
                {dateText&&<button className="datesavebutton" onClick={this.onRemove} >Remove date</button>}
                
                    </HelpPanel>}
                    
            </div>
   
    }

   
}

export default withRouter(UserDate)