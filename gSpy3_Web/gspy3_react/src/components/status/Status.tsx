
import React from 'react';
import ServerStatus from '../../api/views/statusCollection';
import ok_icon from "../../gfx/status-ok.png"
import warning_icon from "../../gfx/status-work.png"
import error_icon from "../../gfx/status-error.png"
import "./Status.css"
import { ModuleStatus, WorkStatus } from '../../api/views/statusCollection';
import dateManager from "../../managers/dateManager"

type StatusState = {


};


type StatusProps = {
    status : ServerStatus
   
};



class  Status extends React.Component<StatusProps,StatusState> {
/*
  constructor(props : UserHomeProps){
    super(props);
  }
*/




  render () : React.ReactElement {

    if(!this.props.status || ! this.props.status.modules){
        return <div>No status found</div>
    }

    
    console.log(this.props.status);

    return <div className="Status flexcontainer">
        <div className="panel">
         <table className="GSpyTable">
          <thead>
              <tr><td>Module</td><td>Command</td><td>Data</td><td>Added</td><td>Started</td><td>Finished</td><td>Result</td><td>OK</td></tr>
          </thead>
          <tbody>
            { this.props.status.modules.map((module, moduleindex)=>{
                return <React.Fragment><tr><td className="leftCell">{module.module_name}<br/><span className='subheader'>Heartbeat {dateManager.utcToLocalDatetime(module.heartbeat_datetime)}</span></td></tr>
                {module.work.map((work, workindex)=>{
                    return <tr><td className="leftCell"></td><td>{work.command}</td>
                    <td>{work.data}</td>
                    <td>{dateManager.utcToLocalDatetime(work.added)}</td>
                    <td>{dateManager.utcToLocalDatetime(work.started)}</td>
                    <td>{dateManager.utcToLocalDatetime(work.finished)}</td>
                    <td>{work.result}</td>
                    <td>{work.success?(work.success==0?<span>False</span>:<span>True</span>):<span></span>}</td>
                    </tr>
                        
                   })}</React.Fragment>
                })
            }
          </tbody>

     </table>
     </div>
       
    </div>
  }
}

export default Status