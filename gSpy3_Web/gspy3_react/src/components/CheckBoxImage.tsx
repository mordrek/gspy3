
import React, { CSSProperties } from 'react';
import "./CheckBoxImage.css"
import checked from "../gfx/yes.png"
import unchecked from "../gfx/no.png"

const hiddenStyle : CSSProperties= {
    display: "none"
  };
  

interface CheckBoxEvent{
    (checked: boolean): void;
}

type CheckBoxImageState = {
};
type CheckBoxImageProps = {
    checked : boolean
    onCheck : CheckBoxEvent

};

class  CheckBoxImage extends React.Component<CheckBoxImageProps,CheckBoxImageState> {
    constructor(props : CheckBoxImageProps){
        super(props);
        this.onClick = this.onClick.bind(this);
    }
  
  onClick() 
  {
      this.props.onCheck(!this.props.checked);

  }

  render () : React.ReactElement {


    
   return <div className="CheckBoxImage">
          
          <button onClick={this.onClick}>
             {this.props.checked && <img alt="yes" className="checkBoxSymbol"  title="yes" src={checked}></img>}
             {!this.props.checked && <img alt="no"  className="checkBoxSymbol"  title="yes" src={unchecked}></img>}
          
          <div>
            {this.props.children}
          </div>
          </button>
       </div>
   
  }
}

export default CheckBoxImage