
import React  from 'react';
import GSpyTable from "../../components/GSpyTable"
import {ColumnLayout} from "../../api/views/columns"
import { QueryPageResponse} from "../../api/queryPageRequests"
import DataView from "../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../components/BaseComponent"
import CoachPanel from "../../components/CoachPanel"
import {Userinfoplus} from "../../auth/UserLoginData"

interface CompetitionCoachRaceState extends BaseComponentState  {
  coach : QueryPageResponse
  compcoachraces : QueryPageResponse

  };
interface CompetitionCoachRaceProps extends BaseComponentProps  {
  userInfo : Userinfoplus|undefined
};

class  GlobalCoachRacePage extends BaseComponent<CompetitionCoachRaceProps,CompetitionCoachRaceState> {

     
  constructor(props : CompetitionCoachRaceProps){ 
    super(props);
    this.views = [DataView.coach,DataView.compCoachRaces, DataView.comp];
    this.onUpdate = this.onUpdate.bind(this);
  }

  onUpdate(){
    super.onUpdate();
    this.setState(
      {
        coach:this.props.selections.responses.response[DataView[DataView.coach]],
        compcoachraces:this.props.selections.responses.response[DataView[DataView.compCoachRaces]]
      });
  }

  render () : React.ReactElement {
    if(!this.state) return <div></div>;
      
    return <div className="flexcontainer">
        <CoachPanel selections={this.props.selections} userInfo={this.props.userInfo} coach={this.state.coach} global={true}></CoachPanel>
  
        <div className="panel">
            <GSpyTable source={this.state.compcoachraces} layout={ColumnLayout.CoachRacesView} filters={this.props.selections.filters}></GSpyTable>
        </div>
      </div> 
      
  }
}

export default GlobalCoachRacePage