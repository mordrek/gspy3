
import React  from 'react';
import GSpyTable from "../../components/GSpyTable"
import {ColumnLayout} from "../../api/views/columns"
import DataView from "../../api/views/dataView"
import { QueryPageResponse} from "../../api/queryPageRequests"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../components/BaseComponent"
import CoachPanel from "../../components/CoachPanel"
import {Userinfoplus} from "../../auth/UserLoginData"

interface CoachState extends BaseComponentState {
    coachcompetitions : QueryPageResponse
    coach : QueryPageResponse
    };
  interface CoachProps extends BaseComponentProps  {
    userInfo : Userinfoplus|undefined
  };

class  GlobalCoachPage extends BaseComponent<CoachProps,CoachState> {

    
  constructor(props : CoachProps){ 
    super(props);

    this.views = [DataView.coach,DataView.coachCompetitions];
    this.onUpdate = this.onUpdate.bind(this);
  } 

  onUpdate(){
    super.onUpdate();
    this.setState(
      {
        coachcompetitions:this.props.selections.responses.response[DataView[DataView.coachCompetitions]],
        coach:this.props.selections.responses.response[DataView[DataView.coach]]
      })
  }
  

  render () : React.ReactElement {
    if(!this.state){return <div></div>}
    return  <div className="flexcontainer">
            <CoachPanel selections={this.props.selections} coach={this.state.coach} global={true} userInfo={this.props.userInfo}></CoachPanel>

            <div className="panel">
            <GSpyTable source={this.state.coachcompetitions} layout={ColumnLayout.CoachCompeitionsView} filters={this.props.selections.filters}></GSpyTable>
            </div>
          </div>
      
  }
}

export default GlobalCoachPage