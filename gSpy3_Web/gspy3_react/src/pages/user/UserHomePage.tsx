
import React from 'react';
import {Userinfoplus} from "../../auth/UserLoginData"
import BaseComponent, { BaseComponentState,BaseComponentProps } from "../../components/BaseComponent"
import DataView from "../../api/views/dataView"
import { QueryPageRequest, QueryPageResponse } from '../../api/queryPageRequests';
import {withRouter} from "react-router-dom"
import {ColumnID, ColumnLayout} from "../../api/views/columns"
import GSpyTable from "../../components/GSpyTable"
import UserManager, { UserConfig } from '../../managers/userManager';
import GSpyTableColumnHeader from '../../components/GSpyTableColumnHeader';
import {Component, createRef} from "react"
import {Link} from "react-router-dom"
import messageManager, { PopupTypeEnum } from '../../managers/messageManager';

interface UserHomeState extends BaseComponentState  {
  lastcomps : QueryPageResponse 
  lastcoachcomps : QueryPageResponse 
  userConfig : UserConfig

};
interface UserHomeProps extends BaseComponentProps {
    userInfo : Userinfoplus|undefined
};

class  UserHomePage extends BaseComponent<UserHomeProps,UserHomeState> {
 
  


  constructor(props : UserHomeProps){
    super(props);
    this.views = [DataView.lastComps, DataView.lastCoachComps];
    this.onUpdate = this.onUpdate.bind(this);
  }

  getData(){
    let userConfig = UserManager.getConfig(this.props.userInfo?.id);
 
    if(userConfig.follows.competitions.length > 0)
    {
      let curFilter  = this.props.selections.filters.getFilter(DataView[DataView.lastComps])
      let specialFilter = "followed:"+userConfig.follows.competitions.join("|");
      
      if(!curFilter.filters){ curFilter.filters = { }}
    
      curFilter.limit=100;
      curFilter.filters[ColumnID[ColumnID.idcompetition]] = specialFilter;

    }
    if(userConfig.follows.coaches.length > 0)
    {
      let curFilter  = this.props.selections.filters.getFilter(DataView[DataView.lastCoachComps])
      let specialFilter = "="+userConfig.follows.coaches.join("|=");
      
      if(!curFilter.filters){ curFilter.filters = { }}
    
      curFilter.limit=100;
      curFilter.filters[ColumnID[ColumnID.idcoach]] = specialFilter;
    }
    super.getData();
  }

onUpdate()
{
  super.onUpdate(); 
  let userConfig = UserManager.getConfig(this.props.userInfo?.id);

  // If we are following collections, then we add them manually to the result
  let alteredLastComps = this.props.selections.responses.response[DataView[DataView.lastComps]];
  if(userConfig.follows.competitions.length > 0)
  {
    let tooOldList : string[] = []
    let lastComps = this.props.selections.responses.response[DataView[DataView.lastComps]];
    if(lastComps&& lastComps.result)
    {
      let idcol = lastComps.result.cols[ColumnID[ColumnID.idcompetition]];
      let cols = alteredLastComps.result.cols;
      for(var i=0; i < userConfig.follows.competitions.length;i++)
      {
        let id = userConfig.follows.competitions[i];
        if(! (Number(id) > 0)){
          let alreadyHaveIt = false;
          for(let r = 0; r < alteredLastComps.result.rows.length;r++){
            if(alteredLastComps.result.rows[r][cols["idcompetition"]] == id)
            {
              alreadyHaveIt = true;
              break;
            }
          }
          if(!alreadyHaveIt){
          let newrow = new Array(Object.keys(cols).length);
          newrow[cols["competition_name"]] = id;
          newrow[cols["idcompetition"]] = id;
          alteredLastComps.result.rows.unshift(newrow);
          }
        }
      }
    }
  }


  this.setState(
    {
      lastcomps:alteredLastComps,
      lastcoachcomps:this.props.selections.responses.response[DataView[DataView.lastCoachComps]],
      userConfig : userConfig
    });

  // Clean up old inactive competitions
  if(userConfig.follows.competitions.length > 0)
  {
    let tooOldList : string[] = []
    let lastComps = this.props.selections.responses.response[DataView[DataView.lastComps]];
    if(lastComps&& lastComps.result)
    {
      let idcol = lastComps.result.cols[ColumnID[ColumnID.idcompetition]];
      let activecol = lastComps.result.cols[ColumnID[ColumnID.active]];
      for(var i=0; i < idcol && userConfig.follows.competitions.length;i++)
      {
        let id = userConfig.follows.competitions[i];
        for(var j=0; j< lastComps.result.rows.length;j++)
        {
          let idcomp = lastComps.result.rows[j][idcol]
          if(idcomp == id && lastComps.result.rows[j][activecol]=="0")
          {
            tooOldList.push(idcomp)
          }
        }

      }
      if(tooOldList.length > 0)
      {
        this.props.selections.messages.addMessage(<span>Removing followed competitions that are inactive...</span>, 3000, PopupTypeEnum.Neutral);
        for(var i=0; i < tooOldList.length;i++)
        {
          userConfig.follows.competitions.splice(userConfig.follows.competitions.indexOf(tooOldList[i]),1)
        }
        UserManager.saveConfig(this.props.userInfo?.id,userConfig);
        setTimeout(()=>
        {
          document.location.reload()
        },3000);

      }
    }
  }

    // Clean up old inactive coaches
  if(userConfig.follows.coaches.length > 0)
  {
    let tooOldList : number[] = []
    let lastCoaches = this.props.selections.responses.response[DataView[DataView.lastCoachComps]];
    if(lastCoaches&& lastCoaches.result)
    {
      let idcol = lastCoaches.result.cols[ColumnID[ColumnID.idcoach]];
      for(var i=0; idcol && i < userConfig.follows.coaches.length;i++)
      {
        let id = userConfig.follows.coaches[i];
        console.log(id);
        let gotIt=false;
        for(var j=0; j< lastCoaches.result.rows.length;j++)
        {
          let idcomp = +lastCoaches.result.rows[j][idcol]
          if(+idcomp == +id)
          {
            gotIt=true;
            break;
          }
        }
        if(!gotIt){ tooOldList.push(id)}

      }
      if(tooOldList.length > 0)
      {
        this.props.selections.messages.addMessage(<span>Removing followed coaches that are inactive...</span>, 3000, PopupTypeEnum.Neutral);
        for(var i=0; i < tooOldList.length;i++)
        {
          userConfig.follows.coaches.splice(userConfig.follows.coaches.indexOf(tooOldList[i]),1)
        }
        UserManager.saveConfig(this.props.userInfo?.id,userConfig);
        setTimeout(()=>
        {
          document.location.reload()
        },3000);

      }
    }
  }
    

}

  render () : React.ReactElement {
    if(!this.state) return <div></div>
    return <div className="UserHome flexcontainer">
      <div className="panel bordered">
        <h2>GoblinSpy User Home</h2>
        
        {this.state.userConfig.presentation.showHelp &&<span>This is GoblinSpy ; use the buttons or the search in the header, find a <Link to="/leagues">League</Link> or click one of the competitions below.<br/>
       
        </span>}
        {(!this.props.userInfo)&&this.state.userConfig.presentation.showHelp && <div className="panel">
          <div>You are not logged in, so any personal settings are bound to this browser</div>
          </div>}
          {!(this.state.userConfig.follows.competitions.length > 0) && this.state.userConfig.presentation.showHelp &&<div>          
            You don't follow any competitions, so the last updated competitions are shown below instead.<br/>
          To follow a competition, go to a competition and it's standings-page, click on the cog menu and select to follow.
          You can later unfollow using the same menu.
        </div>}
        
        </div>


        <div className="panel">
        <GSpyTable source={this.state.lastcomps} layout={ColumnLayout.FollowedCompetitionView} filters={this.props.selections.filters}></GSpyTable>
        {this.state?.userConfig?.follows?.coaches?.length > 0&&
            <GSpyTable source={this.state.lastcoachcomps} layout={ColumnLayout.FollowedCoachView} filters={this.props.selections.filters}></GSpyTable>
        }
        </div>

    </div>
  }
}

export default withRouter(UserHomePage)