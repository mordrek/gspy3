
import React from 'react';
import {Userinfoplus} from "../../auth/UserLoginData"
import BaseComponent, { BaseComponentState,BaseComponentProps } from "../../components/BaseComponent"
import DataView from "../../api/views/dataView"
import { QueryPageResponse } from '../../api/queryPageRequests';
import {withRouter} from "react-router-dom"
import {ColumnLayout} from "../../api/views/columns"
import GSpyTable from "../../components/GSpyTable"
import UserManager,{UserConfig} from "../../managers/userManager"
import { threadId } from 'worker_threads';
import CheckBoxImage from "../../components/CheckBoxImage"
import "./UserSettingsPage.css"

interface UserSettingsState extends BaseComponentState  {
  userConfig : UserConfig

};
interface UserSettingsProps extends BaseComponentProps {
    userInfo : Userinfoplus|undefined
};

class  UserSettingsPage extends BaseComponent<UserSettingsProps,UserSettingsState> {

  constructor(props : UserSettingsProps){
    super(props);
    this.views = [DataView.lastComps];
    this.onUpdate = this.onUpdate.bind(this);
    this.setDisplayStyle = this.setDisplayStyle.bind(this);
    this.setShowHelp = this.setShowHelp.bind(this);
  } 

onUpdate(){
  super.onUpdate();
  this.setState(
    {
        userConfig:UserManager.getConfig(this.props.userInfo?.id)
    });
}
setShowHelp(trueOrFalse : string )
{
    let newConfig =this.state.userConfig;
    if(!newConfig)
    {
        newConfig = UserManager.getConfig(this.props.userInfo?.id);
    }
    console.log(""+trueOrFalse);
    newConfig.presentation.showHelp = trueOrFalse == "true";
    UserManager.saveConfig(this.props.userInfo?.id, newConfig);
  

}

setDisplayStyle(name : string )
{
    let newConfig =this.state.userConfig;
    if(!newConfig)
    {
        newConfig = UserManager.getConfig(this.props.userInfo?.id);
    }
    newConfig.presentation.displayStyle = name;
    UserManager.saveConfig(this.props.userInfo?.id, newConfig);
    window.location.reload(); // Force reload of whole page

}

  render () : React.ReactElement {
    if(!this.state) return <div></div>
    return <div className="UserSettings panel  bordered">
      <h2>User settings</h2>
      {(!this.props.userInfo)&&<div>
          <div>You are not logged in, so any settings are bound to this browser</div>

          </div>}

        <table>
            <thead>
                <tr>
                <th>Presentation</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Display style</td>
                    <td>
                        <select onChange={(e)=>{this.setDisplayStyle(e.target.value)}}  value={this.state.userConfig?.presentation.displayStyle}>
                            <option className="defaultButtonStyle" value="">Default</option>
                            <option className="darkButtonStyle" value="dark">Dark</option>
                        </select>
                    </td>
                </tr>
                <tr>
                  <td>Show help texts</td>
                  <td><select onChange={(e)=>{this.setShowHelp(e.target.value)}}  defaultValue={""+this.state.userConfig?.presentation.showHelp}>
                            <option value="true">Yes</option>
                            <option value="false">No</option>
                        </select></td>
                </tr>
            </tbody>
        </table>

        

        {/*
        <div className="panel">
          <GSpyTable source={this.state.lastcomps} layout={ColumnLayout.LastCompetitionsView} filters={this.props.selections.filters}></GSpyTable>
        </div>
        */}

    </div>
  } 
}

export default withRouter(UserSettingsPage)