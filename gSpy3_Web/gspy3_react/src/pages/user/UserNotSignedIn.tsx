
import React, { CSSProperties } from 'react';
import {Userinfoplus} from "../../auth/UserLoginData"
import GSpy3_api from "../../api/gSpy3_api"

const hiddenStyle : CSSProperties= {
    display: "none"
  };

type UserNotSignedInState = {


};
type UserNotSignedInProps = {
    userInfo : Userinfoplus|undefined
};

class  UserNotSignedIn extends React.Component<UserNotSignedInProps,UserNotSignedInState> {
/*
  constructor(props : UserNotSignedInProps){
    super(props);
  }*/

  handleGoogleLogin (){

    
    try {
        let api = GSpy3_api.getAPI();
        api.getAuthUrl("google").then((url=>{ 
            window.location.assign(url);
        }));
    } catch (e) {
      console.error(e);
    }
     
  }

  render () : React.ReactElement {
      if(this.props.userInfo){
        return <div style={hiddenStyle}></div>;
      } 
      return <div className="UserNotSignedIn">
          <button onClick={this.handleGoogleLogin}>Login</button>
      </div>;
  }
}

/* Was  return <div className="UserNotSignedIn">
          <button onClick={this.handleGoogleLogin}>Login with Google</button>
      </div>;*/

export default UserNotSignedIn