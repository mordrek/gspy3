import React from 'react';
import GSpy3_api from "../../api/gSpy3_api"
import {NeedLoggedInMessage} from "../../components/NeedLoggedInMessage"
import messageManager, { PopupTypeEnum } from '../../managers/messageManager';
import { withRouter,RouteComponentProps } from "react-router-dom";
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../components/BaseComponent"
interface ActivateLeagueState  extends BaseComponentState {
};
interface ActivateLeagueProps extends BaseComponentProps  {
  
}; 

class  ActivateLeaguePage extends BaseComponent<ActivateLeagueProps,ActivateLeagueState> {

  constructor(props : ActivateLeagueProps){
    super(props);
    this.activationClick = this.activationClick.bind(this);
  }

  activationClick(){
    let originObj = document.getElementById("input_origin") as HTMLSelectElement;
    let leagueObj = document.getElementById("input_leaguename") as HTMLInputElement;
    let origin = originObj.options[originObj.selectedIndex].value;
    let league = leagueObj.value;
    this.props.selections.messages.addMessage(<span>Activating {league}...</span>, 10000, PopupTypeEnum.Neutral);
    GSpy3_api.getAPI().postLeagueActivate(origin,  [league]).then((res)=>{ 
      this.props.selections.messages.addMessage(<span>Activating {league} succeeded. Collection will begin shortly.</span>, 5000, PopupTypeEnum.Success);
      console.log(res);
      if((res as any)["ClassName"]=="System.Exception"){
        throw (res as any)["Message"];
      }
      else
      if(res.leagues.length > 0){
        this.props.history.push("/league/"+res.leagues[0].idleague);
      } else if(res.skipped > 0){
        throw "League already exists or failed to get data from cyanide";
      }
          }).catch((e : Error)=>{
      let sError = ""+(e)
      console.log(e);
      this.props.selections.messages.addMessage(<span>Activating {league} FAILED ({sError})</span>, 3000, PopupTypeEnum.Failure);
    });
     
  }

  onAutoClose(){
    this.setState({isActivating:false});
  }

  render () : React.ReactElement {
      return <div className="ActivateLeague bordered panel ">
        <div><h3>Activate your league</h3>
        <div>
          You only need to activate the league , not individual competitions. <br/>
          In BB3, the league name is shown in the community tab. <br/>
          Any competition within the league will be collected after the activation.<br/><br/>
          Only data for active leagues are collected.<br/>  
          When no data has been collected for a while, the league is considered inactive.<br/>
          Inactive leagues can be re-activated to restart collection.<br/><br/>
          The name of the league has to be exactly as written in game.<br/>
        </div>
        {
        <table>
              <thead><tr>
                  <td>Origin</td><td>League name </td><td></td>
                  </tr>
              </thead>
              <tbody>
            <tr>
                <td>
                    <select id='input_origin'>
                    <option value="BB3_pc" defaultChecked>BB3 PC</option>
                    <option value="BB3_ps">BB3 PlayStation 4</option>
                    <option value="BB3_xb1">BB3 XBoxOne</option>
                    <option value="BB2_pc" >BB2 PC</option>
                    <option value="BB2_ps4">BB2 PlayStation 4</option>
                    <option value="BB2_xb1">BB2 XBoxOne</option>
                    </select>
                </td>
                <td>
                    <input className='inputLarge' id='input_leaguename' placeholder="Enter league name here">
                    </input>
                </td>
                <td>
                  <button onClick = {this.activationClick}>Activate</button>
                </td>
            </tr></tbody>

          </table>
        }
        </div>
      </div>
      
  }
}

export default withRouter(ActivateLeaguePage)