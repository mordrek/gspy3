
import React  from 'react';
import GSpyTable from "../../components/GSpyTable"
import {ColumnLayout, ColumnID} from "../../api/views/columns"
import { QueryPageResponse} from "../../api/queryPageRequests"
import DataView from "../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../components/BaseComponent"
import {Userinfoplus} from "../../auth/UserLoginData"

interface LeagueCollectionsPageState extends BaseComponentState  {
  league : QueryPageResponse
  collections : QueryPageResponse
  };
interface LeagueCollectionsPageProps extends BaseComponentProps  {
}; 

class  LeagueCollectionsPage extends BaseComponent<LeagueCollectionsPageProps,LeagueCollectionsPageState> {

    
  constructor(props : LeagueCollectionsPageProps){ 
    super(props);
    
    this.views = [DataView.league, DataView.leagueCollections];
    this.onUpdate = this.onUpdate.bind(this);

  }
 
 
  onUpdate(){
    super.onUpdate();
    this.setState(
      {
        league:this.props.selections.responses.response[DataView[DataView.league]],
        collections : this.props.selections.responses.response[DataView[DataView.leagueCollections]]
      }); 

      
  }


  render () : React.ReactElement { 

    if(!this.state || !this.state.league) return <div></div>;
        return  <div className="panel">
            <div className="bordered panel">
            <h2>League collections</h2><br/>
            {this.state.collections&&this.state.collections.result.rows.length > 0&& <div>
                    Click on collection name to go to the collection page
                        <GSpyTable source={this.state.collections} layout={ColumnLayout.CollectionsView} filters={this.props.selections.filters}></GSpyTable>
                        </div>
            }
            { (!this.state.collections || this.state.collections.result.rows.length == 0)&&<span>
                This league does not contain any collections.<br/>
                <br/>
                Collections is a way to group competitions in order to look at the collected data as one unit.<br/>
                There is currently no user interface for managing collections, but ping mordrek at goblinSpy discord and we'll set it up for you<br></br>
                </span>
            }
            </div>
          </div>
        
      
      
  }
}

export default LeagueCollectionsPage