
import React  from 'react';
import GSpyTable from "../../components/GSpyTable"
import {ColumnLayout, ColumnID, Column} from "../../api/views/columns"
import { QueryPageResponse} from "../../api/queryPageRequests"
import DataView from "../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../components/BaseComponent"
 
interface CollectionPageState extends BaseComponentState  {
  league : QueryPageResponse
  collection : QueryPageResponse
  collectionComps : QueryPageResponse
};
interface CollectionPageProps extends BaseComponentProps  {
}; 

class  CollectionPage extends BaseComponent<CollectionPageProps,CollectionPageState> {

    
  constructor(props : CollectionPageProps){ 
    super(props);
    
    this.views = [DataView.league, DataView.collection, DataView.collectionCompetitions];
    this.onUpdate = this.onUpdate.bind(this);

  }
 
 
  onUpdate(){
    super.onUpdate();
    this.setState( 
      {
        league:this.props.selections.responses.response[DataView[DataView.league]],
        collection : this.props.selections.responses.response[DataView[DataView.collection]],
        collectionComps : this.props.selections.responses.response[DataView[DataView.collectionCompetitions]] 
      });  

      
  }


  render () : React.ReactElement {

    if(!this.state || !this.state.league || !this.state.collection?.result?.rows || this.state.collection?.result?.rows.length==0) return <div></div>;
        return  <div className="panel">
            <div className="bordered panel">
            <h2>{this.state.collection.result.rows[0][this.state.collection.result.cols[ColumnID[ColumnID.collection_name]]]}</h2>
            <small>Collection of {this.state.collection.result.rows[0][this.state.collection.result.cols[ColumnID[ColumnID.filter]]]}</small>
            </div>
            <div className="panel">
                    <GSpyTable source={this.state.collectionComps} layout={ColumnLayout.CompetitionsView} filters={this.props.selections.filters}></GSpyTable>
                    </div>
          </div>
        
      
      
  }
} 

export default CollectionPage