
import React  from 'react';
import GSpyTable from "../../components/GSpyTable"
import {ColumnLayout, ColumnID} from "../../api/views/columns"
import { QueryPageResponse} from "../../api/queryPageRequests"
import DataView from "../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../components/BaseComponent"

interface CollectionTeamPageState extends BaseComponentState  {
  collection : QueryPageResponse
  collectionTeams : QueryPageResponse
};
interface CollectionTeamPageProps extends BaseComponentProps  {
}; 

class  CollectionTeamsPage extends BaseComponent<CollectionTeamPageProps,CollectionTeamPageState> {

    
  constructor(props : CollectionTeamPageProps){ 
    super(props);
    
    this.views = [ DataView.collection, DataView.collectionTeams];
    this.onUpdate = this.onUpdate.bind(this);

  }
 
 
  onUpdate(){
    super.onUpdate();
    this.setState( 
      {
        collection : this.props.selections.responses.response[DataView[DataView.collection]],
        collectionTeams : this.props.selections.responses.response[DataView[DataView.collectionTeams]] 
      });  

      
  }


  render () : React.ReactElement {

    if(!this.state || !this.state.collectionTeams) return <div></div>;
        return  <div className="panel">
            <div className="bordered panel">
            <h2>League CollectionTeams</h2><br/>
            </div>
                  
            <GSpyTable source={this.state.collectionTeams} layout={ColumnLayout.CompTeamView} filters={this.props.selections.filters}></GSpyTable>
                   
          </div> 
        
      
      
  }
} 

export default CollectionTeamsPage