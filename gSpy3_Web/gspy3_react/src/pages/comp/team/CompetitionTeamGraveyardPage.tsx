
import React  from 'react';
import GSpyTable from "../../../components/GSpyTable";
import {ColumnLayout} from "../../../api/views/columns";
import { QueryPageResponse} from "../../../api/queryPageRequests"
import DataView from "../../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../../components/BaseComponent"

interface CompetitionTeamState extends BaseComponentState {
  compteamplayers :QueryPageResponse
  };
interface CompetitionTeamProps extends BaseComponentProps  {
};

class  CompetitionTeamGraveyardPage extends BaseComponent<CompetitionTeamProps,CompetitionTeamState> {

  constructor(props : CompetitionTeamProps){ 
    super(props);

    this.views = [DataView.comp,DataView.team,DataView.compTeamwStats,  DataView.compTeamGraveyard, ];
    this.onUpdate = this.onUpdate.bind(this);
     
  }

  onUpdate(){
    super.onUpdate();
    this.setState(
      {
        compteamplayers:this.props.selections.responses.response[DataView[DataView.compTeamGraveyard]],
      })
  }

  render () : React.ReactElement {
    
   if(!this.state){return <div></div>}
   if(this.state.compteamplayers?.result?.rows?.length ==0)
   {
    return <div>Sorry, no dead players in the graveyard</div>
   }
   return <div className="panel">
      <div className="flexcontainer">  
          <div className="panel">
            <GSpyTable source={this.state.compteamplayers} layout={ColumnLayout.CompTeamGraveyardView} filters={this.props.selections.filters}></GSpyTable>
          </div>
        </div>
      
      </div>
  }
}

export default CompetitionTeamGraveyardPage