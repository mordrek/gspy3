
import React  from 'react';
import GSpyTable from "../../../components/GSpyTable"
import {ColumnLayout, ColumnID} from "../../../api/views/columns"
import { QueryPageResponse} from "../../../api/queryPageRequests"
import DataView from "../../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../../components/BaseComponent"
import replay from "./replay"
import "./replay.css" 

interface MatchReplayState extends BaseComponentState  {
    matchReplay : QueryPageResponse
    matchInfo : QueryPageResponse
    matchTeams : QueryPageResponse
  };
interface MatchReplayProps extends BaseComponentProps  {
 
};
 
class  CompetitionMatchReplayPage extends BaseComponent<MatchReplayProps,MatchReplayState> {
 
    
  constructor(props : MatchReplayProps){ 
    super(props);
    this.views = [DataView.comp, DataView.match,DataView.matchTeams, DataView.matchReplayStats];
    this.onUpdate = this.onUpdate.bind(this);

    window.onresize = ()=>{  

        replay.replayManager.onResize(); 
        };
  }

  onUpdate(){
    super.onUpdate();

    if(!this.props.selections.responses.response[DataView[DataView.matchReplayStats]]){
      return;
    }

    this.setState( 
      {
        matchReplay:this.props.selections.responses.response[DataView[DataView.matchReplayStats]],
        matchInfo:this.props.selections.responses.response[DataView[DataView.match]],
        matchTeams : this.props.selections.responses.response[DataView[DataView.matchTeams]]
      });
  }

 componentDidUpdate(){
     if(this.state && this.state.matchReplay && this.state.matchReplay.result.fileData){
    let replayData = JSON.parse(this.state.matchReplay.result.fileData);
    console.log(replayData);
    replay.replayManager.initialize("replayField","sliderDiv","logDiv","avatarDiv",replayData);

     }
 } 

  render () : React.ReactElement {
    if(!this.state) return <div></div>;
    return  <div className="flexcontainer">
        <div className="panel replaydiv"> 
            {this.state.matchReplay&&<table><tbody>
                <tr><td>
                <div id="sliderDiv"></div>
                <div id="replayField" ></div>
                <div id="avatarDiv"></div>
                </td><td style={{width:"40%"}}>
                <div id="logDiv"></div>
                </td></tr>
                </tbody></table>}
     
          </div>
          </div>
      
  }
}

export default CompetitionMatchReplayPage