
import React  from 'react';
import GSpyTable from "../../../components/GSpyTable"
import {ColumnLayout, ColumnID, Column, ViewColumns} from "../../../api/views/columns"
import { QueryPageResponse} from "../../../api/queryPageRequests"
import DataView from "../../../api/views/dataView"
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../../components/BaseComponent"
import { renderToStaticMarkup } from 'react-dom/server';
import {Link} from "react-router-dom"
import youtubeImg from "../../../gfx/youtube.png"
import twitchImg from "../../../gfx/twitch.png"
import Races from "../../../api/dataformat/apiRaces"
import messageManager, { PopupTypeEnum } from '../../../managers/messageManager';
import GSpy3_api from "../../../api/gSpy3_api"

interface MatchState extends BaseComponentState  {
    matchteams : QueryPageResponse
    matchplayershome : QueryPageResponse
    matchplayersaway : QueryPageResponse

  };
interface MatchProps extends BaseComponentProps  {

};

class  CompetitionMatchPage extends BaseComponent<MatchProps,MatchState> {

    
  constructor(props : MatchProps){ 
    super(props);
    this.views = [DataView.comp, DataView.match,DataView.matchTeams, DataView.matchPlayers];
    this.onUpdate = this.onUpdate.bind(this);
    this.onRequestReplay = this.onRequestReplay.bind(this);
  }

  onUpdate(){
    super.onUpdate();

    if(!this.props.selections.responses.response[DataView[DataView.matchPlayers]]){
      return;
    }
    let teams : QueryPageResponse = this.props.selections.responses.response[DataView[DataView.matchTeams]];
    let mph: QueryPageResponse =  JSON.parse(JSON.stringify(this.props.selections.responses.response[DataView[DataView.matchPlayers]]))
    let mpa: QueryPageResponse =  JSON.parse(JSON.stringify(this.props.selections.responses.response[DataView[DataView.matchPlayers]]))
    let teamidcol = teams.result.cols[ColumnID[ColumnID.idteam]]; 

    if(mph?.result.rows){
      for(let i=0; i < mph.result.rows.length;i++){
        let idcol = mph.result.cols[ColumnID[ColumnID.idteam]];
        if(teams.result.rows.length>1 && mph.result.rows[i] &&  mph.result.rows[i][idcol]===teams.result.rows[1][teamidcol]){
          mph.result.rows.splice(i,1);
          i--;
        }
      }
    }
    if(mpa?.result.rows){
      for(let i=0; i < mpa.result.rows.length;i++){
        let idcol = mpa.result.cols[ColumnID[ColumnID.idteam]];
        if(teams.result.rows.length>0 && mph.result.rows[i] && mpa.result.rows[i][idcol]===teams.result.rows[0][teamidcol]){
          mpa.result.rows.splice(i,1);
          i--;
        }
      } 
    }

    this.setState( 
      {
        matchteams:this.props.selections.responses.response[DataView[DataView.matchTeams]],
        matchplayershome:mph,
        matchplayersaway:mpa,
      });
  }

  onRequestReplay(){
    this.props.selections.messages.addMessage(<span>Requesting competition maintenance...</span>, 10000, PopupTypeEnum.Neutral);
    
    let idmatch = +this.state.matchteams.result.rows[0][this.state.matchteams.result.cols[ColumnID[ColumnID.idmatch]]];
    let idcomp = +this.state.matchteams.result.rows[0][this.state.matchteams.result.cols[ColumnID[ColumnID.idcompetition]]];
    GSpy3_api.getAPI().postRequestReplay(idcomp,idmatch).then((res)=>{ 
        this.props.selections.messages.addMessage(<span>Download of replay has been requested. </span>, 5000, PopupTypeEnum.Success);
        setTimeout(function () {
              document.location.reload();
              }, 1000); 
        
          }).catch((e : Error)=>{
      let sError = ""+(e)
      this.props.selections.messages.addMessage(<span>Request of replay download has FAILED ({sError})</span>, 3000, PopupTypeEnum.Failure);
    });
  }
 



  render () : React.ReactElement {


    if(!this.state) return <div></div>;


    let rows = this.state.matchteams.result.rows;
    if(rows.length<= 1) return <div>Missing team information</div>
    let cols = this.state.matchteams.result.cols;
    let colname = cols[ColumnID[ColumnID.team_name]]
    let colidteam = cols[ColumnID[ColumnID.idteam]]
    let collogo = cols[ColumnID[ColumnID.logo]]
    let colrace = cols[ColumnID[ColumnID.idrace]]
    let colcoach = cols[ColumnID[ColumnID.idcoach]]
    let colcoachname = cols[ColumnID[ColumnID.coach_name]]
    let colscore = cols[ColumnID[ColumnID.score]]
    let colrnd = cols[ColumnID[ColumnID.round]]
    let coldate = cols[ColumnID[ColumnID.finished]]
    let coldur = cols[ColumnID[ColumnID.duration]]
    let coltv = cols[ColumnID[ColumnID.team_value]]
    let colapo = cols[ColumnID[ColumnID.apo]]
    let colRR = cols[ColumnID[ColumnID.rerolls]]
    let colass = cols[ColumnID[ColumnID.asscoaches]]
    let colcheer = cols[ColumnID[ColumnID.cheerleaders]]
    
    let idcomp = +rows[0][cols[ColumnID[ColumnID.idcompetition]]]
    let idmatch = +rows[0][cols[ColumnID[ColumnID.idmatch]]]
    let youtube1 = rows[0][cols[ColumnID[ColumnID.youtube]]]
    let twitch1 = rows[0][cols[ColumnID[ColumnID.twitch]]]
    let youtube2 = rows[1][cols[ColumnID[ColumnID.youtube]]]
    let twitch2 = rows[1][cols[ColumnID[ColumnID.twitch]]]

    let v2Data = rows[0][cols[ColumnID[ColumnID.rushes]]]!="" ; 

    let racename1 = Races.GetName(+(rows[0][colrace]));
    let raceimg1="/gspy/gfx/races/"+racename1+".png";
    let racename2 = Races.GetName(+(rows[1][colrace]));
    let raceimg2="/gspy/gfx/races/"+racename2+".png";

    let replayFile = "";
    replayFile = "/gspy/matches/"+(rows[0][coldate].substr(0,rows[0][coldate].indexOf(" ")))+"/"+( (+(rows[0][cols["match_origin_id"]])).toString(16))+".bbrz";

    return  <div className="flexcontainer">
        <div className="panel bordered">
          <table style={{width:"100%", tableLayout:"fixed"}}><tbody>
            <tr>
            <td style={{width:"55pt"}}>
              <img className="iconLarge"  title={racename1} src={raceimg1}></img>
              </td><td style={{width:"55pt"}}>
              <img className="iconLarge" src={"/gspy/gfx/logos/"+rows[0][collogo]+".png"} onError={(e)=>{(e.target as HTMLImageElement).src='/gspy/gfx/logos/unknown.png'}} ></img></td>
            <td  style={{width:"5*"}}><h2><Link to={"/comp/"+idcomp+"/team/"+rows[0][colidteam]}><span style={{fontSize:30}}>{rows[0][colname]}</span></Link></h2>

                {youtube1&&<a title="Click to open youtube" target="_blank" href={youtube1} className="imageButton"><img  className="iconThin" src={youtubeImg}></img></a>}
                {twitch1&& <a title="Click to open twitch" target="_blank" href={twitch1} className="imageButton"><img  className="iconThin" src={twitchImg}></img></a>}
            
                    <Link to={"/comp/"+idcomp+"/coach/"+rows[0][colcoach]}>{rows[0][colcoachname]}</Link>
            </td>
            <td style={{textAlign:"center",width:"1*", minWidth:"150pt"}}>
              <div style={{fontSize:48}}>
                {rows[0][colscore]} - {rows[1][colscore]}
              </div>
              <div>
                <small>{ViewColumns.writeDateTime(rows[0][coldate], false)}</small>
                <small>, {rows[0][coldur]} min</small>
                {+rows[0][colrnd]>0&&<span><small>, Round {rows[0][colrnd]}</small> </span>}
                {rows[0][cols[ColumnID[ColumnID.supporters]]]&&<span><small>, {rows[0][cols[ColumnID[ColumnID.supporters]]]} supporters</small></span>}  
                {v2Data&&<span>, <a target="_blank" href={replayFile}><small>replay</small></a></span>} 
                {!v2Data&&<span>, <a target="_blank" onClick={this.onRequestReplay}><small>Request replay</small></a></span>}   
              </div>
            </td>
            <td style={{textAlign:"right",width:"5*"}}>
                <h2><Link to={"/comp/"+idcomp+"/team/"+rows[1][colidteam]}><span style={{fontSize:30}}>{rows[1][colname]}</span></Link></h2>
                {youtube2&&<a title="Click to open youtube" target="_blank" href={youtube2} className="imageButton"><img  className="iconThin" src={youtubeImg}></img></a>}
                {twitch2&& <a title="Click to open twitch" target="_blank" href={twitch2} className="imageButton"><img  className="iconThin" src={twitchImg}></img></a>}
                        
                    <Link to={"/comp/"+idcomp+"/coach/"+rows[1][colcoach]}>{rows[1][colcoachname]}</Link>
            </td>
            <td  style={{textAlign:"right", width:"55pt"}}>
              <img className="iconLarge"  src={"/gspy/gfx/logos/"+rows[1][collogo]+".png"} onError={(e)=>{(e.target as HTMLImageElement).src='/gspy/gfx/logos/unknown.png'}} ></img>
              </td><td style={{width:"55pt"}}>
              <img className="iconLarge" title={racename2} src={raceimg2}></img></td>
              </tr>
              </tbody>
            </table>
            <table className="overviewTable">
            <tbody>
              <tr>
              <td >
                
               
                </td>
                <td className="padded">
                <table  className="GSpyTable overviewTable">
                  <thead >
                    <tr>
                      <td  colSpan={5} className="middler">Pass stats</td>
                    </tr>
                  </thead>
                  <tbody>
                    {v2Data&&
                        <tr>
                        <td className="leftie cellHeader">Pickups</td><td><span className="pos">{rows[0][cols[ColumnID[ColumnID.pickups]]]}</span></td>
                        <td><span className="neg">({rows[0][cols[ColumnID[ColumnID.pickups_failed]]]})</span></td>
                        </tr>
                    }
                  <tr>
                  <td className="leftie cellHeader">Passing [m]</td><td>{rows[0][cols[ColumnID[ColumnID.pass_meters]]]}</td>
                  </tr>
                  <tr>
                  <td className="leftie cellHeader">Passes</td><td><span className="pos">{rows[0][cols[ColumnID[ColumnID.passes]]]}</span></td>
                  <td>
                  {v2Data && <span className="neg">({rows[0][cols[ColumnID[ColumnID.passes_failed]]]})</span>} </td>
                  </tr>
                  <tr>
                  <td className="leftie cellHeader">Catches</td><td><span className="pos">{rows[0][cols[ColumnID[ColumnID.catches]]]}</span></td>
                  <td>{v2Data && <span className="neg">({rows[0][cols[ColumnID[ColumnID.catches_failed]]]})</span>}</td>
                  </tr>
                  {v2Data&& 
                    <tr>
                    <td className="leftie cellHeader">Completions</td><td>{rows[0][cols[ColumnID[ColumnID.completions]]]}</td>
                    </tr>
                  }
                 
                  <tr>
                  <td className="leftie cellHeader">Interceptions</td><td>{rows[0][cols[ColumnID[ColumnID.interceptions]]]}</td>
                  </tr>
       
                  </tbody>
                </table>
                <table  className="GSpyTable overviewTable">
                  <thead >
                    <tr>
                      <td  colSpan={5} className="middler">Move stats</td>
                    </tr>
                  </thead>
                  <tbody>
                  <tr>
                  <td className="leftie cellHeader">Running [m]</td><td>{rows[0][cols[ColumnID[ColumnID.run_meters]]]}</td>
                  </tr>
                  {v2Data&&
                    <tr>
                    <td className="leftie cellHeader">Dodges</td><td><span className="pos">{rows[0][cols[ColumnID[ColumnID.dodges]]]}</span></td>
                    <td><span className="neg">({rows[0][cols[ColumnID[ColumnID.dodges_failed]]]})</span></td>
                    </tr>
                  }
                  {v2Data && 
                    <tr>
                    <td className="leftie cellHeader">Rushes</td><td><span className="pos">{rows[0][cols[ColumnID[ColumnID.rushes]]]}</span></td>
                    <td><span className="neg">({rows[0][cols[ColumnID[ColumnID.rushes_failed]]]})</span></td>
                    </tr>
                  }
                  </tbody>
                </table>
                </td>
                <td className="padded">
                <table  className="GSpyTable overviewTable">
                  <thead >
                    <tr>
                      <td  colSpan={5} className="middler">Kill stats</td>
                    </tr>
                  </thead>
                  <tbody>
                  <tr>
                  <td className="leftie cellHeader">Blocks</td><td><span className="pos">{rows[0][cols[ColumnID[ColumnID.blocks_for]]]}</span> </td>
                  <td><span className="neg" title="Blocks received"> ({rows[0][cols[ColumnID[ColumnID.blocks_against]]]})</span></td>
                  </tr>
                  <tr>
                  <td className="leftie cellHeader">Breaks</td><td><span className="pos">{rows[0][cols[ColumnID[ColumnID.breaks_for]]]}</span> </td>
                  <td><span className="neg" title="Breaks received"> ({rows[0][cols[ColumnID[ColumnID.breaks_against]]]})</span></td>
                  </tr>
                  <tr>
                  <td className="leftie cellHeader">Stuns</td><td><span className="pos">{rows[0][cols[ColumnID[ColumnID.stuns_for]]]}</span> </td>
                  <td><span className="neg" title="Stuns received"> ({rows[0][cols[ColumnID[ColumnID.stuns_against]]]})</span></td>
                  </tr>
                  <tr>
                  <td className="leftie cellHeader">KOs</td><td><span className="pos">{rows[0][cols[ColumnID[ColumnID.kos_for]]]}</span> </td>
                  <td><span className="neg" title="KOs received"> ({rows[0][cols[ColumnID[ColumnID.kos_against]]]})</span></td>
                  </tr>
                  <tr>
                  <td className="leftie cellHeader">Cas</td><td><span className="pos">{rows[0][cols[ColumnID[ColumnID.casualties_for]]]}</span> </td>
                  <td><span className="neg" title="Casualties received"> ({rows[0][cols[ColumnID[ColumnID.casualties_against]]]})</span></td>
                  </tr>
                  <tr>
                  <td className="leftie cellHeader">Kill</td><td><span className="pos">{rows[0][cols[ColumnID[ColumnID.kills_for]]]}</span> </td>
                  <td><span className="neg" title="Kills received"> ({rows[0][cols[ColumnID[ColumnID.kills_against]]]})</span></td>
                  </tr>
                  <tr>
                  <td className="leftie cellHeader">Pushouts</td><td><span className="pos">{rows[0][cols[ColumnID[ColumnID.pushouts]]]}</span></td>
                  </tr>
                  <tr>
                  <td className="leftie cellHeader">Fouls</td><td><span className="pos">{v2Data&&rows[0][cols[ColumnID[ColumnID.fouls]]]}</span></td><td><span className="neg">({rows[0][cols[ColumnID[ColumnID.expulsions]]]})</span></td>
                  </tr>
                  </tbody>
                </table>
                </td>
                <td className="padded" style={{alignContent:"center"}}>
                <table  className="GSpyTable overviewTable">
                  <thead >
                    <tr>
                      <td colSpan={5} className="middler">
                        Overview
                      </td>
                    </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>{rows[0][coltv]}</td>
                    <td></td>
                    <td className="middler cellHeader">Team value</td>
                    <td></td>
                    <td className="leftie">{rows[1][coltv]}</td>
                    </tr>
                    <tr>
                    <td >{rows[0][colRR]}</td>
                    <td ></td>
                    <td className="middler cellHeader">Rerolls</td>
                    <td></td>
                    <td  className="leftie">{rows[1][colRR]}</td>
                    </tr>
                    <tr>
                    <td >{rows[0][colapo]} / {rows[0][colass]} / {rows[0][colcheer]}</td>
                    <td ></td>
                    <td className="middler cellHeader">Apo / Ass / Cheer</td>
                    <td></td>
                    <td  className="leftie">{rows[1][colapo]} / {rows[1][colass]} / {rows[1][colcheer]}</td>
                    </tr>
                    <tr>
                    <td >{rows[0][cols[ColumnID[ColumnID.cash_spent]]]}</td>
                    <td ></td>
                    <td className="middler cellHeader">Cash spent</td>
                    <td></td> 
                    <td className="leftie">{rows[1][cols[ColumnID[ColumnID.cash_spent]]]}</td>
                    </tr>
                    <tr>
                    <td >{rows[0][cols[ColumnID[ColumnID.possession]]]}</td>
                    <td ></td>
                    <td className="middler cellHeader">Possession</td>
                    <td></td> 
                    <td className="leftie">{rows[1][cols[ColumnID[ColumnID.possession]]]}</td>
                    </tr>
                    <tr>
                    <td >{rows[0][cols[ColumnID[ColumnID.mvp]]]}</td>
                    <td ></td>
                    <td className="middler cellHeader">MVP</td>
                    <td></td>
                    <td className="leftie">{rows[1][cols[ColumnID[ColumnID.mvp]]]}</td>
                    </tr>
                    <tr>
                    <td >{rows[0][cols[ColumnID[ColumnID.turnovers]]]}</td>
                    <td ></td>
                    <td className="middler cellHeader">Turnovers</td>
                    <td></td>
                    <td className="leftie">{rows[1][cols[ColumnID[ColumnID.turnovers]]]}</td>
                    </tr>
                    { v2Data && <tr>
                    <td >{rows[0][cols[ColumnID[ColumnID.boneheads]]]}</td>
                    <td ></td>
                    <td className="middler cellHeader">Boneheads</td>
                    <td></td>
                    <td className="leftie">{rows[1][cols[ColumnID[ColumnID.boneheads]]]}</td>
                    </tr>
                    }
                    {v2Data&&
                    <tr>
                    <td >{rows[0][cols[ColumnID[ColumnID.sacks]]]}</td>
                    <td ></td>
                    <td className="middler cellHeader">Sacks</td>
                    <td></td>
                    <td className="leftie">{rows[1][cols[ColumnID[ColumnID.sacks]]]}</td>
                    </tr>
                    }
                    {/*
                    <tr>
                    <td className="rightie">{rows[0][cols[ColumnID[ColumnID.occ_own]]]} %</td>
                    <td className="rightie">{rows[0][cols[ColumnID[ColumnID.occ_their]]]} %</td>
                    <td className="middler">Occupation</td>
                    <td>{rows[1][cols[ColumnID[ColumnID.occ_their]]]} %</td>
                    <td>{rows[1][cols[ColumnID[ColumnID.occ_own]]]} %</td>
                    </tr>*/}
                  </tbody>
                </table>
                </td>
                
                <td className="padded">
                <table  className="GSpyTable overviewTable">
                  <thead >
                    <tr>
                      <td  colSpan={5} className="middler">Kill stats</td>
                    </tr>
                  </thead>
                  <tbody>
                  <tr>
                  <td><span className="pos">{rows[1][cols[ColumnID[ColumnID.blocks_for]]]}</span> </td>
                  <td><span className="neg" title="Blocks received"> ({rows[1][cols[ColumnID[ColumnID.blocks_against]]]})</span></td><td className="leftie cellHeader">Blocks</td>
                  </tr>
                  <tr>
                  <td><span className="pos">{rows[1][cols[ColumnID[ColumnID.breaks_for]]]}</span> </td>
                  <td><span className="neg" title="Breaks received"> ({rows[1][cols[ColumnID[ColumnID.breaks_against]]]})</span></td><td className="leftie cellHeader">Breaks</td>
                  </tr>
                  <tr>
                  <td><span className="pos">{rows[1][cols[ColumnID[ColumnID.stuns_for]]]}</span> </td>
                  <td><span className="neg"title="Stuns received"> ({rows[1][cols[ColumnID[ColumnID.stuns_against]]]})</span></td><td className="leftie cellHeader">Stuns</td>
                  </tr>
                  <tr>
                  <td><span className="pos">{rows[1][cols[ColumnID[ColumnID.kos_for]]]}</span> </td>
                  <td><span className="neg"title="KOs received"> ({rows[1][cols[ColumnID[ColumnID.kos_against]]]})</span></td><td className="leftie cellHeader">KOs</td>
                  </tr>
                  <tr>
                  <td><span className="pos">{rows[1][cols[ColumnID[ColumnID.casualties_for]]]}</span> </td>
                  <td><span className="neg"title="Casualties received"> ({rows[1][cols[ColumnID[ColumnID.casualties_against]]]})</span></td><td className="leftie cellHeader">Cas</td>
                  </tr>
                  <tr>
                  <td><span className="pos">{rows[1][cols[ColumnID[ColumnID.kills_for]]]}</span> </td>
                  <td><span className="neg"title="Kills received"> ({rows[1][cols[ColumnID[ColumnID.kills_against]]]})</span></td><td className="leftie cellHeader">Kill</td>
                  </tr>
                  <tr>
                  <td><span className="pos">{rows[0][cols[ColumnID[ColumnID.pushouts]]]}</span></td><td></td><td className="leftie cellHeader">Pushouts</td>
                  </tr>
                  <tr>
                  <td><span className="pos">{v2Data&&rows[1][cols[ColumnID[ColumnID.fouls]]]}</span> </td>
                  <td><span className="neg" title="Expulsions"> ({rows[1][cols[ColumnID[ColumnID.expulsions]]]})</span></td><td className="leftie cellHeader">Fouls</td>
                  </tr>
                  </tbody>
                </table>
                </td>
                <td  className="padded">
                
                <table  className="GSpyTable overviewTable">
                  <thead >
                    <tr>
                      <td  colSpan={5} className="middler">Pass stats</td>
                    </tr>
                  </thead>
                  <tbody>
                    {v2Data&&
                      <tr>
                      <td><span className="pos">{rows[1][cols[ColumnID[ColumnID.pickups]]]}</span> </td>
                      <td><span className="neg" title="Failures"> ({rows[1][cols[ColumnID[ColumnID.pickups_failed]]]})</span></td><td className="leftie cellHeader">Pickups</td>
                        </tr>  }
                  <tr>
                  <td>{rows[1][cols[ColumnID[ColumnID.pass_meters]]]}</td><td></td><td className="leftie cellHeader">Passing [m]</td>
                  </tr>
                  <tr>
                  <td><span className="pos">{rows[1][cols[ColumnID[ColumnID.passes]]]}</span> </td>
                  <td>
                  {v2Data&&
                    <span className="neg" title="Failures"> ({rows[1][cols[ColumnID[ColumnID.passes_failed]]]})</span>
                  }
                   </td>

                   <td className="leftie cellHeader">Passes</td>
                  </tr> 
                  <tr>
                  <td><span className="pos">{rows[1][cols[ColumnID[ColumnID.catches]]]}</span> </td>
                  <td>
                  {v2Data &&
                    <span className="neg" title="Failures"> ({rows[1][cols[ColumnID[ColumnID.catches_failed]]]})</span>
                  }
                    </td><td className="leftie cellHeader">Catches</td>
                  </tr> 
                  {v2Data &&
                    <tr>
                    <td>{rows[1][cols[ColumnID[ColumnID.completions]]]}</td><td></td><td className="leftie cellHeader">Completions</td>
                    </tr>
                  }
                  <tr>
                  <td>{rows[1][cols[ColumnID[ColumnID.interceptions]]]}</td><td></td><td className="leftie cellHeader">Interceptions</td>
                  </tr>

                  </tbody>
                </table>
                <table  className="GSpyTable overviewTable">
                  <thead >
                    <tr>
                      <td  colSpan={5} className="middler">Move stats</td>
                    </tr>
                  </thead>
                  <tbody>
                  <tr>
                  <td>{rows[1][cols[ColumnID[ColumnID.run_meters]]]}</td><td></td><td className="leftie cellHeader">Running [m]</td>
                  </tr>
                  {v2Data &&
                  <tr>
                  <td><span className="pos">{rows[1][cols[ColumnID[ColumnID.dodges]]]}</span> </td>
                  <td><span className="neg" title="Failures"> ({rows[1][cols[ColumnID[ColumnID.dodges_failed]]]})</span></td><td className="leftie cellHeader">Dodges</td>
                  </tr>
                  }
                  {v2Data  &&
                  <tr>
                  <td><span className="pos">{rows[1][cols[ColumnID[ColumnID.rushes]]]}</span> </td>
                  <td><span className="neg" title="Failures"> ({rows[1][cols[ColumnID[ColumnID.rushes_failed]]]})</span></td><td className="leftie cellHeader">Rushes</td>
                  </tr>                  
                  }
                  </tbody>
                  </table>
                  
                </td>
                </tr>
                

              
              </tbody></table>

        </div>
        <div className="panel">
         <GSpyTable source={this.state.matchteams} layout={ColumnLayout.MatchTeamsView} filters={this.props.selections.filters}></GSpyTable>
            </div>
      
          </div>
      
  }
}

export default CompetitionMatchPage