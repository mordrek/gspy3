import React from 'react';
import GSpy3_api from "../../api/gSpy3_api"
import {NeedLoggedInMessage} from "../../components/NeedLoggedInMessage"
import messageManager, { PopupTypeEnum } from '../../managers/messageManager';
import { withRouter,RouteComponentProps, Link } from "react-router-dom";
import BaseComponent,{BaseComponentProps, BaseComponentState} from "../../components/BaseComponent"
import { ColumnLayout } from '../../api/views/columns';
interface AboutBotPageState  extends BaseComponentState {
};
interface AboutBotPageProps extends BaseComponentProps  {
  
}; 

class  AboutBotPage extends BaseComponent<AboutBotPageProps,AboutBotPageState> {

  constructor(props : AboutBotPageProps){
    super(props);
  }


  render () : React.ReactElement {
      return <div className="bordered panel information ">
      <div><h2>GoblinSpy Discord bot</h2>
      <div>
        <div className="bordered">
              <h3>About GoblinSpy discord bot</h3>
              The discord bot is a straight forward bot with good integration with goblinSpy. There are more advanced and/or specialized bots out there, like the Spike! bot and the ExtraArmsBox for those who like that <br></br>
              <br/>
        <h3>Installing</h3>
              Invite the bot to your discord using this link: <b><a target="_blank" href="https://discord.com/oauth2/authorize?client_id=759051074690351104&scope=bot">INVITE ME!</a></b><br/>
              The bot only needs to be able to read and write chat, nothing else. You can now message the bot either in direct message or in any channel.<br/>
              All commands starts with !gs , and you can type just !gs to show the help<br></br>
        </div>
        <div className="bordered">
            <h3>Following competitions</h3>
                To save typing, you can assign competitions (or collections) to a channel and this will be used in commands if you don't write a specific competition.<br/>
            <table><tbody>
            <tr><td><span className="command">!gs channel comp add &lt;id|name&gt;</span></td><td>Adds the given competition to the channel list.</td></tr>
            <tr><td><span className="command">!gs channel comp remove &lt;id|name&gt;</span></td><td>Removes the given competition to the channel list.</td></tr>
            <tr><td><span className="command">!gs channel comp list</span></td><td>Lists competitions that have been added to the channel.</td></tr>
                </tbody></table>
            
            ID or name, means id of the competition, name of a collection or a part of the competition name. <br/>
            
            <br/>
            <h3>Getting match reports</h3>
            To get match reports, first add some competitions to the channel (see above) and then enable receiving match reports with:<br/>
            <table><tbody>
            <tr><td><span className="command">!gs channel comp events &lt;true/false&gt;</span></td><td>Enable or disable match reports from competitions added to this channel</td></tr>
                </tbody></table>
        </div>

    
        <div className="bordered">
            <h3>Competition commands</h3>
            (&lt;id|name&gt;) is optional. If missing, the competitions added to the current channel will be used.<br/>
                <table><tbody>
                <tr><td><span className="command">!gs comp top</span></td><td>Show top standings of competition in channel</td></tr>
                <tr><td><span className="command">!gs comp top (&lt;race&gt;)</span></td><td>Show top standings of competition in channel filtered for specific race</td></tr>
                <tr><td><span className="command">!gs comp (&lt;id|name&gt;) top (&lt;x&gt;) (&lt;race&gt;)</span></td><td>
                        If &lt;x&gt; is given, that's the number of rows shown.<br/>
                        If &lt;race&gt; is given, the result is filtered on the given race.  &lt;race&gt; should be the first letters in the race name, like <i>dark</i>, or a common abbreviation like <i>chorf</i>, <i>dorf</i>
                        </td></tr>
                <tr><td><span className="command">!gs comp (&lt;id|name&gt;) coach &lt;coachname&gt;</span></td><td>Show coach information for given coach in the comp</td></tr>
                <tr><td><span className="command">!gs comp (&lt;id|name&gt;) team &lt;teamname&gt; </span></td><td>Show team information for given team in the comp </td></tr>
                <tr><td><span className="command">!gs comp (&lt;id|name&gt;) overtake &lt;pos&gt; from &lt;frompos&gt;  </span></td><td>Shows how many wins that will be needed for the team at the "frompos" position to take the position given by "pos". </td></tr>
                <tr><td><span className="command">!gs comp (&lt;id|name&gt;) sched next (&lt;x&gt;)</span></td><td>Shows next (x) scheduled matches in the comp </td></tr>
                <tr><td><span className="command">!gs comp (&lt;id|name&gt;) result (&lt;x&gt;)</span></td><td>Shows last (x) results in the comp </td></tr>
        </tbody></table>
            </div>

            <div className="bordered">
            <h3>Misc commands</h3>
                <table><tbody>
                <tr><td><span className="command">!gs coach &lt;name&gt;</span></td><td>Show global information on specified coach</td></tr>
                <tr><td><span className="command">!gs rank &lt;wins&gt; &lt;draws&gt; &lt;losses&gt;</span></td><td>Calculate the rank points based on given wins, draws and losses</td></tr>
                </tbody></table>
            </div>
        </div>
        </div>
        
    </div>
      
  }
}

export default withRouter(AboutBotPage)