import React from 'react';
import LoginCallback from "./auth/LoginCallback"
import './App.css';


import {
  Switch,
  Route 
} from "react-router-dom";
 
import GSpy3_api from './api/gSpy3_api';
import { Userinfoplus } from './auth/UserLoginData';
import GlobalCoachPage from "./pages/global/GlobalCoachPage"
import GlobalCoachRacesPage from "./pages/global/GlobalCoachRacesPage"
import { withRouter } from 'react-router-dom'; 
import {RouteComponentProps} from "react-router";
import Popup from "./components/Popup"
import messageManager, { PopupData } from "./managers/messageManager"
import UserHomePage from "./pages/user/UserHomePage"
import LeaguePage from "./pages/league/LeaguePage"
import FindLeaguePage from "./pages/league/FindLeaguePage"
import  {PageSelectionManager} from "./managers/pageSelectionManager"
import CompetitionPage from "./pages/comp/CompetitionPage" 
import CompetitionCoachPage from "./pages/comp/coach/CompetitionCoachPage"
import CompetitionTeam from "./pages/comp/team/CompetitionTeamPage"
import CompetitionTeamResults from "./pages/comp/team/CompetitionTeamResultsPage"
import CompetitionTeamStanding from "./pages/comp/team/CompetitionTeamStandingPage"
import CompetitionTeamSchedule from "./pages/comp/team/CompetitionTeamSchedulePage"
import CompetitionTeamGraveyard from "./pages/comp/team/CompetitionTeamGraveyardPage"
import { QueryPageResponseView } from './api/queryPageRequests';
import CompetitionMatchPage from "./pages/comp/match/CompetitionMatchPage"
import MatchPlayers from "./pages/comp/match/CompetitionMatchPlayersPage"
import SearchResults from "./components/SeachResults"
import Header from "./components/Header"
import Activity from "./components/Activity"
import Results from "./pages/comp/CompetitionResultsPage"
import CompetitionSchedulePage from "./pages/comp/CompetitionSchedulePage"
import StatusManager from "./managers/statusManager"
import ServerStatus from './api/views/statusCollection';
import Status from "./components/status/Status"
import ActivateLeaguePage from './pages/league/ActivateLeaguePage';
import UserSettingsPage from "./pages/user/UserSettingsPage";
import UserManager, { UserConfig } from "./managers/userManager"
import CompetitionMatchDicePage from "./pages/comp/match/CompetitionMatchDicePage"
import CompetitionMatchReplayPage from "./pages/comp/match/CompetitionMatchReplayPage"
import AboutPage from "./pages/about/AboutPage"
import AboutBotPage from "./pages/about/AboutBotPage"
import LinksPage from "./pages/LinksPage"
import LeagueCollectionsPage from "./pages/league/LeagueCollectionsPage"
import CollectionPage from "./pages/collection/CollectionPage"
import CollectionTeamsPage from "./pages/collection/CollectionTeamsPage"
import CompetitionStatsPage from "./pages/comp/CompetitionStatsPage"
import IDInputPage from "./pages/about/IDInputPage"  
import { ColumnLayout } from './api/views/columns';
import DataView from "./api/views/dataView"
import CompetitionCoachRacePage from "./pages/comp/coach/CompetitionCoachRacePage"
import OvertakePage from "./pages/comp/OvertakePage"

type AppState = {  
  userInfo : Userinfoplus |undefined,
  data : QueryPageResponseView
  popup : PopupData
  gspyLocation : string,
  status : ServerStatus

};
type AppProps  = RouteComponentProps & {
  history : any 
};

class  App extends React.Component<AppProps,AppState> {

  api : GSpy3_api = GSpy3_api.getAPI();
  messages : messageManager = messageManager.getMessageManager();
  pageSelectionManager : PageSelectionManager = new PageSelectionManager();
  statusManager : StatusManager = new StatusManager();

  lastFilter : string = ""

  constructor(props : AppProps){
    super(props);

    this.statusManager.onUpdate = (newStatus : ServerStatus)=> {
      this.setState({status:newStatus});
    }

    this.state = {
      userInfo : this.api.data?.userInfo,
      popup : new PopupData(),
      data : new QueryPageResponseView(),
      gspyLocation :"",
      status : new ServerStatus()
    };

    this.pageSelectionManager.init(this.messages, (o)=>{
        this.setState({data : o});
    });

    this.messages.popupChangedCallback = ()=>{
      this.setState({popup : this.messages.popup});
    }

    this.api.addCallback((newUserInfo)=>{
      this.setState({userInfo:newUserInfo})
    });


  }

  

  getSnapshotBeforeUpdate(prevProps : any){
   
    if(this.props.location !== prevProps.location || this.props.location.pathname !== prevProps.location.pathname ||
      this.lastFilter !== this.pageSelectionManager.filters.getFilterURL()){
      // router changed, so we must update location state
      this.pageSelectionManager.locationChanged();
      let newFilter = this.pageSelectionManager.filters.getFilterURL();
      if(newFilter !== this.lastFilter){
        this.lastFilter = newFilter;
        //console.log("NewFilter: "+this.lastFilter);
        this.setState({gspyLocation:this.lastFilter})
        return true;
      }
    }
    return null;

  }
  componentDidUpdate(){
    let el = document.getElementById("userStyle");
    let userConfig = UserManager.getConfig(this.state.userInfo?.id)

    if(userConfig)
    {
      if(!el || (el && userConfig.presentation.displayStyle != el.className))
      {
        if(el){
        el.parentNode?.removeChild(el);
        }
        if(userConfig?.presentation?.displayStyle){
          let link = document.createElement("link") as HTMLLinkElement;
          link.rel="stylesheet";
          link.type="text/css"; 
          link.className = userConfig?.presentation?.displayStyle;
          link.href = "/gspy/style/"+userConfig?.presentation?.displayStyle+".css";
          link.id="userStyle";
          document.head.appendChild(link);
        }

      }
    }
  }

  render () : React.ReactElement {
    
  return (
    <div className="App">
  

      <Header status={this.state.status} location={this.state.gspyLocation} pageSelectionManager={this.pageSelectionManager} userInfo={this.state.userInfo}></Header>
      
      <div className="mainContent">
        <Popup data={this.state.popup}></Popup>
        <Switch>
          <Route path="/login"></Route>
          <Route path="/oauth_callback"><LoginCallback></LoginCallback></Route> 
          <Route path="/status"><Status status={this.state.status}></Status></Route> 
          <Route path="/user/settings" render={(props) => <UserSettingsPage  userInfo={this.state.userInfo} gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager}></UserSettingsPage>}></Route>
          <Route path="/leagues/activate" render={(props) => <ActivateLeaguePage  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager}></ActivateLeaguePage>}></Route>
          <Route path="/leagues" render={(props) => <FindLeaguePage  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager}></FindLeaguePage>}></Route>
          <Route path="/league/:idleague/collections"  render={(props) => <LeagueCollectionsPage {...props}  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/league/:idleague"  render={(props) => <LeaguePage {...props}  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/coach/:idcoach/races"  render={(props) => <GlobalCoachRacesPage {...props}  userInfo={this.state.userInfo}  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/coach/:idcoach"  render={(props) => <GlobalCoachPage {...props}  userInfo={this.state.userInfo}  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/match/:idmatch/players"  render={(props) => <MatchPlayers {...props}  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/match/:idmatch/dice"  render={(props) => <CompetitionMatchDicePage {...props}  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/match/:idmatch/replay"  render={(props) => <CompetitionMatchReplayPage {...props}  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/match/:idmatch"  render={(props) => <CompetitionMatchPage {...props}  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/search/:search" render={(props) => <SearchResults {...props}   gspyLocation={this.state.gspyLocation} selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/overtake/:from/:to" render={(props) => <OvertakePage {...props}   gspyLocation={this.state.gspyLocation} selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/stats/teams"  render={(props) => <CompetitionStatsPage {...props} sourceQuery={DataView.compStatsTeam}   gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/stats/players"  render={(props) => <CompetitionStatsPage {...props}  sourceQuery={DataView.compStatsPlayer}   gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/stats/coaches"  render={(props) => <CompetitionStatsPage {...props}  sourceQuery={DataView.compStatsCoaches}   gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/stats/races"  render={(props) => <CompetitionStatsPage {...props}  sourceQuery={DataView.compStatsRaces}   gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/stats"  render={(props) => <CompetitionStatsPage {...props}  sourceQuery={DataView.compStatsTeam}   gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/match/:idmatch/players"  render={(props) => <MatchPlayers {...props}  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/match/:idmatch/dice"  render={(props) => <CompetitionMatchDicePage {...props}  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/match/:idmatch/replay"  render={(props) => <CompetitionMatchReplayPage {...props}  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/match/:idmatch"  render={(props) => <CompetitionMatchPage {...props}  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/coach/:idcoach/races"  render={(props) => <CompetitionCoachRacePage  userInfo={this.state.userInfo} {...props} gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/coach/:idcoach"  render={(props) => <CompetitionCoachPage  userInfo={this.state.userInfo} {...props} gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/team/:idteam/results"  render={(props) => <CompetitionTeamResults {...props} gspyLocation={this.state.gspyLocation} selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/team/:idteam/standing"  render={(props) => <CompetitionTeamStanding {...props} gspyLocation={this.state.gspyLocation} selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/team/:idteam/schedule"  render={(props) => <CompetitionTeamSchedule {...props} gspyLocation={this.state.gspyLocation} selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/team/:idteam/graveyard"  render={(props) => <CompetitionTeamGraveyard {...props} gspyLocation={this.state.gspyLocation} selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/team/:idteam"  render={(props) => <CompetitionTeam {...props} gspyLocation={this.state.gspyLocation} selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/activity"  render={(props) => <Activity {...props} gspyLocation={this.state.gspyLocation} selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/results"  render={(props) => <Results {...props} gspyLocation={this.state.gspyLocation} selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp/schedule"  render={(props) => <CompetitionSchedulePage {...props} gspyLocation={this.state.gspyLocation} selections={this.pageSelectionManager} />}></Route>
          <Route path="/comp/:idcomp"  render={(props) => <CompetitionPage {...props} userInfo={this.state.userInfo}   gspyLocation={this.state.gspyLocation} selections={this.pageSelectionManager} />}></Route>
          <Route path="/collection/:idcollection/teams"  render={(props) => <CollectionTeamsPage {...props}  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/collection/:idcollection"  render={(props) => <CollectionPage {...props}  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager} />}></Route>
          <Route path="/help/idinput" render={(props) => <IDInputPage  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager}></IDInputPage>}></Route>
          <Route path="/help/bot" render={(props) => <AboutBotPage  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager}></AboutBotPage>}></Route>
          <Route path="/help" render={(props) => <AboutPage  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager}></AboutPage>}></Route>
          <Route path="/links" render={(props) => <LinksPage  gspyLocation={this.state.gspyLocation}  selections={this.pageSelectionManager}></LinksPage>}></Route>
          <Route path="/search/:search" render={(props) => <SearchResults {...props}  gspyLocation={this.state.gspyLocation} selections={this.pageSelectionManager} />}></Route>
           <Route path="/:somewhere"> 
            Not sure what you are looking for, but I suggest using the buttons or search in the header
          </Route>
          <Route path="/"> 
            <UserHomePage gspyLocation={this.state.gspyLocation} selections={this.pageSelectionManager}  userInfo={this.state.userInfo}></UserHomePage>
          </Route>
        </Switch>
        </div>
    </div>
  );
}
}

export default withRouter(App);
