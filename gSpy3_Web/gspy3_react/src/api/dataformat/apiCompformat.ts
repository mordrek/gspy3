



class APICompFormat{

    getFormatName(idorigin: number) : string{
        switch(idorigin){
            case 0: return "Ladder";
            case 1: return "Single elimination";
            case 2: return "Round robin";
            case 3: return "Swiss";
        }
        return "";
    }
}

export default APICompFormat