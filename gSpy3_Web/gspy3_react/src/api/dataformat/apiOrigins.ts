



class APIOrigins{

    getOriginName(idorigin: number) : string{
        switch(idorigin){
            case 1: return "BB2_pc";
            case 2: return "BB2_xb1";
            case 3: return "BB2_ps4";
            case 4: return "BB3_pc";
            case 5: return "BB3_xbox";
            case 6: return "BB3_ps";
        }
        return "";
    }
}

export default APIOrigins