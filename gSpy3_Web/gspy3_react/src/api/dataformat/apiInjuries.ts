class APIInjuries{
    idToName : {[id:string] : string} = {}
    idToImg : {[id:string] : string} = {}

    alterStat(id:string, value : number, injuries: string) : number {
        if(injuries){
            
            for(let i=0; i < injuries.length;i+=2){
                let inj = injuries.substr(i,2);
                if ((inj==="Sa" || inj==="Sh") && id === "ma"){
                    value--;
                }
                if ((inj==="Fs" || inj==="Sc") && id === "av"){
                    value--;
                }
                if ((inj==="Bn") && id === "ag"){
                    value--;
                }
                if ((inj==="Sb") && id === "st"){
                    value--;
                }
            }
        }
        if(value < 1) value = 1;
        return value;

    }

    missesGame(injuries: string) : boolean {
        if(injuries){
            
            let injObj = JSON.parse(injuries);
            for(let i=0; i < injObj?.length;i++){
                let inj = injObj[i];
                
                if (inj !== "BadlyHurt")
                {
                    return true;
                }
            }
        }

        return false;

    }

    constructor(){
        
        this.idToImg[ "BadlyHurt"]=("/gfx/injuries/inj-Bh.png");
        this.idToImg[ "BrokenJaw"]=("/gfx/injuries/inj-mng.png");
        this.idToImg[ "BrokenRibs"]=("/gfx/injuries/inj-mng.png");
        this.idToImg[ "FracturedArm"]=("/gfx/injuries/inj-mng.png");
        this.idToImg[ "FracturedLeg"]=("/gfx/injuries/inj-mng.png");
        this.idToImg[ "SmashedHand"]=("/gfx/injuries/inj-mng.png");
        this.idToImg[ "GougedEyes"]=("/gfx/injuries/inj-mng.png");
        this.idToImg[ "GroinStrain"]=("/gfx/injuries/inj-mng.png");
        this.idToImg[ "PinchedNerve"]=("/gfx/injuries/inj-mng.png");
        this.idToImg[ "DamagedBack"]=("/gfx/injuries/inj-niggle.png");
        this.idToImg[ "SmashedKnee"]=("/gfx/injuries/inj-niggle.png");
        this.idToImg[ "SmashedAnkle"]=("/gfx/injuries/inj-move.png");
        this.idToImg[ "SmashedHip"]=("/gfx/injuries/inj-move.png");
        this.idToImg[ "FracturedSkull"]=("/gfx/injuries/inj-armour.png");
        this.idToImg[ "SeriousConcussion"]=("/gfx/injuries/inj-armour.png");
        this.idToImg[ "BrokenNeck"]=("/gfx/injuries/inj-agility.png");
        this.idToImg[ "SmashedCollarBone"]=("/gfx/injuries/inj-strength.png");
        this.idToImg[ "Dead"]=("/gfx/injuries/inj-dead.png");

        this.idToName[ "BadlyHurt"]=("Badly Hurt");
        this.idToName[ "BrokenJaw"]=("Broken Jaw - Miss next game");
        this.idToName[ "BrokenRibs"]=("Broken Ribs - Miss next game");
        this.idToName[ "FracturedArm"]=("Fractured Arm - Miss next game");
        this.idToName[ "FracturedLeg"]=("Fractured Leg - Miss next game");
        this.idToName[ "SmashedHand"]=("Smashed Hand - Miss next game");
        this.idToName[ "GougedEyes"]=("Gouged Eye - Miss next game");
        this.idToName[ "GroinStrain"]=("Groin Strain - Miss next game");
        this.idToName[ "PinchedNerve"]=("Pinched Nerve - Miss next game");
        this.idToName[ "DamagedBack"]=("Damaged Back - Niggling injury");
        this.idToName[ "SmashedKnee"]=("Smashed Knee - Niggling injury");
        this.idToName[ "SmashedAnkle"]=("Smashed Ankle - Reduced Movement");
        this.idToName[ "SmashedHip"]=("Smashed Hip - Reduced Movement");
        this.idToName[ "FracturedSkull"]=("Fractured Skull - Reduced Armour");
        this.idToName[ "SeriousConcussion"]=("Serious Concussion - Reduced Armour");
        this.idToName[ "BrokenNeck"]=("Broken Neck - Reduced Agility");
        this.idToName[ "SmashedCollarBone"]=("Smashed Collar bone - Reduced Strength");
        this.idToName[ "Dead"]=("Dead");

        
    }


}


export default APIInjuries