import APILeague from "./apiLeague"

export default class APIActivateResult 
{
    added : number = 0
    updated : number = 0
    skipped : number = 0
    leagues : Array<APILeague>= []
} 