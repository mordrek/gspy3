import UserLoginData from "../auth/UserLoginData"
import { Userinfoplus } from '../auth/UserLoginData';

import { QueryPageRequest, QueryPageResponseView } from "./queryPageRequests";
import ServerStatus from "./views/statusCollection"
import APIActivateResult from "./dataformat/apiActivateResult"
import 'abortcontroller-polyfill/dist/polyfill-patch-fetch'

class GSpy3_api{
    static instance : GSpy3_api

    data : UserLoginData|undefined
    callbacks : {(newUserInfo: Userinfoplus|undefined) : void;}[] = []
    abortController = new window.AbortController();

    static getAPI() : GSpy3_api{
        if(!this.instance){
            this.instance = new GSpy3_api();
        }
        return this.instance;
    }

    constructor(){
        let myItem = localStorage.getItem('gSpy3.useLoginData');
        try{
            if(myItem)
                this.setUserLoginData(JSON.parse(myItem));
        }catch{

        }
    }

    isLoggedIn(){
        return this.data?.userInfo?.id !== undefined;
    }

    addCallback(callback : {(newUserInfo: Userinfoplus | undefined):void;}){
        this.callbacks.push(callback);
    }

    logout(){
        this.setUserLoginData(undefined);
    }

    getBase() : string{
        
        var host = window.location.protocol+"//"+window.location.hostname;
            return host+(window.location.protocol==="https:"?":666/api/v1":":667/api/v1");
    }

    get(url : string) : Promise<Response> {
        let callUrl = encodeURI(this.getBase()+url);
        return fetch(callUrl, {
            signal: this.abortController.signal
          });
/*        let requestHeaders: any = { 'Access-Control-Request-Headers':"X-Requested-With", "Origin":window.location.protocol+"//"+window.location.hostname, 'Content-Type': 'application/json'};
        return fetch(callUrl, {
            method: 'GET',
            headers: requestHeaders,
            //body: requestBody
          });*/
    }
    post(url : string, data : any) : Promise<Response> {
        let callUrl = this.getBase()+url; 

        return fetch(callUrl, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data),
            signal: this.abortController.signal,
          });
    }
    setUserLoginData(data : UserLoginData|undefined ){
        // Store token separately?
        // let token = data.access?.token;
        // if(data.access!=undefined){ data.access.token = "";}

        this.data = data;
        let key = 'gSpy3.useLoginData';
        localStorage.setItem(key, JSON.stringify(this.data));

        this.callbacks.forEach((o=>{
            if(data && data.userInfo){
                o(data.userInfo);
            }
            else{
                o(undefined);
            }
        }))
    }

    getAuthTokenWithCode(provider:string, code: string) : Promise<UserLoginData> {
        let promise = new Promise<UserLoginData>((acc,rej)=>{
            this.get("/auth/token/"+provider+"/"+code).then((res)=>{
                if(res.ok){
                    res.json().then((content)=>{
                        acc(content.token);
                    }).catch((e)=>{
                        console.log(e);
                        rej(e);
                    });
                }else{
                    rej("Unable to fetch url");
                }
            }).catch((e)=>{
                console.log(e);
                rej(e);
            });
        })
        return promise;
    }

    getAuthUrl(provider:string) : Promise<string> {
        let promise = new Promise<string>((acc,rej)=>{
            this.get("/auth/url/"+provider).then((res)=>{
                if(res.ok){
                    res.json().then((content)=>{
                        acc(content.url);
                    }).catch((e)=>{
                        console.log(e);
                        rej(e);
                    });
                }else{
                    res.text().then((body)=>{
                    rej("Unable to fetch url: "+body);
                    });
                }
            }).catch((e)=>{
                rej(e);
            });
        })
        return promise;
    }

    

    postCompMatchSchedule( idcomp: number, schedules : postUserscheduleSetItemStruct []) : Promise<boolean>{
        let promise = new Promise<boolean>((acc,rej)=>{
            let postData = new postUserscheduleSetStruct();
            postData.schedules = schedules;
            console.log(postData);
            this.post("/comp/"+idcomp+"/schedules",postData
            ).then((res)=>{
                if(res.ok){
                    res.json().then((content)=>{
                        acc(content);
                    }).catch((e)=>{
                        console.log(e);
                        rej(e);
                    });
                }else{
                    res.text().then((body)=>{
                        rej("Unable to post data: "+body);
                    });
                    
                }
            }).catch((e)=>{
                if (e.name === 'AbortError') return;
                console.log(e)
                rej(e);
            });
        })
        return promise;
    }

    postLeagueActivate(origin: string,  leagueNames: string[]) : Promise<APIActivateResult>{
        let promise = new Promise<APIActivateResult>((acc,rej)=>{
            this.post("/league/activate/"+origin,leagueNames
            ).then((res)=>{
                if(res.ok){
                    res.json().then((content)=>{
                        acc(content);
                    }).catch((e)=>{
                        console.log(e);
                        rej(e);
                    });
                }else{
                    res.text().then((body)=>{
                        rej("Unable to post data: "+body);
                    });
                    
                }
            }).catch((e)=>{
                if (e.name === 'AbortError') return;
                console.log(e)
                rej(e);
            });
        })
        return promise;
    } 

    postCompMaintenance(idcomp:number) :Promise<boolean>{
        let promise = new Promise<boolean>((acc,rej)=>{
            this.post("/comp/"+idcomp+"/maintenance",undefined
            ).then((res)=>{
                if(res.ok){
                    res.json().then((content)=>{
                        acc(content);
                    }).catch((e)=>{
                        console.log(e);
                        rej(e);
                    });
                }else{
                    res.text().then((body)=>{
                        rej("Unable to post data: "+body);
                    });
                    
                }
            }).catch((e)=>{
                if (e.name === 'AbortError') return;
                console.log(e)
                rej(e);
            });
        })
        return promise;
    }

    
    postRequestReplay(idcomp:number,idmatch:number) :Promise<boolean>{
        let promise = new Promise<boolean>((acc,rej)=>{
            this.post("/comp/"+idcomp+"/match/"+idmatch+"/downloadreplay/",undefined
            ).then((res)=>{
                if(res.ok){
                    res.json().then((content)=>{
                        acc(content);
                    }).catch((e)=>{
                        console.log(e);
                        rej(e);
                    });
                }else{
                    res.text().then((body)=>{
                        rej("Unable to post data: "+body);
                    });
                    
                }
            }).catch((e)=>{
                if (e.name === 'AbortError') return;
                console.log(e)
                rej(e);
            });
        })
        return promise;
    }

   postCompSorting(idcomp : number, sorting : string): Promise<boolean>{
    let promise = new Promise<boolean>((acc,rej)=>{
        this.post("/comp/"+idcomp+"/sorting/"+sorting,undefined
        ).then((res)=>{
            if(res.ok){
                res.json().then((content)=>{
                    acc(content);
                }).catch((e)=>{
                    console.log(e);
                    rej(e);
                });
            }else{
                res.text().then((body)=>{
                    rej("Unable to post data: "+body);
                });
                
            }
        }).catch((e)=>{
            if (e.name === 'AbortError') return;
            console.log(e)
            rej(e);
        });
    })
    return promise;
    }

    postCoachMediaLinks(idcoach: number, youtube : string, twitch : string)
    {
        let promise = new Promise<boolean>((acc,rej)=>{
            this.post("/coach/"+idcoach+"/media",{youtube:youtube, twitch:twitch}
            ).then((res)=>{
                if(res.ok){
                    res.json().then((content)=>{
                        acc(content);
                    }).catch((e)=>{
                        console.log(e);
                        rej(e);
                    });
                }else{
                    res.text().then((body)=>{
                        rej("Unable to post data: "+body);
                    });
                    
                }
            }).catch((e)=>{
                if (e.name === 'AbortError') return;
                console.log(e)
                rej(e);
            });
        })
        return promise;
    }

    postLeagueRequestCollection(idleague : number, from : string, to : string): Promise<boolean>{
        let promise = new Promise<boolean>((acc,rej)=>{
            console.log("POST REQUEST COLLECTION "+"/league/"+idleague+"/request/collection/"+from+"/"+to)
            this.post("/league/"+idleague+"/request/collection/"+from+"/"+to,undefined
            ).then((res)=>{
                if(res.ok){
                    res.json().then((content)=>{
                        acc(content);
                    }).catch((e)=>{
                        console.log(e);
                        rej(e);
                    });
                }else{
                    res.text().then((body)=>{
                        rej("Unable to post data: "+body);
                    });
                    
                }
            }).catch((e)=>{
                if (e.name === 'AbortError') return;
                console.log(e)
                rej(e);
            });
        })
        return promise;
        }

   getQueriesURL(reqArr : QueryPageRequest[]) : string{
    let q = "/queries?req={";
    for(let i=0; i< reqArr.length;i++){
        if(i!=0){q += ","};
        q += "\""+reqArr[i].id+"\":"+JSON.stringify(reqArr[i]).replace("&","%26");

    }
    q+="}";
    return q;
   }
   getQueriesFormatURL(format : string,reqArr : QueryPageRequest[]) : string{
    let q = "/queries/"+format+"/?req={";
    for(let i=0; i< reqArr.length;i++){
        if(i!=0){q += ","};
        q += "\""+reqArr[i].id+"\":"+JSON.stringify(reqArr[i]).replace("&","%26");

    }
    q+="}";
    return q;
   }
    getQueries(reqArr : QueryPageRequest[]) : Promise<QueryPageResponseView>{
        let promise = new Promise<QueryPageResponseView>((acc,rej)=>{
            let q = this.getQueriesURL(reqArr);
console.log("!!!!!"+q);
            this.get(q).then((res)=>{
                if(res.ok){
                    res.text().then((content)=>{
                        //console.log(content);
                        let cobj = JSON.parse(content);
                        console.log("GOT DATA");
                        console.log(cobj);
                        if(!cobj.success){
                            rej("Failed to get data");
                        }else{
                        acc(cobj); 
                        }
                    }).catch((e)=>{ 
                        rej(e);
                    });
                }else{
                    res.text().then((body)=>{
                        rej("Unable to get data: "+body);
                    });
                    
                }
            }).catch((e)=>{
                if (e.name === 'AbortError') return;
                rej(e);
            });
        });
        return promise;
    }


    getStatus() : Promise<ServerStatus>{
        let promise = new Promise<ServerStatus>((acc,rej)=>{
            this.get("/server/status").then((res)=>{
                if(res.ok){
                    res.json().then((content)=>{
                        //console.log(content);
                        acc(content);
                    }).catch((e)=>{ 
                        rej(e);
                    });
                }else{
                    res.text().then((body)=>{
                        rej("Unable to get data: "+body);
                    });
                    
                }
            }).catch((e)=>{
                if (e.name === 'AbortError') return;
                rej(e);
            });
        });
        return promise;
    }

}



class postUserscheduleSetItemStruct  {
    idcontest  :number=0
    datetime    :   string=""
    }
class postUserscheduleSetStruct{
    schedules : postUserscheduleSetItemStruct []=[]
}



export default GSpy3_api