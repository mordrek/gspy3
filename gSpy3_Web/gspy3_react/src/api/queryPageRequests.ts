import QueryResult from "./views/queryResult";

class QueryPageRequests{
    request : { [id: string] : QueryPageRequest; } = {};

    getFilter(key:string){
        let res = this.request[key];
        if(!res){
            res = new QueryPageRequest();
            this.request[key] = res;
        }
        return res;
    }

    getFilterURL(){
        let newUrl = window.location.pathname;
        newUrl+="?filter="+JSON.stringify(this.request);
        if(newUrl.startsWith("/gspy")){
            newUrl = newUrl.substr("/gspy".length);
        }
        return newUrl;
        
    }
}

export class QueryPageRequest{

    id :string|undefined;
    idmap : { [id: string] : string }|undefined ;
    filters : { [id: string] : string; }|undefined ;
    ordercol : string|undefined;
    order : string|undefined;
    limit : number|undefined;
    from : number|undefined;
    group : string|undefined;
    aggr : string | undefined;
}


export class QueryPageResponseView {
    response : { [id: string] : QueryPageResponse; } = {}
    loading : boolean = false
    success : boolean = false
}
export class QueryPageResponse   {
    request : QueryPageRequest = new QueryPageRequest()
    result : QueryResult = new QueryResult()
    loading : boolean = false
}




export default QueryPageRequests