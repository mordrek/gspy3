﻿using gSpy3_Common;
using gSpy3_Common.Database;
using System;

namespace gSpy3_LegacyCollector
{
    class Program
    {
        static void Main(string[] args)
        {
            Log log = new Log();
            log.Info("gSpy3_LegacyCollector v " + typeof(Program).Assembly.GetName().Version);
            try
            {
                log.Info("Disabled. Exit");
                return;

                var settings = PrivateSettings.Load();
                DatabaseManager dbManager = new DatabaseManager(settings);

                LegacyCollector collector = new LegacyCollector(dbManager, settings.BB2APIKey);
                collector.Run();
            }catch(Exception ex)
            {
                log.Error("Error", ex.ToString());
            }
            log.Info("Program exit");
        }
    }
}
