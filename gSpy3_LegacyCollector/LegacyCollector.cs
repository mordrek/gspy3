﻿using gSpy3_Common;
using gSpy3_Common.API.Cyanide;
using gSpy3_Common.API.LegacyGoblinSpy;
using gSpy3_Common.Database;
using gSpy3_Common.Database.AccessInterface;
using gSpy3_Common.Database.Format;
using gSpy3_Common.Helpers;
using gSpy3_Common.Module;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Timers;

namespace gSpy3_LegacyCollector
{
    class LegacyCollector : ModuleBase
    {
        DatabaseManager dbManager;
        Timer heartbeatTimer;
        protected enum Work { Update_Competition_List, Update_Competition }
        protected enum Source { LegacyCollector = 0}
        Log log = new Log();
        LegacyGoblinSpyAPI legacyAPI = new LegacyGoblinSpyAPI();
        CyanideBBAPI bb2API;

        public LegacyCollector(DatabaseManager dbm, string bb2key) : base(ModuleEnum.gSpy3_LegacyCollector, dbm)
        {
            bb2API = new CyanideBBAPI(bb2key);
            LeagueManager.cyanideAPI = bb2API;
            dbManager = dbm;
            heartbeatTimer = new Timer();
            heartbeatTimer.Interval = 1;
            heartbeatTimer.Elapsed += HeartbeatTimer_Elapsed;
            heartbeatTimer.Start();
        }

        private void HeartbeatTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            heartbeatTimer.Enabled = false;
            heartbeatTimer.Interval = 1000 * 60;
            base.Tick();
            heartbeatTimer.Enabled = true;
        }



        public void Run()
        {

#if DEBUG
          //  UpdateCompetitions();


            UpdateSingleCompetition(new UpdateCompetitionWork()
            {
                idleague = 96,
                idorigin = (OriginID)1,
                name = "Ranked 34-5 conc. limit"
            });

#endif


            while (true)
            {
                try 
                {
                    

                    var numRemoved = base.RemoveOldWork();
                    if (numRemoved > 0)
                    {
                        log.Info("Removed " + numRemoved + " old work items");
                    }

                    // Plan new work, if we don't have enough
                    try
                    {
                        int workNeeded = base.GetNumWorkNeeded();
                        if (workNeeded > 0)
                        {
                            
                            var comps = legacyAPI.GetCompetitions();

                            // When was the last time we asked for leagues?
                            var lastLeagueCheck = base.GetLastFinishedCommand(Work.Update_Competition_List.ToString());
                            var lastAddedLeagueCheck = base.GetLastAddedCommand(Work.Update_Competition_List.ToString());

                            for (int i = 0; i < workNeeded; i++)
                            {
                                // If we don't have any league check lately, then that is prio
                                if(lastLeagueCheck==null && lastAddedLeagueCheck==null || lastLeagueCheck!=null && lastAddedLeagueCheck == null && (DateTime.Now-lastLeagueCheck) > new TimeSpan(24, 0, 0))
                                {
                                    base.AddWork(Work.Update_Competition_List.ToString(),"", 100);
                                    lastLeagueCheck = DateTime.Now;
                                }
                                else // If we do have fresh leagues and competitions fetched, then add some inactive competitions to check
                                {
                                    bool stopLoop = false;
                                    foreach(var league in comps.leagues)
                                    {
                                        if(league.name.Trim() != "")
                                        {
                                            foreach (var comp in league.list) 
                                            {
                                                if(comp.name.Trim() != "" && comp.active==false)
                                                {

                                                    var existingCompetition = LeagueManager.FindCompetitionByName(OriginConversion.FromLegacyGoblinSpy(league.platform), (int)league.id, comp.name);
                                                    if (existingCompetition == null)
                                                    {
                                                        var existingLeague = LeagueManager.FindLeagueByName(OriginConversion.FromLegacyGoblinSpy(league.platform), league.name);
                                                        if (existingLeague != null) 
                                                        {
                                                            var compChecked = base.GetState<String>("comps_imported"+ existingLeague.idleague + ";" + comp.name);
                                                            if (compChecked==null)
                                                            {
                                                                UpdateCompetitionWork workData = new UpdateCompetitionWork() { idleague = (int)existingLeague.idleague, name = comp.name, idorigin = OriginConversion.FromLegacyGoblinSpy(league.platform) };
                                                                base.AddWork(Work.Update_Competition.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(workData), 10);
                                                                i++;
                                                                stopLoop = i >= workNeeded;
                                                                base.SetState<String>("comps_imported" + existingLeague.idleague + ";" + comp.name,"");
                                                            }
                                                        }
                                                    }
                                                    if (stopLoop)
                                                    {
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        if (stopLoop) { break; }
                                    }
                                }
                            }
                        }
                    }catch(Exception ex)
                    {
                        log.Info("Error in work planning", ex.ToString());
                    }

                    // Execute work
                    int idwork = 0;
                    try
                    {
#if DEBUG
                        /*UpdateSingleCompetition(new UpdateCompetitionWork()
                        {
                            idleague =224,//ken lay
                             idorigin = OriginID.BB2_pc,
                              name = "FUBAR - Ken Lay League"
                        });*/
                        //LeagueManager.UpdateStandings(898);
                        UpdateCompetitions();
#endif

                        string command, data;
                        if (base.GetNextWork(out idwork, out command, out data))
                        {
                            log.Info("Work: " + command + " : " + data);
                            string result = "";
                            switch (command)
                            {
                                case "Update_Competition_List":
                                    result = UpdateCompetitions();
                                    break;
                                case "Update_Competition":
                                    result = UpdateSingleCompetition(Newtonsoft.Json.JsonConvert.DeserializeObject<UpdateCompetitionWork>(data));
                                    break;
                            }
                            log.Info(result);
                            base.FinishWork(idwork, result);
                        }
                        else System.Threading.Thread.Sleep(30000);

                    }catch(Exception ex)
                    {
                        log.Error("Error in work execution", ex.ToString());
                        base.FailWork(idwork, ex.ToString());
                    }



                    
                }
                catch(Exception ex)
                {
                    log.Error("Error in main loop: " + ex.ToString());
                }
                System.Threading.Thread.Sleep(1000);
            }
        }
        /// <summary>
        /// Add a single competition from gspy1 into gspy3 database
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        string UpdateSingleCompetition(UpdateCompetitionWork data)
        {
            string res = "";
         
            try
            {
                var league = LeagueManager.FindLeagueByID(data.idleague);
                var dbName = legacyAPI.GetDBName(data.idorigin == OriginID.BB2_xb1 ? "xb1" : (data.idorigin == OriginID.BB2_ps4 ? "ps4" : "pc"), league.league_name, data.name);
                var dbURL = legacyAPI.GetDBURL(dbName);
                var dbPath = legacyAPI.GetDBPath(dbName);
                dbPath = System.IO.Path.Combine(System.IO.Path.GetTempPath(), System.IO.Path.GetFileName(dbPath));
               
                var wc = new WebClient();
                if (!File.Exists(dbPath)) 
                {
                    try
                    {
                        wc.DownloadFile(dbURL, dbPath);
                    }
                    catch
                    {
                        var compChecked = base.GetState<String>("comps_imported" + data.idleague + ";" + data.name);
                        if (compChecked == null)
                        {
                            base.SetState<String>("comps_imported" + data.idleague + ";" + data.name, "fail");
                        }

                        log.Error("Unable to download database " + dbURL+" for "+data.name);
                        throw new Exception("Unable to download database " + dbURL + " for " + data.name);
                    }
                }

                using (var db = new DatabaseConnectionSQLite("Data Source="+ dbPath))
                {
                    var trans = dbManager.StatsDB.BeginTransaction();


                    var logoMap = UniqueStringManager.GetLogoMap(trans);
                    var mottoMap = UniqueStringManager.GetMottoMap(trans);
                    var casComboMap = UniqueStringManager.GetCasComboMap(trans);
                    var casMap = UniqueStringManager.GetCasMap(trans);
                    var skComboMap = UniqueStringManager.GetSkillComboMap(trans);
                    var skillMap = UniqueStringManager.GetSkillMap(trans);
                    var plTypeMap = UniqueStringManager.GetPlayerTypeMap(trans);
                    var plNameMap = UniqueStringManager.GetPlayerNameMap(trans);


                    string legacyPlatform = "";
                    try
                    {
                        #region Competition
                        DateTime tComp = DateTime.Now;
                        bool registeredCompetition = false;
                        {
                            // First find the competition basic data and add it as a competition
                            var comps = legacyAPI.GetCompetitions();
                            bool stopLooping = false;
                            foreach (var l in comps.leagues)
                            {
                                if (l.name != "" && l.name == league.league_name && OriginConversion.FromLegacyGoblinSpy(l.platform) == (OriginID)league.idorigin)
                                {
                                    foreach (var comp in l.list)
                                    {
                                        if (comp.name != "" && comp.name == data.name)
                                        {
                                            // Here it is
                                            stopLooping = true;
                                            legacyPlatform = l.platform;


                                            var compChecked = base.GetState<String>("comps_imported" + data.idleague + ";" + data.name);
                                            if(compChecked != null && compChecked != "")
                                            {
                                                trans.Rollback();
                                                return "Competition skipped due to already checked";
                                            }

                                            int numGames = int.Parse(db.Query("select count(*) from matches").Rows[0][0]);
                                            if (numGames == 0)
                                            {
                                                trans.Rollback();
                                                if (compChecked == null)
                                                {
                                                    base.SetState<String>("comps_imported" + data.idleague + ";" + data.name, "fail");
                                                }
                                                
                                                return "Competition skipped due to lack of games";

                                            }
                                            else
                                            {
                                                registeredCompetition = true;
                                                var newCompetition = new Competition(league, comp);
                                                var firstMatch = db.Query("select idcompetition from matches where idcompetition is not null and idcompetition!=0 limit 1");
                                                if (firstMatch.Rows.Count > 0)
                                                {
                                                    newCompetition.competition_origin_uid = firstMatch.Rows[0][0];
                                                }
                                                if(newCompetition.competition_origin_uid =="0" || string.IsNullOrEmpty(newCompetition.competition_origin_uid))
                                                {
                                                    var bb2comps = bb2API.GetCompetitions((int)ID, OriginConversion.FromLegacyGoblinSpy(l.platform), l.name);
                                                    
                                                    foreach(var bb2comp in bb2comps.competitions)
                                                    {
                                                        if (bb2comp.id == null) continue;
                                                        if (bb2comp.name == data.name) 
                                                        {
                                                            newCompetition.competition_origin_uid = bb2comp.id;
                                                        }
                                                    }
                                                }

                                                if (!string.IsNullOrEmpty(newCompetition.competition_origin_uid) && newCompetition.competition_origin_uid  != "0")
                                                {
                                                    LeagueManager.RegisterCompetition(new List<Competition>() { newCompetition }, trans);
                                                }
                                                else
                                                {
                                                    log.Error("Legacy competition has no id... :(");
                                                    trans.Rollback();
                                                    if (compChecked == null)
                                                    {
                                                        base.SetState<String>("comps_imported" + data.idleague + ";" + data.name, "fail");
                                                    }

                                                    return "Competition skipped due to missing idcompetition";
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
                                if (stopLooping) { break; }
                            }
                        }
                        // Grab the added competition
                        var competition = LeagueManager.FindCompetitionByName((OriginID)league.idorigin, (int)league.idleague, data.name);
                        if (legacyPlatform == "") { legacyPlatform = OriginConversion.ToLegacyGoblinSpy((OriginID)league.idorigin); }
                        if (competition == null || legacyPlatform == "")
                        {
                            log.Info("legacyPlatform: '" + legacyPlatform + "'");
                            log.Info("league.idorigin: '" + league.idorigin + "'");
                            log.Info("data.name: '" + data.name+"'");
                            throw new Exception("Unable to find the competition requested: " + data.name);
                        }
                        #endregion
                        #region Coaches
                        List<Coach> addedCoaches = new List<Coach>();
                        {
                            Console.WriteLine("Comp " + (int)(DateTime.Now - tComp).TotalMilliseconds + "ms");
                            DateTime tCoach = DateTime.Now;
                            var coachRes = db.Query("select id,name,twitch,youtube,country,lang from coaches");
                            var newCoaches = new List<Coach>();
                            foreach (var coach in coachRes.Rows)
                            {
                                try
                                {
                                    if (coach[coachRes.Cols["id"]] != "")
                                    {
                                        var newCoach = new Coach()
                                        {
                                            idorigin = league.idorigin,
                                            coach_origin_uid = coach[0],
                                            coach_name = coach[1],
                                            twitch = coach[2],
                                            youtube = coach[3],
                                            country = coach[4],
                                            lang = coach[5],
                                        };
                                        if (string.IsNullOrEmpty(newCoach.twitch)) newCoach.twitch = null;
                                        if (string.IsNullOrEmpty(newCoach.youtube)) newCoach.youtube = null;
                                        if (string.IsNullOrEmpty(newCoach.country)) newCoach.country = null;
                                        if (string.IsNullOrEmpty(newCoach.lang)) newCoach.lang = null;

                                        newCoaches.Add(newCoach);
                                    }
                                }catch(Exception ex2)
                                {
                                    log.Error(ex2.ToString());
                                    throw ex2;
                                }
                            }
                            addedCoaches = CoachManager.AddIfMissing(newCoaches, trans);
                            
                        }
                        #endregion
                        #region Teams
                        var coachMap = CoachManager.GetCoachMap(trans);

                        Console.WriteLine("Coach " + (int)(DateTime.Now - tComp).TotalMilliseconds + "ms");
                        DateTime tTeams = DateTime.Now;
                        List<Team> addedTeamInfos = new List<Team>();
                        var newTeams = new List<Team>();
                        var teamsRes = db.Query("select * from leaguestandings");
                        {



                            foreach (var team in teamsRes.Rows)
                            {
                                var idcoach = -1;
                                var sIdCoach = teamsRes.Cols.ContainsKey("idcoach") ? team[teamsRes.Cols["idcoach"]] : "";
                                if (sIdCoach == "")
                                {
                                    var idcoachRes = db.Query("select idcoach from coaches where name=@name", new Dictionary<string, string>() { { "@name", team[teamsRes.Cols["coachname"]] } }, trans);
                                    if(idcoachRes.Rows.Count > 0)
                                    {
                                        sIdCoach = idcoachRes.Rows[0][0];
                                    }
                                }
                                if (sIdCoach == "" || sIdCoach=="0")
                                { 
                                     throw new Exception("Coach data missing for coach " + team[teamsRes.Cols["coachname"]] + " in " + competition.competition_name);
                                }
                                else
                                {
                                    if (!coachMap.TryGetValue(league.idorigin + ";" + sIdCoach, out idcoach))
                                    {
                                        try
                                        {
                                            var newCoach = new Coach()
                                            {
                                                coach_origin_uid = sIdCoach,
                                                idorigin = league.idorigin,
                                                coach_name = team[teamsRes.Cols["coachname"]]
                                            };
                                            var newCoaches = new List<Coach>();
                                            newCoaches.Add(newCoach);
                                            CoachManager.AddIfMissing(newCoaches, trans);
                                            var coachRes = dbManager.StatsDB.Query("select idcoach from coaches where idorigin="+league.idorigin+" and coach_origin_uid=@coach_origin_uid", new Dictionary<string, string>() { { "@coach_origin_uid", sIdCoach } }, trans, 60 * 60 * 20);
                                            idcoach = int.Parse(coachRes.Rows[0][0]);
                                            coachMap = CoachManager.GetCoachMap(trans);
                                        }
                                        catch (Exception ex2)
                                        {
                                            throw ex2;
                                        }
                                    }
                                }
                                try
                                {
                                    var newTeam = new Team()
                                    {
                                        idorigin = league.idorigin,
                                        team_origin_uid = team[teamsRes.Cols["idteamlisting"]],
                                        team_name = team[teamsRes.Cols["teamname"]],
                                        idrace = int.Parse(team[teamsRes.Cols["idraces"]]),
                                        idcoach = idcoach,
                                        idlogo = UniqueStringManager.GetOrCreateLogo(logoMap, team[teamsRes.Cols["logo"]]),
                                        idmotto = UniqueStringManager.GetOrCreateMotto(mottoMap, team[teamsRes.Cols["teaminfo"]]),
                                    };
                                    if (newTeam.idcoach == 0)
                                    {
                                        throw new Exception("Coach data missing for coach " + team[teamsRes.Cols["coachname"]] + " in " + competition.competition_name);
                                    }
                                    newTeams.Add(newTeam);
                                }catch(Exception ex2)
                                {
                                    log.Error(ex2.ToString());
                                    throw ex2;
                                }

                            }
                            addedTeamInfos=TeamManager.AddIfMissing(newTeams, (int)competition.idcompetition, trans);
                        }
                        #endregion
                        #region Standings
                        Console.WriteLine("Teams ("+ addedTeamInfos .Count+ ") " + (int)(DateTime.Now - tTeams).TotalMilliseconds + "ms");
                        DateTime tStandings = DateTime.Now;

                        var newStandings = new List<Standing>();

                        {

                            foreach (var team in teamsRes.Rows)
                            {
                                try
                                {
                                    Team t = TeamManager.FindTeamByOrgID((OriginID)league.idorigin, team[teamsRes.Cols["idteamlisting"]]);
                                    var newStanding = new Standing()
                                    {
                                        idcompetition = (int)competition.idcompetition,
                                        idteam = (int)t.idteam,
                                        ranking = float.Parse(team[teamsRes.Cols["rank"]].Replace(",", "."), CultureInfo.InvariantCulture),
                                        points = float.Parse(team[teamsRes.Cols["points"]].Replace(",", "."), CultureInfo.InvariantCulture),
                                        sorting = float.Parse(team[teamsRes.Cols["sort"]].Replace(",", "."), CultureInfo.InvariantCulture),
                                        active = 1,// Team is always active, since this state is a bit iffy for old teams. team[teamsRes.Cols["activeteam"]] == "" ? 1 : int.Parse(team[teamsRes.Cols["activeteam"]]),
                                        wins = int.Parse(team[teamsRes.Cols["wins"]]),
                                        draws = int.Parse(team[teamsRes.Cols["draws"]]),
                                        losses = int.Parse(team[teamsRes.Cols["losses"]]),
                                        td = int.Parse(team[teamsRes.Cols["td"]]),
                                        td_opp = int.Parse(team[teamsRes.Cols["tdopp"]]),
                                        td_diff = int.Parse(team[teamsRes.Cols["tddiff"]]),
                                        cas = int.Parse(team[teamsRes.Cols["cas"]]),
                                        cas_opp = int.Parse(team[teamsRes.Cols["casopp"]]),
                                        cas_diff = int.Parse(team[teamsRes.Cols["cas"]]) - int.Parse(team[teamsRes.Cols["casopp"]]),
                                        concedes = int.Parse(team[teamsRes.Cols["concedes"]]),
                                        team_value = team[teamsRes.Cols["value"]] != "" ? int.Parse(team[teamsRes.Cols["value"]]) : 0,
                                        kills = int.Parse(team[teamsRes.Cols["kills"]]),
                                        // Cannot set position. position = int.Parse(team[teamsRes.Cols["position"]]),
                                        gp = int.Parse(team[teamsRes.Cols["gamesplayed"]]),
                                        team_origin_uid = team[teamsRes.Cols["idteamlisting"]],
                                        idorigin = league.idorigin,

                                    };
                                    newStandings.Add(newStanding);
                                }
                                catch (Exception exIn)
                                {
                                    log.Error(exIn.ToString());
                                    throw exIn;
                                }
                            }
                        }
                        var addedTeams = LeagueManager.AddIfMissing(newStandings, (int)competition.idcompetition, trans);
                        #endregion
                        #region Players
                        Console.WriteLine("Standings (" + addedTeams.Count + ") " + (int)(DateTime.Now - tStandings).TotalMilliseconds + "ms");
                        DateTime tPlayers = DateTime.Now;


                        {
                            Dictionary<string, Team> teamInUseMap = new Dictionary<string, Team>();
                            foreach (var ateam in newStandings) { teamInUseMap[ateam.team_origin_uid] = new Team(); }

                            var newPlayers = new List<Player>();
                            
                            {
                                var plRes = db.Query("select puuid,matchplayed,ag,av,st,ma,casualties_state_string,casualties_sustained_string,name,type,skills_string,level,xp,xp_gain,idteam from teamplayers",
                                new Dictionary<string, string>());
                                
                                foreach (var player in plRes.Rows)
                                {
                                    string idteam = player[14];
                                    Team team = new Team();
                                    if (teamInUseMap.TryGetValue(idteam, out team) == false)
                                    {
                                        continue;
                                    }
                                    if (team.idteam == 0 || team.idteam==null)
                                    {
                                        team = TeamManager.FindTeamByOrgID((OriginID)league.idorigin, idteam);
                                        teamInUseMap[idteam] = team;
                                    }


                                    try
                                    {
                                        var newPlayer = new Player()
                                        {
                                            idorigin = league.idorigin,
                                            player_origin_uid = (player[0]),
                                            active = int.Parse(player[1]),
                                            ag = int.Parse(player[2]),
                                            av = int.Parse(player[3]),
                                            st = int.Parse(player[4]),
                                            ma = int.Parse(player[5]),
                                            idcasstate = UniqueStringManager.GetOrCreateCasCombo(casComboMap, casMap, player[6]),
                                            idcassustained = UniqueStringManager.GetOrCreateCasCombo(casComboMap, casMap, player[7]),
                                            idleague = (int)league.idleague,
                                            idplayername = UniqueStringManager.GetOrCreatePlayerName(plNameMap, player[8]),
                                            idplayertype = UniqueStringManager.GetOrCreatePlayerType(plTypeMap, player[9]),
                                            idskillcombo = UniqueStringManager.GetOrCreateSkillCombo(skComboMap,skillMap, player[10]),
                                            idteam = (int)team.idteam,
                                            level = int.Parse(player[11]),
                                            xp = int.Parse(player[12]),
                                            xp_gain = int.Parse(player[13])

                                        };
                                        newPlayers.Add(newPlayer);
                                    }
                                    catch (Exception ex2)
                                    {
                                        log.Error(ex2.ToString());
                                        throw ex2;
                                    }
                                }
                                
                            }
                            DateTime t3 = DateTime.Now;
                            TeamManager.AddIfMissing(newPlayers, trans);
                            Console.WriteLine("Player insert " + (int)(DateTime.Now - t3).TotalMilliseconds + "ms");
                        }
                        #endregion
                        #region Matches
                        Console.WriteLine("Players " + (int)(DateTime.Now - tPlayers).TotalMilliseconds + "ms");
                        DateTime tMatches = DateTime.Now;

                        var newMatches = new List<Match>();
                        {
                            var stadiumMap = UniqueStringManager.GetStadiumMap(trans);
                            var mRes = db.Query("select m.*,ts1.idteamlisting as idteam1,ts2.idteamlisting as idteam2 from matches as m inner join teamstats as ts1 on ts1.idteamstats=m.idteamstatshome inner join teamstats as ts2 on ts2.idteamstats=m.idteamstatsaway");
                            
                            foreach (var sched in mRes.Rows)
                            {
                                try
                                {
                                    var team1 = TeamManager.FindTeamByOrgID((OriginID)competition.idorigin, sched[mRes.Cols["idteam1"]].Split('-')[0]);
                                    var team2 = TeamManager.FindTeamByOrgID((OriginID)competition.idorigin, sched[mRes.Cols["idteam2"]].Split('-')[0]);
                                    if (team1 != null && team2 != null)
                                    {
                                        if (team1.idcoach==0 || team2.idcoach == 0)
                                        {
                                            log.Error("Teams have zero coach");
                                        }


                                            var newMatch = new Match()
                                        {
                                            idorigin = league.idorigin,
                                            match_origin_uid = ""+UInt64.Parse(sched[mRes.Cols["uuid"]], System.Globalization.NumberStyles.HexNumber),
                                            idcompetition = (int)competition.idcompetition,
                                            idteam_home = (int)team1.idteam,
                                            idteam_away = (int)team2.idteam,
                                            duration = (int)(DateTime.Parse(sched[mRes.Cols["finished"]]) - DateTime.Parse(sched[mRes.Cols["started"]])).TotalMinutes,
                                            finished = DateTime.Parse(sched[mRes.Cols["finished"]]).ToDatabaseUniversalString(),
                                            started = DateTime.Parse(sched[mRes.Cols["started"]]).ToDatabaseUniversalString(),
                                            idstadium = UniqueStringManager.GetOrCreateStadium(stadiumMap, sched[mRes.Cols["structstadium"]]),
                                        };
                                        newMatches.Add(newMatch);
                                    }
                                    else
                                    {
                                        log.Error("Unable to find team (" + sched[mRes.Cols["idteam1"]].Split('-')[0] + ") for match " + sched[mRes.Cols["uuid"]] + " in " + competition.competition_name);
                                    }
                                 

                                }
                                catch (Exception ex2)
                                {
                                    log.Error(ex2.ToString());
                                    throw ex2;
                                }
                            }
                            
                        }
                        var addedMatches = MatchManager.AddIfMissing(newMatches, trans);
                        #endregion
                        #region Schedule
                        Console.WriteLine("Matches (" + addedMatches.Count + ") " + (int)(DateTime.Now - tMatches).TotalMilliseconds + "ms");
                        DateTime tSchedule = DateTime.Now;

                        {
                            var scRes = db.Query("select * from schedule");
                            var newScheds = new List<Schedule>();
                            foreach (var sched in scRes.Rows)
                            {
                                try
                                {
                                    if(sched[scRes.Cols["idteam1"]]=="" || sched[scRes.Cols["idteam2"]] == "")
                                    {
                                        log.Error("Unable to add schedule due to empty teams: " + competition.competition_name);
                                        continue;
                                    }
                                    var team1 = TeamManager.FindTeamByOrgID((OriginID)competition.idorigin, sched[scRes.Cols["idteam1"]]);
                                    var team2 = TeamManager.FindTeamByOrgID((OriginID)competition.idorigin, sched[scRes.Cols["idteam2"]]);
                                    if (sched[scRes.Cols["contest_id"]] == "" || team1 == null || team2 == null) {
                                        log.Error("Unable to add schedule due to empty team or contest, competition: " + competition.competition_name);
                                    }
                                    else {
                                        var newSched = new Schedule()
                                        {
                                            idorigin = league.idorigin,
                                            schedule_origin_uid = (sched[scRes.Cols["contest_id"]]),
                                            idcompetition = (int)competition.idcompetition,
                                            idteam_home = (int)team1.idteam,
                                            idteam_away = (int)team2.idteam,
                                            round = int.Parse(sched[scRes.Cols["competition_round"]]),
                                            idscheduletype = (int)ScheduleTypeConversion.FromLegacyGoblinSpy(sched[scRes.Cols["type"]]),
                                            match_origin_uid = string.IsNullOrEmpty(sched[scRes.Cols["match_uuid"]])?"0":""+UInt64.Parse(sched[scRes.Cols["match_uuid"]], NumberStyles.HexNumber)
                                        };

                                        if (sched[scRes.Cols["match_uuid"]] == "")
                                        {
                                            newSched.idmatch = 0;
                                        }
                                        else
                                        {
                                            var matchO = MatchManager.FindMatch((OriginID)newSched.idorigin, UInt64.Parse(sched[scRes.Cols["match_uuid"]], NumberStyles.HexNumber).ToString(), trans);
                                            if (matchO != null)
                                            {
                                                newSched.idmatch = matchO.idmatch;
                                            }
                                            else
                                            {
                                                newSched.idmatch = null;
                                            }
                                        }
                                        if(newSched.idmatch==null || newSched.idmatch == 0) // Only add schedules matches that has a match (this is a legacy inactive league)
                                        {
                                            newScheds.Add(newSched);

                                        }
                                    }
                                }
                                catch(Exception ex2)
                                {
                                    log.Error(ex2.ToString());
                                    throw ex2;
                                }

                              
                            }
                            ScheduleManager.AddIfMissing(newScheds, trans);
                        }

                        #endregion
                        #region TeamMatchStats and PlayerMatchStats
                        Console.WriteLine("Schedule "+ (int)(DateTime.Now - tSchedule).TotalMilliseconds + "ms");
                        DateTime tTeamStats = DateTime.Now;

                        var newMatchStats = new List<TeamMatchStats>();
                        List<PlayerMatchStats> newPlayerStats = new List<PlayerMatchStats>();

                        {
                            db.Query("CREATE INDEX \"teamstats_idmatch\" ON \"teamstats\"(\"idmatch\"); ");
                            db.Query("CREATE INDEX \"playerstats_uuid\" ON \"playerstats\"(\"uuid\"); ");

                            foreach (var match in addedMatches)
                            {
                            
                                var matchIdHex = ulong.Parse(match.match_origin_uid).ToString("x");
                                var mRes = db.Query("select * from teamstats where idmatch=@idmatch", new Dictionary<string, string>() { { "@idmatch", matchIdHex } });
                                var matchInDB = MatchManager.FindMatch((OriginID)match.idorigin, match.match_origin_uid.ToString());
                                var idmatch = matchInDB.idmatch;
                                var team_home = TeamManager.FindTeamByID(match.idteam_home);
                                var team_away = TeamManager.FindTeamByID(match.idteam_away);
                                try
                                {
                                    
                                    for (int iRow = 0; iRow < 2; iRow++) 
                                    {
                                        var row = mRes.Rows[iRow];
                                        var newMatchStatsItem = new TeamMatchStats()
                                        {
                                            idorigin = league.idorigin,
                                            teammatchstat_origin_uid = ""+ ulong.Parse(match.match_origin_uid) * 10 + (UInt64)(team_home.team_origin_uid == row[mRes.Cols["idteamlisting"]] ? 0 : 1),
                                            idcompetition = (int)competition.idcompetition,
                                            idmatch = (UInt64)idmatch,
                                            blocks_for = int.Parse(row[mRes.Cols["inflictedtackles"]]),
                                            blocks_against = int.Parse(mRes.Rows[iRow == 0 ? 1 : 0][mRes.Cols["inflictedtackles"]]),
                                            breaks_for = int.Parse(row[mRes.Cols["inflictedinjuries"]]),
                                            breaks_against = int.Parse(row[mRes.Cols["sustainedinjuries"]]),
                                            kos_for = int.Parse(row[mRes.Cols["inflictedko"]]),
                                            kos_against = int.Parse(row[mRes.Cols["sustainedko"]]),
                                            possession = int.Parse(row[mRes.Cols["possessionball"]]),
                                            casualties_for = int.Parse(row[mRes.Cols["inflictedcasualties"]]),
                                            casualties_against = int.Parse(row[mRes.Cols["sustainedcasualties"]]),
                                            kills_for = int.Parse(row[mRes.Cols["inflicteddead"]]),
                                            kills_against = int.Parse(row[mRes.Cols["sustaineddead"]]),
                                            cash_before_match = int.Parse(row[mRes.Cols["cashbeforematch"]]),
                                            cash_earned = int.Parse(row[mRes.Cols["cashearned"]]),
                                            cash_spent = int.Parse(row[mRes.Cols["cashspentinducements"]]),
                                            catches = int.Parse(row[mRes.Cols["inflictedcatches"]]),
                                            conceded = (int.Parse(row[mRes.Cols["mvp"]]) == 0) ? 1 : 0,
                                            expulsions = int.Parse(row[mRes.Cols["sustainedexpulsions"]]),
                                            home = (team_home.team_origin_uid == row[mRes.Cols["idteamlisting"]] ? 1 : 0),
                                            idteam = (int)((team_home.team_origin_uid == row[mRes.Cols["idteamlisting"]]) ? team_home.idteam : team_away.idteam),
                                            interceptions = int.Parse(row[mRes.Cols["inflictedinterceptions"]]),
                                            mvp = int.Parse(row[mRes.Cols["mvp"]]),
                                            occ_own = int.Parse(row[mRes.Cols["occupationown"]]),
                                            occ_their = int.Parse(row[mRes.Cols["occupationtheir"]]),
                                            passes = int.Parse(row[mRes.Cols["inflictedpasses"]]),
                                            supporters = int.Parse(row[mRes.Cols["nbsupporters"]]),
                                            pushouts = int.Parse(row[mRes.Cols["inflictedpushouts"]]),
                                            pass_meters = int.Parse(row[mRes.Cols["inflictedmeterspassing"]]),
                                            run_meters = int.Parse(row[mRes.Cols["inflictedmetersrunning"]]),
                                            popularity_before_match = int.Parse(row[mRes.Cols["popularitybeforematch"]]),
                                            popularity_gained = int.Parse(row[mRes.Cols["popularitygain"]]),
                                            score = int.Parse(row[mRes.Cols["score"]]),
                                            score_opp= int.Parse(mRes.Rows[iRow == 0 ? 1 : 0][mRes.Cols["score"]]),
                                            td = int.Parse(row[mRes.Cols["inflictedtouchdowns"]]),
                                            td_opp = int.Parse(mRes.Rows[iRow == 0 ? 1 : 0][mRes.Cols["inflictedtouchdowns"]]),
                                            team_value=int.Parse(row[mRes.Cols["value"]]),
                                            cheerleaders =row[mRes.Cols["cheerleaders"]]!="" ? int.Parse(row[mRes.Cols["cheerleaders"]]):0,
                                            rerolls = row[mRes.Cols["rerolls"]] !=""?int.Parse(row[mRes.Cols["rerolls"]]):0,
                                            apo = row[mRes.Cols["apothecary"]]!=""?int.Parse(row[mRes.Cols["apothecary"]]):0,
                                            asscoaches = row[mRes.Cols["assistantcoaches"]]!=""?int.Parse(row[mRes.Cols["assistantcoaches"]]):0,


                                        };
                                        newMatchStatsItem.stuns_for = newMatchStatsItem.breaks_for - newMatchStatsItem.kos_for - newMatchStatsItem.casualties_for;
                                        newMatchStatsItem.stuns_against = newMatchStatsItem.breaks_against - newMatchStatsItem.kos_against - newMatchStatsItem.casualties_against;
                                        if (newMatchStatsItem.stuns_for < 0) newMatchStatsItem.stuns_for = 0;
                                        if (newMatchStatsItem.stuns_against < 0) newMatchStatsItem.stuns_against = 0;
                                        newMatchStatsItem.win = (newMatchStatsItem.score > newMatchStatsItem.score_opp)?1:0;
                                        newMatchStatsItem.draw = (newMatchStatsItem.score == newMatchStatsItem.score_opp) ? 1 : 0;
                                        newMatchStatsItem.loss = (newMatchStatsItem.score < newMatchStatsItem.score_opp) ? 1 : 0;
                                        newMatchStats.Add(newMatchStatsItem);
                                    }


                                }
                                catch (Exception ex2)
                                {
                                    log.Error(ex2.ToString());
                                    throw ex2;
                                }
                                try
                                {
                                    for (int iRow = 0; iRow < 2; iRow++)
                                    {
                                        var row = mRes.Rows[iRow];
                                        var pRes = db.Query("select * from playerstats where uuid=@idmatch and idteam=@idteam", new Dictionary<string, string>() {
                                                { "@idmatch", matchIdHex },
                                                { "@idteam", row[mRes.Cols["idteamlisting"]] }});


                                        foreach(var prow in pRes.Rows)
                                        {
                                            var player = TeamManager.FindPlayerByOrgID((OriginID)match.idorigin, prow[pRes.Cols["puuid"]]);
                                            try
                                            {
                                                PlayerMatchStats pStats = new PlayerMatchStats()
                                                {   
                                                    idmatch = (UInt64)idmatch,
                                                    idcompetition = (int)competition.idcompetition,
                                                    game_played = int.Parse(prow[pRes.Cols["matchplayed"]]),
                                                    ag = int.Parse(prow[pRes.Cols["ag"]]),
                                                    av = int.Parse(prow[pRes.Cols["av"]]),
                                                    st = int.Parse(prow[pRes.Cols["st"]]),
                                                    ma = int.Parse(prow[pRes.Cols["ma"]]),
                                                    idcasstate = UniqueStringManager.GetOrCreateCasCombo(casComboMap, casMap, prow[pRes.Cols["casualties_state_string"]]),
                                                    idcassustained = UniqueStringManager.GetOrCreateCasCombo(casComboMap, casMap, prow[pRes.Cols["casualties_state_string"]]),
                                                    idskillcombo = UniqueStringManager.GetOrCreateSkillCombo(skComboMap, skillMap, prow[pRes.Cols["skills_string"]]),
                                                    idplayertype = UniqueStringManager.GetOrCreatePlayerType(plTypeMap, prow[pRes.Cols["type"]]),
                                                    idplayername = UniqueStringManager.GetOrCreatePlayerName(plNameMap, prow[pRes.Cols["name"]]),
                                                    level = int.Parse(prow[pRes.Cols["level"]]),
                                                    xp = int.Parse(prow[pRes.Cols["xp"]]),
                                                    xp_gain = int.Parse(prow[pRes.Cols["xp_gain"]]),

                                                    blocks_for = int.Parse(prow[pRes.Cols["inflictedtackles"]]),
                                                    blocks_against = int.Parse(prow[pRes.Cols["sustainedtackles"]]),
                                                    breaks_for = int.Parse(prow[pRes.Cols["inflictedinjuries"]]),
                                                    breaks_against = int.Parse(row[pRes.Cols["sustainedinjuries"]]),
                                                    kos_for = int.Parse(prow[pRes.Cols["inflictedko"]]),
                                                    kos_against = int.Parse(prow[pRes.Cols["sustainedko"]]),
                                                    stuns_for = int.Parse(prow[pRes.Cols["inflictedstuns"]]),
                                                    stuns_against = int.Parse(prow[pRes.Cols["sustainedstuns"]]),
                                                    casualties_for = int.Parse(prow[pRes.Cols["inflictedcasualties"]]),
                                                    casualties_against = int.Parse(prow[pRes.Cols["sustainedcasualties"]]),
                                                    kills_for = int.Parse(prow[pRes.Cols["inflicteddead"]]),
                                                    kills_against = int.Parse(prow[pRes.Cols["sustaineddead"]]),

                                                    catches = int.Parse(prow[pRes.Cols["inflictedcatches"]]),
                                                    idplayer = (int)(player != null ? player.idplayer : 0),
                                                    interceptions = int.Parse(prow[pRes.Cols["inflictedinterceptions"]]),
                                                    mvp = int.Parse(prow[pRes.Cols["mvp"]]),
                                                    passes = int.Parse(prow[pRes.Cols["inflictedpasses"]]),
                                                    pass_meters = int.Parse(prow[pRes.Cols["inflictedmeterspassing"]]),
                                                    run_meters = int.Parse(prow[pRes.Cols["inflictedmetersrunning"]]),
                                                    pushouts = int.Parse(prow[pRes.Cols["inflictedpushouts"]]),
                                                    td = int.Parse(prow[pRes.Cols["inflictedtouchdowns"]]),
                                                    idteam = (int)((team_home.team_origin_uid == prow[pRes.Cols["idteam"]]) ? team_home.idteam : team_away.idteam),
                                                };

                                                if (pStats.av < 0) pStats.av = 0;
                                                if (pStats.ag < 0) pStats.ag = 0;
                                                if (pStats.ma < 0) pStats.ma = 0;
                                                if (pStats.st < 0) pStats.st = 0;

                                                if (player == null)
                                                {
                                                    player = new Player()
                                                    {
                                                        active = 1,
                                                        av = pStats.av,
                                                        ag = pStats.ag,
                                                        idcasstate = pStats.idcasstate,
                                                        idcassustained = pStats.idcassustained,
                                                        idleague = (int)league.idleague,
                                                        idorigin = match.idorigin,
                                                        idplayername = pStats.idplayername,
                                                        idplayertype = pStats.idplayertype,
                                                        idskillcombo = pStats.idskillcombo,
                                                        idteam = (int)((team_home.team_origin_uid == prow[pRes.Cols["idteam"]]) ? team_home.idteam : team_away.idteam),
                                                        level = pStats.level,
                                                        ma = pStats.ma,
                                                        player_origin_uid = prow[pRes.Cols["puuid"]],
                                                        st = pStats.st,
                                                        xp = pStats.xp,
                                                        xp_gain = pStats.xp_gain,
                                                    };
                                                    TeamManager.AddIfMissing(new List<Player>() { player });
                                                    var newpl = TeamManager.FindPlayerByOrgID((OriginID)match.idorigin, player.player_origin_uid);
                                                    pStats.idplayer = (int)newpl.idplayer;
                                                }


                                                newPlayerStats.Add(pStats);
                                            }catch(Exception ex2)
                                            {
                                                log.Error(ex2.ToString());
                                                throw ex2;
                                            }
                                        }

                               
                                    }
                                }
                                catch (Exception ex2)
                                {
                                    log.Error(ex2.ToString());
                                    throw ex2;
                                }

                            }

                        }
                        DateTime t1 = DateTime.Now;
                        var addedMatchStats = MatchManager.AddIfMissing(newMatchStats, trans);
                        Console.WriteLine("MatchStats insert " + (int)(DateTime.Now - t1).TotalMilliseconds + "ms");
                        DateTime t2 = DateTime.Now;
                        MatchManager.AddIfMissing(newPlayerStats, trans);
                        Console.WriteLine("Playerstats insert " + (int)(DateTime.Now - t2).TotalMilliseconds + "ms");


                        #endregion

                        #region Updating player active status based on last game played
                        try
                        {
                            foreach (var addedTeam in addedTeams)
                            {
                                var team = TeamManager.FindTeamByOrgID((OriginID)addedTeam.idorigin, addedTeam.team_origin_uid);
                                if (team != null)
                                {
                                    TeamManager.UpdatePlayersFromStats((int)league.idleague, (int)competition.idcompetition, (int)team.idteam, trans);

                                }

                            }
                        } 
                        catch (Exception ex3)
                        {
                            log.Error(ex3.ToString());
                            throw ex3;
                        }
                    #endregion
                    #region UPDATE STANDINGS AND ACTIVITY
                    try
                        {
                            int comp = (int)competition.idcompetition;
                            LeagueManager.UpdateStandings((int)ID, comp);
                            var lastRes = dbManager.StatsDB.Query("select m.finished from teammatchstats as tm inner join matches as m on m.idmatch=tm.idmatch where m.idcompetition=" + comp + "  order by m.finished desc limit 1", null,null, 60 * 60 * 20);
                            var numGamesRes = dbManager.StatsDB.Query("select count(*) from matches where idcompetition=" + comp + " ", null,null, 60 * 60 * 20);
                            var numCoachRes = dbManager.StatsDB.Query("select count(*) from (select 1 from standings as s inner join teams as t on t.idteam = s.idteam where s.idcompetition=" + comp + " group by t.idcoach) as sub", null,null, 60 * 60 * 20);
                            var numTeamsRes = dbManager.StatsDB.Query("select count(*) from standings where idcompetition=" + comp + " ", null,null, 60 * 60 * 20);

                            dbManager.StatsDB.Query("update competitions set last_game=@last_game,num_coaches=@num_coaches, num_teams=@num_teams,num_games=@num_games where idcompetition=" + comp, new Dictionary<string, string>()
                        {
                            { "@last_game", lastRes.Rows.Count > 0 ? lastRes.Rows[0][0] : DateTime.MinValue.ToDatabaseUniversalString()},
                            { "@num_coaches", numCoachRes.Rows[0][0] },
                            { "@num_teams", numTeamsRes.Rows[0][0] },
                            { "@num_games", numGamesRes.Rows[0][0] },
                        },null, 60 * 60 * 20);

                        }catch(Exception ex3)
                        {
                            log.Error(ex3.ToString());
                            throw ex3;
                        }
                        #endregion

                        Console.WriteLine("TeamStats " + (int)(DateTime.Now - tTeamStats).TotalMilliseconds + "ms");


                        trans.Commit();
                        res += "Updated league " + data.name;

                        base.SetState<String>("comps_imported" + data.idleague + ";" + data.name, "ok");


                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        log.Error("Error updating competition " + data.name, ex.ToString());
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error updating competition " + data.name, ex.ToString());
                throw ex;
            }


            return res;
        }
        /// <summary>
        /// Get leagues from gspy1 and add to the gspy3 database
        /// </summary>
        /// <returns></returns>
        string UpdateCompetitions()
        {
            string res = "";


            // Grab data from legacy goblinSpoy
            var comps = legacyAPI.GetCompetitions();
            var trans = dbManager.StatsDB.BeginTransaction();
            int totalLeaguesAdded = 0;
            try
            {
                log.Info("Found " + comps.leagues.Count + " competitions");
                // Add leagues (as unactivated) if they don't already exist
                foreach(var enumName in Enum.GetNames(typeof(OriginID)))
                {
                    List<League> leaguesToAdd = new List<League>();
                    foreach (var league in comps.leagues)
                    {
                        if (OriginConversion.FromLegacyGoblinSpy(league.platform).ToString() == enumName && league.name.Trim()!="") 
                        {
                            int numgames = 0;
                            foreach(var comp in league.list)
                            {
                                numgames+=comp.numgames;
                            }
                            if(numgames == 0)
                            {
                                continue;
                            }
                            var l = new League(league);
                            l.collecting = 0; // Force not collecting since we are getting it from legacy. This will not affect any already registered leagues

                            leaguesToAdd.Add(l);
#if DEBUG
    break;
#endif
                        }

                    }
                    // Add or update
                    var regRes = LeagueManager.RegisterLeague(leaguesToAdd, trans);
                    totalLeaguesAdded += regRes.Added;

                    

                }

                trans.Commit();
                res += "Updated leagues (" + totalLeaguesAdded + " new)";
            }
            catch (Exception ex)
            {
                trans.Rollback();
                log.Error("Error updating competitions", ex.ToString());
                throw ex;
            }
            return res;

        }
    }
}
