﻿using gSpy3_Common.Database.Format;
using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_LegacyCollector
{
    public class UpdateCompetitionWork
    {
        public OriginID idorigin { get; set; }
        public int idleague { get; set; }
        public string name { get; set; }
    }
}
