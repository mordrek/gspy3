﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gSpy3_BB2MatchParser
{
    public class ParserWork
    {
        public UInt64 idmatch { get; set; }
        public string finished { get; set; }
        public string uuid { get; set; }
        public int idorigin { get; set; }
    }
}
