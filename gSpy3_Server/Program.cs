﻿using gSpy3_Common;
using System;
using System.IO;
using System.ServiceProcess;

namespace gSpy3_Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Log log = new Log();
            log.Info("gSpy3_Server v "+typeof(Program).Assembly.GetName().Version);

            string basePath = System.Reflection.Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "");
            var rootDir = System.IO.Path.GetDirectoryName(basePath);
            Directory.SetCurrentDirectory(rootDir);

            var service = new gSpy3Service();
            if (Environment.UserInteractive)
            {
                service.StartManually();
                Console.WriteLine("Enter to exit");
                Console.ReadLine();
                service.StopManually();
            }
            else
            {
                ServiceBase.Run(service);
            }
            log.Info("Program exit");
        }
    }
}
